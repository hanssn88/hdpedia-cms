import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import { connect } from 'react-redux';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import 'rsuite/dist/styles/rsuite-default.css';
import { Login, LupaPassword, Main, PageLoading } from './Template';
import ProtectedRoute from './router/ProtectedRoute';
import PublicRoute from './router/PublicRoute';


const Home = React.lazy(() => import('./features/Beranda'));
const Setting = React.lazy(() => import('./features/Setting'));
const Faq = React.lazy(() => import('./features/Faq'));
const FormFaq = React.lazy(() => import('./features/Faq/FaqForm'));
const Users = React.lazy(() => import('./features/Users'));
const StafAdmin = React.lazy(() => import('./features/Users/StafAdmin'));
const MemberApps = React.lazy(() => import('./features/Users/MemberApps'));
const Dokter = React.lazy(() => import('./features/Users/Dokter'));
const Perawat = React.lazy(() => import('./features/Users/Perawat'));
const StafGudang = React.lazy(() => import('./features/Users/StafGudang'));
const StafRekamMedis = React.lazy(() => import('./features/Users/StafRekamMedis'));
const TimMedis = React.lazy(() => import('./features/Users/TimMedis'));
const Klinik = React.lazy(() => import('./features/Klinik'));
const Info = React.lazy(() => import('./features/InfoMedisPasien'));
const Vendors = React.lazy(() => import('./features/Vendors'));
const Products = React.lazy(() => import('./features/Products'));
const BarangMasuk = React.lazy(() => import('./features/BarangMasuk'));
const BarangKeluar = React.lazy(() => import('./features/BarangKeluar'));
const StockOpname = React.lazy(() => import('./features/StockOpname'));
const ReportBarang = React.lazy(() => import('./features/ReportBarang'));
const Pasiens = React.lazy(() => import('./features/Pasiens'));
const PasienDetail = React.lazy(() => import('./features/Pasiens/PasienDetail'));
const PasienForm = React.lazy(() => import('./features/Pasiens/PasienForm'));
const ListFromPasien = React.lazy(() => import ('./features/Pasiens/listForm'));
const HistoryForm = React.lazy(() => import ('./features/Pasiens/listHistory'));
const HistoryDetail = React.lazy(() => import('./features/Pasiens/HistoryDetail'));
const Treatment = React.lazy(() => import('./features/Treatment'));
const KlinikImg = React.lazy(() => import('./features/Klinik/KlinikImg'));
const ListFormTreatment = React.lazy(() => import('./features/Treatment/listForm'));
const JadwalDokter = React.lazy(() => import('./features/JadwalDokter'));
const VirtualStock = React.lazy(() => import('./features/VirtualStock'));
const Permission = React.lazy(() => import('./features/Permission'));
const SetPermission = React.lazy(() => import('./features/Permission/PermissionFrm'));
const Forbidden = React.lazy(() => import('./Template/Forbidden'));
const HistoryTravelDetail = React.lazy(() => import('./features/Pasiens/historyDetailTravel'));
const HistoryTravel = React.lazy(() => import ('./features/Pasiens/listHistoryTravel'));
const Message = React.lazy(() => import ('./features/Message'));
const Pengumuman = React.lazy(() => import ('./features/Pengumuman'));
const PengumumanForm = React.lazy(() => import ('./features/Pengumuman/PengumumanForm'));
const TataTertib = React.lazy(() => import ('./features/TataTertib/index'));

const getBasename = path => path.substr(0, path.lastIndexOf('/'));

function App({user}) {
    const permission = user?.permission;
    return (
        <div className="App">
            <Router basename={getBasename(window.location.pathname)}>
                <Switch>
                    <PublicRoute exact path="/login">
                        <Login/>
                    </PublicRoute>

                    <PublicRoute exact path="/test">
                        <PageLoading/>
                    </PublicRoute>

                    <PublicRoute exact path="/lupa_password">
                        <LupaPassword/>
                    </PublicRoute>


                    <ProtectedRoute path="/">
                        <Main>
                            <React.Suspense fallback={<PageLoading/>}>
                                <Route exact path="/" component={Home}/>

                                <Route exact path="/hdpedia-cms" component={Home}/>

                                <Route exact path="/setting"
                                       component={parseInt(permission?.setting_view) > 0 ? Setting : Forbidden}/>
                                <Route exact path="/faq"
                                       component={parseInt(permission?.faq_view) > 0 ? Faq : Forbidden}/>
                                <Route exact path="/add_faq" component={FormFaq}/>

                                <Route exact path="/super-admin"
                                       component={parseInt(permission?.superadmin_view) > 0 ? Users : Forbidden}/>
                                <Route exact path="/staf-admin"
                                       component={parseInt(permission?.staff_admin_view) > 0 ? StafAdmin : Forbidden}/>
                                <Route exact path="/dokter"
                                       component={parseInt(permission?.dokter_view) > 0 ? Dokter : Forbidden}/>
                                <Route exact path="/perawat"
                                       component={parseInt(permission?.perawat_view) > 0 ? Perawat : Forbidden}/>
                                <Route exact path="/staf-gudang"
                                       component={parseInt(permission?.staff_gudang_view) > 0 ? StafGudang : Forbidden}/>
                                <Route exact path="/staf-rekam-medis"
                                       component={parseInt(permission?.staff_rm_view) > 0 ? StafRekamMedis : Forbidden}/>
                                <Route exact path="/tim-medis"
                                       component={parseInt(permission?.tim_medis_view) > 0 ? TimMedis : Forbidden}/>
                                <Route exact path="/member_apps"
                                       component={parseInt(permission?.member_apps_view) > 0 ? MemberApps : Forbidden}/>

                                <Route exact path="/klinik"
                                       component={parseInt(permission?.klinik_view) > 0 ? Klinik : Forbidden}/>
                                <Route exact path="/banner"
                                       component={parseInt(permission?.banner_view) > 0 ? Info : Forbidden}/>
                                <Route exact path="/list_img_klinik" component={KlinikImg}/>
                                <Route exact path="/vendor"
                                       component={parseInt(permission?.vendor_view) > 0 ? Vendors : Forbidden}/>
                                <Route exact path="/products"
                                       component={parseInt(permission?.product_view) > 0 ? Products : Forbidden}/>
                                <Route exact path="/barang-masuk"
                                       component={parseInt(permission?.bm_view) > 0 ? BarangMasuk : Forbidden}/>
                                <Route exact path="/barang-keluar"
                                       component={parseInt(permission?.bk_view) > 0 ? BarangKeluar : Forbidden}/>
                                         <Route exact path="/stock-opname"
                                       component={StockOpname}/>
                                <Route exact path="/report-barang"
                                       component={ReportBarang}/>
                                <Route exact path="/pasien"
                                       component={parseInt(permission?.patient_view) > 0 ? Pasiens : Forbidden}/>
                                <Route exact path="/view_pasien"
                                       component={parseInt(permission?.patient_view) > 0 ? PasienDetail : Forbidden}/>
                                <Route exact path="/add_pasien"
                                       component={parseInt(permission?.patient_form) > 0 ? PasienForm : Forbidden}/>
                                <Route exact path="/pasien-form" component={ListFromPasien}/>
                                <Route exact path="/history_form" component={HistoryForm}/>
                                <Route exact path="/history_form_detail" component={HistoryDetail}/>
                                <Route exact path="/treatment"
                                       component={parseInt(permission?.treatment_view) > 0 ? Treatment : Forbidden}/>
                                <Route exact path="/treatment-form" component={ListFormTreatment}/>
                                <Route exact path="/jadwal-dokter"
                                       component={parseInt(permission?.jadwal_dokter_view) > 0 ? JadwalDokter : Forbidden}/>
                                <Route exact path="/virtual-stock"
                                       component={parseInt(permission?.vs_view) > 0 ? VirtualStock : Forbidden}/>
                                <Route exact path="/permission" component={Permission}/>
                                <Route exact path="/set_permission" component={SetPermission}/>
                                <Route exact path="/history_travel_detail" component={HistoryTravelDetail}/>
                                <Route exact path="/history_travel" component={HistoryTravel}/>
                                <Route exact path="/message" component={Message}/>
                                <Route exact path="/tata-tertib-pasien" component={TataTertib}/>
                                <Route exact path="/pengumuman"
                                       component={Pengumuman}/>
                                <Route exact path="/add_pengumuman" component={PengumumanForm}/>

                            </React.Suspense>
                        </Main>
                    </ProtectedRoute>
                </Switch>
            </Router>

        </div>
    );
}

const mapStateToProps = (state) => ({
    user: state.main.currentUser
});
export default connect(mapStateToProps)(App);
