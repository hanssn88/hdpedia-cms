import axios from "axios";
import { useFormik } from 'formik';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import * as Yup from 'yup';
import Button from '../components/button/Button';
import { clearState, userSelector } from '../features/main/mainSlice';

const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

const LupaPassword = () => {
    const {isFetching, isSuccess, errorMessage} = useSelector(
        userSelector
    );
    const history = useHistory();
    const dispatch = useDispatch();
    useEffect(() => {
        return () => {
            dispatch(clearState());
        };
    }, [dispatch]);

    useEffect(() => {
        if (isSuccess) {
            dispatch(clearState());
            history.push('/');
        }
    }, [isSuccess, dispatch, history]);
    const formik = useFormik({
        initialValues: {
            email: '',
        },
        validationSchema: Yup.object({
            email: Yup.string().email('Invalid email')
                .required('Please enter email'),
           
        }),
        onSubmit: (values) => {
            dispatch(handleSubmit(values));
        }
    });

    const hideAlert = () => {
        dispatch(clearState())
    }

    const handleSubmit = async (e) => {

        try {
          const response =   await axios.post(API_URL + '/admin/forgot_pass', e, {
               
            });

            let data = "";
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa;
                if (data.err_code === '00') {
                   
                    return Swal.fire({
                        title: 'Informasi',
                        text: data.err_msg,
                        icon: 'success',
                        confirmButtonText: 'Ok',
                        allowOutsideClick: true
                    })
                } else {
                    res = {
                        err_msg: data.err_msg
                    }

                    return Swal.fire({
                        title: 'Error',
                        text: res.err_msg,
                        icon: 'error',
                        confirmButtonText: 'Ok',
                        allowOutsideClick: true
                    })
                }
            } 

        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    document.getElementById('root').classList = 'hold-transition login-page';

    return (
        <div className="login-box">
            <div className="card card-outline card-primary">
                <div className="card-header text-center h1">
                    <b>HDPedia</b>
                </div>
                <div className="card-body">

                    {errorMessage ? (
                        <div className="alert alert-danger alert-sm">
                            <button onClick={hideAlert} type="button" className="close" data-dismiss="alert"
                                    aria-hidden="true">×
                            </button>
                            <span className="fw-semi-bold text-error-login">Error: {errorMessage}</span>
                        </div>
                    ) : (<p className='login-box-msg'>Forgot Password</p>)}

                    <form onSubmit={formik.handleSubmit}>
                        {formik.touched.email && formik.errors.email ? (
                            <span className="float-right text-error badge badge-danger">{formik.errors.email}</span>
                        ) : null}
                        <div className="input-group mb-3">
                            <input
                                autoFocus
                                autoComplete="off"
                                type="text"
                                className="form-control"
                                placeholder="Email"
                                {...formik.getFieldProps('email')} />
                            <div className="input-group-append">
                                <div className="input-group-text">
                                    <span className="fas fa-user"/>
                                </div>
                            </div>

                        </div>

                        <div className="social-auth-links text-center mt-2 mb-3">
                            <Button
                                block
                                type="submit"
                                isLoading={isFetching}
                                icon="sign"
                                theme="primary"
                            >
                                Forgot Password
                            </Button>
                        </div>
                    </form>
                  
                </div>
            </div>
        </div>

    )
};

export default LupaPassword;