import React, {Component} from 'react'
import {Container, Content} from 'rsuite'
import {MyHeader, MySidebar} from '../Template';
import {connect} from 'react-redux';
import {fetchUserBytoken, onLogout} from '../features/main/mainSlice'

class Main extends Component {

    componentDidMount() {
        this.fetchProfileAdmin();
    }

    fetchProfileAdmin = () => {
        const token = localStorage.getItem(process.env.REACT_APP_TOKEN_LOGIN);
        if (!token) {
            alert('session invalid');
            window.location.reload();
        }
    }

    render() {
        const {children} = this.props;


        document.getElementById('root').classList.remove('login-page');
        document.getElementById('root').classList.remove('hold-transition');
        document.getElementById('root').classList.add('bg-sidebar');
        document.getElementById('root').className += ' sidebar-mini';
        return (

            <div>
                <div className="show-container">
                    <Container>
                        <MyHeader></MyHeader>
                        <Container>
                            <MySidebar/>
                            <Content>

                                {children}
                            </Content>
                        </Container>

                    </Container>
                </div>
            </div>
        )
    }
}

const mapDispatchToPros = (dispatch) => {
    return {
        fetchDataAdmin: (payload) => {
            dispatch(fetchUserBytoken(payload));
        },
        logOut: () => {
            dispatch(onLogout());
        }
    }
}
const mapStateToProps = (state) => ({
    user: state.main.currentUser
});
export default connect(mapStateToProps, mapDispatchToPros)(Main);
