import React, { Component } from 'react';
import { Form } from "react-bootstrap";
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { Link } from 'react-router-dom';
import { Dropdown, Header, Icon, Nav, Navbar } from 'rsuite';
import AppModal from "../components/modal/MyModal";
import { AppSwalSuccess } from "../components/modal/SwalSuccess";
import { addChange, addForm, clickExpand, closeForm, fetchUserBytoken, onLogout, setDefaultOpenKeys } from '../features/main/mainSlice';

const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

class MyHeader extends Component {

    constructor(props) {
        super(props);
        this.initSelected = {

            password_old: '',
            password_new:'',
            password_konfirmasi:''

        }
        this.state = {

           selected: this.initSelected,
            errMsg: this.initSelected,
            loadingForm: false,
        }
 
    }

    componentDidMount() {
        this.fetchProfileAdmin();
    }

    fetchProfileAdmin = async () => {
        await this.props.onLoad();
        const token = localStorage.getItem(tokenLogin);
        if (!token) {
            alert('session invalid');
            window.location.reload();
        }
    }

    handleForgot = async (data) => {
        this.setState({
            errMsg: {},
     
            loadingForm: false
        });
        this.props.showForm();
    }
    handleToggle() {
        this.props.onClickExpand();
    }

    handleLogout() {
        this.props.logOut();
        <Redirect to="/login"/>
    }

    handleChange(event) {
        const {name, value} = event.target
        var val = value;

        this.setState({
            selected: {
                ...this.state.selected,
                [name]: val
            },
            errMsg: {
                ...this.state.errMsg,
                [name]: val ? '' : this.state.errMsg[name]
            },
            loadingForm: false
        });
    }

    handleClose = () => {
        this.props.closeModal();
    };

    handleCloseSwal = () => {
      
        // this.props.closeSwal(param);
        if(this.props.contentMsg === "<div style='font-size:20px; text-align:center;'><strong>Success</strong>, Data berhasil disimpan</div>"){
            this.props.logOut();
            <Redirect to="/login"/>
        }
        this.props.closeSwal();

    };

    handleSubmit() {
        let errors = this.state.errMsg;
        this.setState({
            ...this.state,
            loadingForm: true,
        });
        errors.password_old = !this.state.selected.password_old ? "Required" : '';
        errors.password_new = !this.state.selected.password_new ? "Required" : '';
        errors.password_konfirmasi = !this.state.selected.password_konfirmasi ? "Required" : '';

        if(this.state.selected.password_old === this.state.selected.password_new){
            errors.passwordValid = 'Password Lama tidak boleh sama dengan Password baru';
        }else{

            if(this.state.selected.password_new !== this.state.selected.password_konfirmasi){
                errors.passwordValid = 'Password Konfimasi harus sama dengan Password New';
            }

        }


        this.setState({errors});

        if (this.validateForm(this.state.errMsg)) {

            const param = {
                old_pass: this.state.selected.password_old,
                new_pass:this.state.selected.password_new,
                new_pass_confirmation:this.state.selected.password_konfirmasi
            }

            this.props.onForgot(param);

        
        } else {
            console.error('Invalid Form')
        }

        this.setState({
            ...this.state,
            loadingForm: false,
        });
    }

    validateForm(errors) {
        let valid = true;
        Object.values(errors).forEach(
            (val) => val.length > 0 && (valid = false)
        );
        return valid;
    }



    render() {
        const {selected, errMsg} = this.state;
        const frmPassword = <Form id="myForm">

        
        <Form.Group controlId="password_old" style={{marginTop: "30px"}}>
            <Form.Label>Password Lama</Form.Label>
            {errMsg.password_old ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.password_old}
                        </span>) : ''}
            <Form.Control
                size="sm"
                autoComplete="off"
                name="password_old"
                type="password"
                value={selected?.password_old}
                placeholder="Passsword Old"
                onChange={this.handleChange.bind(this)}/>
        </Form.Group>

        <Form.Group controlId="password_new" style={{marginTop: "30px"}}>
            <Form.Label>Password Baru</Form.Label>
            {errMsg.password_new ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.password_new}
                        </span>) : ''}
            <Form.Control
                size="sm"
                autoComplete="off"
                name="password_new"
                type="password"
                value={selected?.password_new}
                placeholder="Passsword New"
                onChange={this.handleChange.bind(this)}/>
        </Form.Group>

        <Form.Group controlId="password_conf" style={{marginTop: "30px"}}>
            <Form.Label>Password Konfirmasi</Form.Label>
            {errMsg.password_konfirmasi ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.password_konfirmasi}
                        </span>) : ''}
            <Form.Control
                size="sm"
                autoComplete="off"
                name="password_konfirmasi"
                type="password"
                value={selected?.password_konfirmasi}
                placeholder="Passsword Konfirmasi"
                onChange={this.handleChange.bind(this)}/>
        </Form.Group>
        
        {errMsg.passwordValid ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.passwordValid}
                        </span>) : ''}
        <br></br>
    </Form>;



        return (

            <Header>
                <Navbar appearance="inverse" className="my-navbar1">
                    <Navbar.Header>
                        <Link to='/' className="navbar-brand logo"><b>HDPedia</b></Link>
                    </Navbar.Header>
                    <Navbar.Body>
                        <Nav>
                            <Nav.Item icon={<Icon icon="bars"/>} onClick={this.handleToggle.bind(this)}
                                      className="drawwer"></Nav.Item>
                        </Nav>


                        <Nav pullRight>


                            <Dropdown className="show dr-logout" icon={<Icon icon="user-o" size="lg"/>}
                                      title={this.props.user?.name ? (this.props.user?.name) : ("Account")}>

                                <Dropdown.Item onClick={this.handleLogout.bind(this)} className="dropdown-menuu"
                                               icon={<Icon icon="sign-out"/>}>Logout</Dropdown.Item>
                                 <Dropdown.Item onClick={this.handleForgot} className="dropdown-menuu"
                                               icon={<Icon icon="fas fa-lock"/>}>Change Password</Dropdown.Item>


                            </Dropdown>
                        </Nav>


                    </Navbar.Body>
                </Navbar>

                <AppModal
                    show={this.props.showFormAdd}
                    form={frmPassword}
                    size="xs"
                    backdrop={false}
                    keyboard={false}
                    title="Change Password"
                    titleButton="Save change"
                    themeButton="success"
                    handleClose={this.handleClose}
                    isLoading={this.props.isAddLoading ? this.props.isAddLoading : this.state.loadingForm}
                    formSubmit={this.handleSubmit.bind(this)}
                ></AppModal>
                {this.props.showFormSuccess ? (<AppSwalSuccess
                    show={this.props.showFormSuccess}
                    title={<div dangerouslySetInnerHTML={{__html: this.props.contentMsg}}/>}
                    type={this.props.tipeSWAL}
                    handleClose={this.handleCloseSwal}
                >
                </AppSwalSuccess>) : ''}

            </Header>


        )
    }
}

const mapDispatchToPros = (dispatch) => {
    
    return {
        onClickExpand: () => {
            dispatch(clickExpand());
        },
        logOut: () => {
            dispatch(onLogout());
        },
        onLoad: (dt) => {
            dispatch(fetchUserBytoken());
            dispatch(setDefaultOpenKeys(dt));
        },
        onForgot: (param) => {
            dispatch(addChange(param));
        },
        showForm: () => {
            dispatch(addForm());
        },
        closeModal: () => {
            dispatch(closeForm());

        },
        closeSwal: () => {
            dispatch(closeForm());
            // dispatch(fetchData(param));
        },

    }
}
const mapStateToProps = (state) => ({
    user: state.main.currentUser,
    isLoading: state.main.isLoading,
    showFormAdd: state.main.showFormAdd,
    isAddLoading: state.main.isAddLoading,
    contentMsg: state.main.contentMsg || null,
    errorPriority: state.main.errorPriority || null,
    showFormSuccess: state.main.showFormSuccess,
    showFormDelete: state.main.showFormDelete,
    tipeSWAL: state.main.tipeSWAL,
});
export default connect(mapStateToProps, mapDispatchToPros)(MyHeader);