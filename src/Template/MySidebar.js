import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Dropdown, Icon, Nav, Sidebar, Sidenav } from 'rsuite';

class MySidebar extends Component {

    constructor(props) {
        super(props);
        this.state = {lastSegmentUrl: ""}
    }

    componentDidMount = async () => {
        const location = window.location.href;
        const BaseName = location.substring(location.lastIndexOf("/") + 1);
        await this.setState({lastSegmentUrl: BaseName})
    }

    handleMenu = async (dt) => {
        await (this.setState({lastSegmentUrl: dt}));
    }

    render() {
        const {expandMenu} = this.props.main;
        const {user} = this.props;
        const {lastSegmentUrl} = this.state;
        const permission = user?.permission;

        return (
            <div>
                <Sidebar
                    style={{display: 'flex', flexDirection: 'column', backgroundColor: 'rgb(31, 30, 30)'}}
                    width={expandMenu ? 230 : 56}
                    collapsible
                >
                    <Sidenav
                        className="my-sidebar"
                        expanded={expandMenu}
                        //defaultOpenKeys={[`${defaultOpenKeys}`]}
                        appearance="subtle">
                        <Sidenav.Body>
                            {expandMenu ? (
                                    <h5 style={{textAlign: "center", fontWeight: 600, fontSize: 16}}>Selamat datang <br/>dihalaman
                                        Administrator</h5>) :
                                ''}

                            <Nav>
                                <Nav.Item
                                    onSelect={e => this.handleMenu("/")}
                                    componentClass={Link}
                                    to='/'
                                    eventKey='/'
                                    exact='/'
                                    className={lastSegmentUrl === "/" || lastSegmentUrl === "" || lastSegmentUrl === "hdpedia-cms" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                    icon={<Icon icon="home"/>}>
                                    Home
                                </Nav.Item>
                                {parseInt(permission?.member_apps_view) > 0 &&
                                    <Nav.Item
                                        onSelect={e => this.handleMenu('member_apps')}
                                        componentClass={Link}
                                        to='/member_apps'
                                        exact='/member_apps'
                                        eventKey='/member_apps'
                                        className={lastSegmentUrl === "member_apps" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                        icon={<Icon icon="people-group"/>}>
                                        Member Apps
                                    </Nav.Item>
                                }
                                {parseInt(permission?.patient_view) > 0 &&
                                    <Nav.Item
                                        onSelect={e => this.handleMenu('pasien')}
                                        componentClass={Link}
                                        to='/pasien'
                                        exact='/pasien'
                                        eventKey='/pasien'
                                        className={lastSegmentUrl === "pasien" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                        icon={<Icon icon="people-group"/>}>
                                        Pasien
                                    </Nav.Item>
                                }
                                {parseInt(permission?.treatment_view) > 0 &&
                                    <Nav.Item
                                        onSelect={e => this.handleMenu('treatment')}
                                        componentClass={Link}
                                        to='/treatment'
                                        exact='/treatment'
                                        eventKey='/treatment'
                                        className={lastSegmentUrl === "treatment" || lastSegmentUrl === "treatment-form" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                        icon={<Icon icon="people-group"/>}>
                                        Jadwal Treatment
                                    </Nav.Item>
                                }
                                {parseInt(permission?.jadwal_dokter_view) > 0 &&
                                    <Nav.Item
                                        onSelect={e => this.handleMenu('jadwal-dokter')}
                                        componentClass={Link}
                                        to='/jadwal-dokter'
                                        exact='/jadwal-dokter'
                                        eventKey='/jadwal-dokter'
                                        className={lastSegmentUrl === "jadwal-dokter" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                        icon={<Icon icon="people-group"/>}>
                                        Jadwal Dinas
                                    </Nav.Item>
                                }
                                {parseInt(permission?.bm_view) > 0 &&
                                    <Nav.Item
                                        onSelect={e => this.handleMenu('barang-masuk')}
                                        componentClass={Link}
                                        to='/barang-masuk'
                                        exact='/barang-masuk'
                                        eventKey='/barang-masuk'
                                        className={lastSegmentUrl === "barang-masuk" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                        icon={<Icon icon="people-group"/>}>
                                        Barang Masuk
                                    </Nav.Item>
                                }
                                {parseInt(permission?.bk_view) > 0 &&
                                    <Nav.Item
                                        onSelect={e => this.handleMenu('barang-keluar')}
                                        componentClass={Link}
                                        to='/barang-keluar'
                                        exact='/barang-keluar'
                                        eventKey='/barang-keluar'
                                        className={lastSegmentUrl === "barang-keluar" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                        icon={<Icon icon="people-group"/>}>
                                        Barang Keluar
                                    </Nav.Item>
                                }         {parseInt(permission?.bk_view) > 0 &&
                                    <Nav.Item
                                        onSelect={e => this.handleMenu('stock-opname')}
                                        componentClass={Link}
                                        to='/stock-opname'
                                        exact='/stock-opname'
                                        eventKey='/stock-opname'
                                        className={lastSegmentUrl === "stock-opname" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                        icon={<Icon icon="people-group"/>}>
                                        Stock Opname
                                    </Nav.Item>
                                }
                                {
                                    <Nav.Item
                                        onSelect={e => this.handleMenu('report-barang')}
                                        componentClass={Link}
                                        to='/report-barang'
                                        exact='/report-barang'
                                        eventKey='/report-barang'
                                        className={lastSegmentUrl === "report-barang" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                        icon={<Icon icon="people-group"/>}>
                                        Report Barang
                                    </Nav.Item>
                                }
                                {parseInt(permission?.vs_view) > 0 &&
                                    <Nav.Item
                                        onSelect={e => this.handleMenu('virtual-stock')}
                                        componentClass={Link}
                                        to='/virtual-stock'
                                        exact='/virtual-stock'
                                        eventKey='/virtual-stock'
                                        className={lastSegmentUrl === "virtual-stock" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                        icon={<Icon icon="people-group"/>}>
                                        Virtual Stock
                                    </Nav.Item>
                                }
                                {parseInt(permission?.faq_view) > 0 &&
                                    <Nav.Item
                                        onSelect={e => this.handleMenu('faq')}
                                        componentClass={Link}
                                        to='/faq'
                                        exact='/faq'
                                        eventKey='/faq'
                                        className={lastSegmentUrl === "faq" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                        icon={<Icon icon="question2"/>}>
                                        FAQ
                                    </Nav.Item>
                                }
                                <Nav.Item
                                    onSelect={e => this.handleMenu('message')}
                                    componentClass={Link}
                                    to='/message'
                                    exact='/message'
                                    eventKey='/message'
                                    className={lastSegmentUrl === "message" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                    icon={<Icon icon="inbox"/>}>
                                    Message
                                </Nav.Item>

                                <Nav.Item
                                    onSelect={e => this.handleMenu('pengumuman')}
                                    componentClass={Link}
                                    to='/pengumuman'
                                    exact='/pengumuman'
                                    eventKey='/pengumuman'
                                    className={lastSegmentUrl === "pengumuman" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                    icon={<Icon icon="info"/>}>
                                    Pengumuman
                                </Nav.Item>
                                <Dropdown
                                    eventKey="3"
                                    trigger="hover"
                                    title="Master Data"
                                    icon={<Icon icon="list-ul"/>}
                                    placement="rightStart"
                                    className={lastSegmentUrl === "setting" || lastSegmentUrl === "super-admin" || lastSegmentUrl === "klinik" || lastSegmentUrl === "staf-admin" || lastSegmentUrl === "dokter" || lastSegmentUrl === "perawat" || lastSegmentUrl === "vendor" || lastSegmentUrl === "set_permission" || lastSegmentUrl === "tata-tertib-pasien" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                >

                                    {parseInt(permission?.klinik_view) > 0 &&
                                        <Dropdown.Item
                                            onSelect={e => this.handleMenu('klinik')}
                                            componentClass={Link}
                                            to='/klinik'
                                            exact='/klinik'
                                            className={lastSegmentUrl === "klinik" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                            icon={<Icon icon="map-marker"/>}
                                            eventKey="3-1"> Klinik</Dropdown.Item>
                                    }
                                    {parseInt(permission?.vendor_view) > 0 &&
                                        <Dropdown.Item
                                            onSelect={e => this.handleMenu('vendor')}
                                            componentClass={Link}
                                            to='/vendor'
                                            exact='/vendor'
                                            className={lastSegmentUrl === "vendor" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                            icon={<Icon icon="clone"/>}
                                            eventKey="3-2"> Vendor</Dropdown.Item>
                                    }
                                    {parseInt(permission?.product_view) > 0 &&
                                        <Dropdown.Item
                                            onSelect={e => this.handleMenu('products')}
                                            componentClass={Link}
                                            to='/products'
                                            exact='/products'
                                            className={lastSegmentUrl === "products" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                            icon={<Icon icon="cube"/>}
                                            eventKey="3-3"> Products</Dropdown.Item>
                                    }
                                    {parseInt(permission?.dokter_view) > 0 &&
                                        <Dropdown.Item
                                            onSelect={e => this.handleMenu('dokter')}
                                            componentClass={Link}
                                            to='/dokter'
                                            exact='/dokter'
                                            className={lastSegmentUrl === "dokter" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                            icon={<Icon icon="people-group"/>}
                                            eventKey="3-12"> Dokter</Dropdown.Item>
                                    }
                                    {parseInt(permission?.perawat_view) > 0 &&
                                        <Dropdown.Item
                                            onSelect={e => this.handleMenu('perawat')}
                                            componentClass={Link}
                                            to='/perawat'
                                            exact='/perawat'
                                            className={lastSegmentUrl === "perawat" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                            icon={<Icon icon="peoples"/>}
                                            eventKey="3-4"> Perawat</Dropdown.Item>
                                    }
                                    {parseInt(permission?.staff_gudang_view) > 0 &&
                                        <Dropdown.Item
                                            onSelect={e => this.handleMenu('staf-gudang')}
                                            componentClass={Link}
                                            to='/staf-gudang'
                                            exact='/staf-gudang'
                                            className={lastSegmentUrl === "staf-gudang" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                            icon={<Icon icon="peoples"/>}
                                            eventKey="3-5"> Staf Gudang</Dropdown.Item>
                                    }
                                    {parseInt(permission?.staff_rm_view) > 0 &&
                                        <Dropdown.Item
                                            onSelect={e => this.handleMenu('staf-rekam-medis')}
                                            componentClass={Link}
                                            to='/staf-rekam-medis'
                                            exact='/staf-rekam-medis'
                                            className={lastSegmentUrl === "staf-rekam-medis" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                            icon={<Icon icon="peoples"/>}
                                            eventKey="3-6"> Staf Rekam Medis</Dropdown.Item>
                                    }
                                    {parseInt(permission?.tim_medis_view) > 0 &&
                                        <Dropdown.Item
                                            onSelect={e => this.handleMenu('tim-medis')}
                                            componentClass={Link}
                                            to='/tim-medis'
                                            exact='/tim-medis'
                                            className={lastSegmentUrl === "tim-medis" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                            icon={<Icon icon="peoples"/>}
                                            eventKey="3-7"> Tim Medis</Dropdown.Item>
                                    }
                                    {parseInt(permission?.staff_admin_view) > 0 &&
                                        <Dropdown.Item
                                            onSelect={e => this.handleMenu('users')}
                                            componentClass={Link}
                                            to='/staf-admin'
                                            exact='/staf-admin'
                                            className={lastSegmentUrl === "staf-admin" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                            icon={<Icon icon="peoples"/>}
                                            eventKey="3-8"> Staf Admin</Dropdown.Item>
                                    }
                                    {parseInt(permission?.superadmin_view) > 0 &&
                                        <Dropdown.Item
                                            onSelect={e => this.handleMenu('users')}
                                            componentClass={Link}
                                            to='/super-admin'
                                            exact='/super-admin'
                                            className={lastSegmentUrl === "super-admin" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                            icon={<Icon icon="peoples"/>}
                                            eventKey="3-9"> Super Admin</Dropdown.Item>
                                    }
                                    <Dropdown.Item
                                        onSelect={e => this.handleMenu('permission')}
                                        componentClass={Link}
                                        to='/permission'
                                        exact='/permission'
                                        className={lastSegmentUrl === "permission" || lastSegmentUrl === "set_permission" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                        icon={<Icon icon="peoples"/>}
                                        eventKey="3-10"> Permission</Dropdown.Item>
                                    {parseInt(permission?.setting_view) > 0 &&
                                        <Dropdown.Item
                                            onSelect={e => this.handleMenu('setting')}
                                            componentClass={Link}
                                            to='/setting'
                                            exact='/setting'
                                            className={lastSegmentUrl === "setting" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                            icon={<Icon icon="cog"/>}
                                            eventKey="3-11"> Setting
                                        </Dropdown.Item>
                                    }
                                     {parseInt(permission?.banner_view) > 0 &&
                                        <Dropdown.Item
                                            onSelect={e => this.handleMenu('banner')}
                                            componentClass={Link}
                                            to='/banner'
                                            exact='/banner'
                                            className={lastSegmentUrl === "banner" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                            icon={<Icon icon="info"/>}
                                            eventKey="3-13"> Info Medis Pasien</Dropdown.Item>
                                    }
                                     <Dropdown.Item
                                            onSelect={e => this.handleMenu('tata-tertib-pasien')}
                                            componentClass={Link}
                                            to='/tata-tertib-pasien'
                                            exact='/tata-tertib-pasien'
                                            className={lastSegmentUrl === "tata-tertib-pasien" ? ("my-dropdown my-dropdown-active") : ("my-dropdown")}
                                            icon={<Icon icon="inbox"/>}
                                            eventKey="3-12">Tata Tertib Pasien</Dropdown.Item>
                                </Dropdown>

                            </Nav>
                        </Sidenav.Body>

                    </Sidenav>

                </Sidebar>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        main: state.main,
        user: state.main.currentUser
    }
}

export default connect(mapStateToProps)(MySidebar);