import { configureStore } from '@reduxjs/toolkit';
import { bkSlice } from '../features/BarangKeluar/bkSlice';
import { bmSlice } from '../features/BarangMasuk/bmSlice';
import { faqSlice } from '../features/Faq/faqSlice';
import { jadwalDokterSlice } from '../features/JadwalDokter/jadwalDokterSlice';
import { klinikSlice } from '../features/Klinik/klinikSlice';
import { infoMedisSlice } from '../features/InfoMedisPasien/infoMedisSlice';
import { messageSlice } from '../features/Message/messageSlice';
import { pasiensSlice } from '../features/Pasiens/pasiensSlice';
import { pengumumanSlice } from '../features/Pengumuman/pengumumanSlice';
import { permissionSlice } from '../features/Permission/permissionSlice';
import { productSlice } from '../features/Products/productSlice';
import { settingSlice } from '../features/Setting/settingSlice';
import { soSlice } from '../features/StockOpname/soSlice';
import { treatmentSlice } from '../features/Treatment/treatmentSlice';
import { usersSlice } from '../features/Users/usersSlice';
import { vendorSlice } from '../features/Vendors/vendorSlice';
import { vsSlice } from '../features/VirtualStock/vsSlice';
import { mainSlice } from '../features/main/mainSlice';

export const store = configureStore({
    reducer: {
        main: mainSlice.reducer,
        settings: settingSlice.reducer,
        faq: faqSlice.reducer,
        usersAdm: usersSlice.reducer,
        klinik: klinikSlice.reducer,
        vendor: vendorSlice.reducer,
        products: productSlice.reducer,
        barangMasuk: bmSlice.reducer,
        barangKeluar: bkSlice.reducer,
        stockOpname: soSlice.reducer,
        pasiens: pasiensSlice.reducer,
        treatment: treatmentSlice.reducer,
        jadwalDokter: jadwalDokterSlice.reducer,
        virtualStock: vsSlice.reducer,
        permission: permissionSlice.reducer,
        message: messageSlice.reducer,
        pengumuman: pengumumanSlice.reducer,
        info: infoMedisSlice.reducer,
    },
});
