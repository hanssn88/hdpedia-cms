import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux';
import {addData, addForm, clearError, closeForm, confirmDel, deleteData, fetchData} from './bkSlice'
import ReactDatatable from '@ashvin27/react-datatable';
import AppModal from '../../components/modal/MyModal';
import AppButton from '../../components/button/Button';
import {AppSwalSuccess} from '../../components/modal/SwalSuccess';
import {Col, Form} from "react-bootstrap";
import moment from 'moment';
import "moment/locale/id";
import "react-datetime/css/react-datetime.css";
import {SelectBm, SelectKlinik} from "../../components/modal/MySelect";
import NumberFormat from 'react-number-format';


var yesterday = moment().subtract(1, 'day');
var valid_startDate = function (current) {
    return current.isAfter(yesterday);
};

class BarangKeluar extends Component {

    constructor(props) {
        super(props);
        this.initSelected = {
            id_bm: '',
            name: '',
            qty: '',
            stok: '',
            shift: '',
            satuan: '',
            id_kliniks: '',
            nama_kliniks: '',
            operator_by: '',
        }
        this.state = {
            validSd: valid_startDate,
            validEd: valid_startDate,
            sort_order: "DESC",
            sort_column: "id",
            keyword: '',
            page_number: 1,
            per_page: 10,
            id_kliniks: '',
            nama_kliniks: '',
            selected: this.initSelected,
            errMsg: this.initSelected,
            loadingForm: false,
        }
    }

    componentDidMount() {
        this.fetchProfileAdmin();
    }

    fetchProfileAdmin = async () => {
        const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;
        const token = localStorage.getItem(tokenLogin);
        if (!token) {
            alert('session invalid');
            window.location.reload();
        }
        this.props.onLoad(this.state);


    }

    handleClose = () => {
        this.props.closeModal();
    };

    tableChangeHandler = (data) => {
        let queryString = this.state;
        Object.keys(data).map((key) => {
            if (key === "sort_order" && data[key]) {
                queryString.sort_order = data[key].order;
                queryString.sort_column = data[key].column;
            }
            if (key === "page_number") {
                queryString.page_number = data[key];
            }
            if (key === "page_size") {
                queryString.per_page = data[key];
            }
            if (key === "filter_value") {
                queryString.keyword = data[key];
            }
            return true;
        });
        this.props.onLoad(this.state);
    }

    handleChange(event) {
        const {name, value} = event.target
        var val = value;
        this.props.resetError();
        this.setState({
            selected: {
                ...this.state.selected,
                [name]: val
            },
            errMsg: {
                ...this.state.errMsg,
                [name]: val ? '' : this.state.errMsg[name]
            },
            loadingForm: false
        });
        if (!this.state.selected.operator_by) this.setState({
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
    }

    discardChanges = () => {
        this.setState({errMsg: {}, selected: this.initSelected, loadingForm: false});
        if (this.props.user.id_level !== 1) {
            this.setState({
                id_kliniks: this.props.user.id_klinik,
                selected: {
                    ...this.state.selected,
                    id_kliniks: this.props.user.id_klinik
                }
            });
        } else {
            this.setState({
                selected: {
                    ...this.state.selected,
                    id_kliniks: this.state.id_kliniks, nama_kliniks: this.state.nama_kliniks,
                }
            });
        }
        this.props.showForm();
    }


    handleSubmit() {
        var errors = this.state.errMsg;
        this.setState({
            ...this.state,
            loadingForm: true,
        });
        errors.id_bm = !this.state.selected.id_bm ? "Required" : '';
        errors.qty = !this.state.selected.qty ? "Required" : '';
        errors.shift = !this.state.selected.shift ? "Required" : '';
        if (this.state.selected.qty) {
            const dt = this.state.selected.qty.split(',');
            let qty = '';
            for (let i = 0; i < dt.length; i++) {
                qty += dt[i];
            }
            if (Number(qty) > Number(this.state.selected.stok)) errors.qty = 'Stok tidak cukup';
        }
        if (!this.state.selected.operator_by) this.setState({
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
        this.setState({errors});

        if (this.validateForm(this.state.errMsg)) {
            this.props.onAdd(this.state.selected);
        } else {
            console.error('Invalid Form')
        }

        this.setState({
            ...this.state,
            loadingForm: false,
        });
    }

    validateForm(errors) {
        let valid = true;
        Object.values(errors).forEach(
            (val) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    async onchangeSelect(evt) {
        const dt = evt.value.split('_');
        await this.setState({
            ...this.setState,
            selected: {
                ...this.state.selected,
                id_bm: dt[0], name: evt.label, stok: dt[1], satuan: dt[2]
            }
        });
        if (!this.state.selected.operator_by) this.setState({
            ...this.setState,
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
    };

    async onchangeSelectKlinik(evt) {
        await this.setState({
            ...this.setState,
            id_kliniks: evt.value,
            nama_kliniks: evt.label,
            selected: {
                ...this.state.selected,
                id_kliniks: evt.value, nama_kliniks: evt.label
            }
        });
        if (!this.state.selected.operator_by) this.setState({
            ...this.setState,
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
        this.props.onLoad(this.state);
    };

    render() {
        const {data, user} = this.props;
        const {selected, errMsg} = this.state;
        const permission = user?.permission;

        const columns = [
            {
                key: "no",
                text: "No.",
                width: 20,
                align: "center",
                sortable: false,
                cell: (row, index) => <div
                    style={{textAlign: "center"}}>{((this.state.page_number - 1) * this.state.per_page) + index + 1 + '.'}</div>,
                row: 0
            },
            {
                key: "created_at",
                text: "Tgl",
                align: "center",
                sortable: true,
                width: 110,
                cell: record => {
                    return (moment(new Date(record.created_at)).format('DD-MM-YYYY'))
                }
            },
            {
                key: "shift",
                text: "Shift",
                align: "center",
                sortable: true,
                width: 100,
            },
            {
                key: "name",
                text: "Produk",
                align: "center",
                sortable: true,
            },
            {
                key: "batch_no",
                text: "Batch",
                align: "center",
                width: 150,
            },
            {
                key: "expired_date",
                text: "Tgl. Expired",
                align: "center",
                sortable: true,
                width: 150,
                cell: record => {
                    return (moment(new Date(record.expired_date)).format('DD-MM-YYYY'))
                }
            },
            {
                key: "satuan",
                text: "Satuan",
                align: "center",
                sortable: true,
                width: 110,
            },
            {
                key: "qty",
                text: "Quantity",
                align: "center",
                width: 110,
                sortable: true,
                cell: record => {
                    return (<div style={{textAlign: "right"}}><Fragment>
                        <NumberFormat
                            value={record.qty}
                            thousandSeparator={true}
                            decimalScale={2}
                            displayType={'text'}
                        />
                    </Fragment></div>)
                }
            },

        ];
        const config = {
            key_column: 'id',
            page_size: 10,
            length_menu: [10, 20, 50],
            show_filter: true,
            show_pagination: true,
            pagination: 'advance',
            button: {
                excel: false,
                print: false
            },
            language: {
                loading_text: "Please be patient while data loads..."
            }
        }
        const frmUser = <Form id="myForm" style={{marginTop: 10, marginBottom: 25}}>
            <Form.Group controlId="id_products">
                <Form.Label>Produk</Form.Label>

                {errMsg.id_bm ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.id_bm}
                    </span>) : ''}
                <SelectBm
                    myVal={selected.id_bm ? ({value: selected.id_bm, label: selected.name}) : ''}
                    onChange={this.onchangeSelect.bind((this))}
                />
            </Form.Group>


            <Form.Group controlId="name">
                <Form.Label>Stok {selected.satuan ? '(' + selected.satuan + ')' : ''}</Form.Label>
                <NumberFormat
                    readOnly={true}
                    name="stok"
                    className="form-control form-control-sm"
                    value={selected.stok ? selected.stok : ''}
                    thousandSeparator={true}
                    decimalScale={2}
                    inputMode="numeric"
                    autoComplete="off"
                    placeholder="Stok"/>
            </Form.Group>

            <Form.Group controlId="qty">
                <Form.Label>Quantity</Form.Label>
                {errMsg.qty ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.qty}
                    </span>) : ''}
                <NumberFormat
                    onChange={this.handleChange.bind(this)}
                    name="qty"
                    className="form-control form-control-sm"
                    value={selected.qty ? selected.qty : ''}
                    thousandSeparator={true}
                    decimalScale={2}
                    inputMode="numeric"
                    autoComplete="off"
                    placeholder="Quantity"/>
            </Form.Group>
            <Form.Group controlId="shift">
                <Form.Label>Shift</Form.Label>
                {errMsg.shift ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.shift}
                        </span>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="shift"
                    as="select"
                    value={selected.shift}
                    onChange={this.handleChange.bind(this)}
                >
                    <option value="" selected={selected.jumlah_shift === "" ? true : false}>- Pilih -</option>
                    <option value="1" selected={selected.jumlah_shift === "1" ? true : false}>1</option>
                    <option value="2" selected={selected.jumlah_shift === "2" ? true : false}>2</option>
                    <option value="3" selected={selected.jumlah_shift === "3" ? true : false}>3</option>
                </Form.Control>
            </Form.Group>
        </Form>;


        return (

            <div className="content-wrapper">
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0">Barang Keluar</h1>
                            </div>
                            {/* /.col */}

                        </div>
                        {/* /.row */}
                    </div>
                    {/* /.container-fluid */}
                </div>
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                {/* card start */}
                                <div className="card card-success shadow-lg" style={{"minHeight": "800px"}}>
                                    <div className="card-header card-header-content" style={{"color": "#000"}}>

                                        <div className="row">
                                            <div className="col-4">
                                                <AppButton
                                                    disabled={(selected.id_kliniks || this.props?.user?.id_klinik) && parseInt(permission?.bk_add) > 0 ? false : true}
                                                    isLoading={this.props.isLoading}
                                                    theme="info"
                                                    onClick={this.discardChanges}
                                                    icon='add'> Add
                                                </AppButton>
                                            </div>
                                            <div className="pull-right col-2 offset-md-6">
                                                {this.props?.user?.id_level === 1 &&
                                                    <Form>
                                                        <Form.Row>
                                                            <Form.Group as={Col} sm={12} controlId="id_kliniks">
                                                                <SelectKlinik
                                                                    myVal={selected.id_kliniks ? ({
                                                                        value: selected.id_kliniks,
                                                                        label: selected.nama_kliniks
                                                                    }) : ''}
                                                                    onChange={this.onchangeSelectKlinik.bind((this))}
                                                                />

                                                            </Form.Group>
                                                        </Form.Row>
                                                    </Form>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body">
                                        {data ? (
                                            <ReactDatatable
                                                config={config}
                                                records={data}
                                                columns={columns}
                                                dynamic={true}
                                                onChange={this.tableChangeHandler}
                                                loading={this.props.isLoading}
                                                total_record={this.props.totalData}
                                            />
                                        ) : (<p>No Data ...</p>)}

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <AppModal
                    show={this.props.showFormAdd}
                    form={frmUser}
                    size="xs"
                    backdrop={false}
                    keyboard={false}
                    title="Add Barang Keluar"
                    titleButton="Save change"
                    themeButton="success"
                    handleClose={this.handleClose}
                    isLoading={this.props.isAddLoading ? this.props.isAddLoading : this.state.loadingForm}
                    formSubmit={this.handleSubmit.bind(this)}
                ></AppModal>

                {this.props.showFormSuccess ? (<AppSwalSuccess
                    show={this.props.showFormSuccess}
                    title={<div dangerouslySetInnerHTML={{__html: this.props.contentMsg}}/>}
                    type={this.props.tipeSWAL}
                    handleClose={this.props.closeSwal}
                >
                </AppSwalSuccess>) : ''}
            </div>


        )
    }
}

const mapStateToProps = (state) => ({
    data: state.barangKeluar.data || [],
    totalData: state.barangKeluar.totalData,
    isError: state.barangKeluar.isError,
    isLoading: state.barangKeluar.isLoading,
    showFormAdd: state.barangKeluar.showFormAdd,
    isAddLoading: state.barangKeluar.isAddLoading,
    errorPriority: state.barangKeluar.errorPriority || null,
    contentMsg: state.barangKeluar.contentMsg || null,
    showFormSuccess: state.barangKeluar.showFormSuccess,
    showFormDelete: state.barangKeluar.showFormDelete,
    tipeSWAL: state.barangKeluar.tipeSWAL,
    user: state.main.currentUser,
});
const mapDispatchToPros = (dispatch) => {
    return {
        onLoad: (param) => {
            dispatch(fetchData(param));
        },
        onAdd: (param) => {
            dispatch(addData(param));
        },
        showConfirmDel: (data) => {
            dispatch(confirmDel(data));
        },
        showForm: () => {
            dispatch(addForm());
        },
        onDelete: (param) => {
            dispatch(deleteData(param));
        },
        closeModal: () => {
            dispatch(closeForm());

        },
        closeSwal: () => {
            dispatch(closeForm());
            const queryString = {
                sort_order: "DESC",
                sort_column: "id",
                per_page: 10
            }
            dispatch(fetchData(queryString));
        },
        resetError: () => {
            dispatch(clearError());
        },

        closeSwalError: () => {
            dispatch(closeForm());
        }
    }
}
export default connect(mapStateToProps, mapDispatchToPros)(BarangKeluar);