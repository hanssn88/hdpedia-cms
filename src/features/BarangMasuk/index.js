import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux';
import {addData, addForm, clearError, closeForm, confirmDel, deleteData, fetchData} from './bmSlice'
import ReactDatatable from '@ashvin27/react-datatable';
import AppModal from '../../components/modal/MyModal';
import AppButton from '../../components/button/Button';
import {AppSwalSuccess} from '../../components/modal/SwalSuccess';
import {Col, Form} from "react-bootstrap";
import moment from 'moment';
import "moment/locale/id";
import Datetime from 'react-datetime';
import "react-datetime/css/react-datetime.css";
import Button from "react-bootstrap/Button";
import {SelectKlinik, SelectProducts, SelectVendor} from "../../components/modal/MySelect";
import NumberFormat from 'react-number-format';


var yesterday = moment().subtract(1, 'day');
var valid_startDate = function (current) {
    return current.isAfter(yesterday);
};

class BarangMasuk extends Component {

    constructor(props) {
        super(props);
        this.initSelected = {
            id: '',
            name: '',
            vendor_name: '',
            id_products: '',
            id_vendor: '',
            id_kliniks: '',
            nama_kliniks: '',
            tgl_terima: '',
            expired_date: '',
            batch_no: '',
            qty: '',
            validasi_batch: '',
            operator_by: '',
        }
        this.state = {
            validSd: valid_startDate,
            validEd: valid_startDate,
            sort_order: "DESC",
            sort_column: "id",
            keyword: '',
            page_number: 1,
            per_page: 10,
            id_kliniks: '',
            nama_kliniks: '',
            selected: this.initSelected,
            errMsg: this.initSelected,
            loadingForm: false,
        }
        this.handleChangeStartDate = this.handleChangeStartDate.bind(this);
        this.handleChangeEndDate = this.handleChangeEndDate.bind(this);
    }

    componentDidMount() {
        this.fetchProfileAdmin();
    }

    fetchProfileAdmin = async () => {
        const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;
        const token = localStorage.getItem(tokenLogin);
        if (!token) {
            alert('session invalid');
            window.location.reload();
        }
        this.props.onLoad(this.state);
    }

    handleClose = () => {
        this.props.closeModal();
    };

    tableChangeHandler = (data) => {
        let queryString = this.state;
        Object.keys(data).map((key) => {
            if (key === "sort_order" && data[key]) {
                queryString.sort_order = data[key].order;
                queryString.sort_column = data[key].column;
            }
            if (key === "page_number") {
                queryString.page_number = data[key];
            }
            if (key === "page_size") {
                queryString.per_page = data[key];
            }
            if (key === "filter_value") {
                queryString.keyword = data[key];
            }
            return true;
        });
        this.props.onLoad(this.state);
    }

    handleChange(event) {
        const {name, value} = event.target
        var val = value;
        this.props.resetError();
        this.setState({
            selected: {
                ...this.state.selected,
                [name]: val
            },
            errMsg: {
                ...this.state.errMsg,
                [name]: val ? '' : this.state.errMsg[name]
            },
            loadingForm: false
        });
        if (!this.state.selected.operator_by) this.setState({
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
    }

    discardChanges = () => {
        this.setState({errMsg: {}, selected: this.initSelected, loadingForm: false});
        if (this.props.user.id_level !== 1) {
            this.setState({
                id_kliniks: this.props.user.id_klinik,
                selected: {
                    ...this.state.selected,
                    id_kliniks: this.props.user.id_klinik
                }
            });
        } else {
            this.setState({
                selected: {
                    ...this.state.selected,
                    id_kliniks: this.state.id_kliniks, nama_kliniks: this.state.nama_kliniks,
                }
            });
        }
        this.props.showForm();
    }

    editRecord = async (record) => {
        this.setState({
            loadingForm: false,
            errMsg: this.initSelected,
            selected: {...record, operator_by: this.props.user.id_operator}
        });
        this.props.showForm(true);
    }

    deleteRecord = (record) => {
        this.setState({
            selected: {...record, operator_by: this.props.user.id_operator}
        });
        this.props.showConfirmDel(true);
    }

    handleSubmit() {
        var errors = this.state.errMsg;
        this.setState({
            ...this.state,
            loadingForm: true,
        });

        errors.id_products = !this.state.selected.id_products ? "Required" : '';
        errors.id_vendor = !this.state.selected.id_vendor ? "Required" : '';
        errors.qty = !this.state.selected.qty ? "Required" : '';
        errors.tgl_terima = !this.state.selected.tgl_terima ? "Required" : '';
        if (this.state.selected.validasi_batch) {
            errors.batch_no = !this.state.selected.batch_no ? "Required" : '';
            errors.expired_date = !this.state.selected.expired_date ? "Required" : '';
        }
        if (!this.state.selected.operator_by) this.setState({
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
        this.setState({errors});

        if (this.validateForm(this.state.errMsg)) {
            this.props.onAdd(this.state.selected);
        } else {
            console.error('Invalid Form')
        }

        this.setState({
            ...this.state,
            loadingForm: false,
        });
    }

    validateForm(errors) {
        let valid = true;
        Object.values(errors).forEach(
            (val) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    handleDelete() {
        this.props.onDelete(this.state.selected)
    }

    async onchangeSelect(evt) {
        const dt = evt.value.split('_');
        await this.setState({
            ...this.setState,
            selected: {
                ...this.state.selected,
                id_products: dt[0], name: evt.label, validasi_batch: dt[1]
            }
        });
        if (!this.state.selected.operator_by) this.setState({
            ...this.setState,
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
    };

    async onchangeSelectVendor(evt) {

        await this.setState({
            ...this.setState,
            selected: {
                ...this.state.selected,
                id_vendor: evt.value, vendor_name: evt.label
            }
        });
        if (!this.state.selected.operator_by) this.setState({
            ...this.setState,
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
    };

    async handleChangeStartDate(date) {
        this.setState({...this.state, errMsg: {...this.state.errMsg, tgl_terima: ''}})
        if (date) {
            const selectedDate = new Date(date);
            const _date = moment(selectedDate).format('YYYY-MM-DD');
            await this.setState({...this.state, selected: {...this.state.selected, tgl_terima: _date}})
        } else {
            await this.setState({...this.state, selected: {...this.state.selected, tgl_terima: ''}})
        }
        if (!this.state.selected.operator_by) this.setState({
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
    }

    handleChangeEndDate(date) {

        if (date) {
            const selectedDate = new Date(date);
            const _date = moment(selectedDate).format('YYYY-MM-DD');
            this.setState({...this.state, selected: {...this.state.selected, expired_date: _date}})
        } else {
            this.setState({...this.state, selected: {...this.state.selected, expired_date: ''}})
        }
        if (!this.state.selected.operator_by) this.setState({
            ...this.setState,
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
    }

    async onchangeSelectKlinik(evt) {
        await this.setState({
            ...this.setState,
            id_kliniks: evt.value,
            nama_kliniks: evt.label,
            selected: {
                ...this.state.selected,
                id_kliniks: evt.value, nama_kliniks: evt.label
            }
        });
        if (!this.state.selected.operator_by) this.setState({
            ...this.setState,
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
        this.props.onLoad(this.state);
    };

    renderView(mode, renderDefault, name) {
        // Only for years, months and days view
        if (mode === "time") return renderDefault();

        return (
            <div className="wrapper">
                {renderDefault()}
                <div className="controls">
                    <Button variant="warning" type="button" onClick={() => this.clear(name)}>Clear</Button>
                </div>
            </div>
        );
    }

    clear(name) {
        if (name === "expired_date") {
            this.handleChangeEndDate();
        }
        if (name === "tgl_terima") {
            this.handleChangeStartDate();
        }
    }

    render() {
        const {data, user} = this.props;
        const {selected, errMsg} = this.state;
        const permission = user?.permission;

        const columns = [
            {
                key: "no",
                text: "No.",
                width: 20,
                align: "center",
                sortable: false,
                cell: (row, index) => <div
                    style={{textAlign: "center"}}>{((this.state.page_number - 1) * this.state.per_page) + index + 1 + '.'}</div>,
                row: 0
            },
            {
                key: "tgl_terima",
                text: "Tgl. terima",
                align: "center",
                sortable: true,
                width: 110,
                cell: record => {
                    return (moment(new Date(record.tgl_terima)).format('DD-MM-YYYY'))
                }
            },
            {
                key: "name",
                text: "Produk",
                align: "center",
                sortable: true,
            },
            {
                key: "vendor_name",
                text: "Vendor",
                align: "center",
                sortable: true,
            },
            {
                key: "batch_no",
                text: "Batch",
                align: "center",
                width: 150,
            },
            {
                key: "expired_date",
                text: "Tgl. Expired",
                align: "center",
                sortable: true,
                width: 150,
                cell: record => {
                    return (moment(new Date(record.expired_date)).format('DD-MM-YYYY'))
                }
            },
            {
                key: "satuan",
                text: "Satuan",
                align: "center",
                sortable: true,
                width: 110,
            },
            {
                key: "qty_awal",
                text: "Qty Awal",
                align: "center",
                width: 110,
                sortable: true,
                cell: record => {
                    return (<div style={{textAlign: "right"}}><Fragment>
                        <NumberFormat
                            value={record.qty_awal}
                            thousandSeparator={true}
                            decimalScale={2}
                            displayType={'text'}
                        />
                    </Fragment></div>)
                }
            },
            {
                key: "qty_digunakan",
                text: "Qty keluar",
                align: "center",
                width: 110,
                sortable: true,
                cell: record => {
                    return (<div style={{textAlign: "right"}}><Fragment>
                        <NumberFormat
                            value={record.qty_digunakan}
                            thousandSeparator={true}
                            decimalScale={2}
                            displayType={'text'}
                        />
                    </Fragment></div>)
                }
            },
            {
                key: "qty",
                text: "Sisa",
                align: "center",
                width: 110,
                sortable: true,
                cell: record => {
                    return (<div style={{textAlign: "right"}}><Fragment>
                        <NumberFormat
                            value={record.qty}
                            thousandSeparator={true}
                            decimalScale={2}
                            displayType={'text'}
                        />
                    </Fragment></div>)
                }
            },

        ];
        const config = {
            key_column: 'id',
            page_size: 10,
            length_menu: [10, 20, 50],
            show_filter: true,
            show_pagination: true,
            pagination: 'advance',
            button: {
                excel: false,
                print: false
            },
            language: {
                loading_text: "Please be patient while data loads..."
            }
        }
        const frmUser = <Form id="myForm" style={{marginTop: 10, marginBottom: 25}}>

            <Form.Group controlId="tanggal_terima">
                <Form.Label>Tanggal Terima</Form.Label>

                {errMsg.tgl_terima ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.tgl_terima}
                    </span>) : ''}
                <Datetime
                    closeOnSelect={true}
                    timeFormat={false}
                    setViewDate={selected.tgl_terima ? (new Date(selected.tgl_terima)) : new Date()}
                    value={selected.tgl_terima ? (new Date(selected.tgl_terima)) : ''}
                    onChange={this.handleChangeStartDate}
                    inputProps={{
                        readOnly: true,
                        autoComplete: "off",
                        placeholder: 'Tanggal Terima',
                        name: 'tgl_terima',
                        className: 'form-control form-control-sm'
                    }}
                    renderView={(mode, renderDefault, tgl_terima) =>
                        this.renderView(mode, renderDefault, 'tgl_terima')
                    }
                    locale="id"
                />
            </Form.Group>
            <Form.Group controlId="id_products">
                <Form.Label>Produk</Form.Label>

                {errMsg.id_products ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.id_products}
                    </span>) : ''}
                <SelectProducts
                    myVal={selected.id_products ? ({value: selected.id_products, label: selected.name}) : ''}
                    onChange={this.onchangeSelect.bind((this))}
                />
            </Form.Group>

            <Form.Group controlId="id_vendor">
                <Form.Label>Vendor</Form.Label>

                {errMsg.id_vendor ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.id_vendor}
                    </span>) : ''}
                <SelectVendor
                    myVal={selected.id_vendor ? ({value: selected.id_vendor, label: selected.vendor_name}) : ''}
                    onChange={this.onchangeSelectVendor.bind((this))}
                />
            </Form.Group>

            <Form.Group controlId="expired_date">
                <Form.Label>Tanggal Expired</Form.Label>

                {errMsg.expired_date ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.expired_date}
                    </span>) : ''}
                <Datetime
                    closeOnSelect={true}
                    timeFormat={false}
                    setViewDate={selected.expired_date ? (new Date(selected.expired_date)) : new Date()}
                    value={selected.expired_date ? (new Date(selected.expired_date)) : ''}
                    onChange={this.handleChangeEndDate}
                    inputProps={{
                        readOnly: true,
                        autoComplete: "off",
                        placeholder: 'Tanggal Expired',
                        name: 'expired_date',
                        className: 'form-control form-control-sm'
                    }}
                    renderView={(mode, renderDefault, expired_date) =>
                        this.renderView(mode, renderDefault, 'expired_date')
                    }
                    locale="id"
                />
            </Form.Group>

            <Form.Group controlId="name">
                <Form.Label>Batch</Form.Label>

                {errMsg.batch_no ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.batch_no}
                    </span>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="batch_no"
                    type="text"
                    value={selected.batch_no}
                    onChange={this.handleChange.bind(this)}
                    placeholder="Batch"/>
            </Form.Group>

            <Form.Group controlId="qty">
                <Form.Label>Quantity</Form.Label>
                {errMsg.qty ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.qty}
                    </span>) : ''}
                <NumberFormat
                    onChange={this.handleChange.bind(this)}
                    name="qty"
                    className="form-control form-control-sm"
                    value={selected.qty ? selected.qty : ''}
                    thousandSeparator={true}
                    decimalScale={2}
                    inputMode="numeric"
                    autoComplete="off"
                    placeholder="Quantity"/>
            </Form.Group>
        </Form>;

        const contentDelete = <div
            dangerouslySetInnerHTML={{__html: '<div id="caption" style="padding-bottom:20px;">Apakah anda yakin <br/>akan menghapus data ini ?</div>'}}/>;
        return (

            <div className="content-wrapper">
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0">Barang Masuk</h1>
                            </div>
                            {/* /.col */}

                        </div>
                        {/* /.row */}
                    </div>
                    {/* /.container-fluid */}
                </div>
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                {/* card start */}
                                <div className="card card-success shadow-lg" style={{"minHeight": "800px"}}>
                                    <div className="card-header card-header-content">
                                        <div className="row">
                                            <div className="col-4">
                                                <AppButton
                                                    disabled={(selected.id_kliniks || this.props?.user?.id_klinik) && parseInt(permission?.bm_add) > 0 ? false : true}
                                                    isLoading={this.props.isLoading}
                                                    theme="info"
                                                    onClick={this.discardChanges}
                                                    icon='add'
                                                >
                                                    Add
                                                </AppButton>
                                            </div>
                                            <div className="pull-right col-2 offset-md-6">
                                                {this.props?.user?.id_level === 1 &&
                                                    <Form>
                                                        <Form.Row>
                                                            <Form.Group as={Col} sm={12} controlId="id_kliniks">
                                                                <SelectKlinik
                                                                    myVal={selected.id_kliniks ? ({
                                                                        value: selected.id_kliniks,
                                                                        label: selected.nama_kliniks
                                                                    }) : ''}
                                                                    onChange={this.onchangeSelectKlinik.bind((this))}
                                                                />

                                                            </Form.Group>
                                                        </Form.Row>
                                                    </Form>
                                                }
                                            </div>
                                        </div>

                                    </div>
                                    <div className="card-body">
                                        {data ? (
                                            <ReactDatatable
                                                config={config}
                                                records={data}
                                                columns={columns}
                                                dynamic={true}
                                                onChange={this.tableChangeHandler}
                                                loading={this.props.isLoading}
                                                total_record={this.props.totalData}
                                            />
                                        ) : (<p>No Data ...</p>)}

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <AppModal
                    show={this.props.showFormAdd}
                    form={frmUser}
                    size="xs"
                    backdrop={false}
                    keyboard={false}
                    title="Add Barang Masuk"
                    titleButton="Save change"
                    themeButton="success"
                    handleClose={this.handleClose}
                    isLoading={this.props.isAddLoading ? this.props.isAddLoading : this.state.loadingForm}
                    formSubmit={this.handleSubmit.bind(this)}
                ></AppModal>
                <AppModal
                    show={this.props.showFormDelete}
                    size="xs"
                    form={contentDelete}
                    handleClose={this.handleClose}
                    backdrop="static"
                    keyboard={false}
                    title="Delete"
                    titleButton="Delete"
                    themeButton="danger"
                    isLoading={this.props.isAddLoading}
                    formSubmit={this.handleDelete.bind(this)}
                ></AppModal>
                {this.props.showFormSuccess ? (<AppSwalSuccess
                    show={this.props.showFormSuccess}
                    title={<div dangerouslySetInnerHTML={{__html: this.props.contentMsg}}/>}
                    type={this.props.tipeSWAL}
                    handleClose={this.props.closeSwal}
                >
                </AppSwalSuccess>) : ''}
            </div>


        )
    }
}

const mapStateToProps = (state) => ({
    data: state.barangMasuk.data || [],
    totalData: state.barangMasuk.totalData,
    isError: state.barangMasuk.isError,
    isLoading: state.barangMasuk.isLoading,
    showFormAdd: state.barangMasuk.showFormAdd,
    isAddLoading: state.barangMasuk.isAddLoading,
    errorPriority: state.barangMasuk.errorPriority || null,
    contentMsg: state.barangMasuk.contentMsg || null,
    showFormSuccess: state.barangMasuk.showFormSuccess,
    showFormDelete: state.barangMasuk.showFormDelete,
    tipeSWAL: state.barangMasuk.tipeSWAL,
    user: state.main.currentUser,
});
const mapDispatchToPros = (dispatch) => {
    return {
        onLoad: (param) => {
            dispatch(fetchData(param));
        },
        onAdd: (param) => {
            dispatch(addData(param));
        },
        showConfirmDel: (data) => {
            dispatch(confirmDel(data));
        },
        showForm: () => {
            dispatch(addForm());
        },
        onDelete: (param) => {
            dispatch(deleteData(param));
        },
        closeModal: () => {
            dispatch(closeForm());

        },
        closeSwal: () => {
            dispatch(closeForm());
            const queryString = {
                sort_order: "DESC",
                sort_column: "id",
                per_page: 10
            }
            dispatch(fetchData(queryString));
        },
        resetError: () => {
            dispatch(clearError());
        },

        closeSwalError: () => {
            dispatch(closeForm());
        }
    }
}
export default connect(mapStateToProps, mapDispatchToPros)(BarangMasuk);