import ReactDatatable from '@ashvin27/react-datatable';
import React, { Component, Fragment } from 'react';
import { Figure, Form } from "react-bootstrap";
import { connect } from 'react-redux';
import AppButton from '../../components/button/Button';
import AppModal from '../../components/modal/MyModal';
import { AppSwalSuccess } from '../../components/modal/SwalSuccess';
import { addData, addForm, clearError, closeForm, confirmDel, deleteData, editData, fetchData } from './infoMedisSlice';

class InfoMedisPasien extends Component {

    constructor(props) {
        super(props);
        this.initSelected = {
            id: '',
            name: '',
            url: '',
            image: '',
        }
        this.state = {
            sort_order: "ASC",
            sort_column: "name",
            keyword: '',
            page_number: 1,
            per_page: 10,
            selected: this.initSelected,
            errMsg: this.initSelected,
            loadingForm: false,
        }
    }

    componentDidMount() {
        this.fetchProfileAdmin();
        this.props.onLoad(this.state);
    }

    fetchProfileAdmin = async () => {
        const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;
        const token = localStorage.getItem(tokenLogin);
        if (!token) {
            alert('session invalid');
            window.location.reload();
        }
    }
    handleClose = () => {
        this.props.closeModal();
    };

    tableChangeHandler = (data) => {
        let queryString = this.state;
        Object.keys(data).map((key) => {
            if (key === "sort_order" && data[key]) {
                queryString.sort_order = data[key].order;
                queryString.sort_column = data[key].column;
            }
            if (key === "page_number") {
                queryString.page_number = data[key];
            }
            if (key === "page_size") {
                queryString.per_page = data[key];
            }
            if (key === "filter_value") {
                queryString.keyword = data[key];
            }
            return true;
        });
        this.props.onLoad(this.state);
    }

    handleChange(event) {
        const {name, value} = event.target
        var val = value;
        this.props.resetError();


        if (event.target.name === "image") {
            val = event.target.files[0];
            this.setState({selected: {...this.state.selected, imgUpload: "", image: ""}});
            if (!val) return;
            if (!val.name.match(/\.(jpg|jpeg|png)$/)) {
                this.setState({
                    loadingForm: true,
                    errMsg: {...this.state.errMsg, image: "Please select valid image(.jpg .jpeg .png)"}
                });

                //setLoading(true);
                return;
            }
            if (val.size > 2099200) {
                this.setState({loadingForm: true, errMsg: {...this.state.errMsg, image: "File size over 2MB"}});

                //setLoading(true);
                return;
            }
            let reader = new FileReader();
            reader.readAsDataURL(val);
            reader.onloadend = () => {
                this.setState({
                    loadingForm: false,
                    selected: {...this.state.selected, imgUpload: reader.result, image: val}
                });
            };
        }

        this.setState({
            selected: {
                ...this.state.selected,
                [name]: val
            },
            errMsg: {
                ...this.state.errMsg,
                [name]: val ? '' : this.state.errMsg[name]
            },
            loadingForm: false
        });
        if (!this.state.selected.operator_by) this.setState({
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
    }

    discardChanges = () => {
        this.setState({errMsg: {}, selected: this.initSelected, loadingForm: false});
        if (!this.state.selected.operator_by) this.setState({
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
        this.props.showForm();
        this.state.flag = false;
    }

    editRecord = async (record) => {
        this.setState({
            loadingForm: false,
            errMsg: this.initSelected,
            selected: {...record, operator_by: this.props.user.id_operator, imgUpload: record.image}
        });
        this.props.showForm(true);
        this.state.flag = true;
    }

    deleteRecord = (record) => {
        this.setState({
            selected: {...record, operator_by: this.props.user.id_operator}
        });
        this.props.showConfirmDel(true);
    }

    handleSubmit() {
        var errors = this.state.errMsg;
        this.setState({
            ...this.state,
            loadingForm: true,
        });


        errors.name = !this.state.selected.name ? "Required" : '';      
        errors.url = !this.state.selected.url ? "Required" : '';      
        
        if (this.state.selected.image) {
            var fileSize = this.state.selected.image.size;
            if (fileSize > 2099200) { // satuan bytes 2099200 => 2MB
                errors.image = "File size over 2MB";
            }
        }
        if (!this.state.selected.operator_by) this.setState({
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
        this.setState({errors});

        if (this.validateForm(this.state.errMsg)) {
            this.props.onAdd(this.state.selected);
        } else {
            console.error('Invalid Form')
        }

        this.setState({
            ...this.state,
            loadingForm: false,
        });
    }

    validateForm(errors) {
        let valid = true;
        Object.values(errors).forEach(
            (val) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    handleDelete() {
        this.props.onDelete(this.state.selected)
    }

    listIMG = async (record) => {
        if (record) await sessionStorage.setItem('idKlinikHDPEDIA', record.id);
        this.props.history.push("list_img_klinik");
    }

    render() {
        const {data, dataDokter, user} = this.props;
        const {selected, errMsg} = this.state;
        const permission = user?.permission;

        const columns = [
            {
                key: "no",
                text: "No.",
                width: 20,
                align: "center",
                sortable: false,
                cell: (row, index) => <div
                    style={{textAlign: "center"}}>{((this.state.page_number - 1) * this.state.per_page) + index + 1 + '.'}</div>,
                row: 0
            },
            {
                key: "name",
                text: "Name",
                align: "center",
                sortable: true,
            },
            {
                key: "url",
                text: "Url",
                align: "center",
                sortable: true,
            },
         
            {
                key: "action",
                text: "Action",
                width: 200,
                align: "center",
                sortable: false,
                cell: record => {

                    return (
                        <div style={{textAlign: "center"}}>
                            <Fragment>
                                <button
                                    disabled={parseInt(permission?.banner_edit) > 0 ? false : true}
                                    className="btn btn-xs btn-success"
                                    onClick={e => this.editRecord(record)}
                                    style={{marginRight: '5px'}}>
                                    <i className="fa fa-edit"></i> Edit
                                </button>
                                <button
                                    disabled={parseInt(permission?.banner_delete) > 0 ? false : true}
                                    className="btn btn-danger btn-xs"
                                    onClick={() => this.deleteRecord(record)}>
                                    <i className="fa fa-trash"></i> Delete
                                </button>
                            </Fragment>
                        </div>
                    );
                }
            }
        ];
        const config = {
            key_column: 'id',
            page_size: 10,
            length_menu: [10, 20, 50],
            show_filter: true,
            show_pagination: true,
            pagination: 'advance',
            button: {
                excel: false,
                print: false
            },
            language: {
                loading_text: "Please be patient while data loads..."
            }
        }
        const frmUser = <Form id="myForm">
            <Form.Group controlId="name">
                <Form.Label>Nama</Form.Label>

                {errMsg.name ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.name}
                    </span>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="name"
                    type="text"
                    value={selected.name}
                    onChange={this.handleChange.bind(this)}
                    placeholder="Nama"/>
            </Form.Group>
            <Form.Group controlId="url">
                <Form.Label>Url</Form.Label>

                {errMsg.url ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.url}
                    </span>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="url"
                    type="text"
                    value={selected.url}
                    onChange={this.handleChange.bind(this)}
                    placeholder="Url"/>
            </Form.Group>

            <Form.Group controlId="image">
                <Form.Label>Image</Form.Label>{errMsg.img ?
                (<span className="float-right text-error badge badge-danger">{errMsg.image}</span>) : null}
                <Form.File size="sm" name="image" setfieldvalue={selected.image}
                           onChange={this.handleChange.bind(this)}/>
            </Form.Group>
            {selected.imgUpload ? (<Form.Group controlId="imagePreview" style={{marginBottom: 0}}>
                <Figure>
                    <Figure.Image
                        thumbnail
                        width={130}
                        height={100}
                        alt=""
                        src={selected.imgUpload}
                    />
                </Figure>
            </Form.Group>) : ''}

        </Form>;

        const contentDelete = <div
            dangerouslySetInnerHTML={{__html: '<div id="caption" style="padding-bottom:20px;">Apakah anda yakin <br/>akan menghapus data ini ?</div>'}}/>;
        return (

            <div className="content-wrapper">
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0">Info Medis Pasien</h1>
                            </div>
                            {/* /.col */}

                        </div>
                        {/* /.row */}
                    </div>
                    {/* /.container-fluid */}
                </div>
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                {/* card start */}
                                <div className="card card-success shadow-lg" style={{"minHeight": "800px"}}>
                                    <div className="card-header card-header-content">
                                        <AppButton
                                            disabled={parseInt(permission?.banner_add) > 0 ? false : true}
                                            isLoading={this.props.isLoading}
                                            theme="info"
                                            onClick={this.discardChanges}
                                            icon='add'
                                        >
                                            Add
                                        </AppButton>

                                    </div>
                                    <div className="card-body">
                                        {data ? (
                                            <ReactDatatable
                                                config={config}
                                                records={data}
                                                columns={columns}
                                                dynamic={true}
                                                onChange={this.tableChangeHandler}
                                                loading={this.props.isLoading}
                                                total_record={this.props.totalData}
                                            />
                                        ) : (<p>No Data ...</p>)}

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <AppModal
                    show={this.props.showFormAdd}
                    form={frmUser}
                    size="xs"
                    backdrop={false}
                    keyboard={false}
                    title="Add/Edit Info Medis Pasien"
                    titleButton="Save change"
                    themeButton="success"
                    handleClose={this.handleClose}
                    isLoading={this.props.isAddLoading ? this.props.isAddLoading : this.state.loadingForm}
                    formSubmit={this.handleSubmit.bind(this)}
                ></AppModal>
                <AppModal
                    show={this.props.showFormDelete}
                    size="xs"
                    form={contentDelete}
                    handleClose={this.handleClose}
                    backdrop="static"
                    keyboard={false}
                    title="Delete"
                    titleButton="Delete"
                    themeButton="danger"
                    isLoading={this.props.isAddLoading}
                    formSubmit={this.handleDelete.bind(this)}
                ></AppModal>
                {this.props.showFormSuccess ? (<AppSwalSuccess
                    show={this.props.showFormSuccess}
                    title={<div dangerouslySetInnerHTML={{__html: this.props.contentMsg}}/>}
                    type={this.props.tipeSWAL}
                    handleClose={this.props.closeSwal}
                >
                </AppSwalSuccess>) : ''}
            </div>


        )
    }
}

const mapStateToProps = (state) => ({
    data: state.info.data || [],
    totalData: state.info.totalData,
    isError: state.info.isError,
    isLoading: state.info.isLoading,
    showFormAdd: state.info.showFormAdd,
    isAddLoading: state.info.isAddLoading,
    errorPriority: state.info.errorPriority || null,
    contentMsg: state.info.contentMsg || null,
    showFormSuccess: state.info.showFormSuccess,
    showFormDelete: state.info.showFormDelete,
    tipeSWAL: state.info.tipeSWAL,
    user: state.main.currentUser,
});
const mapDispatchToPros = (dispatch) => {
    return {
        onLoad: (param) => {
            dispatch(fetchData(param));
        },
        onAdd: (param) => {
            if(param.id != ''){
                dispatch(editData(param));
            }else{
                dispatch(addData(param));
            }
            
            // dispatch(editData(param));
        },
        showConfirmDel: (data) => {
            dispatch(confirmDel(data));
        },
        showForm: () => {
            dispatch(addForm());
        },
        onDelete: (param) => {
            dispatch(deleteData(param));
        },
        closeModal: () => {
            dispatch(closeForm());

        },
        closeSwal: () => {
            dispatch(closeForm());
            const queryString = {
                sort_order: "ASC",
                sort_column: "name",
                per_page: 10
            }
            dispatch(fetchData(queryString));
        },
        resetError: () => {
            dispatch(clearError());
        },

        closeSwalError: () => {
            dispatch(closeForm());
        }
    }
}
export default connect(mapStateToProps, mapDispatchToPros)(InfoMedisPasien);