import axios from "axios";


const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

class JadwalService {
    postData(param) {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        return axios.post(API_URL + "/admin/search_mapping_petugas_dropdown", param, {
            headers: {
                'Authorization': 'Bearer ' + token
            }
        });
    }
}

export default new JadwalService()