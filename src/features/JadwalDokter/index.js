import moment from "moment/moment";
import React, {Component} from 'react';
import {Calendar, momentLocalizer} from "react-big-calendar";
import "react-big-calendar/lib/css/react-big-calendar.css";
import {Form} from "react-bootstrap";
import {connect} from 'react-redux';
import AppModal from "../../components/modal/MyModal";
import {SelectPetugas} from "../../components/modal/MySelect";
import {AppSwalSuccess} from "../../components/modal/SwalSuccess";
import {fetchData as fetchDataKlinik} from "../Klinik/klinikSlice";
import {fetchDataShift} from "../Treatment/treatmentSlice";
import {fetchData as fetchDataDokter} from '../Users/usersSlice';
import {addData, addForm, closeForm, confirmDel, fetchData} from './jadwalDokterSlice';
import JadwalService from "./JadwalService";

class JadwalDokter extends Component {

    constructor(props) {
        super(props);
        this.initSelected = {
            id: '',
            shift: '',
            klinik: '',
            user_id: '',
            name: '',
            date: '',
           
            note: ''
        }
        this.state = {
            month_filter: `${new Date().getFullYear()}-${new Date().getMonth() + 1}`,
            page_number: 1,
            klinik_id: sessionStorage.getItem('klinikHD'),
            dataPetugas:this.initSelected,
            selected: this.initSelected,
            errMsg: this.initSelected,
            loadingForm: false,
        }
        this.handleNavigate = this.handleNavigate.bind(this);
    }

    componentDidMount() {

        this.props.onLoad(this.state);
    }

    fetchProfileAdmin = async () => {
        const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;
        const token = localStorage.getItem(tokenLogin);
        console.log(token);
        if (!token) {
            alert('session invalid');
            window.location.reload();
        }
    }
    handleClose = () => {
        this.props.closeModal();
    };
    handleCloseSwal = () => {
        const param = {
            month_filter: this.state.month_filter,
            klinik_id: this.state.klinik_id
        }
        this.props.closeSwal(param);
    };

    eventStyleGetter = (event, start, end, isSelected) => {

        let backgroundColor = '#FFA500';
        let color = '#FFFFFF';
        if (event.type === "dokter") {
            backgroundColor = '#FFA500';
            color = '#000000';
        }

        if (event.type === "perawat") backgroundColor = '#008000';
        if (event.type === "admin") backgroundColor = '#0000FF';
        if (event.type === "apoteker") {
            backgroundColor = '#FFC0CB';
            color = '#000000';
        }
        if (event.type === "petugas rm") {
            backgroundColor = '#ff0000';
        }
        var style = {
            backgroundColor: backgroundColor,
            borderRadius: '0px',
            opacity: 0.8,
            color: color,
            border: '0px',
            display: 'block'
        };
        return {
            style: style
        };
    }

    handleSelectEvent = async (data) => {
        this.setState({
            errMsg: {},
            selected: {
                ...this.initSelected,
                id: data.id,
                name: data.name,
                klinik: data.klinik_id,
                user_id: data.user_id,
                shift: data.shift,
                note: data.note,
                date: `${new Date(data.start).getFullYear()}-${new Date(data.start).getMonth() + 1}-${new Date(data.start).getDate()}`,
            },
            loadingForm: false
        });
        const params = {
            ...this.state,
            klinik_id: data.klinik_id,
        }

        this.props.handleKlinik(params);
        this.getData(data.klinik_id);
        this.props.showForm();
    };

    handleSelectSlot = async (data) => {
        this.setState({
            errMsg: {},
            selected: {
                ...this.initSelected,
                date: `${new Date(data.start).getFullYear()}-${new Date(data.start).getMonth() + 1}-${new Date(data.start).getDate()}`,
            },
            loadingForm: false
        });
        this.props.showForm();
    };

    async handleNavigate(date, view, action) {
        //this.refreshEvents();
        const param = {
            month_filter: `${new Date(date).getFullYear()}-${new Date(date).getMonth() + 1}`,
            klinik_id: this.state.klinik_id
        }
        this.setState({...param});
        await this.props.onLoad(param);
    }

    handleChange(event) {
        const {name, value} = event.target
        var val = value;

        this.setState({
            selected: {
                ...this.state.selected,
                [name]: val
            },
            errMsg: {
                ...this.state.errMsg,
                [name]: val ? '' : this.state.errMsg[name]
            },
            loadingForm: false
        });
    }

    handleFilterKlinik(event) {
        const {name, value} = event.target
        var val = value;

        this.setState({
            [name]: val,
            loadingForm: false
        });
        const params = {
            ...this.state,
            klinik_id: val,
        }
        this.props.onFilterKlinik(params);


    }

    handleKlinik(event) {
        const {name, value} = event.target
        var val = value;

        this.setState({
            selected: {
                ...this.state.selected,
                [name]: val
            },
            errMsg: {
                ...this.state.errMsg,
                [name]: val ? '' : this.state.errMsg[name]
            },
            loadingForm: false
        });
        this.setState({
            [name]: val,
            loadingForm: false
        });
        const params = {
            ...this.state,
            klinik_id: val,
        }


        this.props.handleKlinik(params);
        this.getData(val);
    }

    async onchangeSelect(evt) {
        await this.setState({
            ...this.setState,
            selected: {
                ...this.state.selected,
                user_id: evt.value, name: evt.label
            }
        });
    };

    getData = async (param) => {
        const queryString = {
            id_kliniks: param,
            sort_order:'ASC',
            sort_column:'name'
        };
        this.setState(queryString);
        await JadwalService.postData(queryString)
            .then(response => {
                if (response.data.err_code === "00") {
                    const dataPetugas = response.data.data;
                    this.setState({dataPetugas: dataPetugas,  appsLoading: false});
                    // console.log(this.state.dataPetugas);
                }
                if (response.data.err_code === "04") {
                    this.setState({isLoading: false, appsLoading: false});
                }
            })
            .catch(e => {
                console.log(e);
                this.setState({isLoading: false, dtImg: []});
            });
        //this.setState({loadingPage:false})
    };


    handleSubmit() {
        let errors = this.state.errMsg;
        this.setState({
            ...this.state,
            loadingForm: true,
        });
        errors.user_id = !this.state.selected.user_id ? "Required" : '';
        errors.shift = !this.state.selected.shift ? "Required" : '';

        if (this.props?.user?.id_level === 1) {
            errors.klinik = !this.state.selected.klinik ? "Required" : '';
        }


        this.setState({errors});

        if (this.validateForm(this.state.errMsg)) {


            if (this.props?.user?.id_level === 1) {
                const params = {
                    id_petugas: this.state.selected.user_id || 39,
                    klinik_id: this.state.klinik,
                    shift: this.state.selected.shift,
                    klinik: this.state.selected.klinik,
                    date: this.state.selected.date,
                    note: this.state.selected.note,
                    // id: this.state.selected.id,
                }
                this.props.onAdd(params);
            } else {

                const params = {
                    id_petugas: this.state.selected.user_id || 39,
                    klinik_id: this.state.klinik_id,
                    shift: this.state.selected.shift,
                    // klinik_id: this.state.selected.klinik_id,
                    date: this.state.selected.date,
                    note: this.state.selected.note,
                    // id: this.state.selected.id,
                }
                this.props.onAdd(params);
            }


        } else {
            console.error('Invalid Form')
        }

        this.setState({
            ...this.state,
            loadingForm: false,
        });
    }

    validateForm(errors) {
        let valid = true;
        Object.values(errors).forEach(
            (val) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    render() {
        const {data, user, dataShift, dataKlinik} = this.props;
        const {selected, errMsg, klinik_id,dataPetugas} = this.state;
        const localizer = momentLocalizer(moment);
        const permission = user?.permission;
        console.log(dataPetugas);

        const frmUser = <Form id="myForm">

            {[1, 7].includes(parseInt(user.id_level)) ?
                <Form.Group controlId="klink" style={{marginTop: "30px"}}>
                    <Form.Label>Klinik</Form.Label>
                    {errMsg.klinik ?
                        (<span className="float-right text-error badge badge-danger">{errMsg.klinik}
                        </span>) : ''}
                    <Form.Control
                        size="sm"
                        name="klinik"
                        as="select"
                        value={selected.klinik ? selected.klinik : ""}
                        onChange={this.handleKlinik.bind(this)}>
                        <option value="">- Pilih Klinik -</option>
                        {dataKlinik ? (
                            dataKlinik.map(function (klinik) {
                                return <option
                                    selected={klinik.id === selected.klinik ? true : false}
                                    value={klinik.id}
                                    key={klinik.id}>{klinik.name}
                                </option>
                            })

                        ) : ''}

                    </Form.Control>

                </Form.Group>

                : ''}


            <Form.Group controlId="id_petugas" style={{marginTop: "35px"}}>
                <Form.Label>Petugas</Form.Label>
                {errMsg.user_id ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.user_id}
                    </span>) : ''}
                    <Form.Control
                        size="sm"
                        name="user_id"
                        as="select"
                        value={selected.user_id ? selected.user_id : ""}
                        onChange={this.handleChange.bind(this)}>
                        <option value="">- Pilih Petugas -</option>
                        {dataPetugas.length > 0 ? (
                            dataPetugas.map((petugas) => {
                                return <option key={petugas.id_operator} value={petugas.id_operator}>{petugas.name} - {petugas.title}</option>;
                              })

                        ) : ''}

                    </Form.Control>
            </Form.Group>


         

            <Form.Group controlId="shift" style={{marginTop: "30px"}}>
                <Form.Label>Shift</Form.Label>
                {errMsg.shift ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.shift}
                        </span>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="shift"
                    as="select"
                    value={selected.shift}
                    onChange={this.handleChange.bind(this)}
                >
                    <option value="">- Pilih -</option>
                    {dataShift ? (
                        dataShift.map(function (shift) {
                            return <option
                                selected={shift.id === selected.shift ? true : false}
                                value={shift.id}
                                key={shift.id}>{shift.name}
                            </option>
                        })

                    ) : ''}

                </Form.Control>
            </Form.Group>
            <Form.Group controlId="note" style={{marginTop: "30px"}}>
                <Form.Label>Notes</Form.Label>
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="note"
                    type="text"
                    value={selected?.note}
                    placeholder="Notes"
                    onChange={this.handleChange.bind(this)}/>
            </Form.Group>
            <br/>

        </Form>;


        return (

            <div className="content-wrapper">
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0">Jadwal Dinas</h1>
                            </div>
                            {/* /.col */}

                        </div>
                        {/* /.row */}
                    </div>
                    {/* /.container-fluid */}
                </div>
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                {/* card start */}
                                <div className="card card-success shadow-lg" style={{"minHeight": "800px"}}>

                                    <div className="card-header card-header-content">
                                        {/* <AppButton
                                            isLoading={this.props.isLoading}
                                            theme="info"
                                            onClick={this.discardChanges}
                                            icon='add'> Add
                                        </AppButton> */}

                                        <div className="card-tools">
                                            <label style={{color: "#000000"}}>Title Job : </label>
                                            <span title="Dokter" className="badge" style={{
                                                backgroundColor: "#FFA500",
                                                color: "#000000",
                                                marginLeft: "15px"
                                            }}>Dokter</span>
                                            <span title="Perawat" className="badge" style={{
                                                backgroundColor: "#008000",
                                                color: "#FFFFFF",
                                                marginLeft: "15px"
                                            }}>Perawat</span>
                                            <span title="Staf RM" className="badge" style={{
                                                backgroundColor: "#ff0000",
                                                color: "#FFFFFF",
                                                marginLeft: "15px"
                                            }}>Staf RM</span>
                                            <span title="Tim Medis" className="badge" style={{
                                                backgroundColor: "#FFC0CB",
                                                color: "#000000",
                                                marginLeft: "15px"
                                            }}>Tim Medis</span>
                                            <span title="Staf Gudang" className="badge" style={{
                                                backgroundColor: "#dc3545",
                                                color: "#FFFFFF",
                                                marginRight: "15px",
                                                marginLeft: "15px"
                                            }}>Staf Gudang</span>
                                            {[1, 7].includes(parseInt(user.id_level)) ?
                                                <Form.Group controlId="id_kliniks">

                                                    <Form.Control
                                                        style={{width: "95%"}}
                                                        size="sm"
                                                        name="klinik_id"
                                                        as="select"
                                                        value={klinik_id ? klinik_id : ""}
                                                        onChange={this.handleFilterKlinik.bind(this)}>
                                                        <option value="">- Pilih Klinik -</option>
                                                        {dataKlinik ? (
                                                            dataKlinik.map(function (klinik) {
                                                                return <option
                                                                    selected={klinik.id === klinik_id ? true : false}
                                                                    value={klinik.id}
                                                                    key={klinik.id}>{klinik.name}
                                                                </option>
                                                            })

                                                        ) : ''}

                                                    </Form.Control>

                                                </Form.Group>
                                                : ''}

                                        </div>

                                    </div>

                                    <div className="card-body">
                                        <Calendar
                                            defaultDate={moment().toDate()}
                                            defaultView="month"
                                            localizer={localizer}
                                            events={data}
                                            startAccessor="start"
                                            endAccessor="end"
                                            di
                                            style={{height: 700}}
                                            onSelectEvent={parseInt(permission?.jadwal_dokter_edit) > 0 ? this.handleSelectEvent : ''}
                                            onSelectSlot={parseInt(permission?.jadwal_dokter_add) > 0 ? this.handleSelectSlot : ''}
                                            selectable
                                            popup
                                            onNavigate={this.handleNavigate}
                                            eventPropGetter={(this.eventStyleGetter)}
                                            views={
                                                {
                                                    month: true,
                                                    week: true,
                                                    day: false,
                                                    agenda: false
                                                }
                                            }
                                        />

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <AppModal
                    show={this.props.showFormAdd}
                    form={frmUser}
                    size="xs"
                    backdrop={false}
                    keyboard={false}
                    title="Add/Edit Jadwal Petugas"
                    titleButton="Save change"
                    themeButton="success"
                    handleClose={this.handleClose}
                    isLoading={this.props.isAddLoading ? this.props.isAddLoading : this.state.loadingForm}
                    formSubmit={this.handleSubmit.bind(this)}
                ></AppModal>
                {this.props.showFormSuccess ? (<AppSwalSuccess
                    show={this.props.showFormSuccess}
                    title={<div dangerouslySetInnerHTML={{__html: this.props.contentMsg}}/>}
                    type={this.props.tipeSWAL}
                    handleClose={this.handleCloseSwal}
                >
                </AppSwalSuccess>) : ''}
            </div>


        )
    }
}

const mapStateToProps = (state) => ({
    data: state.jadwalDokter.data || [],
    dataDokter: state.usersAdm.data || [],
    dataShift: state.treatment.dataShift || [],
    dataKlinik: state.klinik.data || [],
    totalData: state.jadwalDokter.totalData,
    isError: state.jadwalDokter.isError,
    isLoading: state.jadwalDokter.isLoading,
    showFormAdd: state.jadwalDokter.showFormAdd,
    isAddLoading: state.jadwalDokter.isAddLoading,
    errorPriority: state.jadwalDokter.errorPriority || null,
    contentMsg: state.jadwalDokter.contentMsg || null,
    showFormSuccess: state.jadwalDokter.showFormSuccess,
    showFormDelete: state.jadwalDokter.showFormDelete,
    tipeSWAL: state.jadwalDokter.tipeSWAL,
    user: state.main.currentUser,
});
const mapDispatchToPros = (dispatch) => {
    return {
        onLoad: (param) => {
            dispatch(fetchData(param));
            dispatch(fetchDataDokter({id_levels: 3}));
            dispatch(fetchDataShift(param));
            dispatch(fetchDataKlinik());
        },
        onFilterKlinik: (param) => {
            dispatch(fetchData(param));
            dispatch(fetchDataShift(param));
        },
        handleKlinik: (param) => {
            dispatch(fetchDataShift(param));
           
        },
        onAdd: (param) => {
            dispatch(addData(param));
        },
        showConfirmDel: (data) => {
            dispatch(confirmDel(data));
        },
        showForm: () => {
            dispatch(addForm());
        },
        closeModal: () => {
            dispatch(closeForm());

        },
        closeSwal: (param) => {
            dispatch(closeForm());
            dispatch(fetchData(param));
        },

    }
}
export default connect(mapStateToProps, mapDispatchToPros)(JadwalDokter);