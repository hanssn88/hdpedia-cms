import axios from "axios";


const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

class KlinikService {
    postData(param, action) {
        console.log(action);
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        switch (action) {
            case "GET_IMAGE":
                return axios.post(API_URL + "/detail_klinik", param, {
                    headers: {
                        'Authorization': 'Bearer ' + token
                    }
                });
            case "UPLOAD_IMAGE":
                return axios.post(API_URL + "/upload_banner_klinik", param, {
                    headers: {
                        'Authorization': 'Bearer ' + token
                    }
                });
            case "DEL_IMAGE":
                return axios.post(API_URL + "/delete_banner_klinik", param, {
                    headers: {
                        'Authorization': 'Bearer ' + token
                    }
                });
            default:
                return axios.post(API_URL + "/klinik", param, {
                    headers: {
                        'Authorization': 'Bearer ' + token
                    }
                });
        }
    }
}

export default new KlinikService()