import ReactDatatable from '@ashvin27/react-datatable';
import React, { Component, Fragment } from 'react';
import { Col, Figure, Form } from "react-bootstrap";
import { connect } from 'react-redux';
import AppButton from '../../components/button/Button';
import AppModal from '../../components/modal/MyModal';
import { AppSwalSuccess } from '../../components/modal/SwalSuccess';
import { fetchData as fetchDataDokter } from '../Users/usersSlice';
import { activeOrDeactive, addData, addForm, clearError, closeForm, confirmDel, confrimActive, deleteData, fetchData } from './klinikSlice';

class Klinik extends Component {

    constructor(props) {
        super(props);
        this.initSelected = {
            id: '',
            name: '',
            unit_number: '',
            number_of_bed: '',
            jumlah_shift: '',
            wa1: '',
            wa2: '',
            latitude: '',
            longitude: '',
            dokter_id: '',
            description: '',
            operator_by: '',
            is_active : '',
            logo_image: '',
            flag: false,
        }
        this.state = {
            sort_order: "ASC",
            sort_column: "name",
            keyword: '',
            page_number: 1,
            per_page: 10,
            selected: this.initSelected,
            errMsg: this.initSelected,
            loadingForm: false,
        }
    }

    componentDidMount() {
        this.fetchProfileAdmin();
        this.props.onLoad(this.state);
    }

    fetchProfileAdmin = async () => {
        const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;
        const token = localStorage.getItem(tokenLogin);
        if (!token) {
            alert('session invalid');
            window.location.reload();
        }
    }
    handleClose = () => {
        this.props.closeModal();
    };

    tableChangeHandler = (data) => {
        let queryString = this.state;
        Object.keys(data).map((key) => {
            if (key === "sort_order" && data[key]) {
                queryString.sort_order = data[key].order;
                queryString.sort_column = data[key].column;
            }
            if (key === "page_number") {
                queryString.page_number = data[key];
            }
            if (key === "page_size") {
                queryString.per_page = data[key];
            }
            if (key === "filter_value") {
                queryString.keyword = data[key];
            }
            return true;
        });
        this.props.onLoad(this.state);
    }

    handleChange(event) {
        const {name, value} = event.target
        var val = value;
        this.props.resetError();


        if (event.target.name === "logo_image") {
            val = event.target.files[0];
            this.setState({selected: {...this.state.selected, imgUpload: "", logo_image: ""}});
            if (!val) return;
            if (!val.name.match(/\.(jpg|jpeg|png)$/)) {
                this.setState({
                    loadingForm: true,
                    errMsg: {...this.state.errMsg, logo_image: "Please select valid image(.jpg .jpeg .png)"}
                });

                //setLoading(true);
                return;
            }
            if (val.size > 2099200) {
                this.setState({loadingForm: true, errMsg: {...this.state.errMsg, logo_image: "File size over 2MB"}});

                //setLoading(true);
                return;
            }
            let reader = new FileReader();
            reader.readAsDataURL(val);
            reader.onloadend = () => {
                this.setState({
                    loadingForm: false,
                    selected: {...this.state.selected, imgUpload: reader.result, logo_image: val}
                });
            };
        }

        this.setState({
            selected: {
                ...this.state.selected,
                [name]: val
            },
            errMsg: {
                ...this.state.errMsg,
                [name]: val ? '' : this.state.errMsg[name]
            },
            loadingForm: false
        });
        if (!this.state.selected.operator_by) this.setState({
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
    }

    discardChanges = () => {
        this.setState({errMsg: {}, selected: this.initSelected, loadingForm: false});
        if (!this.state.selected.operator_by) this.setState({
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
        this.props.showForm();
        this.state.flag = false;
    }

    editRecord = async (record) => {
        this.setState({
            loadingForm: false,
            errMsg: this.initSelected,
            selected: {...record, operator_by: this.props.user.id_operator, imgUpload: record.logo_image}
        });
        this.props.showForm(true);
        this.state.flag = true;
    }

    deleteRecord = (record) => {
        this.setState({
            selected: {...record, operator_by: this.props.user.id_operator}
        });
        this.props.showConfirmDel(true);
    }

    activeRecord = (record) => {
        var active_or_deactive = "" ;
        if(record.is_active == "1"){
            active_or_deactive = "0";
        }else{
            active_or_deactive = "1";
        }
        this.setState({
            selected: {...record, operator_by: this.props.user.id_operator, is_active: active_or_deactive  }
        });
        this.props.showConfirActive(true);
    }

    handleSubmit() {
        var errors = this.state.errMsg;
        this.setState({
            ...this.state,
            loadingForm: true,
        });
        const unitNumber = this.state.selected.unit_number.length;

        errors.name = !this.state.selected.name ? "Required" : '';

        // errors.unit_number = !this.state.selected.unit_number || Number(this.state.selected.unit_number) === 0 ? "Required" : '';
        errors.unit_number = !unitNumber || unitNumber != 2 ? "Unit Number wajib 2 Digit" : '';
        // errors.dokter_id = !this.state.selected.dokter_id ? "Required" : '';
        errors.number_of_bed = !this.state.selected.number_of_bed || Number(this.state.selected.number_of_bed) === 0 ? "Required" : '';
        errors.number_of_bed = !this.state.selected.number_of_bed || Number(this.state.selected.number_of_bed) < 0 ? "Minimal 1" : errors.number_of_bed;
        errors.jumlah_shift = !this.state.selected.jumlah_shift || Number(this.state.selected.jumlah_shift) === 0 ? "Required" : '';
        errors.wa1 = !this.state.selected.wa1 ? "Required" : '';
        errors.wa2 = !this.state.selected.wa2 ? "Required" : '';
        if (this.state.selected.logo_image) {
            var fileSize = this.state.selected.logo_image.size;
            if (fileSize > 2099200) { // satuan bytes 2099200 => 2MB
                errors.logo_image = "File size over 2MB";
            }
        }
        if (!this.state.selected.operator_by) this.setState({
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
        this.setState({errors});

        if (this.validateForm(this.state.errMsg)) {
            this.props.onAdd(this.state.selected);
        } else {
            console.error('Invalid Form')
        }

        this.setState({
            ...this.state,
            loadingForm: false,
        });
    }

    validateForm(errors) {
        let valid = true;
        Object.values(errors).forEach(
            (val) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    handleDelete() {
        this.props.onDelete(this.state.selected)
    }

    
    handleActive() {
        var param = {
            "id" : this.state.selected.id,
            "is_active" : this.state.selected.is_active
        }
        this.props.onActive(param)
    }

    listIMG = async (record) => {
        if (record) await sessionStorage.setItem('idKlinikHDPEDIA', record.id);
        this.props.history.push("list_img_klinik");
    }

    render() {
        const {data, dataDokter, user} = this.props;
        const {selected, errMsg} = this.state;
        const permission = user?.permission;

        const columns = [
            {
                key: "no",
                text: "No.",
                width: 20,
                align: "center",
                sortable: false,
                cell: (row, index) => <div
                    style={{textAlign: "center"}}>{((this.state.page_number - 1) * this.state.per_page) + index + 1 + '.'}</div>,
                row: 0
            },
            {
                key: "name",
                text: "Name",
                align: "center",
                sortable: true,
            },
            {
                key: "number_of_bed",
                text: "Jumlah Bed",
                align: "center",
                sortable: true,
            },
            {
                key: "jumlah_shift",
                text: "Jumlah Shift",
                align: "center",
                sortable: true,
            },
            {
                key: "contact",
                text: "Contact",
                align: "center",
                sortable: false,
                cell: record => {
                    return (
                        record.wa1 + ', ' + record.wa2
                    )
                },
            },

            {
                key: "kepala_klinik",
                text: "Kepala Klinik",
                align: "center",
                sortable: true,
            },
            {
                key: "address",
                text: "Alamat",
                align: "center",
                sortable: false,
            },
            {
                key: "koordinat",
                text: "Koordinat",
                align: "center",
                sortable: true,
                cell: record => {
                    return (
                        record.latitude + ', ' + record.longitude
                    )
                },
            },
            {
                key: "action",
                text: "Action",
                width: 200,
                align: "center",
                sortable: false,
                cell: record => {

                    return (
                        <div style={{textAlign: "center"}}>
                            <Fragment>
                                <button
                                    className="btn btn-info btn-xs"
                                    onClick={() => this.listIMG(record)}
                                    style={{marginRight: '5px'}}>
                                    <i className="fa fa-list"></i> Image
                                </button>
                                <button
                                    disabled={parseInt(permission?.klinik_edit) > 0 ? false : true}
                                    className="btn btn-xs btn-success"
                                    onClick={e => this.editRecord(record)}
                                    style={{marginRight: '5px'}}>
                                    <i className="fa fa-edit"></i> Edit
                                </button>
                                <button
                                    disabled={parseInt(permission?.klinik_delete) > 0 ? false : true}
                                    className= {record.is_active == 0 ? "btn btn-success btn-xs" : "btn btn-danger btn-xs"}
                                    onClick={() => this.activeRecord(record)}>
                                        {record.is_active == 0 ? "Active" : "Non Active"}
                                </button>
                            </Fragment>
                        </div>
                    );
                }
            }
        ];
        const config = {
            key_column: 'id',
            page_size: 10,
            length_menu: [10, 20, 50],
            show_filter: true,
            show_pagination: true,
            pagination: 'advance',
            button: {
                excel: false,
                print: false
            },
            language: {
                loading_text: "Please be patient while data loads..."
            }
        }
        const frmUser = <Form id="myForm">
            <Form.Group controlId="name">
                <Form.Label>Nama Klinik</Form.Label>

                {errMsg.name ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.name}
                    </span>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="name"
                    type="text"
                    value={selected.name}
                    onChange={this.handleChange.bind(this)}
                    placeholder="Nama Klinik"/>
            </Form.Group>
            <Form.Row>
                <Form.Group as={Col} controlId="unit_number">
                    <Form.Label>Unit Number</Form.Label>

                    {errMsg.unit_number ?
                        (<span className="float-right text-error badge badge-danger">{errMsg.unit_number}
                    </span>) : ''}
                    <Form.Control
                        size="sm"
                        autoComplete="off"
                        name="unit_number"
                        type="number"
                        disabled={this.state.flag}
                        value={selected.unit_number}
                        onChange={this.handleChange.bind(this)}
                        placeholder="Unit Number"/>
                </Form.Group>


                <Form.Group as={Col} controlId="dokter_id">
                    <Form.Label>Kepala Klinik</Form.Label>
                    {/* {errMsg.dokter_id ?
                        (<span className="float-right text-error badge badge-danger">{errMsg.dokter_id}
                    </span>) : ''} */}
                    <Form.Control
                        size="sm"
                        name="dokter_id"
                        as="select"
                        value={selected.dokter_id ? selected.dokter_id : ""}
                        onChange={this.handleChange.bind(this)}>
                        <option value="">- Pilih -</option>
                        {dataDokter ? (
                            dataDokter.map(function (dokter) {
                                return <option
                                    selected={dokter.id_operator === selected.dokter_id ? true : false}
                                    value={dokter.id_operator}
                                    key={dokter.id_operator}>{dokter.name}
                                </option>
                            })

                        ) : ''}

                    </Form.Control>
                </Form.Group>
            </Form.Row>

            <Form.Row>
                <Form.Group as={Col} controlId="number_of_bed">
                    <Form.Label>Jumlah Bed</Form.Label>

                    {errMsg.number_of_bed ?
                        (<span className="float-right text-error badge badge-danger">{errMsg.number_of_bed}
                        </span>) : ''}
                    <Form.Control
                        size="sm"
                        autoComplete="off"
                        name="number_of_bed"
                        type="number"
                        value={selected.number_of_bed}
                        onChange={this.handleChange.bind(this)}
                        placeholder="Jumlah Bed"/>
                </Form.Group>
                <Form.Group as={Col} controlId="jumlah_shift">
                    <Form.Label>Jumlah Shift</Form.Label>
                    {errMsg.jumlah_shift ?
                        (<span className="float-right text-error badge badge-danger">{errMsg.jumlah_shift}
                        </span>) : ''}
                    <Form.Control
                        size="sm"
                        autoComplete="off"
                        name="jumlah_shift"
                        as="select"
                        value={selected.jumlah_shift}
                        onChange={this.handleChange.bind(this)}
                    >
                        <option value="">- Pilih -</option>
                        <option value="1" selected={selected.jumlah_shift === "1" ? true : false}>1</option>
                        <option value="2" selected={selected.jumlah_shift === "2" ? true : false}>2</option>
                        <option value="3" selected={selected.jumlah_shift === "3" ? true : false}>3</option>
                    </Form.Control>
                </Form.Group>
            </Form.Row>

            <Form.Row>
                <Form.Group as={Col} controlId="wa1">
                    <Form.Label>WhatsApp Admin</Form.Label>
                    {errMsg.wa1 ?
                        (<span className="float-right text-error badge badge-danger">{errMsg.wa1}
                        </span>) : ''}
                    <Form.Control
                        size="sm"
                        autoComplete="off"
                        name="wa1"
                        type="number"
                        value={selected.wa1}
                        onChange={this.handleChange.bind(this)}
                        placeholder="WhatsApp Admin"/>
                </Form.Group>
                <Form.Group as={Col} controlId="jumlah_shift">
                    <Form.Label>WhatsApp Dokter</Form.Label>
                    {errMsg.wa2 ?
                        (<span className="float-right text-error badge badge-danger">{errMsg.wa2}
                        </span>) : ''}
                    <Form.Control
                        size="sm"
                        autoComplete="off"
                        name="wa2"
                        type="number"
                        value={selected.wa2}
                        onChange={this.handleChange.bind(this)}
                        placeholder="WhatsApp Dokter"/>
                </Form.Group>
            </Form.Row>
            <Form.Row>
                <Form.Group as={Col} controlId="latitude">
                    <Form.Label>Latitude</Form.Label>
                    {errMsg.latitude ?
                        (<span className="float-right text-error badge badge-danger">{errMsg.latitude}
                        </span>) : ''}
                    <Form.Control
                        size="sm"
                        autoComplete="off"
                        name="latitude"
                        type="text"
                        value={selected.latitude}
                        onChange={this.handleChange.bind(this)}
                        placeholder="Latitude"/>
                </Form.Group>
                <Form.Group as={Col} controlId="longitude">
                    <Form.Label>Longitude</Form.Label>
                    {errMsg.longitude ?
                        (<span className="float-right text-error badge badge-danger">{errMsg.longitude}
                        </span>) : ''}
                    <Form.Control
                        size="sm"
                        autoComplete="off"
                        name="longitude"
                        type="text"
                        value={selected.longitude}
                        onChange={this.handleChange.bind(this)}
                        placeholder="Longitude"/>
                </Form.Group>
            </Form.Row>
            <Form.Group controlId="address">
                <Form.Label>Alamat</Form.Label>

                {errMsg.address ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.address}
                    </span>) : ''}
                <Form.Control
                    style={{"overflow-y": "scroll"}}
                    as="textarea" rows={4}
                    size="sm"
                    autoComplete="off"
                    name="address"
                    type="text"
                    value={selected.address}
                    onChange={this.handleChange.bind(this)}
                    placeholder="Alamat"/>
            </Form.Group>
            <Form.Group controlId="description">
                <Form.Label>Deskripsi</Form.Label>

                {errMsg.description ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.description}
                    </span>) : ''}
                <Form.Control
                    style={{"overflow-y": "scroll"}}
                    as="textarea" rows={4}
                    size="sm"
                    autoComplete="off"
                    name="description"
                    type="text"
                    value={selected.description}
                    onChange={this.handleChange.bind(this)}
                    placeholder="Deskripsi"/>
            </Form.Group>

            <Form.Group controlId="logo_image">
                <Form.Label>Logo</Form.Label>{errMsg.img ?
                (<span className="float-right text-error badge badge-danger">{errMsg.logo_image}</span>) : null}
                <Form.File size="sm" name="logo_image" setfieldvalue={selected.logo_image}
                           onChange={this.handleChange.bind(this)}/>
            </Form.Group>
            {selected.imgUpload ? (<Form.Group controlId="imagePreview" style={{marginBottom: 0}}>
                <Figure>
                    <Figure.Image
                        thumbnail
                        width={130}
                        height={100}
                        alt=""
                        src={selected.imgUpload}
                    />
                </Figure>
            </Form.Group>) : ''}

        </Form>;

        const contentDelete = <div
            dangerouslySetInnerHTML={{__html: '<div id="caption" style="padding-bottom:20px;">Apakah anda yakin <br/>akan Active atau Non Active data ini ?</div>'}}/>;
        return (

            <div className="content-wrapper">
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0">Klinik</h1>
                            </div>
                            {/* /.col */}

                        </div>
                        {/* /.row */}
                    </div>
                    {/* /.container-fluid */}
                </div>
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                {/* card start */}
                                <div className="card card-success shadow-lg" style={{"minHeight": "800px"}}>
                                    <div className="card-header card-header-content">
                                        <AppButton
                                            disabled={parseInt(permission?.klinik_add) > 0 ? false : true}
                                            isLoading={this.props.isLoading}
                                            theme="info"
                                            onClick={this.discardChanges}
                                            icon='add'
                                        >
                                            Add
                                        </AppButton>

                                    </div>
                                    <div className="card-body">
                                        {data ? (
                                            <ReactDatatable
                                                config={config}
                                                records={data}
                                                columns={columns}
                                                dynamic={true}
                                                onChange={this.tableChangeHandler}
                                                loading={this.props.isLoading}
                                                total_record={this.props.totalData}
                                            />
                                        ) : (<p>No Data ...</p>)}

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <AppModal
                    show={this.props.showFormAdd}
                    form={frmUser}
                    size="xs"
                    backdrop={false}
                    keyboard={false}
                    title="Add/Edit Klinik"
                    titleButton="Save change"
                    themeButton="success"
                    handleClose={this.handleClose}
                    isLoading={this.props.isAddLoading ? this.props.isAddLoading : this.state.loadingForm}
                    formSubmit={this.handleSubmit.bind(this)}
                ></AppModal>
                <AppModal
                    show={this.props.showFormDelete}
                    size="xs"
                    form={contentDelete}
                    handleClose={this.handleClose}
                    backdrop="static"
                    keyboard={false}
                    title="Active/NonActive"
                    titleButton={this.state.selected.is_active == 1 ? "Active" : "Non Active"}
                    themeButton={this.state.selected.is_active == 1 ? "success" : "danger"}
                    isLoading={this.props.isAddLoading}
                    formSubmit={this.handleActive.bind(this)}
                ></AppModal>
                {this.props.showFormSuccess ? (<AppSwalSuccess
                    show={this.props.showFormSuccess}
                    title={<div dangerouslySetInnerHTML={{__html: this.props.contentMsg}}/>}
                    type={this.props.tipeSWAL}
                    handleClose={this.props.closeSwal}
                >
                </AppSwalSuccess>) : ''}
            </div>


        )
    }
}

const mapStateToProps = (state) => ({
    data: state.klinik.data || [],
    dataDokter: state.usersAdm.data || [],
    totalData: state.klinik.totalData,
    isError: state.klinik.isError,
    isLoading: state.klinik.isLoading,
    showFormAdd: state.klinik.showFormAdd,
    isAddLoading: state.klinik.isAddLoading,
    errorPriority: state.klinik.errorPriority || null,
    contentMsg: state.klinik.contentMsg || null,
    showFormSuccess: state.klinik.showFormSuccess,
    showFormDelete: state.klinik.showFormDelete,
    tipeSWAL: state.klinik.tipeSWAL,
    user: state.main.currentUser,
});
const mapDispatchToPros = (dispatch) => {
    return {
        onLoad: (param) => {
            dispatch(fetchData(param));
            dispatch(fetchDataDokter({id_levels: 3}));
        },
        onAdd: (param) => {
            dispatch(addData(param));
        },
        showConfirmDel: (data) => {
            dispatch(confirmDel(data));
        },
        showConfirActive: (data) => {
            dispatch(confrimActive(data));
        },
        showForm: () => {
            dispatch(addForm());
        },
        onDelete: (param) => {
            dispatch(deleteData(param));
        },
        onActive: (param) => {
            dispatch(activeOrDeactive(param));
        },
        closeModal: () => {
            dispatch(closeForm());

        },
        closeSwal: () => {
            dispatch(closeForm());
            const queryString = {
                sort_order: "ASC",
                sort_column: "name",
                per_page: 10
            }
            dispatch(fetchData(queryString));
        },
        resetError: () => {
            dispatch(clearError());
        },

        closeSwalError: () => {
            dispatch(closeForm());
        }
    }
}
export default connect(mapStateToProps, mapDispatchToPros)(Klinik);