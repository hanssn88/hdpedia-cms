import axios from 'axios';
import React, { Component } from 'react';
import { Col, Form } from 'react-bootstrap';
import ReactPaginate from 'react-paginate';
import { connect } from 'react-redux';
import AppButton from "../../components/button/Button";
import ReadMore from "../../components/button/ReadMore";
import { SelectProvMulti } from "../../components/modal/MySelect";
import { addData, addForm, clearError, closeForm, confirmDel, fetchData } from "./messageSlice";

class Message extends Component {
    constructor(props) {
        super(props);
        this.initValue = {
            is_blast: '',
            id_level_blast: '',
            subject: '',
            body: '',
        }
        this.state = {
            isLoading: false,
            appsLoading: false,
            loadArea: true,
            isLoadingSelected: true,
            errMsg: this.initValue,
            subject: '',
            body: '',
            selectOptions: [],
            showConfirm: false,
            multiValue: [],
            is_blast: "0",
            page_number: 1,
            per_page: 10,
            id_level_blast: "0"
        };

    }

    componentDidMount() {
        this.props.onLoad(this.state);
        this.getOptions();
    }

    async getOptions() {
        const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        this.setState({isLoadingSelected: true})
        const param = {is_wh: 1}
        const url = process.env.REACT_APP_URL_API + "/message/recipient"
        const res = await axios.post(url, param, {
            headers: {
                'Authorization': 'Bearer ' + token
            }
        });
        const err_code = res.data.err_code
        if (err_code === '00') {
            const data = res.data.data
            const options = data.map(d => ({
                "value": d.id,
                "label": d.name
            }))
            setTimeout(() => {
                this.setState({
                    ...this.state,
                    selectOptions: options,
                    isLoadingSelected: false,
                    loadArea: false,
                })
            }, 400);
        } else {
            setTimeout(() => {
                this.setState({
                    ...this.state,
                    selectOptions: null,
                    isLoadingSelected: true,
                    loadArea: true,
                })
            }, 400);

        }
    }

    handleMultiChange(option) {
        this.setState({
            loadArea: false,
            errMsg: {
                ...this.state.errMsg,
                member: ''
            }
        })
        this.setState(state => {
            return {
                multiValue: option
            };
        });
    }

    handleChange(evt) {
        const name = evt.target.name;
        var value = evt.target.value;
        if (name === 'is_blast' && value === '0') {
            this.setState({loadArea: true, multiValue: [], id_level_blast: "0"});
            this.getOptions();
        }
        if (name === 'is_blast' && value === '1') {
            this.setState({
                multiValue: [],
                loadArea: true,
                errMsg: {...this.state.errMsg, member: ''}
            })
        }


        this.setState({
            [name]: value,
            errMsg: {
                ...this.state.errMsg,
                [name]: ''
            }
        })

    }

    handleSubmit() {

        var errors = this.state.errMsg;
        this.setState({
            ...this.state,
            isLoading: true,
        });

        errors.body = !this.state.body ? "Pesan required" : '';
        errors.subject = !this.state.subject ? "Subject required" : '';
        errors.member = parseInt(this.state.is_blast) === 0 && this.state.multiValue.length === 0 ? "Recipient required" : '';
        errors.id_level_blast = parseInt(this.state.is_blast) > 0 && parseInt(this.state.id_level_blast) === 0 ? "Level required" : '';

        this.setState({errors});

        if (this.validateForm(this.state.errMsg)) {
            this.handleSave();
        } else {
            this.setState({
                ...this.state,
                isLoading: false,
            });
            console.error('Invalid Form')

            
        }

    }

     formatDate = (dateString) => {
        const date = new Date(dateString);
        const day = String(date.getDate()).padStart(2, '0');
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        const month = monthNames[date.getMonth()]; // Months are zero-based
        const year = date.getFullYear();
        return `${day} ${month} ${year}`;
    };


    handleSave() {
        let recipient_ids = [];
        Object.values(this.state.multiValue).forEach(
            (val) => recipient_ids.push(val.value)
        );
        const param = {
            is_blast: this.state.is_blast,
            id_level_blast: parseInt(this.state.is_blast) > 0 ? this.state.id_level_blast : 0,
            subject: this.state.subject,
            body: this.state.body,
            recipient_ids: parseInt(this.state.is_blast) === 0 ? recipient_ids : []
        }
        this.props.onAdd(param);
        this.setState({
            ...this.state,
            isLoading: false,
            appsLoading: false,
            loadArea: true,
            isLoadingSelected: true,
            errMsg: this.initValue,
            subject: '',
            body: '',
            selectOptions: [],
            showConfirm: false,
            multiValue: [],
            is_blast: "0",
            id_level_blast: "0"
            
        });
        this.props.onLoad(this.state);
        this.getOptions();
       
    }
     handlePageClick = (selectedPage) => {
        const selectedPages = selectedPage.selected; // Ini adalah zero-based index
       const page = selectedPages + 1;
    //    this.setState({
    //     selected: {
    //         ...this.state.selected,
    //         page_number: page,
    //         per_page : 10

    //     }
    // });
    let queryString = this.state;
    queryString.page_number = page;
    queryString.per_page = 10;

       this.props.onLoad(this.state);
    };

    validateForm(errors) {
        let valid = true;
        Object.values(errors).forEach(
            (val) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    render() {
        const {data, totalData} = this.props;
        const {errMsg} = this.state;

        return (
            <div>

                <div className="content-wrapper">
                    <div className="content-header">
                        <div className="container-fluid">
                            <div className="row mb-2">
                                <div className="col-sm-6">
                                    <h1 className="m-0">Message</h1>
                                </div>

                            </div>
                        </div>
                    </div>

                    <section className="content">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-lg-4">
                                    <div className="card shadow-lg" style={{"minHeight": "800px"}}>
                                        <div className="card-header card-header-custom">
                                            <h1 className="card-title card-title-custom">List Message</h1>

                                        </div>

                                        <div className="card-body inbox_chat">

                                            <ul className="todo-list ui-sortable" data-widget="todo-list">
                                                {data ? (
                                                    data.map((dt, i) => (
                                                        <li key={dt.id} className='li_wh'>
                                                            <span
                                                                className="text-second-li"><strong>Kepada : {dt.to_name}</strong></span><br/>
                                                                  <span
                                                                className="text-second-li"><strong>Tanggal : {this.formatDate(dt.created_at)}</strong></span><br/>
                                                            <span
                                                                className="text-second-li"><strong>Subject : {dt.subject}</strong></span><br/>
                                                             <span
                                                                className="text-second-li">  <ReadMore>{dt.body}</ReadMore></span>
                                                                

                                                        </li>
                                                    ))

                                                ) : ''}
                                            </ul>
                                                
                                            <ReactPaginate
                                                previousLabel="Previous"
                                                nextLabel="Next"
                                                pageClassName="page-item"
                                                pageLinkClassName="page-link"
                                                previousClassName="page-item"
                                                previousLinkClassName="page-link"
                                                nextClassName="page-item"
                                                nextLinkClassName="page-link"
                                                breakLabel="..."
                                                breakClassName="page-item"
                                                breakLinkClassName="page-link"
                                                pageCount={totalData / 10}
                                                marginPagesDisplayed={1}
                                                pageRangeDisplayed={0}
                                                onPageChange={this.handlePageClick}
                                                containerClassName="pagination"
                                                activeClassName="active"
                                                forcePage={0}
                                            />
                                        
                                        </div>

                                    </div>
                                </div>
                                <div className="col-lg-8">
                                    <div className="card shadow-lg" style={{"minHeight": "800px"}}>
                                        <div className="card-header card-header-custom">
                                            <h1 className="card-title card-title-custom">
                                                <span className="area" style={{
                                                    "fontWeight": "600",
                                                    "color": "green"
                                                }}> Form Message</span></h1>


                                        </div>

                                        <div className="card-body inbox_chat">
                                            <Form>
                                                <div className="card-body my-card-body">
                                                    <Form.Row>
                                                        <Form.Group as={Col} controlId="is_blast">
                                                            <Form.Label>Blast</Form.Label>

                                                            <Form.Control
                                                                name="is_blast"
                                                                size="sm"
                                                                as="select"
                                                                value={this.state.is_blast}
                                                                onChange={this.handleChange.bind(this)}>

                                                                <option value="1">Ya</option>
                                                                <option value="0">Tidak</option>

                                                            </Form.Control>

                                                        </Form.Group>
                                                        <Form.Group as={Col} controlId="id_level_blast">
                                                            <Form.Label>Level</Form.Label>
                                                            {errMsg.id_level_blast ?
                                                                (<span
                                                                    className="float-right text-error badge badge-danger">{errMsg.id_level_blast}
                                                                </span>) : ''}
                                                            <Form.Control
                                                                disabled={this.state.is_blast > 0 ? false : true}
                                                                name="id_level_blast"
                                                                size="sm"
                                                                as="select"
                                                                value={this.state.id_level_blast}
                                                                onChange={this.handleChange.bind(this)}>
                                                                <option value="0">Select ...</option>
                                                                <option value="3">Dokter</option>
                                                                <option value="4">Perawat</option>
                                                                <option value="8">Pasien</option>

                                                            </Form.Control>
                                                        </Form.Group>

                                                    </Form.Row>

                                                    <Form.Row>
                                                        <Form.Group as={Col} controlId="subject">
                                                            <Form.Label>Subject</Form.Label>
                                                            {errMsg.subject ?
                                                                (<span
                                                                    className="float-right text-error badge badge-danger">{errMsg.subject}
                                                                </span>) : ''}
                                                            <Form.Control
                                                                size="sm"
                                                                autoComplete="off"
                                                                name="subject"
                                                                type="text"
                                                                value={this.state.subject}
                                                                onChange={this.handleChange.bind(this)}
                                                                placeholder="Subject"/>
                                                        </Form.Group>
                                                        <Form.Group as={Col} controlId="recipient_ids">
                                                            <Form.Label>Recipient</Form.Label>
                                                            {errMsg.member ?
                                                                (<span
                                                                    className="float-right text-error badge badge-danger">{errMsg.member}
                                                                </span>) : ''}
                                                            <SelectProvMulti
                                                                myVal={this.state.multiValue && this.state.multiValue}
                                                                getData={this.state.selectOptions}
                                                                isLoading={this.state.loadArea}
                                                                onChange={this.handleMultiChange.bind(this)}
                                                            />
                                                        </Form.Group>


                                                    </Form.Row>

                                                    <Form.Row>
                                                        <Form.Group as={Col} controlId="body">
                                                            <Form.Label>Pesan</Form.Label>
                                                            {errMsg.body ?
                                                                (<span
                                                                    className="float-right text-error badge badge-danger">{errMsg.body}
                                                                </span>) : ''}
                                                            <Form.Control as="textarea" rows={21}
                                                                          name="body"
                                                                          value={this.state.body}
                                                                          onChange={this.handleChange.bind(this)}
                                                            />
                                                        </Form.Group>
                                                    </Form.Row>

                                                </div>

                                                <div className="card-footer">
                                                    <AppButton
                                                        isLoading={this.state.isLoading}
                                                        onClick={this.handleSubmit.bind(this)}
                                                        theme="success">
                                                        Kirim Pesan
                                                    </AppButton>
                                                </div>
                                            </Form>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>


                </div>
                <div>

                </div>

            </div>
        )
    }
}

const Pagination = ({ totalPages, paginate }) => {
    const pageNumbers = [];

    for (let i = 1; i <= totalPages; i++) {
        pageNumbers.push(i);
    }

    return (
        <nav>
            <ul className="pagination">
                {pageNumbers.map((number) => (
                    <li key={number} className="page-item">
                        <a onClick={() => paginate(number)} href="#!" className="page-link">
                            {number}
                        </a>
                    </li>
                ))}
            </ul>
        </nav>
    );
};

const mapStateToProps = (state) => ({
    data: state.message.data || [],
    totalData: state.message.totalData,
    isError: state.message.isError,
    isLoading: state.message.isLoading,
    showFormAdd: state.message.showFormAdd,
    isAddLoading: state.message.isAddLoading,
    errorPriority: state.message.errorPriority || null,
    contentMsg: state.message.contentMsg || null,
    showFormSuccess: state.message.showFormSuccess,
    showFormDelete: state.message.showFormDelete,
    tipeSWAL: state.message.tipeSWAL,
    user: state.main.currentUser,
});
const mapDispatchToPros = (dispatch) => {
    return {
        onLoad: (param) => {
            dispatch(fetchData(param));
        },
        onAdd: (param) => {
            dispatch(addData(param));
        },
        showConfirmDel: (data) => {
            dispatch(confirmDel(data));
        },
        showForm: () => {
            dispatch(addForm());
        },

        closeModal: () => {
            dispatch(closeForm());

        },
        closeSwal: () => {
            dispatch(closeForm());
            const queryString = {
                sort_order: "ASC",
                sort_column: "name",
                per_page: 10
            }
            dispatch(fetchData(queryString));
        },
        resetError: () => {
            dispatch(clearError());
        },

        closeSwalError: () => {
            dispatch(closeForm());
        }
    }
}
export default connect(mapStateToProps, mapDispatchToPros)(Message)
