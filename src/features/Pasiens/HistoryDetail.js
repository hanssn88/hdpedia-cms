import "moment/locale/id";
import React, {Component, Fragment} from 'react';
import {Form, Image} from "react-bootstrap";
import {connect} from 'react-redux';
import {Placeholder} from 'rsuite';
import noImg from '../../assets/noPhoto.jpg';
import PasienService from './PasienService';
import {closeForm, fetchDataPasien} from "./pasiensSlice";

class HistoryDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            appsLoading: true,
            showConfirmReject: false,
            showConfirmApprove: false,
            showConfirmRevisi: false,
            isLoading: false,
            showSwalSuccess: false,
            dtRes: {},
            patient_id: '',
            errMsg: null,
            targetId: sessionStorage.getItem('targetId'),
            type: sessionStorage.getItem('typeForm'),
        }
    }


    componentDidMount() {
        const patient_id = sessionStorage.getItem('idPasienHDPedia');
        this.setState({patient_id});
        this.props.onLoadDataPasien({patient_id});
        this.getData();
    }

    handleClose = () => {
        this.setState({
            ...this.state,
            showConfirm: false,
            errMsg: null,
            showConfirmReject: false,
            showConfirmApprove: false,
            showConfirmRevisi: false,

        });
    };

    closeSwal = async () => {
        this.setState({
            ...this.state,
            errMsg: null,
            showConfirmRevisi: false,
            showConfirmApprove: false,
            showConfirmReject: false,
            showSwalSuccess: false,
        });
        await this.props.closeSwal();
        await this.getData();
    }


    getData = async () => {
        this.setState({appsLoading: true});
        const targetId = sessionStorage.getItem('targetId');
        const type = sessionStorage.getItem('typeForm');
        const queryString = {target_id: targetId, type: type}
        await PasienService.postData(queryString, "VIEW_DETAIL_FORM")
            .then(response => {
                setTimeout(() => {
                    if (response.data.err_code === '00') {
                        this.setState({
                            ...this.state,
                            dtRes: response.data.data
                        });
                        console.log('Data :' + this.state.dtRes);
                    }
                    if (response.data.err_code === '04') {
                        this.setState({
                            ...this.state,
                            dtRes: {},
                        });
                    }
                    this.setState({appsLoading: false});
                }, 400);
            })
            .catch(e => {
                console.log(e);
                this.setState({appsLoading: false});
            });
    };


    render() {
        const {Paragraph} = Placeholder;
        const {
            dataPasien
        } = this.props;


        return (

            <>

                <div className="content-wrapper">
                    {/* Content Header (Page header) */}
                    <div className="content-header">
                        <div className="container-fluid">
                            <div className="row mb-2">
                                <div className="col-sm-6">
                                    <h1 className="m-0">History Form Detail</h1>
                                </div>

                            </div>
                        </div>
                    </div>

                    <section className="content">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-12">
                                    {this.state.appsLoading ? (
                                        <div className="card shadow-lg">
                                            <div className="card-body">
                                                <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                                           style={{marginTop: 30}}/>
                                            </div>
                                        </div>

                                    ) : (
                                        <div className="card shadow-lg">
                                            <div className="card-body">

                                                <table className="table table-condensed">

                                                    <tbody>

                                                    <Fragment>

                                                        <tr>
                                                            <td style={{
                                                                "backgroundColor": "rgba(0,0,0,.1)",
                                                                "fontWeight": "bold",
                                                                "fontSize": "16px"
                                                            }} colSpan="12" align="center">
                                                                {this.state.type}
                                                            </td>
                                                        </tr>

                                                        {this.state.type == 'Resep' ? (
                                                            <Fragment>

                                                                <tr>

                                                                    <td width="8%"><strong>Pasien</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="28%" colSpan="7">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.name ? this.state.dtRes.name : ''}
                                                                        />
                                                                    </td>

                                                                </tr>

                                                                <tr>
                                                                    <td width="8%">
                                                                        <strong>Umur</strong>
                                                                    </td>
                                                                    <td width="1%">:</td>
                                                                    <td width="28%" colSpan="7">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.age ? this.state.dtRes.age : ''}
                                                                        />
                                                                    </td>

                                                                </tr>

                                                                <tr>
                                                                    <td width="8%">
                                                                        <strong>No.RM</strong>
                                                                    </td>
                                                                    <td width="1%">:</td>
                                                                    <td width="28%" colSpan="7">

                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.rm_number ? this.state.dtRes.rm_number : ''}
                                                                        />
                                                                    </td>

                                                                </tr>

                                                                <tr>
                                                                    <td width="8%">
                                                                        <strong>Resep</strong>
                                                                    </td>
                                                                    <td width="1%">:</td>
                                                                    <td width="28%" colSpan="7">
                                                                        <Form.Control
                                                                            style={{"overflow-y": "scroll"}}
                                                                            as="textarea" rows={3}
                                                                            size="sm"
                                                                            id="resep"
                                                                            name="resep"
                                                                            autoComplete="off"
                                                                            type="text"
                                                                            disabled="true"
                                                                            placeholder="Resep"
                                                                            value={this.state.dtRes.resep ? this.state.dtRes.resep : ''}
                                                                        />
                                                                    </td>

                                                                </tr>

                                                            </Fragment>
                                                        ) : this.state.type == 'Reuse Dialiser' ? (

                                                            <Fragment>

                                                                <tr>

                                                                    <td width="8%"><strong>Pasien</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="28%" colSpan="7">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.name ? this.state.dtRes.name : ''}
                                                                        />
                                                                    </td>

                                                                </tr>

                                                                <tr>
                                                                    <td width="5%"><strong>Sunxin 150L</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="sunxin"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.sunxin ? this.state.dtRes.sunxin : ''}
                                                                        />
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td width="5%"><strong>Re-use Ke</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="reuse_ke"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.reuse_ke ? this.state.dtRes.reuse_ke : ''}
                                                                            placeholder="Re-use Ke"/>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td width="5%"><strong>Volume</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="volume"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.volume ? this.state.dtRes.volume : ''}
                                                                            placeholder="Volume"/>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td width="5%"><strong>Kondisi</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="kkondisi_dialiserond"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.kondisi_dialiser ? this.state.dtRes.kondisi_dialiser : ''}
                                                                            placeholder="Kondisi"/>
                                                                    </td>
                                                                </tr>

                                                            </Fragment>


                                                        ) : this.state.type == 'Surat Rujukan' ? (
                                                            <Fragment>

                                                                <tr>
                                                                    <td colSpan="12">
                                                                        <h6>Kepada Yth</h6></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="10%"><strong>T.S Dokter</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="ts_dokter"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.ts_dokter ? this.state.dtRes.ts_dokter : ''}
                                                                            type="text"
                                                                            placeholder="T.S Dokter"/>

                                                                    </td>


                                                                </tr>
                                                                <tr>
                                                                    <td width="5%"><strong>Tanggal</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="tanggal"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.date ? this.state.dtRes.date : ''}
                                                                            type="text"
                                                                            placeholder="tanggal"/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colSpan="12"><strong>Ditempat Dengan hormat
                                                                        Mohon pemeriksaan dan penatalaksanaan lebih
                                                                        lanjut pasien :</strong></td>
                                                                    <br/>

                                                                </tr>

                                                                <tr>
                                                                    <td width="5%"><strong>Pasien</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.name ? this.state.dtRes.name : ''}/>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td width="5%"><strong>Jenis Kelamin</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">

                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.gender ? this.state.dtRes.gender : ''}/>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td width="5%"><strong>Klinis</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">
                                                                        <Form.Control
                                                                            style={{"overflow-y": "scroll"}}
                                                                            as="textarea" rows={3}
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="klinis"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.klinis ? this.state.dtRes.klinis : ''}
                                                                            type="text"
                                                                            placeholder="Klinis"/>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td width="5%"><strong>Diagnosa</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">
                                                                        <Form.Control
                                                                            style={{"overflow-y": "scroll"}}
                                                                            as="textarea" rows={3}
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="diagnosa"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.diagnosa ? this.state.dtRes.diagnosa : ''}
                                                                            type="text"
                                                                            placeholder="Diagnosa"/>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td width="5%"><strong>Pemeriksaan
                                                                        Penunjang</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">
                                                                        <Form.Control
                                                                            style={{"overflow-y": "scroll"}}
                                                                            as="textarea" rows={3}
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="pemeriksaan_penunjang"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.pemeriksaan_penunjang ? this.state.dtRes.pemeriksaan_penunjang : ''}
                                                                            type="text"
                                                                            placeholder="Pemeriksaan Penunjang"/>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td width="5%"><strong>Terapi yang sudah
                                                                        diberikan</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">
                                                                        <Form.Control
                                                                            style={{"overflow-y": "scroll"}}
                                                                            as="textarea" rows={3}
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="terapi"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.terapi ? this.state.dtRes.terapi : ''}
                                                                            type="text"
                                                                            placeholder="Terapi yang sudah diberikan"/>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td width="5%"><strong>Memerlukan</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">
                                                                        <Form.Control
                                                                            style={{"overflow-y": "scroll"}}
                                                                            as="textarea" rows={3}
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="memerlukan"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.memerlukan ? this.state.dtRes.memerlukan : ''}
                                                                            type="text"
                                                                            placeholder="Memerlukan"/>
                                                                    </td>
                                                                </tr>


                                                                <tr>
                                                                    <td width="5%"><strong>Signature</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">

                                                                        <Image
                                                                            src={this.state.dtRes.image_salam_sejawat ? this.state.dtRes.image_salam_sejawat : noImg}
                                                                            thumbnail

                                                                        />

                                                                    </td>


                                                                </tr>

                                                            </Fragment>
                                                        ) : this.state.type == 'Surat Keterangan Sakit' ? (
                                                            <Fragment>
                                                                <tr>
                                                                    <td style={{

                                                                        "fontWeight": "bold",
                                                                        "fontSize": "16px"
                                                                    }} colSpan="12" align="center">
                                                                        <h6>Yang bertanda tangan dibawah ini menerangkan
                                                                            bahwa:</h6></td>
                                                                </tr>

                                                                <tr>
                                                                    <td><strong>Pasien</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="no_rm"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.name ? this.state.dtRes.name : ''}
                                                                        />
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td width="5%"><strong>Umur</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">

                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.age ? this.state.dtRes.age : ''}
                                                                        />
                                                                    </td>

                                                                </tr>

                                                                <tr>
                                                                    <td width="5%"><strong>Pekerjaan</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">

                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.job ? this.state.dtRes.job : ''}/>
                                                                    </td>

                                                                </tr>

                                                                <tr>
                                                                    <td width="8%">
                                                                        <strong>Alamat</strong>
                                                                    </td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">
                                                                        <Form.Control
                                                                            style={{"overflow-y": "scroll"}}
                                                                            as="textarea" rows={3}
                                                                            size="sm"
                                                                            disabled="true"
                                                                            autoComplete="off"

                                                                            value={this.state.dtRes.address ? this.state.dtRes.address : ''}
                                                                            type="text"
                                                                            placeholder=""/>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td style={{

                                                                        "fontWeight": "bold",
                                                                        "fontSize": "16px"
                                                                    }} colSpan="12">
                                                                        <h6>Perlu beristirahat karena sakit selama </h6>
                                                                    </td>
                                                                </tr>


                                                                <tr>

                                                                    <td width="5%"><strong>Hari</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="sakit_hari"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.sakit_hari ? this.state.dtRes.sakit_hari : ''}
                                                                            type="text"
                                                                            placeholder="hari"/>
                                                                    </td>


                                                                </tr>

                                                                <tr>
                                                                    <td style={{

                                                                        "fontWeight": "bold",
                                                                        "fontSize": "16px"
                                                                    }} colSpan="12" align="center">
                                                                        <h6>hari terhitung mulai tanggal </h6>
                                                                    </td>
                                                                </tr>

                                                                <tr>

                                                                    <td width="5%"><strong>Tanggal</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="28%" colSpan="5">

                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="tanggal_mulai"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.tanggal_start ? this.state.dtRes.tanggal_start : ''}
                                                                            type="text"
                                                                            placeholder="Tanggal Mulai"/>
                                                                    </td>

                                                                    <td style={{

                                                                        "fontWeight": "bold",
                                                                        "fontSize": "16px"
                                                                    }} colSpan="2" align="center"><strong>-</strong>
                                                                    </td>

                                                                    <td width="5%"><strong>Tanggal</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="28%" colSpan="5">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="tanggal_end"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.tanggal_start ? this.state.dtRes.tanggal_start : ''}
                                                                            type="text"
                                                                            placeholder="Tanggal End"/>

                                                                    </td>


                                                                </tr>

                                                                <tr>
                                                                    <td style={{

                                                                        "fontWeight": "bold",
                                                                        "fontSize": "16px"
                                                                    }} colSpan="12">
                                                                        <h6>Harap yang berkepentingan maklum, Terima
                                                                            Kasih.</h6>
                                                                    </td>
                                                                </tr>

                                                                <tr>

                                                                    <td width="5%"><strong>Tanggal</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">

                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="tanggal_ttd"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.tanggal_ttd ? this.state.dtRes.tanggal_ttd : ''}
                                                                            type="text"
                                                                            placeholder="Tanggal"/>


                                                                    </td>


                                                                </tr>

                                                                <tr>
                                                                    <td width="5%"><strong>Signature</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">
                                                                        <Image
                                                                            src={this.state.dtRes.image_dokter_pemeriksa ? this.state.dtRes.image_dokter_pemeriksa : noImg}
                                                                            thumbnail
                                                                        />
                                                                    </td>
                                                                </tr>

                                                            </Fragment>
                                                        ) : this.state.type == 'Surat Keterangan Kematian' ? (
                                                            <Fragment>

                                                                <tr>
                                                                    <td colSpan="12">
                                                                        <h6>Saya yang bertanda tangan di bawah ini</h6>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="10%"><strong>Nama</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="14%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="nama"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.nama ? this.state.dtRes.nama : ''}
                                                                            type="text"
                                                                            placeholder="Nama"/>
                                                                    </td>
                                                                    <td width="5%"><strong>Jabatan</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="jabatan"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.jabatan ? this.state.dtRes.jabatan : ''}
                                                                            type="text"
                                                                            placeholder="Jabatan"/>
                                                                    </td>


                                                                </tr>

                                                                <tr>
                                                                    <td colSpan="12">
                                                                        <h6>Menyatakan bahwa pasien sebagai berikut
                                                                            telah meninggal dunia :</h6>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td width="10%"><strong>Nama Lengkap</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="14%">

                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="nama_lengkap"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.nama_lengkap ? this.state.dtRes.nama_lengkap : ''}
                                                                        />

                                                                    </td>
                                                                    <td width="5%"><strong>No. Rekam Medis</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">

                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="rm_number"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.rm_number ? this.state.dtRes.rm_number : ''}
                                                                        />
                                                                    </td>


                                                                </tr>

                                                                <tr>
                                                                    <td width="10%"><strong>Jenis Kelamin</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="14%">

                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="jenis_kelamin"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.jenis_kelamin ? this.state.dtRes.jenis_kelamin : ''}
                                                                        />

                                                                    </td>
                                                                    <td width="5%"><strong>Tempat Tanggal Lahir</strong>
                                                                    </td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="ttl"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={dataPasien.ttl}/>

                                                                    </td>


                                                                </tr>

                                                                <tr>
                                                                    <td width="10%"><strong>Agama</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="14%">

                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="agama"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={dataPasien.religion === 1 ? "Islam" : dataPasien.religion === 2 ? "Kristen" : dataPasien.religion === 3 ? "Katholik" : dataPasien.religion === 4 ? "Hindu" : dataPasien.religion === 5 ? "Buddha" : dataPasien.religion === 6 ? "Konghucu" : ""}
                                                                        />
                                                                    </td>
                                                                    <td width="5%"><strong>Alamat</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">

                                                                        <Form.Control
                                                                            style={{"overflow-y": "scroll"}}
                                                                            as="textarea" rows={3}
                                                                            size="sm"
                                                                            disabled="true"
                                                                            name="alamat"
                                                                            autoComplete="off"
                                                                            value={dataPasien.address ? dataPasien.address : ''}
                                                                            type="text"
                                                                            placeholder=""

                                                                        />
                                                                    </td>


                                                                </tr>

                                                                <tr>
                                                                    <td width="10%"><strong>Nomor Identitas</strong>
                                                                    </td>
                                                                    <td width="1%">:</td>
                                                                    <td>

                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="identitas"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.no_ktp != null ? this.state.dtRes.no_ktp : this.state.dtRes != null ? this.state.dtRes.no_passport : this.state.dtRes.no_sim != null ? this.state.dtRes.no_sim : ''}
                                                                        />
                                                                    </td>

                                                                    <td width="10%"><strong>Tanggal Meninggal</strong>
                                                                    </td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="tgl_meninggal"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.tanggal_meninggal ? this.state.dtRes.tanggal_meninggal : ''}/>


                                                                    </td>


                                                                </tr>

                                                                <tr>

                                                                    <td width="5%"><strong>Jam Meninggal</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">

                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="jam_meninggal"
                                                                            type="text"
                                                                            disabled="true"
                                                                            placeholder="Jam Meninggal"
                                                                            value={this.state.dtRes.jam_meninggal ? this.state.dtRes.jam_meninggal : ''}/>
                                                                    </td>

                                                                    <td width="10%"><strong>Tempat Meninggal</strong>
                                                                    </td>
                                                                    <td width="1%">:</td>
                                                                    <td width="14%">

                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="tempat_meninggal"
                                                                            type="text"
                                                                            disabled="true"
                                                                            placeholder="Tempat Meninggal"
                                                                            value={this.state.dtRes.tempat_meninggal ? this.state.dtRes.tempat_meninggal : ''}/>

                                                                    </td>


                                                                </tr>

                                                                <tr>

                                                                    <td width="5%"><strong>Alamat Unit
                                                                        Hemodialisis</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">
                                                                        <Form.Control
                                                                            style={{"overflow-y": "scroll"}}
                                                                            as="textarea" rows={3}
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="alamat_unit_hd"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.alamat_unit_hd ? this.state.dtRes.alamat_unit_hd : ''}
                                                                            type="text"
                                                                            placeholder="Alamat Unit Hemodialisis"/>
                                                                    </td>

                                                                    <td width="10%"><strong>Pemeriksa Jenazah</strong>
                                                                    </td>
                                                                    <td width="1%">:</td>
                                                                    <td width="14%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="pemeriksaan_jenazah"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.pemeriksaan_jenazah ? this.state.dtRes.pemeriksaan_jenazah : ''}
                                                                            type="text"
                                                                            placeholder="Pemeriksa Jenazah"/>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td width="5%"><strong>Tanggal Pemeriksaan</strong>
                                                                    </td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">

                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="tanggal_pemeriksaan"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.tanggal_pemeriksaan ? this.state.dtRes.tanggal_pemeriksaan : ''}
                                                                            type="text"
                                                                            placeholder="Tanggal Pemeriksaan"/>

                                                                    </td>

                                                                    <td width="10%"><strong>Jam Pemeriksaan</strong>
                                                                    </td>
                                                                    <td width="1%">:</td>
                                                                    <td width="14%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="jam_pemeriksaan"
                                                                            type="jam_pemeriksaan"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.jam_pemeriksaan ? this.state.dtRes.jam_pemeriksaan : ''}
                                                                            placeholder="Jam Pemeriksaan"/>

                                                                    </td>

                                                                </tr>

                                                                <tr>

                                                                    <td width="5%"><strong>Penyebab Kematian</strong>
                                                                    </td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="penyebab_kematian"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.penyebab_kematian ? this.state.dtRes.penyebab_kematian : ''}
                                                                            type="text"
                                                                            placeholder="Penyebab Kematian"/>
                                                                    </td>

                                                                    <td width="10%"><strong>Tanggal</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">


                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="tanggal"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.tanggal ? this.state.dtRes.tanggal : ''}
                                                                            type="text"
                                                                            placeholder="Tanggal"/>

                                                                    </td>

                                                                </tr>

                                                                <tr>
                                                                    <td width="5%"><strong>Signature</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td colSpan="12">
                                                                        <Image
                                                                            src={this.state.dtRes.image_ttd ? this.state.dtRes.image_ttd : noImg}
                                                                            thumbnail
                                                                        />
                                                                    </td>
                                                                </tr>

                                                            </Fragment>
                                                        ) : this.state.type == 'Hasil Rekapitulasi Laboratorium' ? (
                                                            <Fragment>
                                                                <tr>
                                                                    <td width="10%"><strong>Pasien</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="14%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="name"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.name ? this.state.dtRes.name : ''}
                                                                        />

                                                                    </td>
                                                                    <td width="5%"><strong>No.RM</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">

                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="no_rm"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.rm_number ? this.state.dtRes.rm_number : ''}
                                                                        />
                                                                    </td>


                                                                </tr>
                                                                <tr>
                                                                    <td width="10%"><strong>Tgl Lahir</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="14%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="tgl_lahir"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.tgl_lahir ? this.state.dtRes.tgl_lahir : ''}
                                                                        />


                                                                    </td>
                                                                    <td width="5%"><strong>Golongan Darah</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="anti_golongan_darahhbs"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.golongan_darah ? this.state.dtRes.golongan_darah : ''}
                                                                        >


                                                                        </Form.Control>
                                                                    </td>


                                                                </tr>

                                                                <tr>
                                                                    <td width="10%"><strong>Rh +/-</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">


                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="rhesus"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.rhesus ? this.state.dtRes.rhesus : ''}
                                                                        />

                                                                    </td>
                                                                    <td width="5%"><strong>Tanggal Masuk</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="tgl_masuk"
                                                                            type="text"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.tgl_masuk ? this.state.dtRes.tgl_masuk : ''}
                                                                        />
                                                                    </td>


                                                                </tr>

                                                                <tr>
                                                                    <td width="10%"><strong>Hemoglobin</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="hemoglobin"
                                                                            id="hemoglobin"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.hemoglobin ? this.state.dtRes.hemoglobin : ''}
                                                                            type="text"
                                                                            placeholder="Hemoglobin"/>

                                                                    </td>
                                                                    <td width="5%"><strong>Ureum</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="ureum"
                                                                            id="ureum"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.ureum ? this.state.dtRes.ureum : ''}
                                                                            type="text"
                                                                            placeholder="Ureum"/>
                                                                    </td>


                                                                </tr>

                                                                <tr>
                                                                    <td width="10%"><strong>Kreatinin</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="14%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="kreatinin"
                                                                            id="kreatinin"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.kreatinin ? this.state.dtRes.kreatinin : ''}
                                                                            type="text"
                                                                            placeholder="Kreatinin"/>

                                                                    </td>
                                                                    <td width="5%"><strong>Albumin</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="albumin"
                                                                            id="albumin"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.albumin ? this.state.dtRes.albumin : ''}
                                                                            type="text"
                                                                            placeholder="Albumin"/>
                                                                    </td>


                                                                </tr>

                                                                <tr>
                                                                    <td width="10%"><strong>Natrium</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="14%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="natrium"
                                                                            id="natrium"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.natrium ? this.state.dtRes.natrium : ''}
                                                                            type="text"
                                                                            placeholder="Natrium"/>

                                                                    </td>
                                                                    <td width="5%"><strong>Kalium</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="kalium"
                                                                            id="kalium"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.kalium ? this.state.dtRes.kalium : ''}
                                                                            type="text"
                                                                            placeholder="Kalium"/>
                                                                    </td>


                                                                </tr>

                                                                <tr>
                                                                    <td width="10%"><strong>Chlorida</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="14%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="chlorida"
                                                                            id="chlorida"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.chlorida ? this.state.dtRes.chlorida : ''}
                                                                            type="text"
                                                                            placeholder="Chlorida"/>

                                                                    </td>
                                                                    <td width="5%"><strong>Calcium total</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="calcium_total"
                                                                            id="calcium_total"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.calcium_total ? this.state.dtRes.calcium_total : ''}
                                                                            type="text"
                                                                            placeholder="Calcium total"/>
                                                                    </td>


                                                                </tr>

                                                                <tr>
                                                                    <td width="10%"><strong>Calcium Ion</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="14%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="calcium_ion"
                                                                            id="calcium_ion"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.calcium_ion ? this.state.dtRes.calcium_ion : ''}
                                                                            type="text"
                                                                            placeholder="Calcium Ion"/>

                                                                    </td>
                                                                    <td width="5%"><strong>Fosfor Anorganic</strong>
                                                                    </td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="fosfor_anorganic"
                                                                            id="fosfor_anorganic"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.fosfor_anorganic ? this.state.dtRes.fosfor_anorganic : ''}
                                                                            type="text"
                                                                            placeholder="Fosfor Anorganic"/>
                                                                    </td>


                                                                </tr>

                                                                <tr>
                                                                    <td width="10%"><strong>HBsAg</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="14%">


                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="hbsag"
                                                                            id="hbsag"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.hbsag ? this.state.dtRes.hbsag : ''}
                                                                            type="text"
                                                                            placeholder="HBsAg"/>

                                                                    </td>
                                                                    <td width="5%"><strong>Anti HBs</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">

                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="anti_hbs"
                                                                            id="anti_hbs"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.anti_hbs ? this.state.dtRes.anti_hbs : ''}
                                                                            type="text"
                                                                            placeholder="Anti HBs"/>
                                                                    </td>


                                                                </tr>

                                                                <tr>
                                                                    <td width="10%"><strong>Anti HCV</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="14%">

                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="anti_hcv"
                                                                            id="anti_hcv"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.anti_hcv ? this.state.dtRes.anti_hcv : ''}
                                                                            type="text"
                                                                            placeholder="Anti HCV"/>

                                                                    </td>
                                                                    <td width="5%"><strong>Anti HIV</strong></td>
                                                                    <td width="1%">:</td>
                                                                    <td width="19%">

                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="anti_HIV"
                                                                            id="anti_HIV"
                                                                            disabled="true"
                                                                            value={this.state.dtRes.anti_HIV ? this.state.dtRes.anti_HIV : ''}
                                                                            type="text"
                                                                            placeholder="Anti HIV"/>
                                                                    </td>


                                                                </tr>
                                                            </Fragment>
                                                        ) : null


                                                        }

                                                    </Fragment>

                                                    </tbody>

                                                </table>

                                            </div>
                                            <div className="card-footer clearfix">
                                                <button type="button" onClick={() => this.props.history.goBack()}
                                                        className="btn btn-flat btn-danger btn-sm">Back
                                                </button>


                                            </div>


                                        </div>
                                    )}
                                </div>

                            </div>
                        </div>
                    </section>
                </div>

            </>

        )

    }

}

const mapStateToProps = (state) => ({
    dataPasien: state.pasiens.dataPasien || {},
    contentMsg: state.pasiens.contentMsg || null,
    showFormSuccess: state.pasiens.showFormSuccess,
    tipeSWAL: state.pasiens.tipeSWAL,
    user: state.main.currentUser,
});

const mapDispatchToPros = (dispatch) => {
    return {
        onLoadDataPasien: (param) => {
            dispatch(fetchDataPasien(param));
        },
        closeSwal: () => {
            dispatch(closeForm());
        },
    }
}

export default connect(mapStateToProps, mapDispatchToPros)(HistoryDetail);