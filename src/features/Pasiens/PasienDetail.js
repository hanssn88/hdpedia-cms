import moment from "moment";
import "moment/locale/id";
import React, {Component, Fragment} from 'react';
import {Figure, Form, Image} from 'react-bootstrap';
import {connect} from 'react-redux';
import {Placeholder} from 'rsuite';
import noImgs from '../../assets/PDF-en-PNG.png';
import noImg from '../../assets/noPhoto.jpg';
import AppModal from '../../components/modal/MyModal';
import {AppSwalSuccess} from '../../components/modal/SwalSuccess';
import PasienService from './PasienService';
import {approveData, closeForm, rejectData, revisitData} from "./pasiensSlice";

class PasienDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            appsLoading: true,
            showConfirmReject: false,
            showConfirmApprove: false,
            showConfirmRevisi: false,
            isLoading: false,
            showSwalSuccess: false,
            notes: '',
            dtRes: {},
            errMsg: null,
            id: sessionStorage.getItem('idPasienHDPedia'),
        }
    }

    componentDidMount() {
        this.getData();
    }

    handleClose = () => {
        this.setState({
            ...this.state,
            showConfirm: false,
            errMsg: null,
            showConfirmReject: false,
            showConfirmApprove: false,
            showConfirmRevisi: false,
            notes: ''
        });
    };

    closeSwal = async () => {
        this.setState({
            ...this.state,
            errMsg: null,
            notes: "",
            showConfirmRevisi: false,
            showConfirmApprove: false,
            showConfirmReject: false,
            showSwalSuccess: false,
        });
        await this.props.closeSwal();
        await this.getData();
    }


    confirmReject = async () => {
        await this.setState({...this.state, showConfirmReject: true, errMsg: null});
    }

    confirmApprove = async () => {
        await this.setState({...this.state, showConfirmApprove: true, errMsg: null});
    }

    confirmRevisi = async () => {
        await this.setState({...this.state, showConfirmRevisi: true, errMsg: null, notes: ''});
    }

    getData = async () => {
        this.setState({appsLoading: true});
        const selectedId = sessionStorage.getItem('idPasienHDPedia');
        const queryString = {id: selectedId}
        await PasienService.postData(queryString, "VIEW_DETAIL")
            .then(response => {
                setTimeout(() => {
                    if (response.data.err_code === "00") {
                        this.setState({
                            ...this.state,
                            dtRes: response.data.data
                        });
                    }
                    if (response.data.err_code === "04") {
                        this.setState({
                            ...this.state,
                            dtRes: {},
                        });
                    }
                    this.setState({appsLoading: false});
                }, 400);
            })
            .catch(e => {
                console.log(e);
                this.setState({appsLoading: false});
            });
    };

    handleChange(evt) {
        const name = evt.target.name;
        var value = evt.target.value;
        this.setState({
            [name]: value
        })
    }

    handleApprove() {
        const selected = {id: this.state.id}
        this.props.onApprove(selected);
    }

    handleReject() {
        const selected = {id: this.state.id}
        this.props.onReject(selected);
    }

    handleRevisi() {
        const selected = {id: this.state.id, note: this.state.notes}
        this.props.onRevisi(selected);
    }

    render() {
        const {Paragraph} = Placeholder;

        var dataInfeksius = [];

        if (this.state.dtRes.status_infeksius != null && this.state.dtRes.status_infeksius.length > 0) {
            dataInfeksius = this.state.dtRes.status_infeksius;
        }
        const contentConfirmReject = <div
            dangerouslySetInnerHTML={{__html: '<div id="caption" style=padding-bottom:20px;">Apakah anda yakin<br/><b>menolak</b> data ini ?</div>'}}/>;

        const contentConfirmApprove = <div
            dangerouslySetInnerHTML={{__html: '<div id="caption" style=padding-bottom:20px;">Apakah anda yakin<br/><b>menerima</b> data ini ?</div>'}}/>;

        const contentConfirmRevisi = <Form id="myForm">
            <div id="caption">
                Apakah anda yakin <br/><b>memproses</b> data ini ?
            </div>
            <Form.Group controlId="remark_hold">
                <Form.Label>Catatan</Form.Label>
                <Form.Control size="sm" name="notes" as="textarea" rows={5} value={this.state.notes}
                              onChange={this.handleChange.bind(this)} placeholder="Catatan . . ."/>
            </Form.Group>
        </Form>;


        return (


            <>
                <div className="content-wrapper">
                    {/* Content Header (Page header) */}
                    <div className="content-header">
                        <div className="container-fluid">
                            <div className="row mb-2">
                                <div className="col-sm-6">
                                    <h1 className="m-0">Pasien Detail</h1>
                                </div>

                            </div>
                        </div>
                    </div>

                    <section className="content">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-12">
                                    {this.state.appsLoading ? (
                                        <div className="card shadow-lg">
                                            <div className="card-body">
                                                <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                                           style={{marginTop: 30}}/>
                                            </div>
                                        </div>

                                    ) : (
                                        <div className="card shadow-lg">
                                            <div className="card-body">
                                                <table className="table table-condensed">

                                                    <tbody>
                                                    <tr>
                                                        <td style={{
                                                            "backgroundColor": "rgba(0,0,0,.1)",
                                                            "fontWeight": "bold",
                                                            "fontSize": "16px"
                                                        }} colSpan="9" align="center">
                                                            Information
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="8%"><strong>Nama</strong></td>
                                                        <td width="1%">:</td>
                                                        <td width="28%">{`${this.state.dtRes.title ? this.state.dtRes.title : ''} ${this.state.dtRes.name}`}</td>
                                                        <td width="10%"><strong>Tempat, Tanggal lahir</strong></td>
                                                        <td width="1%">:</td>
                                                        <td width="28%">{this.state.dtRes.ttl}</td>

                                                        <td width="12%"><strong>Perkawinan</strong></td>
                                                        <td width="1%">:</td>

                                                        <td width="17%">{this.state.dtRes.perkawinan === 1 ? "Lajang" : this.state.dtRes.perkawinan === 2 ? "Kawin" : this.state.dtRes.perkawinan === 3 ? "Cerai Hidup" : this.state.dtRes.perkawinan === 4 ? "Cerai Mati" : ""}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>No.KTP</strong></td>
                                                        <td>:</td>
                                                        <td>{this.state.dtRes.no_ktp}</td>
                                                        <td><strong>No.SIM</strong></td>
                                                        <td>:</td>
                                                        <td>{this.state.dtRes.no_sim}</td>
                                                        <td><strong>Agama</strong></td>
                                                        <td>:</td>
                                                        <td>{this.state.dtRes.religion === 1 ? "Islam" : this.state.dtRes.religion === 2 ? "Kristen" : this.state.dtRes.religion === 3 ? "Katholik" : this.state.dtRes.religion === 4 ? "Hindu" : this.state.dtRes.religion === 5 ? "Buddha" : this.state.dtRes.religion === 6 ? "Konghucu" : ""}</td>
                                                    </tr>


                                                    <tr>
                                                        <td><strong>Phone Number</strong></td>
                                                        <td>:</td>
                                                        <td>{this.state.dtRes.phone_number}</td>
                                                        <td><strong>Mobile Number</strong></td>
                                                        <td>:</td>
                                                        <td>{this.state.dtRes.mobile_number}</td>
                                                        <td><strong>Pendidikan</strong></td>
                                                        <td>:</td>
                                                        <td>{parseInt(this.state.dtRes.education) === 1 ? "SD" : parseInt(this.state.dtRes.education) === 2 ? "SMP" : parseInt(this.state.dtRes.education) === 3 ? "SMA" : parseInt(this.state.dtRes.education) === 4 ? "Sarjana(S1/S2/S3)" : ""}</td>
                                                    </tr>


                                                    <tr>
                                                        <td><strong>Jaminan Kesehatan (Asuransi)</strong></td>
                                                        <td>:</td>
                                                        <td>
                                                            {
                                                                this.state.dtRes.guarantee.map((item) =>
                                                                    <>
                                                                        {
                                                                            item == 0 ? "Tidak ada asuransi" :
                                                                                item == 1 ? "BPJS" :
                                                                                    item == 2 ? "Swasta" :
                                                                                        ""
                                                                        }
                                                                        <br></br>
                                                                    </>
                                                                )
                                                            }
                                                        </td>
                                                        <td><strong>Hobby</strong></td>
                                                        <td>:</td>
                                                        <td>{this.state.dtRes.hobby}</td>
                                                        <td><strong>Pekerjaan</strong></td>
                                                        <td>:</td>
                                                        <td>{this.state.dtRes.job}</td>
                                                    </tr>

                                                    <tr>
                                                        <td><strong>Perusahaan</strong></td>
                                                        <td>:</td>
                                                        <td>{this.state.dtRes.company_name}</td>
                                                        <td><strong>Alamat Perusahaan</strong></td>
                                                        <td>:</td>
                                                        <td>{this.state.dtRes.company_address}</td>
                                                        <td><strong>Gol.Darah</strong></td>
                                                        <td>:</td>
                                                        <td>{this.state.dtRes.blood_type}</td>
                                                    </tr>

                                                    <tr>
                                                        <td><strong>No.Rekam Medis</strong></td>
                                                        <td>:</td>
                                                        <td>
                                                            {this.state.dtRes.rm_number}</td>
                                                        <td><strong>Rhesus</strong></td>
                                                        <td>:</td>
                                                        <td>{this.state.dtRes.rhesus}</td>
                                                        {/* <td><strong>Notes</strong></td>
                                                        <td>:</td>
                                                        <td>{this.state.dtRes.notes}</td> */}
                                                        <td><strong>Tgl. Register</strong></td>
                                                        <td>:</td>
                                                        <td>{moment(new Date(this.state.dtRes.created_at)).format('DD MMMM YYYY HH:mm')}</td>
                                                    </tr>

                                                    <tr>


                                                        <td><strong>Status Infeksius</strong></td>
                                                        <td>:</td>

                                                        <td>
                                                            {
                                                                dataInfeksius.map((item) =>
                                                                    <>
                                                                        {item.label + '\n'}
                                                                    </>
                                                                )
                                                            }
                                                        </td>


                                                        <td><strong>Alamat</strong></td>
                                                        <td>:</td>
                                                        <td>
                                                            {this.state.dtRes.address} {this.state.dtRes.rtrw} {this.state.dtRes.sub_district} {this.state.dtRes.district}
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td style={{
                                                            "backgroundColor": "rgba(0,0,0,.08)",
                                                            "fontWeight": "bold",
                                                            "fontSize": "16px"
                                                        }} colSpan="9" align="center">Penanggung Jawab
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>Nama</strong></td>
                                                        <td>:</td>
                                                        <td>{this.state.dtRes.name_responsible}</td>
                                                        <td><strong>Hubungan</strong></td>
                                                        <td>:</td>
                                                        <td>{this.state.dtRes.relationship}</td>
                                                        <td><strong>Pekerjaan</strong></td>
                                                        <td>:</td>
                                                        <td>{this.state.dtRes.job_responsible}</td>
                                                    </tr>

                                                    <tr>
                                                        <td><strong>Phone Number 1</strong></td>
                                                        <td>:</td>
                                                        <td>{this.state.dtRes.phone_number_1}</td>
                                                        <td><strong>Phone Number 2</strong></td>
                                                        <td>:</td>
                                                        <td>{this.state.dtRes.phone_number_2}</td>
                                                        <td><strong>Mobile Number</strong></td>
                                                        <td>:</td>
                                                        <td>{this.state.dtRes.mobile_number_responsible}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>Alamat</strong></td>
                                                        <td>:</td>
                                                        <td colSpan={4}>{this.state.dtRes.address_responsible}</td>
                                                        <td><strong>Klinik</strong></td>
                                                        <td>:</td>
                                                        <td>{this.state.dtRes.name_klinik}</td>
                                                    </tr>

                                                    <tr>
                                                        <td style={{
                                                            "backgroundColor": "rgba(0,0,0,.08)",
                                                            "fontWeight": "bold",
                                                            "fontSize": "16px"
                                                        }} colSpan="9" align="center">List Dokumen
                                                        </td>
                                                    </tr>

                                                    </tbody>
                                                </table>
                                                <div className="row mt-3">
                                                    <div className="col-3">
                                                        <Figure
                                                            key={this.state.dtRes.url_image_routine_hematology ? this.state.dtRes.url_image_routine_hematology : noImg}
                                                            className="img-product-pic"
                                                            style={{
                                                                "marginRight": "10px",
                                                                "marginLeft": "5px",
                                                                "textAlign": "center",
                                                                "fontWeight": "bold"
                                                            }}>
                                                            <Image
                                                                src={this.state.dtRes.url_image_routine_hematology ? this.state.dtRes.url_image_routine_hematology : noImg}
                                                                thumbnail
                                                                style={{width: "80%"}}/>
                                                            <Figure.Caption>
                                                                Routine Hematology
                                                            </Figure.Caption>
                                                        </Figure>
                                                    </div>

                                                    <div className="col-3">
                                                        <Figure
                                                            key={this.state.dtRes.url_image_ureum_creatinine_hbsag ? this.state.dtRes.url_image_ureum_creatinine_hbsag : noImg}
                                                            className="img-product-pic"
                                                            style={{
                                                                "marginRight": "10px",
                                                                "marginLeft": "5px",
                                                                "textAlign": "center",
                                                                "fontWeight": "bold",
                                                                alignContent: "center"
                                                            }}>
                                                            <Image
                                                                src={this.state.dtRes.url_image_ureum_creatinine_hbsag ? this.state.dtRes.url_image_ureum_creatinine_hbsag : noImg}
                                                                thumbnail
                                                                style={{width: "80%"}}/>
                                                            <Figure.Caption>
                                                                Hasil Lab Ureum/Kreatinnin/HBSAG
                                                            </Figure.Caption>
                                                        </Figure>
                                                    </div>


                                                    <div className="col-3">
                                                        <Figure
                                                            key={this.state.dtRes.url_image_anti_hiv ? this.state.dtRes.url_image_anti_hiv : noImg}
                                                            className="img-product-pic"
                                                            style={{
                                                                "marginRight": "10px",
                                                                "marginLeft": "5px",
                                                                "textAlign": "center",
                                                                "fontWeight": "bold"
                                                            }}>
                                                            <Image
                                                                src={this.state.dtRes.url_image_anti_hiv ? this.state.dtRes.url_image_anti_hiv : noImg}
                                                                thumbnail
                                                                style={{width: "80%"}}/>
                                                            <Figure.Caption>
                                                                Anti HIV
                                                            </Figure.Caption>
                                                        </Figure>
                                                    </div>


                                                    <div className="col-3">
                                                        <Figure
                                                            key={this.state.dtRes.url_image_anti_hcv ? this.state.dtRes.url_image_anti_hcv : noImg}
                                                            className="img-product-pic"
                                                            style={{
                                                                "marginRight": "10px",
                                                                "marginLeft": "5px",
                                                                "textAlign": "center",
                                                                "fontWeight": "bold"
                                                            }}>
                                                            <Image
                                                                src={this.state.dtRes.url_image_anti_hcv ? this.state.dtRes.url_image_anti_hcv : noImg}
                                                                thumbnail
                                                                style={{width: "80%"}}/>
                                                            <Figure.Caption>
                                                                Anti HCV
                                                            </Figure.Caption>
                                                        </Figure>
                                                    </div>

                                                    <div className="col-3">
                                                        <Figure
                                                            key={this.state.dtRes.url_image_pcr_or_antigen ? this.state.dtRes.url_image_pcr_or_antigen : noImg}
                                                            className="img-product-pic"
                                                            style={{
                                                                "marginRight": "10px",
                                                                "marginLeft": "5px",
                                                                "textAlign": "center",
                                                                "fontWeight": "bold"
                                                            }}>
                                                            <Image
                                                                src={this.state.dtRes.url_image_pcr_or_antigen ? this.state.dtRes.url_image_pcr_or_antigen : noImg}
                                                                thumbnail
                                                                style={{width: "80%"}}/>
                                                            <Figure.Caption>
                                                                PCR or Antigen
                                                            </Figure.Caption>
                                                        </Figure>
                                                    </div>

                                                    <div className="col-3">
                                                        <Figure
                                                            key={this.state.dtRes.url_image_rujukan_faskes_from ? this.state.dtRes.url_image_rujukan_faskes_from : noImg}
                                                            className="img-product-pic"
                                                            style={{
                                                                "marginRight": "10px",
                                                                "marginLeft": "5px",
                                                                "textAlign": "center",
                                                                "fontWeight": "bold"
                                                            }}>
                                                            <Image
                                                                src={this.state.dtRes.url_image_rujukan_faskes_from ? this.state.dtRes.url_image_rujukan_faskes_from : noImg}
                                                                thumbnail
                                                                style={{width: "80%"}}/>
                                                            <Figure.Caption>
                                                                Surat Travelling HD
                                                            </Figure.Caption>
                                                        </Figure>
                                                    </div>

                                                    <div className="col-3">
                                                        <Figure
                                                            key={this.state.dtRes.url_image_rujukan_faskes_to ? this.state.dtRes.url_image_rujukan_faskes_to : noImg}
                                                            className="img-product-pic"
                                                            style={{
                                                                "marginRight": "10px",
                                                                "marginLeft": "5px",
                                                                "textAlign": "center",
                                                                "fontWeight": "bold"
                                                            }}>
                                                            <Image
                                                                src={this.state.dtRes.url_image_rujukan_faskes_to ? this.state.dtRes.url_image_rujukan_faskes_to : noImg}
                                                                thumbnail
                                                                style={{width: "80%"}}/>
                                                            <Figure.Caption>
                                                                Rujukan Faskes tujuan
                                                            </Figure.Caption>
                                                        </Figure>
                                                    </div>

                                                    <div className="col-3">
                                                        <Figure
                                                            key={this.state.dtRes.url_image_bpjs ? this.state.dtRes.url_image_bpjs : noImg}
                                                            className="img-product-pic"
                                                            style={{
                                                                "marginRight": "10px",
                                                                "marginLeft": "5px",
                                                                "textAlign": "center",
                                                                "fontWeight": "bold"
                                                            }}>
                                                            <Image
                                                                src={this.state.dtRes.url_image_rujukan_faskes_to ? this.state.dtRes.url_image_bpjs : noImg}
                                                                thumbnail
                                                                style={{width: "80%"}}/>
                                                            <Figure.Caption>
                                                                Kartu BPJS
                                                            </Figure.Caption>
                                                        </Figure>
                                                    </div>

                                                    <div className="col-3">
                                                        <Figure
                                                            key={this.state.dtRes.url_image_ktp ? this.state.dtRes.url_image_ktp : noImg}
                                                            className="img-product-pic"
                                                            style={{
                                                                "marginRight": "10px",
                                                                "marginLeft": "5px",
                                                                "textAlign": "center",
                                                                "fontWeight": "bold"
                                                            }}>
                                                            <Image
                                                                src={this.state.dtRes.url_image_ktp ? this.state.dtRes.url_image_ktp : noImg}
                                                                thumbnail
                                                                style={{width: "80%"}}/>
                                                            <Figure.Caption>
                                                                KTP
                                                            </Figure.Caption>
                                                        </Figure>
                                                    </div>

                                                    <div className="col-3">
                                                        <Figure
                                                            key={this.state.dtRes.url_image_kk ? this.state.dtRes.url_image_kk : noImg}
                                                            className="img-product-pic"
                                                            style={{
                                                                "marginRight": "10px",
                                                                "marginLeft": "5px",
                                                                "textAlign": "center",
                                                                "fontWeight": "bold"
                                                            }}>
                                                            <Image
                                                                src={this.state.dtRes.url_image_kk ? this.state.dtRes.url_image_kk : noImg}
                                                                thumbnail
                                                                style={{width: "80%"}}/>
                                                            <Figure.Caption>
                                                                KK
                                                            </Figure.Caption>
                                                        </Figure>
                                                    </div>

                                                    <div className="col-3">
                                                        <Figure
                                                            key={this.state.dtRes.url_image_syarat_tatib ? this.state.dtRes.url_image_syarat_tatib : noImg}
                                                            className="img-product-pic"
                                                            style={{
                                                                "marginRight": "10px",
                                                                "marginLeft": "5px",
                                                                "textAlign": "center",
                                                                "fontWeight": "bold"
                                                            }}>
                                                            {
                                                                this.state.dtRes.url_image_syarat_tatib != null ?
                                                                    this.state.dtRes.url_image_syarat_tatib.includes(".pdf") ?
                                                                        <a href={this.state.dtRes.url_image_syarat_tatib}
                                                                           target="_blank">
                                                                            <Image src={noImgs} thumbnail
                                                                                   style={{width: "80%"}}/>
                                                                        </a>
                                                                        :
                                                                        <Image
                                                                            src={this.state.dtRes.url_image_syarat_tatib ? this.state.dtRes.url_image_syarat_tatib : noImg}
                                                                            thumbnail
                                                                            style={{width: "80%"}}/>
                                                                    :
                                                                    <Image
                                                                        src={noImg}
                                                                        thumbnail
                                                                        style={{width: "80%"}}/>
                                                            }

                                                            <Figure.Caption>
                                                                Syarat Tata Tertib
                                                            </Figure.Caption>
                                                        </Figure>
                                                    </div>

                                                </div>

                                            </div>
                                            <div className="card-footer clearfix">
                                                <button type="button" onClick={() => this.props.history.goBack()}
                                                        className="btn btn-flat btn-danger btn-sm">Back
                                                </button>

                                                {this.state.dtRes.status === 0 &&
                                                    <Fragment>
                                                        <button type="button" onClick={this.confirmReject}
                                                                style={{marginLeft: 3}}
                                                                className="btn bnt-flat btn-warning btn-sm">Reject
                                                        </button>
                                                        <button type="button" onClick={this.confirmRevisi}
                                                                style={{marginLeft: 3}}
                                                                className="btn bnt-flat btn-info btn-sm">Revisi
                                                        </button>
                                                        <button type="button" onClick={this.confirmApprove}
                                                                style={{marginLeft: 3}}
                                                                className="btn bnt-flat btn-success btn-sm">Approve
                                                        </button>
                                                    </Fragment>
                                                }
                                            </div>


                                        </div>
                                    )}
                                </div>

                            </div>
                        </div>
                    </section>

                    {this.props.showFormSuccess ? (<AppSwalSuccess
                        show={this.props.showFormSuccess}
                        title={<div dangerouslySetInnerHTML={{__html: this.props.contentMsg}}/>}
                        type={this.props.tipeSWAL}
                        handleClose={this.closeSwal.bind(this)}>

                    </AppSwalSuccess>) : ''}
                    <AppModal
                        show={this.state.showConfirmReject}
                        size="xs"
                        form={contentConfirmReject}
                        handleClose={this.handleClose.bind(this)}
                        backdrop="static"
                        keyboard={false}
                        title="Confirm"
                        titleButton="Yes, Reject"
                        themeButton="danger"
                        isLoading={this.state.isLoading}
                        formSubmit={this.handleReject.bind(this)}
                    ></AppModal>

                    <AppModal
                        show={this.state.showConfirmRevisi}
                        size="xs"
                        form={contentConfirmRevisi}
                        handleClose={this.handleClose.bind(this)}
                        backdrop="static"
                        keyboard={false}
                        title="Confirm"
                        titleButton="Yes, Revisi"
                        themeButton="warning"
                        isLoading={this.state.isLoading}
                        formSubmit={this.handleRevisi.bind(this)}
                    ></AppModal>

                    <AppModal
                        show={this.state.showConfirmApprove}
                        size="xs"
                        form={contentConfirmApprove}
                        handleClose={this.handleClose.bind(this)}
                        backdrop="static"
                        keyboard={false}
                        title="Confirm"
                        titleButton="Yes, Approve"
                        themeButton="success"
                        isLoading={this.state.isLoading}
                        formSubmit={this.handleApprove.bind(this)}
                    ></AppModal>


                </div>
                <div>

                </div>

            </>


        )
    }
}


const mapStateToProps = (state) => ({
    contentMsg: state.pasiens.contentMsg || null,
    showFormSuccess: state.pasiens.showFormSuccess,
    tipeSWAL: state.pasiens.tipeSWAL,
    user: state.main.currentUser,
});

const mapDispatchToPros = (dispatch) => {
    return {
        onApprove: (param) => {
            dispatch(approveData(param));
        },
        onReject: (param) => {
            dispatch(rejectData(param));
        },

        onRevisi: (param) => {
            dispatch(revisitData(param));
        },
        closeSwal: () => {
            dispatch(closeForm());
        },
    }
}
export default connect(mapStateToProps, mapDispatchToPros)(PasienDetail);