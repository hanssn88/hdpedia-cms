import styled from "@emotion/styled";
import moment from 'moment';
import "moment/locale/id";
import React, {Component, Fragment} from 'react';
import {Col, Figure, Form, Image} from 'react-bootstrap';
import Button from "react-bootstrap/Button";
import Datetime from 'react-datetime';
import "react-datetime/css/react-datetime.css";
import Select from "react-dropdown-select";
import {connect} from 'react-redux';
import {Placeholder} from 'rsuite';
import noImgs from '../../assets/PDF-en-PNG.png';
import noImg from '../../assets/noPhoto.jpg';
import {AppSwalSuccess} from '../../components/modal/SwalSuccess';
import PasienService from './PasienService';
import {addData, closeForm, uplodDokumen} from "./pasiensSlice";


var yesterday = moment();
var valid_startDate = function (current) {
    return current.isBefore(yesterday);
};

class PasienForm extends Component {
    constructor(props) {
        super(props);
        this.initSelected = {
            email: '',
            name: '',
            pass: '',
            user_name: '',
            address: '',
            alamat: '',
            rt: '',
            rw: '',
            sub_district: '',
            district: '',
            phone_number: '',
            mobile_number: '',
            perkawinan: '',
            religion: '',
            education: '',
            no_ktp: '',
            no_sim: '',
            no_identitas: '',
            tipe_identitas: '',
            guarantee: [],
            hobby: '',
            job: '',
            company_name: '',
            company_address: '',
            blood_type: '',
            rhesus: '',
            notes: '',
            tpt_lahir: '',
            tgl_lahir: '',
            name_responsible: "",
            address_responsible: "",
            mobile_number_responsible: "",
            relationship: "",
            job_responsible: "",
            title: "",
            rm_number: "",
            status_infeksius: []

        }
        this.state = {
            validSd: valid_startDate,
            appsLoading: false,
            showConfirmReject: false,
            showConfirmApprove: false,
            showConfirmRevisi: false,
            isLoading: false,
            showSwalSuccess: false,
            selectValues: [],
            selectValuesGuarantee: [],
            options: [
                {
                    value: "Non Hep",
                    label: "Non Hep",
                },
                {
                    value: "Hep B",
                    label: "Hep B"
                },
                {
                    value: "Hep C",
                    label: "Hep C"
                }
            ],
            notes: '',
            dtRes: {
                imgUpload_url_image_routine_hematology: noImg,
                imgUpload_url_image_ureum_creatinine_hbsag: noImg,
                imgUpload_url_image_anti_hiv: noImg,
                imgUpload_url_image_anti_hcv: noImg,
                imgUpload_url_image_pcr_or_antigen: noImg,
                imgUpload_url_image_bpjs: noImg,
                imgUpload_url_image_ktp: noImg,
                imgUpload_url_image_kk: noImg,
                imgUpload_url_image_rujukan_faskes_from: noImg,
                imgUpload_url_image_rujukan_faskes_to: noImg,
                imgUpload_url_image_syarat_tatib: noImg,
            },
            errMsg: this.initSelected,
            id: sessionStorage.getItem('idPasienHDPedia'),
            optionsGuarantee: [
                {
                    value: "0",
                    label: "Tidak ada asuransi",
                },
                {
                    value: "1",
                    label: "BPJS"
                },
                {
                    value: "2",
                    label: "Swasta"
                }
            ],
        }
        this.handleChangeStartDate = this.handleChangeStartDate.bind(this);
        this.handleChangeImage = this.handleChangeImage.bind(this);
    }

    componentDidMount() {
        const selectedId = sessionStorage.getItem('idPasienHDPedia');
        if (parseInt(selectedId) > 0) this.getData();
    }

    handleClose = () => {
        this.setState({
            ...this.state,
            showConfirm: false,
            errMsg: null,
            showConfirmReject: false,
            showConfirmApprove: false,
            showConfirmRevisi: false,
            notes: ''
        });
    };

    onChangeAlphaNumericInput(e) {
        let name = e.target.name;
        const value = e.target.value;
        const regex = /^[0-9(\-)]+$/; //this will admit letters, numbers and dashes
        if (value.match(regex) || value === "") {
            this.setState({
                dtRes: {
                    ...this.state.dtRes,
                    [name]: value
                }
            })
        }
    }


    closeSwal = async () => {
        this.setState({
            ...this.state,
            errMsg: {},
            notes: "",
            showSwalSuccess: false
        });
        await this.props.closeSwal();
        if (this.props.tipeSWAL === "success") await this.getData();
    }

    formatDate(string) {
        var options = {day: 'numeric', month: 'long', year: 'numeric'};
        return new Date(string).toLocaleDateString([], options);
    }

    getData = async () => {
        this.setState({appsLoading: true});
        const selectedId = sessionStorage.getItem('idPasienHDPedia');
        const queryString = {id: selectedId}
        await PasienService.postData(queryString, "VIEW_DETAIL")
            .then(response => {
                setTimeout(() => {
                    if (response.data.err_code === "00") {
                        this.setState({
                            ...this.state,
                            dtRes: response.data.data
                        });
                        const dtRes = response.data.data;
                        let guarantee = [];
                        let ttls = '';
                        let optionsGuarantee = this.state.optionsGuarantee;
                        Object.keys(dtRes).map((key) => {
                            let name = `imgUpload_${key}`;
                            if (key === 'guarantee') {
                                if (dtRes[key] != null) {
                                    if (dtRes[key].includes("1") || dtRes[key].includes(1)) {
                                        guarantee.push({
                                                value: "1",
                                                label: "BPJS"
                                            },
                                        )
                                    }
                                    if (dtRes[key].includes("2") || dtRes[key].includes(2)) {
                                        guarantee.push({
                                            value: "2",
                                            label: "Swasta"
                                        })
                                    }

                                    if (dtRes[key].includes("0") || dtRes[key].includes(0)) {
                                        guarantee = [
                                            {
                                                value: "0",
                                                label: "Tidak ada asuransi",
                                            }
                                        ];
                                    }

                                    if (dtRes[key].includes("1") || dtRes[key].includes("2") || dtRes[key].includes(1) || dtRes[key].includes(2)) {
                                        optionsGuarantee = [
                                            {
                                                value: "0",
                                                label: "Tidak ada asuransi",
                                                "disabled": true,
                                            },
                                            {
                                                value: "1",
                                                label: "BPJS"
                                            },
                                            {
                                                value: "2",
                                                label: "Swasta"
                                            }

                                        ]
                                    } else {
                                        optionsGuarantee = [
                                            {
                                                value: "0",
                                                label: "Tidak ada asuransi",

                                            },
                                            {
                                                value: "1",
                                                label: "BPJS",
                                                "disabled": true,
                                            },
                                            {
                                                value: "2",
                                                label: "Swasta",
                                                "disabled": true,
                                            }
                                        ]
                                    }

                                }
                            } else if (key === 'ttl') {
                                var tl = '', td = '', dates = '';

                                tl = dtRes[key].split(',')[0];
                                td = dtRes[key].split(',')[1];
                                td = td.split(' ')[1];
                                td = td.split('-')
                                dates = td[2] + '-' + td[1] + '-' + td[0];
                                // dates = moment(dates).format("DD-MM-YYYY")
                                // ttls = tl +", "+ dates;

                                this.handleChangeStartDate(dates);

                            }

                            this.setState({
                                ...this.state,
                                optionsGuarantee: optionsGuarantee,
                                dtRes: {
                                    ...this.state.dtRes,
                                    ['guarantee']: guarantee,
                                    // ['ttl']:ttls,

                                    no_identitas: this.state.dtRes.no_ktp ? this.state.dtRes.no_ktp : this.state.dtRes.no_sim,
                                    tipe_identitas: this.state.dtRes.no_ktp ? "ktp" : "sim",
                                    [name]: dtRes[key]
                                }
                            });
                            return 1;
                        });

                        this.setValues(response.data.data.status_infeksius);
                    }
                    if (response.data.err_code === "04") {
                        this.setState({
                            ...this.state,
                            dtRes: {},
                        });
                    }
                    this.setState({appsLoading: false});
                }, 400);
            })
            .catch(e => {
                console.log(e);
                this.setState({appsLoading: false});
            });
    };

    handleChange(evt) {
        let name = evt.target.name;
        var value = evt.target.value;
        if (name === "tpt_lahir") {
            const tglLahir = this.state.dtRes.ttl ? this.state.dtRes.ttl.split(',')[1] : ''

            name = "ttl";
            value = tglLahir ? value + ', ' + tglLahir : value;
        }
        this.setState({
            dtRes: {
                ...this.state.dtRes,
                [name]: value
            }
        })
    }

    handleChangeImage(evt) {
        const name = evt.target.name;
        var value = evt.target.value;
        this.setState({isLoading: false})
        this.setState({errMsg: {img: ""}})
        value = evt.target.files[0];
        this.setState({img: ''})
        if (!value) return;
        if (!value.name.match(/\.(jpg|jpeg|png|pdf)$/)) {
            this.setState({errMsg: {[name]: "Extension Invalid"}})
            this.setState({isLoading: true})
            return;
        }
        if (value.size > 2099200) {
            this.setState({errMsg: {[name]: "File size over 2MB"}})
            this.setState({isLoading: true})
            return;
        }
        let reader = new FileReader();
        reader.readAsDataURL(value);
        const varUpload = `imgUpload_${name}`;
        reader.onloadend = () => {
            if (value.name.match(/\.(pdf)$/)) {
                this.setState({
                    dtRes: {
                        ...this.state.dtRes,
                        [name]: value,
                        [varUpload]: value.name
                    }
                })
            } else {
                this.setState({
                    dtRes: {
                        ...this.state.dtRes,
                        [name]: value,
                        [varUpload]: reader.result
                    }
                })
            }
            const param = {
                patient_id: this.state.dtRes.id, [name.replace('url_', '')]: value
            }
            this.props.onUpload(param);


        };
    }


    handleChangeStartDate(date) {
        const tptLahir = this.state.dtRes.ttl ? this.state.dtRes.ttl.split(',')[0] : ''
        if (date) {
            const selectedDate = new Date(date);
            const _date = moment(selectedDate).format('YYYY-MM-DD');
            this.setState({
                dtRes: {
                    ...this.state.dtRes,
                    ttl: tptLahir + ', ' + _date
                }
            })
        } else {
            this.setState({
                dtRes: {
                    ...this.state.dtRes,
                    ttl: tptLahir
                }
            })
        }

    }

    setValues(selectValues) {
        // setValues = selectValues => this.setState({ selectValues });

        this.setState({
            options: [

                {
                    value: "Non Hep",
                    label: "Non Hep",
                },
                {
                    value: "Hep B",
                    label: "Hep B"
                },
                {
                    value: "Hep c",
                    label: "Hep C"
                }

            ]
        })

        this.setState({
            selectValues
        })

        const status_infeksius = [];

        for (var i in selectValues) {

            const name = selectValues[i];
            status_infeksius.push(selectValues[i]);

            if (name.label == "Hep B" || name.label == "Hep C") {

                this.setState({
                    options: [

                        {
                            value: "Non Hep",
                            label: "Non Hep",
                            "disabled": true,
                        },
                        {
                            value: "Hep B",
                            label: "Hep B"
                        },
                        {
                            value: "Hep C",
                            label: "Hep C"
                        }

                    ]
                })

            } else if (name.label == "Non Hep") {
                this.setState({
                    options: [

                        {
                            value: "Non Hep",
                            label: "Non Hep",

                        },
                        {
                            value: "Hep B",
                            label: "Hep B",
                            "disabled": true,
                        },
                        {
                            value: "Hep C",
                            label: "Hep C",
                            "disabled": true,
                        }

                    ]
                })


            }
            console.log("Status :" + [status_infeksius]);
            this.setState({
                dtRes: {
                    ...this.state.dtRes,
                    ['status_infeksius']: status_infeksius
                }
            })

        }


    }

    setValuesGuarantee(selectValuesGuarantee) {
        // setValues = selectValues => this.setState({ selectValues });


        this.setState({
            optionsGuarantee: [
                {
                    value: "0",
                    label: "Tidak ada asuransi",
                },
                {
                    value: "1",
                    label: "BPJS"
                },
                {
                    value: "2",
                    label: "Swasta"
                }
            ]
        })

        this.setState({
            selectValuesGuarantee
        })

        const guarantee = [];

        for (var i in selectValuesGuarantee) {

            const name = selectValuesGuarantee[i];

            guarantee.push(selectValuesGuarantee[i]);

            if (name.label == "BPJS" || name.label == "Swasta") {

                this.setState({
                    optionsGuarantee: [

                        {
                            value: "0",
                            label: "Tidak ada asuransi",
                            "disabled": true,
                        },
                        {
                            value: "1",
                            label: "BPJS"
                        },
                        {
                            value: "2",
                            label: "Swasta"
                        }

                    ]
                })

            } else if (name.label == "Tidak ada asuransi") {
                this.setState({
                    optionsGuarantee: [

                        {
                            value: "0",
                            label: "Tidak ada asuransi",

                        },
                        {
                            value: "1",
                            label: "BPJS",
                            "disabled": true,
                        },
                        {
                            value: "2",
                            label: "Swasta",
                            "disabled": true,
                        }

                    ]
                })


            }

            this.setState({
                dtRes: {
                    ...this.state.dtRes,
                    ['guarantee']: guarantee
                }
            })

        }

    }


    renderView(mode, renderDefault, name) {
        // Only for years, months and days view
        if (mode === "time") return renderDefault();

        return (
            <div className="wrapper">
                {renderDefault()}
                <div className="controls">
                    <Button variant="warning" type="button" onClick={() => this.clear(name)}>Clear</Button>
                </div>
            </div>
        );
    }

    clear(name) {
        if (name === "tgl_lahir") {
            this.handleChangeStartDate('');
        }

    }

    async handleSubmit() {
        var errors = this.state.errMsg;
        this.setState({
            ...this.state,
            loadingForm: true,
        });
        const patternEmail = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

        const uppercaseRegExp = new RegExp(/(?=.*?[A-Z])/);
        const lowercaseRegExp = new RegExp(/(?=.*?[a-z])/);
        const digitsRegExp = new RegExp(/(?=.*?[0-9])/);
        const specialCharRegExp = /(?=.*?[#?!@$%^&*-])/;

        const minLengthRegExp = new RegExp(/.{8,}/);
        errors.name = !this.state.dtRes.name ? "Required" : '';
        errors.email = !this.state.dtRes.email && !this.state.dtRes.id ? "Required" : '';
        errors.pass = !this.state.dtRes.pass && !this.state.dtRes.id ? "Required" : '';
        errors.user_name = !this.state.dtRes.user_name && !this.state.dtRes.id ? "Required" : '';
        errors.address = !this.state.dtRes.address ? "Required" : '';
        errors.phone_number = !this.state.dtRes.phone_number ? "Required" : '';
        errors.tipe_identitas = !this.state.dtRes.tipe_identitas ? "Required" : '';
        errors.no_identitas = !this.state.dtRes.no_identitas ? "Required" : '';
        errors.religion = !this.state.dtRes.religion ? "Required" : '';
        errors.education = !this.state.dtRes.education ? "Required" : '';
        errors.blood_type = !this.state.dtRes.blood_type ? "Required" : '';
        errors.rhesus = !this.state.dtRes.rhesus ? "Required" : '';
        errors.tpt_lahir = !this.state.dtRes.tpt_lahir && !this.state.dtRes.ttl ? "Required" : '';
        errors.tgl_lahir = !this.state.dtRes.tgl_lahir && !this.state.dtRes.ttl ? "Required" : '';
        errors.mobile_number = !this.state.dtRes.mobile_number ? "Required" : '';
        errors.guarantee = !this.state.dtRes.guarantee ? "Required" : '';
        errors.hobby = !this.state.dtRes.hobby ? "Required" : '';
        errors.job = !this.state.dtRes.job ? "Required" : '';
        errors.company_name = !this.state.dtRes.company_name ? "Required" : '';
        errors.company_address = !this.state.dtRes.company_address ? "Required" : '';
        errors.sub_district = !this.state.dtRes.sub_district ? "Required" : '';
        errors.district = !this.state.dtRes.district ? "Required" : '';
        errors.rt = !this.state.dtRes.rt ? "Required" : '';
        errors.rw = !this.state.dtRes.rw ? "Required" : '';
        errors.perkawinan = !this.state.dtRes.perkawinan ? "Required" : '';
        errors.name_responsible = !this.state.dtRes.name_responsible ? "Required" : '';
        errors.address_responsible = !this.state.dtRes.address_responsible ? "Required" : '';
        errors.mobile_number_responsible = !this.state.dtRes.mobile_number_responsible ? "Required" : '';
        errors.relationship = !this.state.dtRes.relationship ? "Required" : '';
        errors.job_responsible = !this.state.dtRes.job_responsible ? "Required" : '';
        // errors.rm_number = !this.state.dtRes.rm_number ? "Required" : '';
        errors.status_infeksius = !this.state.dtRes.status_infeksius ? "Required" : '';


        if (this.state.dtRes.email) {
            errors.email = !patternEmail.test(this.state.dtRes.email) ? "Format invalid" : errors.email;
        }
        if (this.state.dtRes.pass) {
            const minLengthPassword = minLengthRegExp.test(this.state.dtRes.pass);
            const digitsPassword = digitsRegExp.test(this.state.dtRes.pass);
            const uppercasePassword = uppercaseRegExp.test(this.state.dtRes.pass);
            const lowercasePassword = lowercaseRegExp.test(this.state.dtRes.pass);
            const specialCharPassword = specialCharRegExp.test(this.state.dtRes.pass);
            errors.pass = !uppercasePassword ? "At least one Uppercase" : '';
            errors.pass = !lowercasePassword ? "At least one Lowercase" : errors.pass;
            errors.pass = !digitsPassword ? "At least one digit" : errors.pass;
            errors.pass = !minLengthPassword ? "At least minimum 8 characters" : errors.pass;
            errors.pass = !specialCharPassword ? "At least one special characters" : errors.pass;
        }

        this.setState({errors});

        if (this.validateForm(this.state.errMsg)) {
            const guarantee = [];
            for (var i in this.state.dtRes.guarantee) {
                guarantee.push(parseInt(this.state.dtRes.guarantee[i].value));
            }
            var dates = "", arrays = "", tmp = "", tgl = "";

            arrays = this.state.dtRes.ttl.split(',');
            tmp = arrays[0];
            dates = new Date(arrays[1]);
            tgl = moment(dates).format("DD-MM-YYYY");
            const ttls = tmp + ", " + tgl;


            const param = {
                ...this.state.dtRes,
                guarantee: guarantee,
                ttl: ttls,
                id_patient: this.state.dtRes.id,
                no_ktp: this.state.dtRes.tipe_identitas === 'ktp' && this.state.dtRes.no_identitas,
                no_sim: this.state.dtRes.tipe_identitas === 'sim' && this.state.dtRes.no_identitas,
                person_responsible_name: this.state.dtRes.name_responsible,
                person_responsible_address: this.state.dtRes.address_responsible,
                person_responsible_phone_number_1: this.state.dtRes.phone_number_1,
                person_responsible_phone_number_2: this.state.dtRes.phone_number_2,
                person_responsible_mobile_number: this.state.dtRes.mobile_number_responsible,
                person_responsible_relationship: this.state.dtRes.relationship,
                person_responsible_job: this.state.dtRes.job_responsible,
            }
            await this.props.onAdd(param);
        } else {
            console.error('Invalid Form')
        }
        this.setState({
            ...this.state,
            loadingForm: false,
        });
    }

    validateForm(errors) {
        let valid = true;
        Object.values(errors).forEach(
            (val) => val.length > 0 && (valid = false)
        );
        return valid;
    }


    render() {
        const {Paragraph} = Placeholder;
        const {errMsg} = this.state;


        return (


            <>
                <div className="content-wrapper">
                    {/* Content Header (Page header) */}
                    <div className="content-header">
                        <div className="container-fluid">
                            <div className="row mb-2">
                                <div className="col-sm-6">
                                    <h1 className="m-0">Form Pasien</h1>
                                </div>

                            </div>
                        </div>
                    </div>

                    <section className="content">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-12">
                                    {this.state.appsLoading ? (
                                        <div className="card shadow-lg">
                                            <div className="card-body">
                                                <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                                           style={{marginTop: 30}}/>
                                            </div>
                                        </div>

                                    ) : (
                                        <Form>
                                            <div className="card shadow-lg">
                                                <div className="card-body">
                                                    <table className="table table-condensed">

                                                        <tbody>
                                                        <tr>
                                                            <td style={{
                                                                "backgroundColor": "rgba(0,0,0,.1)",
                                                                "fontWeight": "bold",
                                                                "fontSize": "16px"
                                                            }} colSpan="9" align="center">
                                                                Information
                                                            </td>
                                                        </tr>
                                                        {!this.state.dtRes.id &&
                                                            <tr>
                                                                <td width="8%">
                                                                    <strong>Email</strong>
                                                                </td>
                                                                <td width="1%">:</td>
                                                                <td width="28%">
                                                                    {errMsg.email ? (<Fragment><span
                                                                        className="text-error badge badge-danger mb-1">{errMsg.email}</span></Fragment>) : ''}
                                                                    <Form.Control
                                                                        size="sm"
                                                                        autoComplete="off"
                                                                        name="email"
                                                                        type="text"
                                                                        value={this.state.dtRes.email}
                                                                        onChange={this.handleChange.bind(this)}
                                                                        placeholder="Email"/>
                                                                </td>

                                                                <td width="8%"><strong>Username</strong>
                                                                </td>
                                                                <td width="1%">:</td>
                                                                <td width="28%">
                                                                    {errMsg.user_name ? (<Fragment><span
                                                                        className="text-error badge badge-danger mb-1">{errMsg.user_name}</span></Fragment>) : ''}
                                                                    <Form.Control
                                                                        size="sm"
                                                                        autoComplete="off"
                                                                        name="user_name"
                                                                        type="text"
                                                                        value={this.state.dtRes.user_name}
                                                                        onChange={this.handleChange.bind(this)}
                                                                        placeholder="Username"/>
                                                                </td>

                                                                <td width="8%"><strong>Password</strong>

                                                                </td>
                                                                <td width="1%">:</td>

                                                                <td>
                                                                    {errMsg.pass ? (<Fragment><span
                                                                        className="text-error badge badge-danger mb-1">{errMsg.pass}</span></Fragment>) : ''}
                                                                    <Form.Control
                                                                        size="sm"
                                                                        autoComplete="off"
                                                                        name="pass"
                                                                        type="text"
                                                                        value={this.state.dtRes.pass}
                                                                        onChange={this.handleChange.bind(this)}
                                                                        placeholder="Password"/>
                                                                </td>

                                                            </tr>
                                                        }
                                                        <tr>

                                                            <td width="8%">
                                                                <strong>No.Rekam Medis</strong>
                                                            </td>
                                                            <td width="1%">:</td>
                                                            <td width="28%">
                                                                {errMsg.rm_number ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.rm_number}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    name="rm_number"
                                                                    disabled={true}
                                                                    type="text"
                                                                    value={this.state.dtRes.rm_number}
                                                                    onChange={this.handleChange.bind(this)}
                                                                    placeholder="No Rekamedis"/>
                                                            </td>

                                                            <td width="8%"><strong>Title-Nama</strong>
                                                            </td>
                                                            <td width="1%">:</td>
                                                            <td width="28%">

                                                                <Form.Row>
                                                                    <Form.Group controlId="title" style={{
                                                                        marginBottom: "0px",
                                                                        marginLeft: "5px"
                                                                    }}>
                                                                        {errMsg.title ? (<Fragment><span
                                                                            className="text-error badge badge-danger mb-1">{errMsg.title}</span></Fragment>) : ''}
                                                                        <Form.Control
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            name="title"
                                                                            as="select"
                                                                            value={this.state.dtRes.title && this.state.dtRes.title}
                                                                            onChange={this.handleChange.bind(this)}
                                                                        >
                                                                            <option value="">- Pilih -</option>
                                                                            <option
                                                                                value="Tn"
                                                                                selected={this.state.dtRes.title === "Tn" ? true : false}>Tn
                                                                            </option>
                                                                            <option value="Ny"
                                                                                    selected={this.state.dtRes.title === "Ny" ? true : false}>Ny
                                                                            </option>

                                                                        </Form.Control>
                                                                    </Form.Group>
                                                                    <Form.Group as={Col} controlId="name">
                                                                        {errMsg.name ? (<Fragment><span
                                                                            className="text-error badge badge-danger mb-1">{errMsg.name}</span></Fragment>) : ''}
                                                                        <Form.Control
                                                                            name="name"
                                                                            style={{width: "88%"}}
                                                                            type="text"
                                                                            placeholder="Name"
                                                                            size="sm"
                                                                            autoComplete="off"
                                                                            onChange={this.handleChange.bind(this)}
                                                                            value={this.state.dtRes.name && this.state.dtRes.name}
                                                                        />
                                                                    </Form.Group>
                                                                </Form.Row>

                                                            </td>
                                                            <td width="8%"><strong>Tempat Lahir</strong></td>
                                                            <td width="1%">:</td>
                                                            <td width="28%">
                                                                {errMsg.tpt_lahir ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.tpt_lahir}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    name="tpt_lahir"
                                                                    type="text"
                                                                    placeholder="Tempat Lahir"
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    onChange={this.handleChange.bind(this)}
                                                                    value={this.state.dtRes.ttl && this.state.dtRes.ttl.split(',')[0]}
                                                                />
                                                            </td>

                                                        </tr>
                                                        <tr>

                                                            <td width="8%"><strong>Tanggal lahir</strong></td>
                                                            <td width="1%">:</td>

                                                            <td>
                                                                {errMsg.tgl_lahir ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.tgl_lahir}</span></Fragment>) : ''}
                                                                <Datetime
                                                                    setViewDate={this.state.dtRes.ttl && this.state.dtRes.ttl.split(',')[1] ? moment(this.state.dtRes.ttl.split(',')[1]).format("DD-MM-YYYY") : ''}
                                                                    onChange={this.handleChangeStartDate}
                                                                    timeFormat={false}
                                                                    closeOnSelect={true}
                                                                    inputProps={{
                                                                        value: this.state.dtRes.ttl && this.state.dtRes.ttl.split(',')[1] ? moment(this.state.dtRes.ttl.split(',')[1]).format("DD-MM-YYYY") : '',
                                                                        readOnly: true,
                                                                        autoComplete: "off",
                                                                        placeholder: 'Tanggal Lahir',
                                                                        name: 'tgl_lahir',
                                                                        className: 'form-control form-control-sm'
                                                                    }}
                                                                    renderView={(mode, renderDefault, tgl_lahir) =>
                                                                        this.renderView(mode, renderDefault, 'tgl_lahir')
                                                                    }
                                                                    locale="id" isValidDate={this.state.validSd}
                                                                />
                                                            </td>

                                                            <td><strong>Tipe Identitas</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.tipe_identitas ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.tipe_identitas}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    size="sm"
                                                                    style={{width: "90%"}}
                                                                    autoComplete="off"
                                                                    name="tipe_identitas"
                                                                    as="select"
                                                                    value={this.state.dtRes.tipe_identitas && this.state.dtRes.tipe_identitas}
                                                                    onChange={this.handleChange.bind(this)}
                                                                >
                                                                    <option value="">- Pilih -</option>
                                                                    <option
                                                                        value="ktp"
                                                                        selected={this.state.dtRes.no_ktp ? true : false}>
                                                                        KTP
                                                                    </option>
                                                                    <option value="sim"
                                                                            selected={this.state.dtRes.no_sim ? true : false}>SIM
                                                                    </option>

                                                                </Form.Control>
                                                            </td>
                                                            <td><strong>No.Identitas</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.no_identitas ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.no_identitas}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    name="no_identitas"
                                                                    type="number"
                                                                    placeholder="No.Identitas"
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    onChange={this.handleChange.bind(this)}
                                                                    value={this.state.dtRes.no_identitas && this.state.dtRes.no_identitas}
                                                                />
                                                            </td>

                                                        </tr>


                                                        <tr>

                                                            <td><strong>Agama</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.religion ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.religion}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    name="religion"
                                                                    as="select"
                                                                    value={this.state.dtRes.religion && this.state.dtRes.religion}
                                                                    onChange={this.handleChange.bind(this)}
                                                                >
                                                                    <option value="">- Pilih -</option>
                                                                    <option
                                                                        value="1"
                                                                        selected={this.state.dtRes.religion === "1" ? true : false}>
                                                                        Islam
                                                                    </option>
                                                                    <option value="2"
                                                                            selected={this.state.dtRes.religion === "2" ? true : false}>Kristen
                                                                    </option>
                                                                    <option value="3"
                                                                            selected={this.state.dtRes.religion === "3" ? true : false}>Katholik
                                                                    </option>
                                                                    <option value="4"
                                                                            selected={this.state.dtRes.religion === "4" ? true : false}>Hindu
                                                                    </option>
                                                                    <option value="5"
                                                                            selected={this.state.dtRes.religion === "5" ? true : false}>Buddha
                                                                    </option>
                                                                    <option value="6"
                                                                            selected={this.state.dtRes.religion === "6" ? true : false}>Konghucu
                                                                    </option>
                                                                </Form.Control>
                                                            </td>

                                                            <td><strong>No telpon 1</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.phone_number ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.phone_number}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    name="phone_number"
                                                                    style={{width: "90%"}}
                                                                    type="text"
                                                                    placeholder="No telpon 1"
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    onChange={this.onChangeAlphaNumericInput.bind(this)}
                                                                    value={this.state.dtRes.phone_number && this.state.dtRes.phone_number}
                                                                />
                                                            </td>
                                                            <td><strong>No telpon 2</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.mobile_number ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.mobile_number}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    name="mobile_number"
                                                                    type="text"
                                                                    placeholder="No telpon 2"
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    onChange={this.onChangeAlphaNumericInput.bind(this)}
                                                                    value={this.state.dtRes.mobile_number && this.state.dtRes.mobile_number}
                                                                />
                                                            </td>

                                                        </tr>

                                                        <tr>

                                                            <td><strong>Pendidikan</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.education ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.education}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    name="education"
                                                                    as="select"
                                                                    value={this.state.dtRes.education && this.state.dtRes.education}
                                                                    onChange={this.handleChange.bind(this)}
                                                                >
                                                                    <option value="">- Pilih -</option>
                                                                    <option
                                                                        value="1"
                                                                        selected={this.state.dtRes.education === "1" ? true : false}>
                                                                        SD
                                                                    </option>
                                                                    <option
                                                                        value="2"
                                                                        selected={this.state.dtRes.education === "2" ? true : false}>
                                                                        SMP
                                                                    </option>
                                                                    <option
                                                                        value="3"
                                                                        selected={this.state.dtRes.education === "3" ? true : false}>
                                                                        SMA
                                                                    </option>
                                                                    <option
                                                                        value="4"
                                                                        selected={this.state.dtRes.education === "4" ? true : false}>
                                                                        Sarjana(S1/S2/S3)
                                                                    </option>

                                                                </Form.Control>
                                                            </td>

                                                            <td><strong>Jaminan Kesehatan (Asuransi)</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.guarantee ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.guarantee}</span></Fragment>) : ''}

                                                                <Select
                                                                    size="sm"
                                                                    style={{width: "90%"}}
                                                                    name="guarantee"
                                                                    placeholder="Jaminan Kesehatan (Asuransi)"
                                                                    labelField="label"
                                                                    valueField="label"
                                                                    options={
                                                                        this.state.optionsGuarantee
                                                                    }
                                                                    //  clearable={this.state.dtRes.status_infeksius != null ? true : false}
                                                                    values={this.state.dtRes.guarantee != null ? this.state.dtRes.guarantee : []}
                                                                    keepSelectedInList={true}
                                                                    multi={true}
                                                                    onChange={values => this.setValuesGuarantee(values)}

                                                                >

                                                                </Select>

                                                            </td>
                                                            <td><strong>Aktivitas yang disukai</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.hobby ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.hobby}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    name="hobby"
                                                                    type="text"
                                                                    placeholder="Aktivitas yang disukai"
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    onChange={this.handleChange.bind(this)}
                                                                    value={this.state.dtRes.hobby && this.state.dtRes.hobby}
                                                                />
                                                            </td>

                                                        </tr>

                                                        <tr>

                                                            <td><strong>Pekerjaan</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.job ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.job}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    name="job"
                                                                    as="select"
                                                                    value={this.state.dtRes.job && this.state.dtRes.job}
                                                                    onChange={this.handleChange.bind(this)}
                                                                >
                                                                    <option value="">- Pilih -</option>
                                                                    <option
                                                                        value="Pegawai Negeri"
                                                                        selected={this.state.dtRes.job === "Pegawai Negeri" ? true : false}>
                                                                        Pegawai Negeri
                                                                    </option>
                                                                    <option
                                                                        value="Pegawai Swasta"
                                                                        selected={this.state.dtRes.job === "Pegawai Swasta" ? true : false}>
                                                                        Pegawai Swasta
                                                                    </option>
                                                                    <option
                                                                        value="Wiraswasta"
                                                                        selected={this.state.dtRes.job === "Wiraswasta" ? true : false}>
                                                                        Wiraswasta
                                                                    </option>
                                                                    <option
                                                                        value="Pelajar/Mahasiswa"
                                                                        selected={this.state.dtRes.job === "Pelajar/Mahasiswa" ? true : false}>
                                                                        Pelajar/Mahasiswa
                                                                    </option>
                                                                    <option
                                                                        value="Ibu Rumah Tangga"
                                                                        selected={this.state.dtRes.job === "Ibu Rumah Tangga" ? true : false}>
                                                                        Ibu Rumah Tangga
                                                                    </option>
                                                                    <option
                                                                        value="Guru/Pendidik"
                                                                        selected={this.state.dtRes.job === "Guru/Pendidik" ? true : false}>
                                                                        Guru/Pendidik
                                                                    </option>
                                                                    <option
                                                                        value="Koki/Juru masak"
                                                                        selected={this.state.dtRes.job === "Koki/Juru masak" ? true : false}>
                                                                        Koki/Juru masak
                                                                    </option>
                                                                    <option
                                                                        value="Trader/Investor"
                                                                        selected={this.state.dtRes.job === "Trader/Investor" ? true : false}>
                                                                        Trader/Investor
                                                                    </option>
                                                                    <option
                                                                        value="Broker/Pialang"
                                                                        selected={this.state.dtRes.job === "Broker/Pialang" ? true : false}>
                                                                        Broker/Pialang
                                                                    </option>
                                                                    <option
                                                                        value="Tidak Bekerja"
                                                                        selected={this.state.dtRes.job === "Tidak Bekerja" ? true : false}>
                                                                        Tidak Bekerja
                                                                    </option>
                                                                </Form.Control>

                                                            </td>

                                                            <td><strong>Perusahaan</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.company_name ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.company_name}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    name="company_name"
                                                                    style={{width: "90%"}}
                                                                    type="text"
                                                                    placeholder="Perusahaan"
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    onChange={this.handleChange.bind(this)}
                                                                    value={this.state.dtRes.company_name && this.state.dtRes.company_name}
                                                                />
                                                            </td>
                                                            <td><strong>Alamat Perusahaan</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.company_address ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.company_address}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    name="company_address"
                                                                    type="text"
                                                                    placeholder="Alamat Perusahaan"
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    onChange={this.handleChange.bind(this)}
                                                                    value={this.state.dtRes.company_address && this.state.dtRes.company_address}
                                                                />
                                                            </td>

                                                        </tr>

                                                        <tr>

                                                            <td><strong>Gol.Darah</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.blood_type ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.blood_type}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    name="blood_type"
                                                                    as="select"
                                                                    value={this.state.dtRes.blood_type && this.state.dtRes.blood_type}
                                                                    onChange={this.handleChange.bind(this)}
                                                                >
                                                                    <option value="">- Pilih -</option>
                                                                    <option
                                                                        value="A"
                                                                        selected={this.state.dtRes.blood_type === "A" ? true : false}>
                                                                        A
                                                                    </option>
                                                                    <option
                                                                        value="B"
                                                                        selected={this.state.dtRes.blood_type === "B" ? true : false}>
                                                                        B
                                                                    </option>
                                                                    <option
                                                                        value="O"
                                                                        selected={this.state.dtRes.blood_type === "O" ? true : false}>
                                                                        O
                                                                    </option>
                                                                    <option
                                                                        value="AB"
                                                                        selected={this.state.dtRes.blood_type === "AB" ? true : false}>
                                                                        AB
                                                                    </option>
                                                                </Form.Control>


                                                            </td>

                                                            <td><strong>Rhesus</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.rhesus ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.rhesus}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    size="sm"
                                                                    style={{width: "90%"}}
                                                                    autoComplete="off"
                                                                    name="rhesus"
                                                                    as="select"
                                                                    value={this.state.dtRes.rhesus && this.state.dtRes.rhesus}
                                                                    onChange={this.handleChange.bind(this)}
                                                                >
                                                                    <option value="">- Pilih -</option>
                                                                    <option
                                                                        value="Positif"
                                                                        selected={this.state.dtRes.rhesus === "Positif" ? true : false}>
                                                                        Positif(+)
                                                                    </option>
                                                                    <option
                                                                        value="Negatif"
                                                                        selected={this.state.dtRes.rhesus === "Negatif" ? true : false}>
                                                                        Negatif(-)
                                                                    </option>

                                                                </Form.Control>


                                                            </td>
                                                            <td><strong>Perkawinan</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.perkawinan ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.perkawinan}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    name="perkawinan"
                                                                    as="select"
                                                                    value={this.state.dtRes.perkawinan && this.state.dtRes.perkawinan}
                                                                    onChange={this.handleChange.bind(this)}
                                                                >
                                                                    <option value="">- Pilih -</option>
                                                                    <option value="1"
                                                                            selected={this.state.dtRes.perkawinan === "1" ? true : false}>Lajang

                                                                    </option>
                                                                    <option value="2"
                                                                            selected={this.state.dtRes.perkawinan === "2" ? true : false}>Kawin
                                                                    </option>
                                                                    <option value="3"
                                                                            selected={this.state.dtRes.perkawinan === "3" ? true : false}>Cerai
                                                                        Hidup
                                                                    </option>
                                                                    <option value="4"
                                                                            selected={this.state.dtRes.perkawinan === "4" ? true : false}>Cerai
                                                                        Mati
                                                                    </option>
                                                                </Form.Control>
                                                            </td>

                                                        </tr>

                                                        <tr>


                                                            <td><strong>Kelurahan</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.sub_district ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.sub_district}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    name="sub_district"
                                                                    type="text"
                                                                    placeholder="Kelurahan"
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    onChange={this.handleChange.bind(this)}
                                                                    value={this.state.dtRes.sub_district && this.state.dtRes.sub_district}
                                                                />
                                                            </td>

                                                            <td><strong>RT</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.rt ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.rt}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    name="rt"
                                                                    style={{width: "90%"}}
                                                                    type="text"
                                                                    placeholder="RT"
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    onChange={this.handleChange.bind(this)}
                                                                    value={this.state.dtRes.rt && this.state.dtRes.rt}
                                                                />
                                                            </td>
                                                            <td><strong>RW</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.rw ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.rw}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    name="rw"
                                                                    type="text"
                                                                    placeholder="RW"
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    onChange={this.handleChange.bind(this)}
                                                                    value={this.state.dtRes.rw && this.state.dtRes.rw}
                                                                />
                                                            </td>

                                                        </tr>

                                                        <tr>

                                                            <td><strong>Kecamatan</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.district ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.district}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    name="district"
                                                                    type="text"
                                                                    placeholder="Kecamatan"
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    onChange={this.handleChange.bind(this)}
                                                                    value={this.state.dtRes.district && this.state.dtRes.district}
                                                                />
                                                            </td>

                                                            <td><strong>Alamat</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.address ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.address}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    name="address"
                                                                    type="text"
                                                                    style={{width: "90%"}}
                                                                    placeholder="Alamat"
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    onChange={this.handleChange.bind(this)}
                                                                    value={this.state.dtRes.address && this.state.dtRes.address}
                                                                />
                                                            </td>

                                                            <td><strong>Status Infeksius</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.status_infeksius ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.status_infeksius}</span></Fragment>) : ''}

                                                                <Select
                                                                    name="status_infeksius"
                                                                    placeholder="Select Status Infeksius"
                                                                    labelField="label"
                                                                    valueField="label"
                                                                    options={
                                                                        this.state.options
                                                                    }
                                                                    //  clearable={this.state.dtRes.status_infeksius != null ? true : false}
                                                                    values={this.state.dtRes.status_infeksius != null ? this.state.dtRes.status_infeksius : []}
                                                                    keepSelectedInList={this.state.keepSelectedInList}
                                                                    multi={true}
                                                                    onChange={values => this.setValues(values)}

                                                                >

                                                                </Select>
                                                            </td>


                                                        </tr>
                                                        <tr>

                                                        </tr>
                                                        <tr>
                                                            <td style={{
                                                                "backgroundColor": "rgba(0,0,0,.08)",
                                                                "fontWeight": "bold",
                                                                "fontSize": "16px"
                                                            }} colSpan="9" align="center">Penanggung Jawab
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Nama</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.name_responsible ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.name_responsible}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    name="name_responsible"
                                                                    style={{width: "90%"}}
                                                                    type="text"
                                                                    placeholder="Nama"
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    onChange={this.handleChange.bind(this)}
                                                                    value={this.state.dtRes.name_responsible && this.state.dtRes.name_responsible}
                                                                />
                                                            </td>
                                                            <td><strong>Hubungan</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.relationship ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.relationship}</span></Fragment>) : ''}


                                                                <Form.Control
                                                                    size="sm"
                                                                    style={{width: "90%"}}
                                                                    autoComplete="off"
                                                                    name="relationship"
                                                                    as="select"
                                                                    value={this.state.dtRes.relationship && this.state.dtRes.relationship}
                                                                    onChange={this.handleChange.bind(this)}
                                                                >
                                                                    <option value="">- Pilih -</option>
                                                                    <option
                                                                        value="Orang Tua"
                                                                        selected={this.state.dtRes.relationship === "Orang Tua" ? true : false}>
                                                                        Orang tua
                                                                    </option>
                                                                    <option
                                                                        value="Saudara"
                                                                        selected={this.state.dtRes.relationship === "Saudara" ? true : false}>
                                                                        Saudara
                                                                    </option>
                                                                    <option
                                                                        value="Kerabat"
                                                                        selected={this.state.dtRes.relationship === "Kerabat" ? true : false}>
                                                                        Kerabat
                                                                    </option>

                                                                </Form.Control>
                                                            </td>
                                                            <td><strong>Pekerjaan</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.job_responsible ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.job_responsible}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    name="job_responsible"

                                                                    type="text"
                                                                    placeholder="Pekerjaan"
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    onChange={this.handleChange.bind(this)}
                                                                    value={this.state.dtRes.job_responsible && this.state.dtRes.job_responsible}
                                                                />
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><strong>Phone Number 1</strong></td>
                                                            <td>:</td>
                                                            <td>

                                                                <Form.Control
                                                                    name="phone_number_1"
                                                                    style={{width: "90%"}}
                                                                    type="text"
                                                                    placeholder="Phone Number 1"
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    onChange={this.onChangeAlphaNumericInput.bind(this)}
                                                                    value={this.state.dtRes.phone_number_1 && this.state.dtRes.phone_number_1}
                                                                />
                                                            </td>
                                                            <td><strong>Phone Number 2</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                <Form.Control
                                                                    name="phone_number_2"
                                                                    style={{width: "90%"}}
                                                                    type="text"
                                                                    placeholder="Phone Number 2"
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    onChange={this.onChangeAlphaNumericInput.bind(this)}
                                                                    value={this.state.dtRes.phone_number_2 && this.state.dtRes.phone_number_2}
                                                                />
                                                            </td>
                                                            <td><strong>Mobile Number</strong></td>
                                                            <td>:</td>
                                                            <td>
                                                                {errMsg.mobile_number_responsible ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.mobile_number_responsible}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    name="mobile_number_responsible"

                                                                    type="text"
                                                                    placeholder="Mobile Number"
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    onChange={this.onChangeAlphaNumericInput.bind(this)}
                                                                    value={this.state.dtRes.mobile_number_responsible && this.state.dtRes.mobile_number_responsible}
                                                                />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Alamat</strong></td>
                                                            <td>:</td>
                                                            <td colSpan={7}>
                                                                {errMsg.address_responsible ? (<Fragment><span
                                                                    className="text-error badge badge-danger mb-1">{errMsg.address_responsible}</span></Fragment>) : ''}
                                                                <Form.Control
                                                                    name="address_responsible"

                                                                    type="text"
                                                                    placeholder="Alamat"
                                                                    size="sm"
                                                                    autoComplete="off"
                                                                    onChange={this.handleChange.bind(this)}
                                                                    value={this.state.dtRes.address_responsible && this.state.dtRes.address_responsible}
                                                                />
                                                            </td>

                                                        </tr>
                                                        {this.state.dtRes.user_id &&
                                                            <tr>
                                                                <td style={{
                                                                    "backgroundColor": "rgba(0,0,0,.08)",
                                                                    "fontWeight": "bold",
                                                                    "fontSize": "16px"
                                                                }} colSpan="9" align="center">List Dokumen
                                                                </td>
                                                            </tr>}

                                                        </tbody>
                                                    </table>
                                                    {this.state.dtRes.user_id &&
                                                        <div className="row mt-3">
                                                            <div className="col-3">
                                                                <Figure
                                                                    key={this.state.dtRes.url_image_routine_hematology ? this.state.dtRes.url_image_routine_hematology : noImg}
                                                                    className="img-pasien-pic"
                                                                    onClick={(e) => this.url_image_routine_hematology.click()}
                                                                    style={{
                                                                        "marginRight": "10px",
                                                                        "marginLeft": "5px",
                                                                        "textAlign": "center",
                                                                        "fontWeight": "bold",
                                                                        alignContent: "center"
                                                                    }}>
                                                                    <Image
                                                                        src={this.state.dtRes.imgUpload_url_image_routine_hematology ? this.state.dtRes.imgUpload_url_image_routine_hematology : noImg}
                                                                        thumbnail
                                                                        style={{width: "80%", minHeight: "310px"}}/>
                                                                    <Figure.Caption>
                                                                        Hasil Lab Rutin Hematology
                                                                    </Figure.Caption>
                                                                    <Form.File
                                                                        setfieldvalue={this.state.dtRes.url_image_routine_hematology ? this.state.dtRes.url_image_routine_hematology : ""}
                                                                        onChange={this.handleChangeImage}
                                                                        size="sm"
                                                                        name="url_image_routine_hematology"
                                                                        style={{display: "none"}}
                                                                        // accept=".zip,.rar"
                                                                        ref={(ref) => this.url_image_routine_hematology = ref}
                                                                    />
                                                                </Figure>
                                                            </div>

                                                            <div className="col-3">
                                                                <Figure
                                                                    key={this.state.dtRes.url_image_ureum_creatinine_hbsag ? this.state.dtRes.url_image_ureum_creatinine_hbsag : noImg}
                                                                    className="img-pasien-pic"
                                                                    onClick={(e) => this.url_image_ureum_creatinine_hbsag.click()}
                                                                    style={{
                                                                        "marginRight": "10px",
                                                                        "marginLeft": "5px",
                                                                        "textAlign": "center",
                                                                        "fontWeight": "bold",
                                                                        alignContent: "center"
                                                                    }}>
                                                                    <Image
                                                                        src={this.state.dtRes.imgUpload_url_image_ureum_creatinine_hbsag ? this.state.dtRes.imgUpload_url_image_ureum_creatinine_hbsag : noImg}
                                                                        thumbnail
                                                                        style={{width: "80%", minHeight: "310px"}}/>
                                                                    <Figure.Caption>
                                                                        Hasil Lab Ureum/Kreatinnin/HBSAG
                                                                    </Figure.Caption>
                                                                    <Form.File
                                                                        setfieldvalue={this.state.dtRes.url_image_ureum_creatinine_hbsag ? this.state.dtRes.url_image_ureum_creatinine_hbsag : ""}
                                                                        onChange={this.handleChangeImage}
                                                                        size="sm"
                                                                        name="url_image_ureum_creatinine_hbsag"
                                                                        style={{display: "none"}}
                                                                        // accept=".zip,.rar"
                                                                        ref={(ref) => this.url_image_ureum_creatinine_hbsag = ref}
                                                                    />
                                                                </Figure>
                                                            </div>


                                                            <div className="col-3">
                                                                <Figure
                                                                    key={this.state.dtRes.url_image_anti_hiv ? this.state.dtRes.url_image_anti_hiv : noImg}
                                                                    className="img-pasien-pic"
                                                                    onClick={(e) => this.url_image_anti_hiv.click()}
                                                                    style={{
                                                                        "marginRight": "10px",
                                                                        "marginLeft": "5px",
                                                                        "textAlign": "center",
                                                                        "fontWeight": "bold",
                                                                        alignContent: "center"
                                                                    }}>
                                                                    <Image
                                                                        src={this.state.dtRes.imgUpload_url_image_anti_hiv ? this.state.dtRes.imgUpload_url_image_anti_hiv : noImg}
                                                                        thumbnail
                                                                        style={{width: "80%", minHeight: "310px"}}/>
                                                                    <Figure.Caption>
                                                                        Anti HIV
                                                                    </Figure.Caption>
                                                                    <Form.File
                                                                        setfieldvalue={this.state.dtRes.url_image_anti_hiv ? this.state.dtRes.url_image_anti_hiv : ""}
                                                                        onChange={this.handleChangeImage}
                                                                        size="sm"
                                                                        name="url_image_anti_hiv"
                                                                        style={{display: "none"}}
                                                                        // accept=".zip,.rar"
                                                                        ref={(ref) => this.url_image_anti_hiv = ref}
                                                                    />
                                                                </Figure>
                                                            </div>


                                                            <div className="col-3">
                                                                <Figure
                                                                    key={this.state.dtRes.url_image_anti_hcv ? this.state.dtRes.url_image_anti_hcv : noImg}
                                                                    className="img-pasien-pic"
                                                                    onClick={(e) => this.url_image_anti_hcv.click()}
                                                                    style={{
                                                                        "marginRight": "10px",
                                                                        "marginLeft": "5px",
                                                                        "textAlign": "center",
                                                                        "fontWeight": "bold",
                                                                        alignContent: "center"
                                                                    }}>
                                                                    <Image
                                                                        src={this.state.dtRes.imgUpload_url_image_anti_hcv ? this.state.dtRes.imgUpload_url_image_anti_hcv : noImg}
                                                                        thumbnail
                                                                        style={{width: "80%", minHeight: "310px"}}/>
                                                                    <Figure.Caption>
                                                                        Anti HCV
                                                                    </Figure.Caption>
                                                                    <Form.File
                                                                        setfieldvalue={this.state.dtRes.url_image_anti_hcv ? this.state.dtRes.url_image_anti_hcv : ""}
                                                                        onChange={this.handleChangeImage}
                                                                        size="sm"
                                                                        name="url_image_anti_hcv"
                                                                        style={{display: "none"}}
                                                                        // accept=".zip,.rar"
                                                                        ref={(ref) => this.url_image_anti_hcv = ref}
                                                                    />
                                                                </Figure>

                                                            </div>

                                                            <div className="col-3">
                                                                <Figure
                                                                    key={this.state.dtRes.url_image_pcr_or_antigen ? this.state.dtRes.url_image_pcr_or_antigen : noImg}
                                                                    className="img-pasien-pic"
                                                                    onClick={(e) => this.url_image_pcr_or_antigen.click()}
                                                                    style={{
                                                                        "marginRight": "10px",
                                                                        "marginLeft": "5px",
                                                                        "textAlign": "center",
                                                                        "fontWeight": "bold",
                                                                        alignContent: "center"
                                                                    }}>
                                                                    <Image
                                                                        src={this.state.dtRes.imgUpload_url_image_pcr_or_antigen ? this.state.dtRes.imgUpload_url_image_pcr_or_antigen : noImg}
                                                                        thumbnail
                                                                        style={{width: "80%", minHeight: "310px"}}/>
                                                                    <Figure.Caption>
                                                                        PCR or Antigen
                                                                    </Figure.Caption>
                                                                    <Form.File
                                                                        setfieldvalue={this.state.dtRes.url_image_pcr_or_antigen ? this.state.dtRes.url_image_pcr_or_antigen : ""}
                                                                        onChange={this.handleChangeImage}
                                                                        size="sm"
                                                                        name="url_image_pcr_or_antigen"
                                                                        style={{display: "none"}}
                                                                        // accept=".zip,.rar"
                                                                        ref={(ref) => this.url_image_pcr_or_antigen = ref}
                                                                    />
                                                                </Figure>

                                                            </div>

                                                            <div className="col-3">
                                                                <Figure
                                                                    key={this.state.dtRes.url_image_rujukan_faskes_from ? this.state.dtRes.url_image_rujukan_faskes_from : noImg}
                                                                    className="img-pasien-pic"
                                                                    onClick={(e) => this.url_image_rujukan_faskes_from.click()}
                                                                    style={{
                                                                        "marginRight": "10px",
                                                                        "marginLeft": "5px",
                                                                        "textAlign": "center",
                                                                        "fontWeight": "bold",
                                                                        alignContent: "center"
                                                                    }}>
                                                                    <Image
                                                                        src={this.state.dtRes.imgUpload_url_image_rujukan_faskes_from ? this.state.dtRes.imgUpload_url_image_rujukan_faskes_from : noImg}
                                                                        thumbnail
                                                                        style={{width: "80%", minHeight: "310px"}}/>
                                                                    <Figure.Caption>
                                                                        Surat Travelling HD
                                                                    </Figure.Caption>
                                                                    <Form.File
                                                                        setfieldvalue={this.state.dtRes.url_image_rujukan_faskes_from ? this.state.dtRes.url_image_rujukan_faskes_from : ""}
                                                                        onChange={this.handleChangeImage}
                                                                        size="sm"
                                                                        name="url_image_rujukan_faskes_from"
                                                                        style={{display: "none"}}
                                                                        // accept=".zip,.rar"
                                                                        ref={(ref) => this.url_image_rujukan_faskes_from = ref}
                                                                    />
                                                                </Figure>


                                                            </div>

                                                            <div className="col-3">
                                                                <Figure
                                                                    key={this.state.dtRes.url_image_rujukan_faskes_to ? this.state.dtRes.url_image_rujukan_faskes_to : noImg}
                                                                    className="img-pasien-pic"
                                                                    onClick={(e) => this.url_image_rujukan_faskes_to.click()}
                                                                    style={{
                                                                        "marginRight": "10px",
                                                                        "marginLeft": "5px",
                                                                        "textAlign": "center",
                                                                        "fontWeight": "bold",
                                                                        alignContent: "center"
                                                                    }}>
                                                                    <Image
                                                                        src={this.state.dtRes.imgUpload_url_image_rujukan_faskes_to ? this.state.dtRes.imgUpload_url_image_rujukan_faskes_to : noImg}
                                                                        thumbnail
                                                                        style={{width: "80%", minHeight: "310px"}}/>
                                                                    <Figure.Caption>
                                                                        Rujukan Faskes tujuan
                                                                    </Figure.Caption>
                                                                    <Form.File
                                                                        setfieldvalue={this.state.dtRes.url_image_rujukan_faskes_to ? this.state.dtRes.url_image_rujukan_faskes_to : ""}
                                                                        onChange={this.handleChangeImage}
                                                                        size="sm"
                                                                        name="url_image_rujukan_faskes_to"
                                                                        style={{display: "none"}}
                                                                        // accept=".zip,.rar"
                                                                        ref={(ref) => this.url_image_rujukan_faskes_to = ref}
                                                                    />
                                                                </Figure>

                                                            </div>

                                                            <div className="col-3">
                                                                <Figure
                                                                    key={this.state.dtRes.url_image_bpjs ? this.state.dtRes.url_image_bpjs : noImg}
                                                                    className="img-pasien-pic"
                                                                    onClick={(e) => this.url_image_bpjs.click()}
                                                                    style={{
                                                                        "marginRight": "10px",
                                                                        "marginLeft": "5px",
                                                                        "textAlign": "center",
                                                                        "fontWeight": "bold",
                                                                        alignContent: "center"
                                                                    }}>
                                                                    <Image
                                                                        src={this.state.dtRes.imgUpload_url_image_bpjs ? this.state.dtRes.imgUpload_url_image_bpjs : noImg}
                                                                        thumbnail
                                                                        style={{width: "80%", minHeight: "310px"}}/>
                                                                    <Figure.Caption>
                                                                        Kartu BPJS
                                                                    </Figure.Caption>
                                                                    <Form.File
                                                                        setfieldvalue={this.state.dtRes.url_image_bpjs ? this.state.dtRes.url_image_bpjs : ""}
                                                                        onChange={this.handleChangeImage}
                                                                        size="sm"
                                                                        name="url_image_bpjs"
                                                                        style={{display: "none"}}
                                                                        // accept=".zip,.rar"
                                                                        ref={(ref) => this.url_image_bpjs = ref}
                                                                    />
                                                                </Figure>
                                                            </div>

                                                            <div className="col-3">
                                                                <Figure
                                                                    key={this.state.dtRes.url_image_ktp ? this.state.dtRes.url_image_ktp : noImg}
                                                                    className="img-pasien-pic"
                                                                    onClick={(e) => this.url_image_ktp.click()}
                                                                    style={{
                                                                        "marginRight": "10px",
                                                                        "marginLeft": "5px",
                                                                        "textAlign": "center",
                                                                        "fontWeight": "bold",
                                                                        alignContent: "center"
                                                                    }}>
                                                                    <Image
                                                                        src={this.state.dtRes.imgUpload_url_image_ktp ? this.state.dtRes.imgUpload_url_image_ktp : noImg}
                                                                        thumbnail
                                                                        style={{width: "80%", minHeight: "310px"}}/>
                                                                    <Figure.Caption>
                                                                        KTP
                                                                    </Figure.Caption>
                                                                    <Form.File
                                                                        setfieldvalue={this.state.dtRes.url_image_ktp ? this.state.dtRes.url_image_ktp : ""}
                                                                        onChange={this.handleChangeImage}
                                                                        size="sm"
                                                                        name="url_image_ktp"
                                                                        style={{display: "none"}}
                                                                        // accept=".zip,.rar"
                                                                        ref={(ref) => this.url_image_ktp = ref}
                                                                    />
                                                                </Figure>
                                                            </div>

                                                            <div className="col-3">
                                                                <Figure
                                                                    key={this.state.dtRes.url_image_kk ? this.state.dtRes.url_image_kk : noImg}
                                                                    className="img-pasien-pic"
                                                                    onClick={(e) => this.url_image_kk.click()}
                                                                    style={{
                                                                        "marginRight": "10px",
                                                                        "marginLeft": "5px",
                                                                        "textAlign": "center",
                                                                        "fontWeight": "bold",
                                                                        alignContent: "center"
                                                                    }}>
                                                                    <Image
                                                                        src={this.state.dtRes.imgUpload_url_image_kk ? this.state.dtRes.imgUpload_url_image_kk : noImg}
                                                                        thumbnail
                                                                        style={{width: "80%", minHeight: "310px"}}/>
                                                                    <Figure.Caption>
                                                                        KK
                                                                    </Figure.Caption>
                                                                    <Form.File
                                                                        setfieldvalue={this.state.dtRes.url_image_kk ? this.state.dtRes.url_image_kk : ""}
                                                                        onChange={this.handleChangeImage}
                                                                        size="sm"
                                                                        name="url_image_kk"
                                                                        style={{display: "none"}}
                                                                        // accept=".zip,.rar"
                                                                        ref={(ref) => this.url_image_kk = ref}
                                                                    />
                                                                </Figure>
                                                            </div>

                                                            <div className="col-3">
                                                                <Figure
                                                                    key={this.state.dtRes.url_image_syarat_tatib ? this.state.dtRes.url_image_syarat_tatib : noImg}
                                                                    className="img-pasien-pic"
                                                                    onClick={(e) => this.url_image_syarat_tatib.click()}
                                                                    style={{
                                                                        "marginRight": "10px",
                                                                        "marginLeft": "5px",
                                                                        "textAlign": "center",
                                                                        "fontWeight": "bold",
                                                                        alignContent: "center"
                                                                    }}>


                                                                    {
                                                                        this.state.dtRes.imgUpload_url_image_syarat_tatib != null ?
                                                                            this.state.dtRes.imgUpload_url_image_syarat_tatib.includes(".pdf") ?
                                                                                <Image src={noImgs} thumbnail
                                                                                       style={{width: "80%"}}/>
                                                                                // <a href={this.state.dtRes.imgUpload_url_image_syarat_tatib} target="_blank">
                                                                                //     <Image src={noImgs} thumbnail style={{width: "80%"}}/>
                                                                                // </a>
                                                                                :
                                                                                <Image
                                                                                    src={this.state.dtRes.imgUpload_url_image_syarat_tatib ? this.state.dtRes.imgUpload_url_image_syarat_tatib : noImg}
                                                                                    thumbnail
                                                                                    style={{
                                                                                        width: "80%",
                                                                                        minHeight: "310px"
                                                                                    }}/>

                                                                            :
                                                                            <Image
                                                                                src={this.state.dtRes.imgUpload_url_image_syarat_tatib ? this.state.dtRes.imgUpload_url_image_syarat_tatib : noImg}
                                                                                thumbnail
                                                                                style={{
                                                                                    width: "80%",
                                                                                    minHeight: "310px"
                                                                                }}/>

                                                                    }

                                                                    <Figure.Caption>
                                                                        Syarat Tata Tertib
                                                                    </Figure.Caption>
                                                                    <Form.File
                                                                        setfieldvalue={this.state.dtRes.url_image_syarat_tatib ? this.state.dtRes.url_image_syarat_tatib : ""}
                                                                        onChange={this.handleChangeImage}
                                                                        size="sm"
                                                                        name="url_image_syarat_tatib"
                                                                        style={{display: "none"}}
                                                                        // accept=".zip,.rar"
                                                                        ref={(ref) => this.url_image_syarat_tatib = ref}
                                                                    />
                                                                </Figure>
                                                            </div>

                                                        </div>
                                                    }
                                                </div>
                                                <div className="card-footer clearfix">
                                                    <button type="button" onClick={() => this.props.history.goBack()}
                                                            className="btn btn-flat btn-danger btn-sm">Back
                                                    </button>

                                                    <button type="button" onClick={this.handleSubmit.bind(this)}
                                                            style={{marginLeft: 3}}
                                                            className="btn bnt-flat btn-success btn-sm">Save
                                                    </button>
                                                </div>


                                            </div>
                                        </Form>
                                    )}
                                </div>

                            </div>
                        </div>
                    </section>

                    {this.props.showFormSuccess ? (<AppSwalSuccess
                        show={this.props.showFormSuccess}
                        title={<div dangerouslySetInnerHTML={{__html: this.props.contentMsg}}/>}
                        type={this.props.tipeSWAL}
                        handleClose={this.closeSwal.bind(this)}>

                    </AppSwalSuccess>) : ''}


                </div>
                <div>

                </div>

            </>


        )
    }
}

const StyledSelect = styled(Select)`
    ${({dropdownRenderer}) =>
    dropdownRenderer &&
    `
		.react-dropdown-select-dropdown {
			overflow: initial;
		}
	`}
`;

const mapStateToProps = (state) => ({
    contentMsg: state.pasiens.contentMsg || null,
    showFormSuccess: state.pasiens.showFormSuccess,
    tipeSWAL: state.pasiens.tipeSWAL,
    errorPriority: state.pasiens.errorPriority || null,
    user: state.main.currentUser,
});

const mapDispatchToPros = (dispatch) => {
    return {
        onAdd: (param) => {
            dispatch(addData(param));
        },
        onUpload: (param) => {
            dispatch(uplodDokumen(param));

        },
        closeSwal: () => {
            dispatch(closeForm());
        },
    }
}
export default connect(mapStateToProps, mapDispatchToPros)(PasienForm);