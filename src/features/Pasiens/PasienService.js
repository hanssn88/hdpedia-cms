import axios from "axios";

const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

class PasienService {
    postData(param, action) {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        switch (action) {
            case "VIEW_DETAIL":
                return axios.post(API_URL + "/detail_patient", param, {
                    headers: {
                        'Authorization': 'Bearer ' + token
                    }
                })
            case "VIEW_DETAIL_FORM":
                return axios.post(API_URL + "/dokter/detail_additional_form", param, {
                    headers: {
                        'Authorization': 'Bearer ' + token
                    }
                })
            case "VIEW_DETAIL_TRAVELING":
                return axios.post(API_URL + "/traveling_hd_detail", param, {
                    headers: {
                        'Authorization': 'Bearer ' + token
                    }
                })
            default:
                return axios.post(API_URL + "/detail_patient", param)
        }
    }

}

export default new PasienService()