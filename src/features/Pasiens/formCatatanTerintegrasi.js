import axios from "axios";
import React, {useEffect, useState} from "react";
import {Form} from "react-bootstrap";
import {Placeholder} from "rsuite";
import Swal from "sweetalert2";


const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;
export const FormCatatanTerintegrasi = () => {

    const [dtas, setDatas] = useState(null);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {

        async function setData() {

            setOnChange("dokter_id", sessionStorage.getItem('idOP'))
            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);
        }

        setData();


    }, [])


    const setOnChange = async (key, val) => {
        await setDatas({...dtas, [key]: val});


    };


    const handleSubmit = async (e) => {
        const {id, created_at, updated_at, ...params} = dtas;
        if (typeof params.dokter_id === "undefined" || params.dokter_id === "")
            params.dokter_id = sessionStorage.getItem('idOP');
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/dokter/catatan_terintegrasi', params, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = "";
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa;
                if (data.err_code === '00') {

                    return Swal.fire({
                        title: 'Informasi',
                        text: "Success add Catatan Terintegrasi",
                        icon: 'success',
                        confirmButtonText: 'Ok',
                        allowOutsideClick: true
                    })
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return Swal.fire({
                        title: "Error",
                        text: data.err_msg,
                        icon: 'error',
                        confirmButtonText: 'Ok',
                        allowOutsideClick: true
                    })
                }
            }

        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    const {Paragraph} = Placeholder;

    return (
        <div>
            {isLoading ? (
                <div className="card shadow-lg">
                    <div className="card-body">
                        <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                   style={{marginTop: 30}}/>
                    </div>
                </div>

            ) : (
                <Form>
                    <h3>Form Catatan Terintegrasi</h3>
                    <table className="table table-condensed ">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="9" align="center">
                                Data Catatan Terintegrasi
                            </td>
                        </tr>


                        <tr>
                            <td width="8%">
                                <strong>Nefrolog/SpPD(HD)</strong>
                            </td>
                            <td width="1%">:</td>
                            <td width="28%" colSpan="7">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="nefrolog"
                                    id="nefrolog"
                                    type="text"

                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Nefrolog/SpPD(HD)"/>
                            </td>

                        </tr>


                        <tr>
                            <td width="8%">
                                <strong>Tanggal/Jam</strong>
                            </td>
                            <td width="1%">:</td>
                            <td width="28%" colSpan="7">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="datetime"
                                    id="datetime"
                                    type="text"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Tanggal/Jam"/>
                            </td>

                        </tr>

                        <tr>
                            <td width="8%">
                                <strong>Catatan Terintegrasi</strong>
                            </td>
                            <td width="1%">:</td>
                            <td width="28%" colSpan="7">
                                <Form.Control
                                    style={{"overflow-y": "scroll"}}
                                    as="textarea" rows={3}
                                    size="sm"
                                    autoComplete="off"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    name="catatan_terintegrasi"
                                    id="catatan_terintegrasi"
                                    type="text"
                                    placeholder="Catatan Terintegrasi"/>
                            </td>

                        </tr>


                        </tbody>
                    </table>
                    <button type="button" onClick={(e) => handleSubmit()}
                            className="btn btn-success btn-sm">Submit Form Data Catatan Terintegrasi
                    </button>
                </Form>)
            }
        </div>
    )
}
