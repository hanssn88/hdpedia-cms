import axios from "axios";
import moment from 'moment';
import "moment/locale/id";
import React, {useEffect, useState} from "react";
import {Form} from "react-bootstrap";
import Datetime from "react-datetime";
import "react-datetime/css/react-datetime.css";
import {Placeholder} from "rsuite";
import Swal from 'sweetalert2';

const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

export const FormHasilRekapitulasi = (data) => {
    const [datas, getDatas] = useState(data?.data);
    const [dtas, setDatas] = useState(null);
    const [errMsg, setErrMsg] = useState(null);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        async function getData() {

            await getDatas(data?.data);

            // const tgl = datas.ttl.split(',');
            // setOnChange('tgl_lahir',tgl[1]);


        }

        async function setData() {

            await setDatas(data?.data);
            const tgl = datas.ttl.split(',');

            const paramDatas = {
                "tgl_lahir": tgl[1],
                "tgl_masuk": "",
                "hemoglobin": "",
                "ureum": "",
                "kreatinin": "",
                "albumin": "",
                "natrium": "",
                "kalium": "",
                "chlorida": "",
                "calcium_total": "",
                "calcium_ion": "",
                "fosfor_anorganic": "",
                "hbsag": "",
                "anti_hbs": "",
                "anti_hcv": "",
                "anti_HIV": "",
            };
            await setDatas(paramDatas);

            const paramValid = {
                "tgl_masuk": "",
                "hemoglobin": "",
                "ureum": "",
                "kreatinin": "",
                "albumin": "",
                "natrium": "",
                "kalium": "",
                "chlorida": "",
                "calcium_total": "",
                "calcium_ion": "",
                "fosfor_anorganic": "",
                "hbsag": "",
                "anti_hbs": "",
                "anti_hcv": "",
                "anti_HIV": "",
            };

            await setErrMsg(paramValid);

            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);

        }


        setData();
        getData();


    }, [data])


    const setOnChange = async (key, val) => {
        if (key === 'tgl_masuk') {
            if (val == '') {
                await setDatas({...dtas, [key]: val});
            } else {
                val = moment(new Date(val)).format('YYYY-MM-DD');
                await setDatas({...dtas, [key]: val});
            }

        } else {
            await setDatas({...dtas, [key]: val});
        }

        // const dt = {key: "patient_id", value: this.datas.patient_id};


    };


    const handleValid = async (e) => {


        const paramValid = {
            "tgl_masuk": dtas.tgl_masuk == '' ? "Required" : "",
            "hemoglobin": dtas.hemoglobin == '' ? "Required" : "",
            "kreatinin": dtas.kreatinin == '' ? "Required" : "",
            "ureum": dtas.ureum == '' ? "Required" : "",
            "albumin": dtas.albumin == '' ? "Required" : "",
            "natrium": dtas.natrium == '' ? "Required" : "",
            "kalium": dtas.kalium == '' ? "Required" : "",
            "chlorida": dtas.chlorida == '' ? "Required" : "",
            "calcium_total": dtas.calcium_total == '' ? "Required" : "",
            "calcium_ion": dtas.calcium_ion == '' ? "Required" : "",
            "fosfor_anorganic": dtas.fosfor_anorganic == '' ? "Required" : "",
            "hbsag": dtas.hbsag == '' ? "Required" : "",
            "anti_hbs": dtas.anti_hbs == '' ? "Required" : "",
            "anti_hcv": dtas.anti_hcv == '' ? "Required" : "",
            "anti_HIV": dtas.anti_HIV == '' ? "Required" : "",
        };

        await setErrMsg(paramValid);

        if (dtas.tgl_masuk != '' && dtas.hemoglobin != '' && dtas.kreatinin != ''
            && dtas.albumin != '' && dtas.natrium != '' && dtas.kalium != ''
            && dtas.chlorida != '' && dtas.calcium_total != '' && dtas.calcium_ion != ''
            && dtas.fosfor_anorganic != '' && dtas.hbsag != '' && dtas.anti_hbs != ''
            && dtas.anti_hcv != '' && dtas.anti_HIV != '' && dtas.ureum != ''
        ) {

            handleSubmit();

        } else {

            console.error('Invalid Form');
        }

    }

    const handleSubmit = async (e) => {
        const {id, created_at, updated_at, ...params} = dtas;
        if (typeof params.patient_id === "undefined" || params.patient_id === "")
            params.patient_id = sessionStorage.getItem('idPasienHDPedia');
        params.dokter_id = sessionStorage.getItem('idOP');
        const tgl = datas.ttl.split(',');
        params.tgl_lahir = tgl[1];
        params.golongan_darah = datas.blood_type;
        params.rhesus = datas.rhesus;

        if (datas.rm_number != null) {
            params.no_rm = datas.rm_number;
        } else {
            params.no_rm = "null";
        }
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/dokter/hasil_rekapitulasi_laboratorium', params, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });

            let data = "";
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa;
                if (data.err_code === '00') {

                    return Swal.fire({
                        title: 'Informasi',
                        text: "Success add Hasil Rekapitulasi Laboratorium",
                        icon: 'success',
                        confirmButtonText: 'Ok',
                        allowOutsideClick: true
                    })
                } else {
                    res = {
                        err_msg: data.err_msg
                    }

                    return Swal.fire({
                        title: 'Error',
                        text: res.err_msg,
                        icon: 'error',
                        confirmButtonText: 'Ok',
                        allowOutsideClick: true
                    })
                }
            }
            // return Swal.fire({
            //     title: 'Informasi',
            //     text: "Success add resep",
            //     icon: 'success',
            //     confirmButtonText: 'Ok',
            //     allowOutsideClick: true
            // })
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };


    const {Paragraph} = Placeholder;
    return (
        <div>
            {isLoading ? (
                <div className="card shadow-lg">
                    <div className="card-body">
                        <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                   style={{marginTop: 30}}/>
                    </div>
                </div>

            ) : (
                <Form>
                    <h3>Form Hasil Rekapitulasi Laboratorium</h3>
                    <table className="table table-condensed table-form-askep">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="12" align="center">
                                Data Hasil Rekapitulasi Laboratorium
                            </td>
                        </tr>
                        <tr>
                            <td width="10%"><strong>Pasien</strong></td>
                            <td width="1%">:</td>
                            <td width="14%">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="name"
                                    type="text"
                                    disabled="true"
                                    value={datas ? datas.name : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                />

                            </td>
                            <td width="5%"><strong>No.RM</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">

                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="no_rm"
                                    type="text"
                                    disabled="true"
                                    value={datas ? datas.rm_number : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}/>
                            </td>


                        </tr>
                        <tr>
                            <td width="10%"><strong>Tgl Lahir</strong></td>
                            <td width="1%">:</td>
                            <td width="14%">
                                {/* {dtas?.tgl_lahir} */}
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="tgl_lahir"
                                    type="text"
                                    disabled="true"
                                    value={datas ? dtas.tgl_lahir : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}/>

                            </td>
                            <td width="5%"><strong>Golongan Darah</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="anti_golongan_darahhbs"
                                    disabled="true"
                                    value={datas ? datas.blood_type : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                >


                                </Form.Control>
                            </td>


                        </tr>

                        <tr>
                            <td width="10%"><strong>Rh +/-</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    name="rhesus"
                                    disabled="true"
                                    value={datas ? datas.rhesus : ''}
                                    type="text"
                                    placeholder="Rh +/-"/>

                                {/* <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="rhesus"
                                    as="select"
                                    value={datas ? datas.ekstremitas : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                >
                                    <option value="">- Pilih -</option>
                                    <option value="Positif">Positif</option>
                                    <option value="Negatif">Negatif</option>
                                    
                               
                                </Form.Control> */}

                            </td>
                            <td width="5%"><strong>Tanggal Masuk</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">
                                {errMsg.tgl_masuk === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.tgl_masuk}</span></React.Fragment>) : ''}
                                <Datetime

                                    onChange={e => setOnChange("tgl_masuk", e)}
                                    timeFormat={false}
                                    closeOnSelect={true}
                                    inputProps={{
                                        readOnly: true,
                                        autoComplete: "off",
                                        placeholder: 'Tanggal Masuk',
                                        name: 'tgl_masuk',
                                        className: 'form-control form-control-sm'
                                    }}
                                />
                            </td>


                        </tr>

                        <tr>
                            <td width="10%"><strong>Hemoglobin</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">
                                {errMsg.hemoglobin === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.hemoglobin}</span></React.Fragment>) : ''}
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="hemoglobin"
                                    id="hemoglobin"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="number"
                                    placeholder="Hemoglobin"/>

                            </td>
                            <td width="5%"><strong>Ureum</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">
                                {errMsg.ureum === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.ureum}</span></React.Fragment>) : ''}

                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="ureum"
                                    id="ureum"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Ureum"/>
                            </td>


                        </tr>

                        <tr>
                            <td width="10%"><strong>Kreatinin</strong></td>
                            <td width="1%">:</td>
                            <td width="14%">
                                {errMsg.kreatinin === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.kreatinin}</span></React.Fragment>) : ''}
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="kreatinin"
                                    id="kreatinin"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Kreatinin"/>

                            </td>
                            <td width="5%"><strong>Albumin</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">
                                {errMsg.albumin === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.albumin}</span></React.Fragment>) : ''}

                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="albumin"
                                    id="albumin"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Albumin"/>
                            </td>


                        </tr>

                        <tr>
                            <td width="10%"><strong>Natrium</strong></td>
                            <td width="1%">:</td>
                            <td width="14%">
                                {errMsg.natrium === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.natrium}</span></React.Fragment>) : ''}

                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="natrium"
                                    id="natrium"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Natrium"/>

                            </td>
                            <td width="5%"><strong>Kalium</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">
                                {errMsg.kalium === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.kalium}</span></React.Fragment>) : ''}

                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="kalium"
                                    id="kalium"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Kalium"/>
                            </td>


                        </tr>

                        <tr>
                            <td width="10%"><strong>Chlorida</strong></td>
                            <td width="1%">:</td>
                            <td width="14%">
                                {errMsg.chlorida === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.chlorida}</span></React.Fragment>) : ''}

                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="chlorida"
                                    id="chlorida"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Chlorida"/>

                            </td>
                            <td width="5%"><strong>Calcium total</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">
                                {errMsg.calcium_total === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.calcium_total}</span></React.Fragment>) : ''}

                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="calcium_total"
                                    id="calcium_total"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Calcium total"/>
                            </td>


                        </tr>

                        <tr>
                            <td width="10%"><strong>Calcium Ion</strong></td>
                            <td width="1%">:</td>
                            <td width="14%">
                                {errMsg.calcium_ion === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.calcium_ion}</span></React.Fragment>) : ''}

                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="calcium_ion"
                                    id="calcium_ion"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Calcium Ion"/>

                            </td>
                            <td width="5%"><strong>Fosfor Anorganic</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">
                                {errMsg.fosfor_anorganic === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.fosfor_anorganic}</span></React.Fragment>) : ''}

                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="fosfor_anorganic"
                                    id="fosfor_anorganic"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Fosfor Anorganic"/>
                            </td>


                        </tr>

                        <tr>
                            <td width="10%"><strong>HBsAg</strong></td>
                            <td width="1%">:</td>
                            <td width="14%">
                                {errMsg.hbsag === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.hbsag}</span></React.Fragment>) : ''}

                                <Form.Control

                                    size="sm"
                                    autoComplete="off"
                                    name="hbsag"
                                    as="select"
                                    value={datas ? datas.ekstremitas : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                >
                                    <option value="">- Pilih -</option>
                                    <option value="Positif">Positif</option>
                                    <option value="Negatif">Negatif</option>

                                </Form.Control>

                            </td>
                            <td width="5%"><strong>Anti HBs</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">
                                {errMsg.anti_hbs === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.anti_hbs}</span></React.Fragment>) : ''}

                                <Form.Control

                                    size="sm"
                                    autoComplete="off"
                                    name="anti_hbs"
                                    as="select"
                                    value={datas ? datas.ekstremitas : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                >
                                    <option value="">- Pilih -</option>
                                    <option value="Positif">Positif</option>
                                    <option value="Negatif">Negatif</option>

                                </Form.Control>
                            </td>


                        </tr>

                        <tr>
                            <td width="10%"><strong>Anti HCV</strong></td>
                            <td width="1%">:</td>
                            <td width="14%">
                                {errMsg.anti_hcv === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.anti_hcv}</span></React.Fragment>) : ''}

                                <Form.Control

                                    size="sm"
                                    autoComplete="off"
                                    name="anti_hcv"
                                    as="select"
                                    value={datas ? datas.ekstremitas : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                >
                                    <option value="">- Pilih -</option>
                                    <option value="Positif">Positif</option>
                                    <option value="Negatif">Negatif</option>

                                </Form.Control>

                            </td>
                            <td width="5%"><strong>Anti HIV</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">
                                {errMsg.anti_HIV === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.anti_HIV}</span></React.Fragment>) : ''}

                                <Form.Control

                                    size="sm"
                                    autoComplete="off"
                                    name="anti_HIV"
                                    as="select"
                                    value={datas ? datas.ekstremitas : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                >
                                    <option value="">- Pilih -</option>
                                    <option value="Positif">Positif</option>
                                    <option value="Negatif">Negatif</option>

                                </Form.Control>
                            </td>


                        </tr>

                        </tbody>
                    </table>


                    <button
                        id="submitFormSuratKeteranganSakit"
                        type="button"
                        onClick={(e) => handleValid()}
                        className="btn btn-success btn-sm">Submit Form Hasil Rekapitulasi Laboratorium
                    </button>
                </Form>)
            }

        </div>
    )
}
