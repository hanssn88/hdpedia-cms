import axios from "axios";
import React, {useEffect, useState} from "react";
import {Form} from "react-bootstrap";
import {Placeholder} from "rsuite";
import Swal from "sweetalert2";

const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;
export const FormResep = (data) => {

    const [datas, getDatas] = useState(data?.data);
    const [dtas, setDatas] = useState(null);
    const [errMsg, setErrMsg] = useState(null);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        async function getData() {
            await sessionStorage.setItem('reload1', false);
            await getDatas(data?.data);
            console.log("Datas :" + datas);
        }

        async function setData() {

            await getDatas(data?.data);
            setOnChange("patient_id", datas.patient_id);
            setOnChange("resep", '');
            // setOnChange("dokter_id",sessionStorage.getItem('idOP'))
            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);
            console.log("Datas :" + datas);
        }

        setValid('resep', '');

        setData();
        getData();
        // setValid();


    }, [data])


    const setOnChange = async (key, val) => {

        // const dt = {key: "patient_id", value: this.datas.patient_id};
        await setDatas({...dtas, [key]: val});


    };

    const setValid = (key, val) => {

        setErrMsg({...errMsg, [key]: val})

    }

    const validateForm = async (errors) => {
        let valid = true;
        Object.values(errors).forEach(
            (val) => val.length > 0 && (valid = false)
        );
        return valid;
    };


    const handleValid = async (e) => {


        setValid('resep', dtas.resep == '' ? "Required" : "");


        if (dtas.resep != '') {

            handleSubmit();

        } else {

            console.error('Invalid Form');
        }

    }

    const handleSubmit = async (e) => {
        const {id, created_at, updated_at, ...params} = dtas;
        if (typeof params.patient_id === "undefined" || params.patient_id === "")
            params.patient_id = sessionStorage.getItem('idPasienHDPedia');
        params.dokter_id = sessionStorage.getItem('idOP');
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";

        try {
            const response = await axios.post(API_URL + '/dokter/resep', params, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });

            let data = "";
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa;
                if (data.err_code === '00') {

                    return Swal.fire({
                        title: 'Informasi',
                        text: "Success add Catatan Terintegrasi",
                        icon: 'success',
                        confirmButtonText: 'Ok',
                        allowOutsideClick: true
                    })
                } else {
                    res = {
                        err_msg: data.err_msg
                    }

                    return Swal.fire({
                        title: 'Informasi',
                        text: res.err_msg,
                        icon: 'error',
                        confirmButtonText: 'Ok',
                        allowOutsideClick: true
                    })
                }
            }
            // return Swal.fire({
            //     title: 'Informasi',
            //     text: "Success add resep",
            //     icon: 'success',
            //     confirmButtonText: 'Ok',
            //     allowOutsideClick: true
            // })
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    const {Paragraph} = Placeholder;
    return (
        <div>
            {isLoading ? (
                    <div className="card shadow-lg">
                        <div className="card-body">
                            <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                       style={{marginTop: 30}}/>
                        </div>
                    </div>

                ) :
                (<Form>
                    <h3>Form Resep</h3>
                    <table className="table table-condensed ">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="9" align="center">
                                Data Resep
                            </td>
                        </tr>

                        <tr>

                            <td width="8%"><strong>Pasien</strong></td>
                            <td width="1%">:</td>
                            <td width="28%" colSpan="7">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    type="text"
                                    disabled="true"
                                    value={datas ? datas.name : ''}/>
                            </td>


                        </tr>

                        <tr>
                            <td width="8%">
                                <strong>Umur</strong>
                            </td>
                            <td width="1%">:</td>
                            <td width="28%" colSpan="7">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    type="text"
                                    disabled="true"
                                    value={datas ? datas.age : ''}/>
                            </td>

                        </tr>

                        <tr>
                            <td width="8%">
                                <strong>No.RM</strong>
                            </td>
                            <td width="1%">:</td>
                            <td width="28%" colSpan="7">

                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    type="text"
                                    disabled="true"
                                    value={datas ? datas.rm_number : ''}/>
                            </td>

                        </tr>

                        <tr>
                            <td width="8%">
                                <strong>Resep</strong>
                            </td>
                            <td width="1%">:</td>
                            <td width="28%" colSpan="7">
                                {errMsg.resep === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.resep}</span></React.Fragment>) : ''}
                                <Form.Control
                                    style={{"overflow-y": "scroll"}}
                                    as="textarea" rows={3}
                                    size="sm"
                                    id="resep"
                                    name="resep"
                                    autoComplete="off"
                                    type="text"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Resep"/>
                            </td>

                        </tr>


                        </tbody>
                    </table>

                    <button type="button" onClick={(e) => handleValid()}
                            className="btn btn-success btn-sm">Submit Form Data Resep
                    </button>
                </Form>)
            }
        </div>
    )
}
