import axios from "axios";
import moment from 'moment';
import "moment/locale/id";
import React, {useEffect, useState} from "react";
import {Form} from "react-bootstrap";
import Datetime from "react-datetime";
import "react-datetime/css/react-datetime.css";
import SignaturePad from 'react-signature-canvas';
import {Placeholder} from "rsuite";
import Swal from 'sweetalert2';
import AppModal from "../../components/modal/MyModal";

const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

export const FormSuratKeteranganKematian = (data) => {

    const [datas, getDatas] = useState(data?.data);
    const [dtas, setDatas] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [trimmedDataURL, setTrimData] = useState(null);
    const [errMsg, setErrMsg] = useState(null);
    const [sigPads, sigPad] = useState({});
    const [addFormSignature, setAddFormSignature] = useState(false);

    useEffect(() => {
        async function getData() {
            await sessionStorage.setItem('reload1', false);
            await getDatas(data?.data);

            console.log("Datas :" + datas);
        }

        async function setData() {

            const paramDatas = {
                "nama": "",
                "jabatan": "",
                "tanggal_meninggal": "",
                "jam_meninggal": "",
                "tempat_meninggal": "",
                "no_ktp": "",
                "no_sim": "",
                "no_passport": "",
                "alamat_unit_hd": "",
                "pemeriksaan_jenazah": "",
                "tanggal_pemeriksaan": "",
                "jam_pemeriksaan": "",
                "penyebab_kematian": "",
                "tanggal": "",
                "image_ttd": "",
            };
            await setDatas(paramDatas);

            const paramValid = {
                "nama": "",
                "jabatan": "",
                "identitas": "",
                "tanggal_meninggal": "",
                "jam_meninggal": "",
                "tempat_meninggal": "",
                "alamat_unit_hd": "",
                "pemeriksaan_jenazah": "",
                "tanggal_pemeriksaan": "",
                "jam_pemeriksaan": "",
                "penyebab_kematian": "",
                "tanggal": "",
                "image_ttd": "",
            };

            await setErrMsg(paramValid);

            await getDatas(data?.data);

            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);
            console.log("Datas :" + datas);
        }

        setData();
        getData();

    }, [data])


    const setOnChange = async (key, val) => {
        if (key == 'tanggal_meninggal' || key == 'tanggal_pemeriksaan' || key == 'tanggal') {
            val = moment(new Date(val)).format('YYYY-MM-DD');
            await setDatas({...dtas, [key]: val});
        } else {
            await setDatas({...dtas, [key]: val});
        }


    };

    const handleValid = async (e) => {

        let identitas = '';

        if (dtas.no_ktp != '') {
            identitas = dtas.no_ktp;
        } else if (dtas.no_sim != '') {
            identitas = dtas.no_sim;
        } else if (dtas.no_passport != '') {
            identitas = dtas.no_passport;
        } else {
            identitas = '';
        }

        const paramValid = {
            "nama": dtas.nama == '' ? "Required" : "",
            "jabatan": dtas.jabatan == '' ? "Required" : "",
            "identitas": identitas == '' ? "Required" : "",
            "tanggal_meninggal": dtas.tanggal_meninggal == '' ? "Required" : "",
            "jam_meninggal": dtas.jam_meninggal == '' ? "Required" : "",
            "tempat_meninggal": dtas.tempat_meninggal == '' ? "Required" : "",
            "alamat_unit_hd": dtas.alamat_unit_hd == '' ? "Required" : "",
            "pemeriksaan_jenazah": dtas.pemeriksaan_jenazah == '' ? "Required" : "",
            "tanggal_pemeriksaan": dtas.tanggal_pemeriksaan == '' ? "Required" : "",
            "jam_pemeriksaan": dtas.jam_pemeriksaan == '' ? "Required" : "",
            "penyebab_kematian": dtas.penyebab_kematian == '' ? "Required" : "",
            "tanggal": dtas.tanggal == '' ? "Required" : "",
            "image_ttd": dtas.image_ttd == '' ? "Required" : "",


        };

        await setErrMsg(paramValid);

        if (dtas.nama != '' && dtas.jabatan != '' && dtas.tanggal_meninggal != ''
            && dtas.jam_meninggal != '' && dtas.tempat_meninggal && dtas.alamat_unit_hd
            && dtas.pemeriksaan_jenazah != '' && dtas.tanggal_pemeriksaan != '' && dtas.jam_pemeriksaan != ''
            && dtas.penyebab_kematian != '' && dtas.tanggal != '' && dtas.image_ttd != ''
            && identitas != ''
        ) {

            handleSubmit();

        } else {

            console.error('Invalid Form');
        }

    }

    const handleSubmit = async (e) => {
        const {id, created_at, updated_at, ...params} = dtas;
        if (typeof params.patient_id === "undefined" || params.patient_id === "")
            params.patient_id = sessionStorage.getItem('idPasienHDPedia');
        params.dokter_id = sessionStorage.getItem('idOP');
        params.nama_lengkap = datas?.name;
        if (datas.rm_number != null) {
            params.rm_number = datas.rm_number;
        } else {
            params.rm_number = null;
        }
        params.jenis_kelamin = datas?.gender;
        params.ttl = datas?.ttl;
        params.alamat = datas?.address;
        params.agama = datas?.religion === 1 ? "Islam" : datas?.religion === 2 ? "Kristen" : datas?.religion === 3 ? "Katholik" : datas?.religion === 4 ? "Hindu" : datas?.religion === 5 ? "Buddha" : datas?.religion === 6 ? "Konghucu" : "";

        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";

        const form = Object.keys(params).reduce((f, k) => {
            f.append(k, params[k]);
            return f;
        }, new FormData());


        try {
            const response = await axios.post(API_URL + '/dokter/surat_keterangan_kematian', form, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });

            let data = "";
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa;
                if (data.err_code === '00') {

                    return Swal.fire({
                        title: 'Informasi',
                        text: "Success add Surat Keterangan Kematian",
                        icon: 'success',
                        confirmButtonText: 'Ok',
                        allowOutsideClick: true
                    })
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return Swal.fire({
                        title: 'Error',
                        text: res.err_msg,
                        icon: 'error',
                        confirmButtonText: 'Ok',
                        allowOutsideClick: true
                    })
                }
            }
            // return Swal.fire({
            //     title: 'Informasi',
            //     text: "Success add resep",
            //     icon: 'success',
            //     confirmButtonText: 'Ok',
            //     allowOutsideClick: true
            // })
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    async function addSignature(e) {
        e.preventDefault();
        await setAddFormSignature(true);
        // await setTrimData({});
    }

    async function closeSignature(e) {
        e.preventDefault();
        await setAddFormSignature(false);
        // await setTrimData({});
    }


    const clear = (e) => {
        sigPads.clear()
    }

    const trim = async (e) => {
        setTrimData(sigPads.getTrimmedCanvas()
            .toDataURL('image/png'));
        setAddFormSignature(false);

        // setDatas({...dtas, ["image_ttd"]: sigPads.getTrimmedCanvas()
        // .toDataURL('image/png')});

        const base64Response = await fetch(sigPads.getTrimmedCanvas()
            .toDataURL('image/png'));
        const imgaes = await base64Response.blob();

        let reader = new FileReader();
        reader.readAsDataURL(imgaes);
        reader.onloadend = () => {
            setDatas({...dtas, ["image_ttd"]: imgaes});

        }

    }


    const {Paragraph} = Placeholder;

    const frmSignature = <Form>

        <table>

            <tr>

                <SignaturePad penColor='green'
                              canvasProps={{width: 350, height: 400, className: 'sigCanvas'}} ref={(ref) => {
                    sigPad(ref)
                }}/>


            </tr>

            <tr>

                <button
                    id="clearSig"
                    type="button" clear
                    onClick={(e) => clear()}
                    className="btn btn-danger btn-sm">Clear
                </button>
            </tr>
        </table>
    </Form>

    return (
        <div>
            {isLoading ? (
                <div className="card shadow-lg">
                    <div className="card-body">
                        <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                   style={{marginTop: 30}}/>
                    </div>
                </div>

            ) : (
                <Form>
                    <h3>Form Surat Keterangan Kematian</h3>
                    <table className="table table-condensed table-form-askep">

                        <tbody>

                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="12" align="center">
                                Data Surat Keterangan Kematian
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="12">
                                <h6>Saya yang bertanda tangan di bawah ini</h6>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%"><strong>Nama</strong></td>
                            <td width="1%">:</td>
                            <td width="14%">
                                {errMsg.nama === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.nama}</span></React.Fragment>) : ''}
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="nama"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Nama"/>
                            </td>
                            <td width="5%"><strong>Jabatan</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">
                                {errMsg.jabatan === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.jabatan}</span></React.Fragment>) : ''}
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="jabatan"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Jabatan"/>
                            </td>


                        </tr>

                        <tr>
                            <td colSpan="12">
                                <h6>Menyatakan bahwa pasien sebagai berikut telah meninggal dunia :</h6>
                            </td>
                        </tr>

                        <tr>
                            <td width="10%"><strong>Nama Lengkap</strong></td>
                            <td width="1%">:</td>
                            <td width="14%">

                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="nama_lengkap"
                                    type="text"
                                    disabled="true"
                                    value={datas ? datas.name : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                />

                            </td>
                            <td width="5%"><strong>No. Rekam Medis</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">

                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="rm_number"
                                    type="text"
                                    disabled="true"
                                    value={datas ? datas.rm_number : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                />
                            </td>


                        </tr>

                        <tr>
                            <td width="10%"><strong>Jenis Kelamin</strong></td>
                            <td width="1%">:</td>
                            <td width="14%">

                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="jenis_kelamin"
                                    type="text"
                                    disabled="true"
                                    value={datas ? datas.gender : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                />

                            </td>
                            <td width="5%"><strong>Tempat Tanggal Lahir</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="ttl"
                                    type="text"
                                    disabled="true"
                                    value={datas ? datas.ttl : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}/>

                            </td>


                        </tr>

                        <tr>
                            <td width="10%"><strong>Agama</strong></td>
                            <td width="1%">:</td>
                            <td width="14%">

                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="agama"
                                    type="text"
                                    disabled="true"
                                    value={datas?.religion === 1 ? "Islam" : datas?.religion === 2 ? "Kristen" : datas?.religion === 3 ? "Katholik" : datas?.religion === 4 ? "Hindu" : datas?.religion === 5 ? "Buddha" : datas?.religion === 6 ? "Konghucu" : ""}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}/>
                            </td>
                            <td width="5%"><strong>Alamat</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">

                                <Form.Control
                                    style={{"overflow-y": "scroll"}}
                                    as="textarea" rows={3}
                                    size="sm"
                                    disabled="true"
                                    name="alamat"
                                    autoComplete="off"
                                    value={datas ? datas.address : ''}
                                    type="text"
                                    placeholder=""
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                />
                            </td>


                        </tr>

                        <tr>
                            <td width="10%"><strong>Nomor Identitas</strong></td>
                            <td width="1%">:</td>
                            <td>
                                {errMsg.identitas === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.identitas}</span></React.Fragment>) : ''}
                                <Form.Group className="mb-3" controlId="no_identitas">

                                    <Form.Check
                                        type="radio"
                                        name="no_identitas"
                                        id="no_identitas"
                                        value="No.KTP"
                                        label="No.KTP"
                                        onChange={e => setOnChange("identitas", "no_ktp")}
                                    />
                                    <Form.Check
                                        type="radio"
                                        name="no_identitas"
                                        id="no_identitas"
                                        value="No.Sim"
                                        onChange={e => setOnChange("identitas", "no_sim")}
                                        label="No.Sim"/>
                                    <Form.Check
                                        type="radio"
                                        name="no_identitas"
                                        id="no_identitas"
                                        value="Passport"
                                        onChange={e => setOnChange("identitas", "no_passport")}
                                        label="Passport"/>


                                </Form.Group>
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    disabled={dtas?.identitas === 'no_ktp' ? false : dtas?.identitas === 'no_sim' ? false : dtas?.identitas === false ? 'No Passport' : true}
                                    name="no_ktp"
                                    type="number"
                                    onChange={e => setOnChange(dtas?.identitas === 'no_ktp' ? 'no_ktp' : dtas?.identitas === 'no_sim' ? 'no_sim' : dtas?.identitas === 'no_passport' ? 'no_passport' : '', e.target.value)}
                                    placeholder={dtas?.identitas === 'no_ktp' ? 'No KTP' : dtas?.identitas === 'no_sim' ? 'No SIM' : dtas?.identitas === 'no_passport' ? 'No Passport' : ''}/>
                            </td>

                            <td width="10%"><strong>Tanggal Meninggal</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">
                                {errMsg.tanggal_meninggal === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.tanggal_meninggal}</span></React.Fragment>) : ''}
                                <Datetime

                                    onChange={e => setOnChange("tanggal_meninggal", e)}
                                    timeFormat={false}
                                    closeOnSelect={true}
                                    inputProps={{
                                        readOnly: true,
                                        autoComplete: "off",
                                        placeholder: 'Tanggal Meninggal',
                                        name: 'tanggal_meninggal',
                                        className: 'form-control form-control-sm'
                                    }}
                                />

                            </td>


                        </tr>

                        <tr>

                            <td width="5%"><strong>Jam Meninggal</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">
                                {errMsg.jam_meninggal === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.jam_meninggal}</span></React.Fragment>) : ''}
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="jam_meninggal"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Jam Meninggal"/>
                            </td>

                            <td width="10%"><strong>Tempat Meninggal</strong></td>
                            <td width="1%">:</td>
                            <td width="14%">
                                {errMsg.tempat_meninggal === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.tempat_meninggal}</span></React.Fragment>) : ''}
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="tempat_meninggal"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Tempat Meninggal"/>

                            </td>


                        </tr>

                        <tr>

                            <td width="5%"><strong>Alamat Unit Hemodialisis</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">
                                {errMsg.alamat_unit_hd === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.alamat_unit_hd}</span></React.Fragment>) : ''}
                                <Form.Control
                                    style={{"overflow-y": "scroll"}}
                                    as="textarea" rows={3}
                                    size="sm"
                                    autoComplete="off"
                                    name="alamat_unit_hd"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Alamat Unit Hemodialisis"/>
                            </td>

                            <td width="10%"><strong>Pemeriksa Jenazah</strong></td>
                            <td width="1%">:</td>
                            <td width="14%">
                                {errMsg.pemeriksaan_jenazah === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.pemeriksaan_jenazah}</span></React.Fragment>) : ''}
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="pemeriksaan_jenazah"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Pemeriksa Jenazah"/>

                            </td>

                        </tr>

                        <tr>

                            <td width="5%"><strong>Tanggal Pemeriksaan</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">

                                {errMsg.tanggal_pemeriksaan === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.tanggal_pemeriksaan}</span></React.Fragment>) : ''}
                                <Datetime

                                    onChange={e => setOnChange("tanggal_pemeriksaan", e)}
                                    timeFormat={false}
                                    closeOnSelect={true}
                                    inputProps={{
                                        readOnly: true,
                                        autoComplete: "off",
                                        placeholder: 'Tanggal Pemeriksaan',
                                        name: 'tanggal_pemeriksaan',
                                        className: 'form-control form-control-sm'
                                    }}
                                />

                            </td>

                            <td width="10%"><strong>Jam Pemeriksaan</strong></td>
                            <td width="1%">:</td>
                            <td width="14%">
                                {errMsg.jam_pemeriksaan === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.jam_pemeriksaan}</span></React.Fragment>) : ''}
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="jam_pemeriksaan"
                                    type="jam_pemeriksaan"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Jam Pemeriksaan"/>

                            </td>

                        </tr>

                        <tr>

                            <td width="5%"><strong>Penyebab Kematian</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">
                                {errMsg.penyebab_kematian === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.penyebab_kematian}</span></React.Fragment>) : ''}
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="penyebab_kematian"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Penyebab Kematian"/>
                            </td>

                            <td width="10%"><strong>Tanggal</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">
                                {errMsg.tanggal === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.tanggal}</span></React.Fragment>) : ''}
                                <Datetime

                                    onChange={e => setOnChange("tanggal", e)}
                                    timeFormat={false}
                                    closeOnSelect={true}
                                    inputProps={{
                                        readOnly: true,
                                        autoComplete: "off",
                                        placeholder: 'Tanggal',
                                        name: 'tanggal',
                                        className: 'form-control form-control-sm'
                                    }}
                                />

                            </td>

                        </tr>

                        <tr>
                            <td width="5%"><strong>Signature</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">
                                {errMsg.image_ttd === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.image_ttd}</span></React.Fragment>) : ''}
                                <div className="rtd">
                                    <button
                                        type="button"
                                        id="signature"
                                        onClick={addSignature}
                                        className="btn btn-info btn-lg">
                                        <i className="fa fa-edit mr-2"></i> Signature
                                    </button>
                                </div>
                            </td>
                        </tr>
                        <tr>

                            {trimmedDataURL != null
                                ? <img
                                    src={trimmedDataURL}/>
                                : ''}

                        </tr>


                        </tbody>
                    </table>


                    <button
                        id="submitFormSuratKeteranganSakit"
                        type="button"
                        onClick={(e) => handleValid()}
                        className="btn btn-success btn-sm">Submit Form Surat Keterangan Kematian
                    </button>
                </Form>)
            }
            <AppModal
                show={addFormSignature}
                form={frmSignature}
                size="xs"
                backdrop={false}
                keyboard={false}
                title='Signature'
                titleButton="Save change"
                themeButton="success"
                handleClose={closeSignature}
                /*isLoading={this.props.isAddLoading ? this.props.isAddLoading : this.state.loadingForm}*/
                formSubmit={trim}
            ></AppModal>

        </div>
    )
}
