import axios from "axios";
import moment from 'moment';
import "moment/locale/id";
import React, {useEffect, useState} from "react";
import {Form} from "react-bootstrap";
import Datetime from "react-datetime";
import "react-datetime/css/react-datetime.css";
import SignaturePad from 'react-signature-canvas';
import {Placeholder} from "rsuite";
import Swal from 'sweetalert2';
import AppModal from "../../components/modal/MyModal";


const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;


export const FormSuratKeteranganSakit = (data) => {

    const [datas, getDatas] = useState(data?.data);
    const [dtas, setDatas] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [trimmedDataURL, setTrimData] = useState(null);
    const [errMsg, setErrMsg] = useState(null);
    const [sigPads, sigPad] = useState({});
    const [addFormSignature, setAddFormSignature] = useState(false);

    useEffect(() => {
        async function getData() {

            await getDatas(data?.data);
            console.log("Datas :" + datas);
        }

        async function setData() {

            const paramDatas = {
                "sakit_hari": "",
                "tanggal_start": "",
                "tanggal_end": "",
                "image_dokter_pemeriksa": "",
                "tanggal_ttd": "",
            };
            await setDatas(paramDatas);

            const paramValid = {
                "sakit_hari": "",
                "tanggal_start": "",
                "tanggal_end": "",
                "image_dokter_pemeriksa": "",
                "tanggal_ttd": "",
            };

            await setErrMsg(paramValid);

            await getDatas(data?.data);
            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);
            console.log("Datas :" + datas);


        }

        setData();
        getData();

    }, [data])


    const setOnChange = async (key, val) => {

        // const dt = {key: "patient_id", value: this.datas.patient_id};
        if (key === 'tanggal_start' || key === 'tanggal_end' || key === 'tanggal_ttd') {
            val = moment(new Date(val)).format('YYYY-MM-DD');
            await setDatas({...dtas, [key]: val});

        } else {
            await setDatas({...dtas, [key]: val});
        }


    };


    const handleValid = async (e) => {


        const paramValid = {
            "sakit_hari": dtas.sakit_hari == '' ? "Required" : "",
            "tanggal_start": dtas.tanggal_start == '' ? "Required" : "",
            "tanggal_end": dtas.tanggal_end == '' ? "Required" : "",
            "image_dokter_pemeriksa": dtas.image_dokter_pemeriksa == '' ? "Required" : "",
            "tanggal_ttd": dtas.tanggal_ttd == '' ? "Required" : "",
        };

        await setErrMsg(paramValid);

        if (dtas.sakit_hari != '' && dtas.tanggal_start != '' && dtas.tanggal_end != ''
            && dtas.image_dokter_pemeriksa != '' && dtas.tanggal_ttd
        ) {

            handleSubmit();

        } else {

            console.error('Invalid Form');
        }

    }


    const handleSubmit = async (e) => {
        const {id, created_at, updated_at, ...params} = dtas;
        if (typeof params.patient_id === "undefined" || params.patient_id === "")
            params.patient_id = sessionStorage.getItem('idPasienHDPedia');
        params.dokter_id = sessionStorage.getItem('idOP');
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";

        const form = Object.keys(params).reduce((f, k) => {
            f.append(k, params[k]);
            return f;
        }, new FormData());
        try {
            const response = await axios.post(API_URL + '/dokter/surat_keterangan_sakit', form, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });

            let data = "";
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa;
                if (data.err_code === '00') {

                    return Swal.fire({
                        title: 'Informasi',
                        text: "Success add Surat Keterangan Sakit",
                        icon: 'success',
                        confirmButtonText: 'Ok',
                        allowOutsideClick: true
                    })
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return Swal.fire({
                        title: 'Error',
                        text: res.err_msg,
                        icon: 'error',
                        confirmButtonText: 'Ok',
                        allowOutsideClick: true
                    })
                }
            }
            // return Swal.fire({
            //     title: 'Informasi',
            //     text: "Success add resep",
            //     icon: 'success',
            //     confirmButtonText: 'Ok',
            //     allowOutsideClick: true
            // })
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    async function addSignature(e) {
        e.preventDefault();
        await setAddFormSignature(true);
        // await setTrimData({});
    }

    async function closeSignature(e) {
        e.preventDefault();
        await setAddFormSignature(false);
        // await setTrimData({});
    }


    const clear = (e) => {
        sigPads.clear()
    }

    const trim = async (e) => {
        setTrimData(sigPads.getTrimmedCanvas()
            .toDataURL('image/png'));
        setAddFormSignature(false);


        const base64Response = await fetch(sigPads.getTrimmedCanvas()
            .toDataURL('image/png'));
        const imgaes = await base64Response.blob();

        let reader = new FileReader();
        reader.readAsDataURL(imgaes);
        reader.onloadend = () => {
            setDatas({...dtas, ["image_dokter_pemeriksa"]: imgaes});

        }
    }


    const {Paragraph} = Placeholder;


    const frmSignature = <Form>

        <table>

            <tr>

                <SignaturePad penColor='green'
                              canvasProps={{width: 350, height: 400, className: 'sigCanvas'}} ref={(ref) => {
                    sigPad(ref)
                }}/>


            </tr>

            <tr>

                <button
                    id="clearSig"
                    type="button" clear
                    onClick={(e) => clear()}
                    className="btn btn-danger btn-sm">Clear
                </button>
            </tr>
        </table>
    </Form>

    return (

        <div>
            {isLoading ? (
                <div className="card shadow-lg">
                    <div className="card-body">
                        <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                   style={{marginTop: 30}}/>
                    </div>
                </div>

            ) : (
                <Form>
                    <h3>Form Surat Keterangan Sakit</h3>
                    <table className="table table-condensed table-form-askep">

                        <tbody>


                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="12" align="center">
                                Data Surat Keterangan Sakit
                            </td>
                        </tr>
                        <tr>
                            <td style={{

                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="12" align="center">
                                <h6>Yang bertanda tangan dibawah ini menerangkan bahwa:</h6></td>
                        </tr>

                        <tr>
                            <td><strong>Pasien</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="no_rm"
                                    type="text"
                                    disabled="true"
                                    value={datas ? datas.name : ''}
                                />
                            </td>


                        </tr>

                        <tr>
                            <td width="5%"><strong>Umur</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">

                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    type="text"
                                    disabled="true"
                                    value={datas ? datas.age : ''}/>
                            </td>

                        </tr>

                        <tr>
                            <td width="5%"><strong>Pekerjaan</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">

                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    type="text"
                                    disabled="true"
                                    value={datas ? datas.job : ''}/>
                            </td>

                        </tr>


                        <tr>
                            <td width="8%">
                                <strong>Alamat</strong>
                            </td>
                            <td width="1%">:</td>
                            <td colSpan="12">
                                <Form.Control
                                    style={{"overflow-y": "scroll"}}
                                    as="textarea" rows={3}
                                    size="sm"
                                    disabled="true"
                                    autoComplete="off"
                                    value={datas ? datas.address : ''}
                                    type="text"
                                    placeholder=""/>
                            </td>

                        </tr>

                        <tr>
                            <td style={{

                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="12">
                                <h6>Perlu beristirahat karena sakit selama </h6>
                            </td>
                        </tr>

                        <tr>

                            <td width="5%"><strong>Hari</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">
                                {errMsg.sakit_hari === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.sakit_hari}</span></React.Fragment>) : ''}
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="sakit_hari"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="hari"/>
                            </td>


                        </tr>

                        <tr>
                            <td style={{

                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="12" align="center">
                                <h6>hari terhitung mulai tanggal </h6>
                            </td>
                        </tr>

                        <tr>

                            <td width="5%"><strong>Tanggal</strong></td>
                            <td width="1%">:</td>
                            <td width="28%" colSpan="5">
                                {errMsg.tanggal_start === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.tanggal_start}</span></React.Fragment>) : ''}
                                <Datetime

                                    onChange={e => setOnChange("tanggal_start", e)}
                                    timeFormat={false}
                                    closeOnSelect={true}
                                    inputProps={{
                                        readOnly: true,
                                        autoComplete: "off",
                                        placeholder: 'Tanggal Start',
                                        name: 'tanggal_start',
                                        className: 'form-control form-control-sm'
                                    }}
                                />
                            </td>

                            <td style={{

                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="2" align="center"><strong>-</strong></td>

                            <td width="5%"><strong>Tanggal</strong></td>
                            <td width="1%">:</td>
                            <td width="28%" colSpan="5">
                                {errMsg.tanggal_end === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.tanggal_end}</span></React.Fragment>) : ''}

                                <Datetime

                                    onChange={e => setOnChange("tanggal_end", e)}
                                    timeFormat={false}
                                    closeOnSelect={true}
                                    inputProps={{
                                        readOnly: true,
                                        autoComplete: "off",
                                        placeholder: 'Tanggal End',
                                        name: 'tanggal_end',
                                        className: 'form-control form-control-sm'
                                    }}
                                />
                            </td>


                        </tr>

                        <tr>
                            <td style={{

                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="12">
                                <h6>Harap yang berkepentingan maklum, Terima Kasih.</h6>
                            </td>
                        </tr>

                        <tr>

                            <td width="5%"><strong>Tanggal</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">
                                {errMsg.tanggal_ttd === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.tanggal_ttd}</span></React.Fragment>) : ''}

                                <Datetime

                                    onChange={e => setOnChange("tanggal_ttd", e)}
                                    timeFormat={false}
                                    closeOnSelect={true}
                                    inputProps={{
                                        readOnly: true,
                                        autoComplete: "off",
                                        placeholder: 'Tanggal',
                                        name: 'tanggal_ttd',
                                        className: 'form-control form-control-sm'
                                    }}
                                />
                            </td>


                        </tr>
                        <tr>
                            <td width="5%"><strong>Signature</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">
                                {errMsg.image_dokter_pemeriksa === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.image_dokter_pemeriksa}</span></React.Fragment>) : ''}

                                <div class="rdt">
                                    <button
                                        type="button"
                                        id="signature"
                                        onClick={addSignature}
                                        className="btn btn-info btn-lg">
                                        <i className="fa fa-edit mr-2"></i> Signature
                                    </button>
                                </div>
                            </td>
                        </tr>
                        <tr>

                            {/* <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="image_dokter_pemeriksa"
                                    setfieldvalue={trimmedDataURL}
                                    onChange={e => setOnChange(e.target.name, e.target.files[0])}
                                    type="file"
                                    placeholder=""/> */}

                            {trimmedDataURL != null
                                ? <img
                                    src={trimmedDataURL}/>
                                : ''}

                        </tr>

                        </tbody>
                    </table>


                    <button
                        id="submitFormSuratKeteranganSakit"
                        type="button"
                        onClick={(e) => handleValid()}
                        className="btn btn-success btn-sm">Submit Form Surat Keterangan Sakit
                    </button>
                </Form>


            )


            }
            <AppModal
                show={addFormSignature}
                form={frmSignature}
                size="xs"
                backdrop={false}
                keyboard={false}
                title='Signature'
                titleButton="Save change"
                themeButton="success"
                handleClose={closeSignature}
                /*isLoading={this.props.isAddLoading ? this.props.isAddLoading : this.state.loadingForm}*/
                formSubmit={trim}
            ></AppModal>

        </div>
    )
}
