import axios from "axios";
import moment from 'moment';
import "moment/locale/id";
import React, {useEffect, useState} from "react";
import {Form} from "react-bootstrap";
import Datetime from "react-datetime";
import "react-datetime/css/react-datetime.css";
import SignaturePad from 'react-signature-canvas';
import {Placeholder} from "rsuite";
import Swal from 'sweetalert2';
import AppModal from "../../components/modal/MyModal";


const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

export const FormSuratRujukan = (data) => {


    const [datas, getDatas] = useState(data?.data);
    const [dtas, setDatas] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [trimmedDataURL, setTrimData] = useState(null);
    const [errMsg, setErrMsg] = useState(null);
    const [sigPads, sigPad] = useState({});
    const [addFormSignature, setAddFormSignature] = useState(false);

    useEffect(() => {
        async function getData() {
            // await sessionStorage.setItem('reload1', false);
            await getDatas(data?.data);
        }

        async function setData() {

            const paramDatas = {
                "ts_dokter": "",
                "date": "",
                "klinis": "",
                "diagnosa": "",
                "pemeriksaan_penunjang": "",
                "terapi": "",
                "memerlukan": "",
                "image_salam_sejawat": "",
            };
            await setDatas(paramDatas);

            const paramValid = {
                "ts_dokter": "",
                "date": "",
                "klinis": "",
                "diagnosa": "",
                "pemeriksaan_penunjang": "",
                "terapi": "",
                "memerlukan": "",
                "image_salam_sejawat": "",
            };

            await setErrMsg(paramValid);

            await getDatas(data?.data);

            // setOnChange("dokter_id",sessionStorage.getItem('idOP'))
            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);
        }

        setData();
        getData();


    }, [data])


    const setOnChange = async (key, val) => {

        // const dt = {key: "patient_id", value: this.datas.patient_id};
        if (key === 'date') {
            val = moment(new Date(val)).format('YYYY-MM-DD');
            await setDatas({...dtas, [key]: val});
        } else {
            await setDatas({...dtas, [key]: val});
        }


    };


    const handleValid = async (e) => {


        const paramValid = {
            "ts_dokter": dtas.ts_dokter == '' ? "Required" : "",
            "date": dtas.date == '' ? "Required" : "",
            "klinis": dtas.klinis == '' ? "Required" : "",
            "diagnosa": dtas.diagnosa == '' ? "Required" : "",
            "pemeriksaan_penunjang": dtas.pemeriksaan_penunjang == '' ? "Required" : "",
            "terapi": dtas.terapi == '' ? "Required" : "",
            "memerlukan": dtas.memerlukan == '' ? "Required" : "",
            "image_salam_sejawat": dtas.image_salam_sejawat == '' ? "Required" : "",

        };

        await setErrMsg(paramValid);

        if (dtas.ts_dokter != '' && dtas.date != '' && dtas.klinis != ''
            && dtas.diagnosa != '' && dtas.pemeriksaan_penunjang && dtas.terapi
            && dtas.memerlukan && dtas.image_salam_sejawat
        ) {

            handleSubmit();

        } else {

            console.error('Invalid Form');
        }

    }

    const handleSubmit = async (e) => {
        const {id, created_at, updated_at, ...params} = dtas;
        if (typeof params.patient_id === "undefined" || params.patient_id === "")
            params.patient_id = sessionStorage.getItem('idPasienHDPedia');
        params.dokter_id = sessionStorage.getItem('idOP');
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        if (datas.rm_number != null) {
            params.rm_number = datas.rm_number;
        } else {
            params.rm_number = null;
        }

        const form = Object.keys(params).reduce((f, k) => {
            f.append(k, params[k]);
            return f;
        }, new FormData());

        try {
            const response = await axios.post(API_URL + '/dokter/surat_rujukan', form, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });

            let data = "";
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa;
                if (data.err_code === '00') {

                    return Swal.fire({
                        title: 'Informasi',
                        text: "Success add Surat Rujukan",
                        icon: 'success',
                        confirmButtonText: 'Ok',
                        allowOutsideClick: true
                    })
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return Swal.fire({
                        title: 'Error',
                        text: res.err_msg,
                        icon: 'error',
                        confirmButtonText: 'Ok',
                        allowOutsideClick: true
                    })
                }
            }
            // return Swal.fire({
            //     title: 'Informasi',
            //     text: "Success add resep",
            //     icon: 'success',
            //     confirmButtonText: 'Ok',
            //     allowOutsideClick: true
            // })
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };


    async function addSignature(e) {
        e.preventDefault();
        await setAddFormSignature(true);
        // await setTrimData({});
    }

    async function closeSignature(e) {
        e.preventDefault();
        await setAddFormSignature(false);
        // await setTrimData({});
    }


    const clear = (e) => {
        sigPads.clear()
    }

    const trim = async (e) => {
        setTrimData(sigPads.getTrimmedCanvas()
            .toDataURL('image/png'));
        setAddFormSignature(false);

        // setDatas({...dtas, ["image_salam_sejawat"]: sigPads.getTrimmedCanvas()
        // .toDataURL('image/png')});


        const base64Response = await fetch(sigPads.getTrimmedCanvas()
            .toDataURL('image/png'));
        const imgaes = await base64Response.blob();

        let reader = new FileReader();
        reader.readAsDataURL(imgaes);
        reader.onloadend = () => {
            setDatas({...dtas, ["image_salam_sejawat"]: imgaes});

        }
    }


    const {Paragraph} = Placeholder;


    const frmSignature = <Form>

        <table>

            <tr>

                <SignaturePad penColor='green'
                              canvasProps={{width: 350, height: 400, className: 'sigCanvas'}} ref={(ref) => {
                    sigPad(ref)
                }}/>


            </tr>

            <tr>

                <button
                    id="clearSig"
                    type="button" clear
                    onClick={(e) => clear()}
                    className="btn btn-danger btn-sm">Clear
                </button>
            </tr>
        </table>
    </Form>

    return (
        <div>
            {isLoading ? (
                <div className="card shadow-lg">
                    <div className="card-body">
                        <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                   style={{marginTop: 30}}/>
                    </div>
                </div>

            ) : (
                <Form>
                    <h3>Form Surat Rujukan</h3>
                    <table className="table table-condensed table-form-askep">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="12" align="center">
                                Data Surat Rujukan
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="12">
                                <h6>Kepada Yth</h6></td>
                        </tr>
                        <tr>
                            <td width="10%"><strong>T.S Dokter</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">
                                {errMsg.ts_dokter === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.ts_dokter}</span></React.Fragment>) : ''}
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="ts_dokter"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="T.S Dokter"/>

                            </td>


                        </tr>
                        <tr>
                            <td width="5%"><strong>Tanggal</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">
                                {errMsg.date === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.date}</span></React.Fragment>) : ''}
                                <Datetime

                                    onChange={e => setOnChange("date", e)}
                                    timeFormat={false}
                                    closeOnSelect={true}
                                    inputProps={{
                                        readOnly: true,
                                        autoComplete: "off",
                                        placeholder: 'Tanggal',
                                        name: 'date',
                                        className: 'form-control form-control-sm'
                                    }}
                                />

                            </td>
                        </tr>
                        <tr>
                            <td colSpan="12"><strong>Ditempat Dengan hormat Mohon pemeriksaan dan penatalaksanaan lebih
                                lanjut pasien :</strong></td>
                            <br/>

                        </tr>

                        <tr>
                            <td width="5%"><strong>Pasien</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    type="text"
                                    disabled="true"
                                    value={datas ? datas.name : ''}/>
                            </td>
                        </tr>

                        <tr>
                            <td width="5%"><strong>Jenis Kelamin</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">

                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    type="text"
                                    disabled="true"
                                    value={datas ? datas.gender : ''}/>
                            </td>
                        </tr>

                        <tr>
                            <td width="5%"><strong>Klinis</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">
                                {errMsg.klinis === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.klinis}</span></React.Fragment>) : ''}
                                <Form.Control
                                    style={{"overflow-y": "scroll"}}
                                    as="textarea" rows={3}
                                    size="sm"
                                    autoComplete="off"
                                    name="klinis"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Klinis"/>
                            </td>
                        </tr>

                        <tr>
                            <td width="5%"><strong>Diagnosa</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">
                                {errMsg.diagnosa === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.diagnosa}</span></React.Fragment>) : ''}
                                <Form.Control
                                    style={{"overflow-y": "scroll"}}
                                    as="textarea" rows={3}
                                    size="sm"
                                    autoComplete="off"
                                    name="diagnosa"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Diagnosa"/>
                            </td>
                        </tr>

                        <tr>
                            <td width="5%"><strong>Pemeriksaan Penunjang</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">
                                {errMsg.pemeriksaan_penunjang === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.pemeriksaan_penunjang}</span></React.Fragment>) : ''}
                                <Form.Control
                                    style={{"overflow-y": "scroll"}}
                                    as="textarea" rows={3}
                                    size="sm"
                                    autoComplete="off"
                                    name="pemeriksaan_penunjang"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Pemeriksaan Penunjang"/>
                            </td>
                        </tr>

                        <tr>
                            <td width="5%"><strong>Terapi yang sudah diberikan</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">
                                {errMsg.terapi === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.terapi}</span></React.Fragment>) : ''}
                                <Form.Control
                                    style={{"overflow-y": "scroll"}}
                                    as="textarea" rows={3}
                                    size="sm"
                                    autoComplete="off"
                                    name="terapi"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Terapi yang sudah diberikan"/>
                            </td>
                        </tr>

                        <tr>
                            <td width="5%"><strong>Memerlukan</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">
                                {errMsg.memerlukan === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.memerlukan}</span></React.Fragment>) : ''}
                                <Form.Control
                                    style={{"overflow-y": "scroll"}}
                                    as="textarea" rows={3}
                                    size="sm"
                                    autoComplete="off"
                                    name="memerlukan"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    type="text"
                                    placeholder="Memerlukan"/>
                            </td>
                        </tr>

                        <tr>
                            <td width="5%"><strong>Signature</strong></td>
                            <td width="1%">:</td>
                            <td>
                                {errMsg.image_salam_sejawat === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.image_salam_sejawat}</span></React.Fragment>) : ''}

                                <div className="rdt">
                                    <button
                                        type="button"
                                        id="signature"
                                        onClick={addSignature}
                                        className="btn btn-info btn-lg">
                                        <i className="fa fa-edit mr-2"></i> Signature
                                    </button>
                                </div>

                            </td>
                        </tr>
                        <tr>

                            {trimmedDataURL != null
                                ? <img
                                    src={trimmedDataURL}/>
                                : ''}

                        </tr>


                        </tbody>
                    </table>


                    <button
                        id="submitFormSuratKeteranganSakit"
                        type="button"
                        onClick={(e) => handleValid()}
                        className="btn btn-success btn-sm">Submit Form Surat Keterangan Sakit
                    </button>
                </Form>
            )
            }


            <AppModal
                show={addFormSignature}
                form={frmSignature}
                size="xs"
                backdrop={false}
                keyboard={false}
                title='Signature'
                titleButton="Save change"
                themeButton="success"
                handleClose={closeSignature}
                /*isLoading={this.props.isAddLoading ? this.props.isAddLoading : this.state.loadingForm}*/
                formSubmit={trim}
            ></AppModal>

        </div>
    )
}
