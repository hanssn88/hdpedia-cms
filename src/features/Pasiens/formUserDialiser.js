import axios from "axios";
import React, {useEffect, useState} from "react";
import {Form} from "react-bootstrap";
import {Placeholder} from "rsuite";
import Swal from "sweetalert2";


const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

export const FormUserDialiser = (data) => {

    const [datas, getDatas] = useState(data?.data);
    const [dtas, setDatas] = useState(null);
    const [errMsg, setErrMsg] = useState(null);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        async function getData() {
            // await sessionStorage.setItem('reload1', false);
            await getDatas(data?.data);
            console.log("Datas :" + datas);
        }

        async function setData() {

            const paramDatas = {
                "reuse_ke": "",
                "sunxin": "",
                "volume": "",
                "kondisi_dialiser": "",
            };
            await setDatas(paramDatas);

            const paramValid = {
                "reuse_ke": "",
                "sunxin": "",
                "volume": "",
                "kondisi_dialiser": "",
            };

            await setErrMsg(paramValid);

            await getDatas(data?.data);


            // setOnChange("dokter_id",sessionStorage.getItem('idOP'))
            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);
            console.log("Datas :" + datas);
        }

        setData();
        getData();

    }, [data])


    const setOnChange = async (key, val) => {

        // const dt = {key: "patient_id", value: this.datas.patient_id};
        await setDatas({...dtas, [key]: val});


    };

    const handleValid = async (e) => {


        const paramValid = {
            "reuse_ke": dtas.reuse_ke == '' ? "Required" : "",
            "volume": dtas.volume == '' ? "Required" : "",
            "kondisi_dialiser": dtas.kondisi_dialiser == '' ? "Required" : "",
            "sunxin": dtas.sunxin == '' ? "Required" : "",
        };

        await setErrMsg(paramValid);

        if (dtas.reuse_ke != '' && dtas.volume != '' && dtas.kondisi_dialiser != '' && dtas.sunxin != ''
        ) {

            handleSubmit();

        } else {

            console.error('Invalid Form');
        }

    }


    const handleSubmit = async (e) => {
        const {id, created_at, updated_at, ...params} = dtas;
        if (typeof params.patient_id === "undefined" || params.patient_id === "")
            params.patient_id = sessionStorage.getItem('idPasienHDPedia');
        params.dokter_id = sessionStorage.getItem('idOP');
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/dokter/reuse_dialiser', params, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });

            let data = "";
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa;
                if (data.err_code === '00') {

                    return Swal.fire({
                        title: 'Informasi',
                        text: "Success add Re-use Dialiser",
                        icon: 'success',
                        confirmButtonText: 'Ok',
                        allowOutsideClick: true
                    })
                } else {


                    return Swal.fire({
                        title: 'Error',
                        text: data.err_msg,
                        icon: 'error',
                        confirmButtonText: 'Ok',
                        allowOutsideClick: true
                    })
                }
            }
            // return Swal.fire({
            //     title: 'Informasi',
            //     text: "Success add resep",
            //     icon: 'success',
            //     confirmButtonText: 'Ok',
            //     allowOutsideClick: true
            // })
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    const {Paragraph} = Placeholder;
    return (
        <div>
            {isLoading ? (
                <div className="card shadow-lg">
                    <div className="card-body">
                        <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                   style={{marginTop: 30}}/>
                    </div>
                </div>

            ) : (
                <Form>
                    <h3>Form Re-use Dialiser</h3>
                    <table className="table table-condensed table-form-askep">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="12" align="center">
                                Data Re-use Dialiser
                            </td>
                        </tr>

                        <tr>
                            <td width="5%"><strong>Pasien</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    type="text"
                                    disabled="true"
                                    value={datas ? datas.name : ''}/>
                            </td>
                        </tr>

                        <tr>
                            <td width="5%"><strong>Sunxin 150L</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">
                                {errMsg.sunxin === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.sunxin}</span></React.Fragment>) : ''}
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="sunxin"
                                    type="text"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Sunxin 150L"/>
                            </td>
                        </tr>

                        <tr>
                            <td width="5%"><strong>Re-use Ke</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">
                                {errMsg.reuse_ke === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.reuse_ke}</span></React.Fragment>) : ''}
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="reuse_ke"
                                    type="text"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Re-use Ke"/>
                            </td>
                        </tr>


                        <tr>
                            <td width="5%"><strong>Volume</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">
                                {errMsg.volume === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.volume}</span></React.Fragment>) : ''}
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="volume"
                                    type="text"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Volume"/>
                            </td>
                        </tr>


                        <tr>

                            <td width="5%"><strong>Kondisi</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="12">
                                {errMsg.kondisi_dialiser === "Required" ? (<React.Fragment><span
                                    className="text-error badge badge-danger mb-1">{errMsg.kondisi_dialiser}</span></React.Fragment>) : ''}
                                <Form.Group className="mb-3" controlId="kondisi">

                                    <Form.Check
                                        type="radio"
                                        name="kondisi"
                                        id="kondisi"
                                        value="TV >= 80%"
                                        label="TV >= 80%"
                                        onChange={e => setOnChange("kondisi_dialiser", "TV >= 80%")}
                                    />
                                    <Form.Check
                                        type="radio"
                                        name="kondisi"
                                        id="kondisi"
                                        value="TV < 80%"
                                        onChange={e => setOnChange("kondisi_dialiser", "TV < 80%")}
                                        label="TV < 80%"/>
                                    <Form.Check
                                        type="radio"
                                        name="kondisi"
                                        id="kondisi"
                                        value="Rusak"
                                        onChange={e => setOnChange("kondisi_dialiser", "Rusak")}
                                        label="Rusak"/>

                                    <Form.Check
                                        type="radio"
                                        name="kondisi"
                                        id="kondisi"
                                        value="Diganti atas permintaan pasien"
                                        onChange={e => setOnChange("kondisi_dialiser", "Diganti atas permintaan pasien")}
                                        label="Di ganti atas permintaan pasien"/>


                                </Form.Group>

                            </td>

                        </tr>


                        </tbody>
                    </table>


                    <button
                        id="submitFormSuratKeteranganSakit"
                        type="button"
                        onClick={(e) => handleValid()}
                        className="btn btn-success btn-sm">Submit Form Surat Dialiser
                    </button>
                </Form>)
            }

        </div>
    )
}
