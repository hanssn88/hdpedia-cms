import "moment/locale/id";
import React, {Component} from 'react';
import {Form, Image} from "react-bootstrap";
import {connect} from 'react-redux';
import {Placeholder} from 'rsuite';
import noImg from '../../assets/noPhoto.jpg';
import PasienService from './PasienService';
import {closeForm, fetchDataPasien} from "./pasiensSlice";

class HistoryDetailTravel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            appsLoading: true,
            showConfirmReject: false,
            showConfirmApprove: false,
            showConfirmRevisi: false,
            isLoading: false,
            showSwalSuccess: false,
            dtRes: {},
            patient_id: '',
            errMsg: null,
            idDetail: sessionStorage.getItem('idDetail'),
        }
    }


    componentDidMount() {
        const patient_id = sessionStorage.getItem('idPasienHDPedia');
        this.setState({patient_id});
        this.props.onLoadDataPasien({patient_id});
        this.getData();
    }

    handleClose = () => {
        this.setState({
            ...this.state,
            showConfirm: false,
            errMsg: null,
            showConfirmReject: false,
            showConfirmApprove: false,
            showConfirmRevisi: false,

        });
    };

    closeSwal = async () => {
        this.setState({
            ...this.state,
            errMsg: null,
            showConfirmRevisi: false,
            showConfirmApprove: false,
            showConfirmReject: false,
            showSwalSuccess: false,
        });
        await this.props.closeSwal();
        await this.getData();
    }


    getData = async () => {
        this.setState({appsLoading: true});
        const idDetail = sessionStorage.getItem('idDetail');
        const queryString = {id: idDetail,}
        await PasienService.postData(queryString, "VIEW_DETAIL_TRAVELING")
            .then(response => {
                setTimeout(() => {
                    if (response.data.err_code === '00') {
                        this.setState({
                            ...this.state,
                            dtRes: response.data.data
                        });
                        console.log('Data :' + this.state.dtRes);
                    }
                    if (response.data.err_code === '04') {
                        this.setState({
                            ...this.state,
                            dtRes: {},
                        });
                    }
                    this.setState({appsLoading: false});
                }, 400);
            })
            .catch(e => {
                console.log(e);
                this.setState({appsLoading: false});
            });
    };


    render() {
        const {Paragraph} = Placeholder;
        const {
            dataPasien
        } = this.props;


        return (

            <>

                <div className="content-wrapper">
                    {/* Content Header (Page header) */}
                    <div className="content-header">
                        <div className="container-fluid">
                            <div className="row mb-2">
                                <div className="col-sm-6">
                                    <h1 className="m-0">History Form Detail</h1>
                                </div>

                            </div>
                        </div>
                    </div>

                    <section className="content">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-12">
                                    {this.state.appsLoading ? (
                                        <div className="card shadow-lg">
                                            <div className="card-body">
                                                <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                                           style={{marginTop: 30}}/>
                                            </div>
                                        </div>

                                    ) : (
                                        <div className="card shadow-lg">
                                            <div className="card-body">

                                                <table className="table table-condensed">

                                                    <tbody>

                                                    <tr>

                                                        <td width="8%"><strong>Pasien</strong></td>
                                                        <td width="1%">:</td>
                                                        <td width="28%" colSpan="12">
                                                            <Form.Control
                                                                size="sm"
                                                                autoComplete="off"
                                                                type="text"
                                                                disabled="true"
                                                                value={this.state.dtRes.patient_name ? this.state.dtRes.patient_name : ''}
                                                            />
                                                        </td>

                                                    </tr>

                                                    <tr>
                                                        <td style={{
                                                            "backgroundColor": "rgba(0,0,0,.1)",
                                                            "fontWeight": "bold",
                                                            "fontSize": "16px"
                                                        }} colSpan="12" align="center">
                                                            Recent Blood Pressure Status
                                                        </td>
                                                    </tr>

                                                    <tr>

                                                        <td width="8%"><strong>Sistole (mmHg)</strong></td>
                                                        <td width="1%">:</td>
                                                        <td width="28%" colSpan="6">
                                                            <Form.Control
                                                                size="sm"
                                                                autoComplete="off"
                                                                type="text"
                                                                disabled="true"
                                                                value={this.state.dtRes.sistole ? this.state.dtRes.sistole : ''}
                                                            />
                                                        </td>

                                                        <td width="8%"><strong>Diastole (mmHg)</strong></td>
                                                        <td width="1%">:</td>
                                                        <td width="28%" colSpan="6">
                                                            <Form.Control
                                                                size="sm"
                                                                autoComplete="off"
                                                                type="text"
                                                                disabled="true"
                                                                value={this.state.dtRes.diastole ? this.state.dtRes.diastole : ''}
                                                            />
                                                        </td>

                                                    </tr>

                                                    <tr>

                                                        <td width="20%"><strong>What Dialyzer & Delivery System is the
                                                            Patient currently using</strong></td>
                                                        <td width="1%">:</td>
                                                        <td width="28%" colSpan="6">
                                                            <Form.Control
                                                                size="sm"
                                                                autoComplete="off"
                                                                type="text"
                                                                disabled="true"
                                                                value={this.state.dtRes.composition_of_dialysat || this.state.dtRes.patient_dializer_system ? this.state.dtRes.composition_of_dialysat + ' ' + this.state.dtRes.patient_dializer_system : ''}
                                                            />
                                                        </td>

                                                        <td width="8%"><strong>Blood Access</strong></td>
                                                        <td width="1%">:</td>
                                                        <td width="28%" colSpan="6">
                                                            <Form.Control
                                                                size="sm"
                                                                autoComplete="off"
                                                                type="text"
                                                                disabled="true"
                                                                value={this.state.dtRes.blood_access ? this.state.dtRes.blood_access : ''}
                                                            />
                                                        </td>

                                                    </tr>


                                                    <tr>

                                                        <td width="20%"><strong>Frequency of HD (week)</strong></td>
                                                        <td width="1%">:</td>
                                                        <td width="28%" colSpan="6">
                                                            <Form.Control
                                                                size="sm"
                                                                autoComplete="off"
                                                                type="text"
                                                                disabled="true"
                                                                value={this.state.dtRes.freq_hd ? this.state.dtRes.freq_hd : ''}
                                                            />
                                                        </td>

                                                        <td width="8%"><strong>How Long per Run (hours)</strong></td>
                                                        <td width="1%">:</td>
                                                        <td width="28%" colSpan="6">
                                                            <Form.Control
                                                                size="sm"
                                                                autoComplete="off"
                                                                type="text"
                                                                disabled="true"
                                                                value={this.state.dtRes.long_per_run ? this.state.dtRes.long_per_run : ''}
                                                            />
                                                        </td>

                                                    </tr>


                                                    <tr>
                                                        <td style={{
                                                            "backgroundColor": "rgba(0,0,0,.1)",
                                                            "fontWeight": "bold",
                                                            "fontSize": "16px"
                                                        }} colSpan="12" align="center">
                                                            Type of Heparization
                                                        </td>
                                                    </tr>


                                                    <tr>

                                                        <td width="20%"><strong>Initial Herparin (iu)</strong></td>
                                                        <td width="1%">:</td>
                                                        <td width="28%" colSpan="6">
                                                            <Form.Control
                                                                size="sm"
                                                                autoComplete="off"
                                                                type="text"
                                                                disabled="true"
                                                                value={this.state.dtRes.initial_heparin ? this.state.dtRes.initial_heparin : ''}
                                                            />
                                                        </td>

                                                        <td width="8%"><strong>Hourly Herparin (iu/hour)</strong></td>
                                                        <td width="1%">:</td>
                                                        <td width="28%" colSpan="6">
                                                            <Form.Control
                                                                size="sm"
                                                                autoComplete="off"
                                                                type="text"
                                                                disabled="true"
                                                                value={this.state.dtRes.hourly_heparin ? this.state.dtRes.hourly_heparin : ''}
                                                            />
                                                        </td>

                                                    </tr>


                                                    <tr>

                                                        <td width="20%"><strong>Patient’s Dry Weight (kgs)</strong></td>
                                                        <td width="1%">:</td>
                                                        <td width="28%" colSpan="6">
                                                            <Form.Control
                                                                size="sm"
                                                                autoComplete="off"
                                                                type="text"
                                                                disabled="true"
                                                                value={this.state.dtRes.patient_dry_weight ? this.state.dtRes.patient_dry_weight : ''}
                                                            />
                                                        </td>

                                                        <td width="8%"><strong>Know Complication with Dialysis
                                                            Run</strong></td>
                                                        <td width="1%">:</td>
                                                        <td width="28%" colSpan="6">
                                                            <Form.Control
                                                                size="sm"
                                                                autoComplete="off"
                                                                type="text"
                                                                disabled="true"
                                                                value={this.state.dtRes.compilation_dialysis ? this.state.dtRes.compilation_dialysis : ''}
                                                            />
                                                        </td>

                                                    </tr>


                                                    <tr>

                                                        <td width="20%"><strong>Blood Transfusion (date)</strong></td>
                                                        <td width="1%">:</td>
                                                        <td width="28%" colSpan="6">
                                                            <Form.Control
                                                                size="sm"
                                                                autoComplete="off"
                                                                type="text"
                                                                disabled="true"
                                                                value={this.state.dtRes.blood_transfusion ? this.state.dtRes.blood_transfusion : ''}
                                                            />
                                                        </td>

                                                        <td width="8%"><strong>Drugs</strong></td>
                                                        <td width="1%">:</td>
                                                        <td width="28%" colSpan="6">
                                                            <Form.Control
                                                                size="sm"
                                                                autoComplete="off"
                                                                type="text"
                                                                disabled="true"
                                                                value={this.state.dtRes.drugs ? this.state.dtRes.drugs : ''}
                                                            />
                                                        </td>

                                                    </tr>


                                                    <tr>
                                                        <td style={{
                                                            "backgroundColor": "rgba(0,0,0,.1)",
                                                            "fontWeight": "bold",
                                                            "fontSize": "16px"
                                                        }} colSpan="12" align="center">
                                                            Laboratory
                                                        </td>
                                                    </tr>


                                                    <tr>

                                                        <td width="20%"><strong>HBsAg</strong></td>
                                                        <td width="1%">:</td>
                                                        <td width="28%" colSpan="6">
                                                            <Form.Control
                                                                size="sm"
                                                                autoComplete="off"
                                                                type="text"
                                                                disabled="true"
                                                                value={this.state.dtRes.hbsag ? this.state.dtRes.hbsag : ''}
                                                            />
                                                        </td>

                                                        <td width="8%"><strong>Anti HCV</strong></td>
                                                        <td width="1%">:</td>
                                                        <td width="28%" colSpan="6">
                                                            <Form.Control
                                                                size="sm"
                                                                autoComplete="off"
                                                                type="text"
                                                                disabled="true"
                                                                value={this.state.dtRes.anti_hcv ? this.state.dtRes.anti_hcv : ''}
                                                            />
                                                        </td>

                                                    </tr>


                                                    <tr>

                                                        <td width="20%"><strong>Anti HIV</strong></td>
                                                        <td width="1%">:</td>
                                                        <td width="28%" colSpan="6">
                                                            <Form.Control
                                                                size="sm"
                                                                autoComplete="off"
                                                                type="text"
                                                                disabled="true"
                                                                value={this.state.dtRes.anti_hiv ? this.state.dtRes.anti_hiv : ''}
                                                            />
                                                        </td>

                                                        <td width="8%"><strong>Diet</strong></td>
                                                        <td width="1%">:</td>
                                                        <td width="28%" colSpan="6">
                                                            <Form.Control
                                                                size="sm"
                                                                autoComplete="off"
                                                                type="text"
                                                                disabled="true"
                                                                value={this.state.dtRes.diet ? this.state.dtRes.diet : ''}
                                                            />
                                                        </td>

                                                    </tr>

                                                    <tr>
                                                        <td width="8%"><strong>Signature</strong></td>
                                                        <td width="1%">:</td>
                                                        <td width="28%" colSpan="6">

                                                            <Image
                                                                src={this.state.dtRes.image_signature ? this.state.dtRes.image_signature : noImg}
                                                                thumbnail
                                                                style={{height: "150px"}}
                                                            />

                                                        </td>


                                                    </tr>

                                                    </tbody>

                                                </table>

                                            </div>
                                            <div className="card-footer clearfix">
                                                <button type="button" onClick={() => this.props.history.goBack()}
                                                        className="btn btn-flat btn-danger btn-sm">Back
                                                </button>


                                            </div>


                                        </div>
                                    )}
                                </div>

                            </div>
                        </div>
                    </section>
                </div>

            </>

        )

    }

}

const mapStateToProps = (state) => ({
    dataPasien: state.pasiens.dataPasien || {},
    contentMsg: state.pasiens.contentMsg || null,
    showFormSuccess: state.pasiens.showFormSuccess,
    tipeSWAL: state.pasiens.tipeSWAL,
    user: state.main.currentUser,
});

const mapDispatchToPros = (dispatch) => {
    return {
        onLoadDataPasien: (param) => {
            dispatch(fetchDataPasien(param));
        },
        closeSwal: () => {
            dispatch(closeForm());
        },
    }
}

export default connect(mapStateToProps, mapDispatchToPros)(HistoryDetailTravel);