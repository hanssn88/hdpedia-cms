import ReactDatatable from '@ashvin27/react-datatable';
import moment from "moment/moment";
import React, {Component, Fragment} from 'react';
import Dropdown from 'react-bootstrap/Dropdown';
import {connect} from 'react-redux';
import AppButton from '../../components/button/Button';
import {fetchData as fetchDataDokter} from '../Users/usersSlice';
import {addData, addForm, clearError, closeForm, confirmDel, fetchData} from './pasiensSlice';

class Pasiens extends Component {

    constructor(props) {
        super(props);
        this.initSelected = {
            id: '',
            name: '',
            number_of_bed: '',
            jumlah_shift: '',
            wa1: '',
            wa2: '',
            latitude: '',
            longitude: '',
            dokter_id: '',
            id_operator: '',
            operator_by: '',
        }
        this.state = {
            sort_order: "asc ",
            sort_column: "status",
            keyword: '',
            page_number: 1,
            per_page: 10,
            selected: this.initSelected,
            errMsg: this.initSelected,
            loadingForm: false,
        }
    }

    componentDidMount() {
        sessionStorage.removeItem('idPasienHDPedia');
        this.fetchProfileAdmin();
        this.props.onLoad(this.state);
        // setInterval(() => {

        // }, 60000);
    }

    fetchProfileAdmin = async () => {
        const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;
        const token = localStorage.getItem(tokenLogin);
        if (!token) {
            alert('session invalid');
            window.location.reload();
        }
    }
    handleClose = () => {
        this.props.closeModal();
    };

    tableChangeHandler = (data) => {
        let queryString = this.state;
        Object.keys(data).map((key) => {
            if (key === "sort_order" && data[key]) {
                queryString.sort_order = data[key].order;
                queryString.sort_column = data[key].column;
            }
            if (key === "page_number") {
                queryString.page_number = data[key];
            }
            if (key === "page_size") {
                queryString.per_page = data[key];
            }
            if (key === "filter_value") {
                queryString.keyword = data[key];
            }
            return true;
        });
        this.props.onLoad(this.state);
    }


    discardChanges = async (record) => {
        if (record) await sessionStorage.setItem('idPasienHDPedia', record.id);
        this.props.history.push("add_pasien");
    }

    viewDetail = async (record) => {
        if (record)
            await sessionStorage.setItem('idPasienHDPedia', record.id);
        this.props.history.push("view_pasien");
    }


    historyForm = async (record) => {
        if (record)
            await sessionStorage.setItem('idPasienHDPedia', record.id);
        this.props.history.push("history_form");
    }

    isiForm = async (record) => {
        if (record) await sessionStorage.setItem('idPasienHDPedia', record.id);
        await sessionStorage.setItem('idOP', this.props.user.id_operator);
        this.props.history.push("pasien-form");
    }

    historyTravel = async (record) => {
        if (record)
            await sessionStorage.setItem('idPasienHDPedia', record.id);
        this.props.history.push("history_travel");
    }


    render() {
        const {data, user} = this.props;
        const permission = user?.permission;


        const columns = [
            {
                key: "no",
                text: "No.",
                width: 20,
                align: "center",
                sortable: false,
                cell: (row, index) => <div
                    style={{textAlign: "center"}}>{((this.state.page_number - 1) * this.state.per_page) + index + 1 + '.'}</div>,
                row: 0
            },
            {
                key: "created_at ",
                text: "Tanggal Registrasi",
                align: "center",
                width: 150,
                sortable: true,
                cell: record => {
                    return (moment(new Date(record.created_at)).format('DD-MM-YYYY'))
                }
            },
            {
                key: "rm_number",
                text: "No rekam medis",
                align: "center",
                sortable: true,
            },
            {
                key: "name",
                text: "Name",
                align: "center",
                sortable: true,
            },
            {
                key: "mobile_number",
                text: "Phone",
                align: "center",
                sortable: true,
            },
            {
                key: "email",
                text: "Email",
                align: "center",
                sortable: true,

            },

            {
                key: "gender",
                text: "Gender",
                align: "center",
                sortable: true,
            },
            {
                key: "age",
                text: "Usia",
                align: "center",
                sortable: true,
            },
            {
                key: "blood_type",
                text: "Gol Darah",
                align: "center",
                sortable: true,
            },
            // {
            //     key: "status",
            //     text: "Status Infeeksius",
            //     align: "center",
            //     sortable: true,
            // },
            {
                key: "status",
                text: "Status",
                align: "center",
                sortable: true,
            },

            {
                key: "action",
                text: "Action",
                width: 50,
                align: "center",
                sortable: false,
                cell: record => {

                    return (
                        <div style={{textAlign: "center"}}>
                            <Fragment>
                                {/* <button
                                    disabled={parseInt(permission?.patient_edit) > 0 ? false : true}
                                    className="btn btn-xs btn-info"
                                    onClick={e => this.viewDetail(record)}
                                    style={{marginRight: '5px'}}>
                                    <i className="fa fa-eye"></i> View
                                </button> */}
                                {/* <button
                                    disabled={parseInt(permission?.patient_form) > 0 ? false : true}
                                    className="btn btn-primary btn-xs"
                                    onClick={e => this.isiForm(record)}
                                    hidden={record.status === 'Approved' ? false : true}
                                    style={{marginRight: '5px'}}>
                                    <i className="fa fa-file-medical"></i> Isi Form

                                </button>
                                <button
                                    className="btn btn-warning btn-xs"
                                    onClick={e => this.historyForm(record)}
                                    hidden={record.status === 'Approved' ? false : true}
                                    style={{marginRight: '5px'}}>
                                    <i className="fa fa-history"></i> history Form

                                </button>
                                <button
                                    disabled={user && parseInt(user.id_klinik) > 0 && parseInt(permission?.patient_delete) > 0 ? false : true}
                                    className="btn btn-xs btn-success"
                                    onClick={e => this.discardChanges(record)}
                                    style={{marginRight: '5px'}}>
                                    <i className="fa fa-edit"></i> Edit
                                </button>
                                <button
                                    className="btn btn-danger btn-xs" disabled
                                    onClick={() => this.deleteRecord(record)}>
                                    <i className="fa fa-trash"></i> Delete
                                </button>
                               <button
                                    className="btn btn-warning btn-xs"
                                    onClick={e => this.historyTravel(record)}
                                   hidden={record.status === 'Approved' ? false : true}
                                    style={{marginLeft: '5px'}}>
                                    <i className="fa fa-history"></i> history Travel
                             
                                </button> */}

                                {/* <i className="fa fa-file-medical text-primary"
                                     disabled={parseInt(permission?.patient_form) > 0 ? false : true}
                                     onClick={e => this.isiForm(record)}
                                     hidden={record.status === 'Approved' ? false : true}
                                     style={{marginRight: '5px'}}
                                     data-toggle="tooltip" 
                                     title="Isi Form"
                                 ></i>
                                 <i className="fa fa-history text-warning"
                                     disabled={parseInt(permission?.patient_form) > 0 ? false : true}
                                     onClick={e => this.historyForm(record)}
                                     hidden={record.status === 'Approved' ? false : true}
                                     style={{marginRight: '5px'}}
                                     data-toggle="tooltip" 
                                     title="History Form"
                                 ></i>
                                  <i className="fa fa-eye text-info"
                                     disabled={parseInt(permission?.patient_edit) > 0 ? false : true}
                                     onClick={e => this.viewDetail(record)}
                                     style={{marginRight: '5px'}}
                                     data-toggle="tooltip" 
                                     title="View"
                                 ></i>
                                 <i className="fa fa-edit text-success"
                                    disabled={user && parseInt(user.id_klinik) > 0 && parseInt(permission?.patient_delete) > 0 ? false : true}
                                     onClick={e => this.discardChanges(record)}
                                     style={{marginRight: '5px'}}
                                     data-toggle="tooltip" 
                                     title="Edit"
                                 ></i>
                                    <i className="fa fa-trash text-danger"
                                     disabled
                                     onClick={() => this.deleteRecord(record)}
                                     style={{marginRight: '5px'}}
                                     data-toggle="tooltip" 
                                     title="Delete"
                                 ></i>
                                   <i className="fa fa-history text-secondary"
                                    onClick={e => this.historyTravel(record)}
                                    hidden={record.status === 'Approved' ? false : true}
                                     style={{marginLeft: '5px'}}
                                     data-toggle="tooltip" 
                                     title="History Travel"
                                 ></i> */}

                                <Dropdown>
                                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                                        Acton
                                    </Dropdown.Toggle>

                                    <Dropdown.Menu>

                                        {
                                            record.status === 'Approved' ?
                                                <Dropdown.Item
                                                    disabled={parseInt(permission?.patient_form) > 0 ? false : true}
                                                    href="" onClick={e => this.isiForm(record)}>Isi Form</Dropdown.Item>

                                                :

                                                ''
                                        }

                                        <Dropdown.Item disabled={parseInt(permission?.patient_view) > 0 ? false : true}
                                                       href=""
                                                       onClick={e => this.viewDetail(record)}>View</Dropdown.Item>
                                        <Dropdown.Item
                                            disabled={user && parseInt(user.id_klinik) > 0 && parseInt(permission?.patient_edit) > 0 ? false : true}
                                            href="" onClick={e => this.discardChanges(record)}>Edit</Dropdown.Item>
                                        <Dropdown.Item
                                            disabled={parseInt(permission?.patient_delete) > 0 ? false : true} href=""
                                            onClick={e => this.deleteRecord(record)}>Delete</Dropdown.Item>
                                        {
                                            record.status === 'Approved' ?
                                                <Dropdown.Item href="" onClick={e => this.historyForm(record)}>History
                                                    Form</Dropdown.Item>

                                                :

                                                ''
                                        }
                                        {
                                            record.status === 'Approved' ?
                                                <Dropdown.Item href="" onClick={e => this.historyTravel(record)}>History
                                                    Travel</Dropdown.Item>

                                                :

                                                ''
                                        }

                                    </Dropdown.Menu>
                                </Dropdown>

                            </Fragment>
                        </div>
                    );
                }
            }
        ];
        const config = {
            key_column: 'id',
            page_size: 10,
            length_menu: [10, 20, 50],
            show_filter: true,
            show_pagination: true,
            pagination: 'advance',
            button: {
                excel: false,
                print: false
            },
            language: {
                loading_text: "Please be patient while data loads..."
            }
        }


        return (

            <div className="content-wrapper">
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0">Pasien</h1>
                            </div>
                            {/* /.col */}

                        </div>
                        {/* /.row */}
                    </div>
                    {/* /.container-fluid */}
                </div>
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                {/* card start */}
                                <div className="card card-success shadow-lg" style={{"minHeight": "800px"}}>
                                    <div className="card-header card-header-content">
                                        <AppButton
                                            disabled={user && parseInt(user.id_klinik) > 0 && parseInt(permission?.patient_form) > 0 ? false : true}
                                            isLoading={this.props.isLoading}
                                            theme="info"
                                            onClick={this.discardChanges}
                                            icon='add'
                                        >
                                            Add
                                        </AppButton>

                                    </div>
                                    <div className="card-body">
                                        {data ? (
                                            <ReactDatatable
                                                config={config}
                                                records={data}
                                                columns={columns}
                                                dynamic={true}
                                                onChange={this.tableChangeHandler}
                                                loading={this.props.isLoading}
                                                total_record={this.props.totalData}
                                            />
                                        ) : (<p>No Data ...</p>)}

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>


            </div>


        )
    }
}

const mapStateToProps = (state) => ({
    data: state.pasiens.data || [],
    dataDokter: state.usersAdm.data || [],
    totalData: state.pasiens.totalData,
    isError: state.pasiens.isError,
    isLoading: state.pasiens.isLoading,
    showFormAdd: state.pasiens.showFormAdd,
    isAddLoading: state.pasiens.isAddLoading,
    errorPriority: state.pasiens.errorPriority || null,
    contentMsg: state.pasiens.contentMsg || null,
    showFormSuccess: state.pasiens.showFormSuccess,
    showFormDelete: state.pasiens.showFormDelete,
    tipeSWAL: state.pasiens.tipeSWAL,
    user: state.main.currentUser,
});
const mapDispatchToPros = (dispatch) => {
    return {
        onLoad: (param) => {
            dispatch(fetchData(param));
            dispatch(fetchDataDokter({id_levels: 3}));

        },
        onAdd: (param) => {
            dispatch(addData(param));
        },
        showConfirmDel: (data) => {
            dispatch(confirmDel(data));
        },
        showForm: () => {
            dispatch(addForm());
        },
        closeModal: () => {
            dispatch(closeForm());

        },
        closeSwal: () => {
            dispatch(closeForm());
            const queryString = {
                sort_order: "asc",
                sort_column: "status",
                per_page: 10
            }
            dispatch(fetchData(queryString));
        },
        resetError: () => {
            dispatch(clearError());
        },

        closeSwalError: () => {
            dispatch(closeForm());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToPros)(Pasiens);