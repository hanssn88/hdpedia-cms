import React, {Component} from 'react';
import {connect} from 'react-redux';
import {FormHasilRekapitulasi} from './formHasilRekapitulasi';
import {FormResep} from './formResep';
import {FormSuratKeteranganKematian} from './formSuratKeteranganKematian';
import {FormSuratKeteranganSakit} from './formSuratKeteranganSakit';
import {FormSuratRujukan} from './formSuratRujukan';
import {FormUserDialiser} from './formUserDialiser';
import {clearError, closeForm, fetchDataPasien} from "./pasiensSlice";


class ListForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            key: 1,
            patient_id: '',
        }
        this.handleSelectTab = this.handleSelectTab.bind(this);

    }

    componentDidMount() {
        const patient_id = sessionStorage.getItem('idPasienHDPedia');
        const reload = sessionStorage.getItem('reload1');
        // sessionStorage.setItem('idOP', this.props.user.id_operator);
        this.setState({patient_id});
        if (reload === true)
            this.props.onLoadDataPasien({patient_id});
        this.handleSelectTab(1);

    }

    handleSelectTab = async (key) => {
        const patient_id = this.state.patient_id || sessionStorage.getItem('idPasienHDPedia');
        if (key === 1) await this.props.onLoadDataPasien({patient_id});
        await this.setState({key});
    }


    render() {

        const {
            dataPasien
        } = this.props;

        return (
            <div className="content-wrapper">
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0">Form Pasien</h1>
                            </div>
                            {/* /.col */}

                        </div>
                        {/* /.row */}
                    </div>
                    {/* /.container-fluid */}
                </div>
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                {/* card start */}
                                <div className="card shadow-lg" style={{"minHeight": "470px"}}>

                                    <div className="card-body">
                                        <div className="row parent p-5">
                                            <button
                                                className={this.state.key === 1 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(1)}>
                                                Form Resep
                                            </button>
                                            {/* <button
                                                className={this.state.key === 2 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(2)}>
                                                Form Catatan Terintegrasi
                                            </button> */}
                                            <button
                                                className={this.state.key === 3 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(3)}>
                                                Form Hasil Rekapitulasi Pemeriksaan Laboratorium
                                            </button>
                                            <button
                                                className={this.state.key === 4 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(4)}>
                                                Form Surat Keterangan Sakit
                                            </button>

                                            <button
                                                className={this.state.key === 5 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(5)}>
                                                Form Surat Rujukan
                                            </button>
                                            <button
                                                className={this.state.key === 6 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(6)}>
                                                Form Surat Keterangan Kematian
                                            </button>
                                            <button
                                                className={this.state.key === 7 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(7)}>
                                                Form Re-use Dialiser
                                            </button>

                                        </div>

                                        <hr/>
                                        <div className="tab-content">
                                            {this.state.key === 1 && (<FormResep data={dataPasien}/>)}
                                            {/* {this.state.key === 2 && (<FormCatatanTerintegrasi/>)} */}
                                            {this.state.key === 3 && (<FormHasilRekapitulasi data={dataPasien}/>)}
                                            {this.state.key === 4 && (<FormSuratKeteranganSakit data={dataPasien}/>)}
                                            {this.state.key === 5 && (<FormSuratRujukan data={dataPasien}/>)}
                                            {this.state.key === 6 && (<FormSuratKeteranganKematian data={dataPasien}/>)}
                                            {this.state.key === 7 && (<FormUserDialiser data={dataPasien}/>)}

                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>


        )
    }
}


const mapStateToProps = (state) => ({
    dataPasien: state.pasiens.dataPasien || {},
    isError: state.pasiens.isError,
    isLoading: state.pasiens.isLoading,
    showFormAdd: state.pasiens.showFormAdd,
    isAddLoading: state.pasiens.isAddLoading,
    errorPriority: state.pasiens.errorPriority || null,
    contentMsg: state.pasiens.contentMsg || null,
    showFormSuccess: state.pasiens.showFormSuccess,
    showFormDelete: state.pasiens.showFormDelete,
    tipeSWAL: state.pasiens.tipeSWAL,
    user: state.main.currentUser,
});

const mapDispatchToPros = (dispatch) => {
    return {
        onLoad: (param) => {

        },
        onLoadDataPasien: (param) => {
            dispatch(fetchDataPasien(param));
        },

        closeModal: () => {
            dispatch(closeForm());
        },
        closeSwal: (param) => {
            dispatch(closeForm());
        },
        resetError: () => {
            dispatch(clearError());
        },

        closeSwalError: () => {
            dispatch(closeForm());
        }
    }
}
export default connect(mapStateToProps, mapDispatchToPros)(ListForm);