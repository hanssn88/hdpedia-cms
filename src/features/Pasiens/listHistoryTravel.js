import ReactDatatable from '@ashvin27/react-datatable';
import moment from "moment/moment";
import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {addForm, clearError, closeForm, confirmDel, fetchDataHistoryTraveling} from './pasiensSlice';

class ListHistoryTravel extends Component {

    constructor(props) {
        super(props);
        this.initSelected = {
            id: '',
            name: '',
            number_of_bed: '',
            jumlah_shift: '',
            wa1: '',
            wa2: '',
            latitude: '',
            longitude: '',
            dokter_id: '',
            id_operator: '',
            operator_by: '',
        }
        this.state = {
            sort_order: "DESC",
            sort_column: "created_at",
            keyword: '',
            patient_id: '',
            page_number: 1,
            per_page: 10,
            selected: this.initSelected,
            errMsg: this.initSelected,
            loadingForm: false,
        }
    }

    componentDidMount() {
        const patient_id = sessionStorage.getItem('idPasienHDPedia');
        this.fetchProfileAdmin();
        // this.props.onLoad({patient_id});
        this.state.patient_id = patient_id;
        this.props.onLoad(this.state);
    }

    fetchProfileAdmin = async () => {
        const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;
        const token = localStorage.getItem(tokenLogin);
        if (!token) {
            alert('session invalid');
            window.location.reload();
        }
    }
    handleClose = () => {
        this.props.closeModal();
    };

    tableChangeHandler = (dataHistory) => {
        let queryString = this.state;
        const patient_id = sessionStorage.getItem('idPasienHDPedia');
        Object.keys(dataHistory).map((key) => {
            if (key === "sort_order" && dataHistory[key]) {
                queryString.sort_order = dataHistory[key].order;
                queryString.sort_column = dataHistory[key].column;
            }
            if (key === "page_number") {
                queryString.page_number = dataHistory[key];
            }
            if (key === "page_size") {
                queryString.per_page = dataHistory[key];
            }
            if (key === "filter_value") {
                queryString.keyword = dataHistory[key];
            }
            if (key == "patient_id") {
                queryString.patient_id = patient_id;
            }
            return true;
        });


        this.props.onLoad(this.state);
    }


    viewDetail = async (record) => {
        if (record)
            await sessionStorage.setItem('idDetail', record.id);
        this.props.history.push("history_travel_detail");
    }


    render() {
        const {dataHistory, user} = this.props;

        const columns = [
            {
                key: "no",
                text: "No.",
                width: 20,
                align: "center",
                sortable: false,
                cell: (row, index) => <div
                    style={{textAlign: "center"}}>{((this.state.page_number - 1) * this.state.per_page) + index + 1 + '.'}</div>,
                row: 0
            },
            {
                key: "created_at ",
                text: "Tanggal",
                align: "center",
                width: 150,
                sortable: true,
                cell: record => {
                    return (moment(new Date(record.created_at)).format('DD-MM-YYYY'))
                }
            },
            {
                key: "status_name",
                text: "Status",
                align: "center",
                sortable: true,
            },
            {
                key: "action",
                text: "Action",
                width: 180,
                align: "center",
                sortable: false,
                cell: record => {

                    return (
                        <div style={{textAlign: "center"}}>
                            <Fragment>
                                <button
                                    className="btn btn-xs btn-info"
                                    onClick={e => this.viewDetail(record)}
                                    style={{marginRight: '5px'}}>
                                    <i className="fa fa-eye"></i> View
                                </button>

                            </Fragment>
                        </div>
                    );
                }
            }
        ];
        const config = {
            key_column: 'id',
            page_size: 10,
            length_menu: [10, 20, 50],
            show_filter: true,
            show_pagination: true,
            pagination: 'advance',
            button: {
                excel: false,
                print: false
            },
            language: {
                loading_text: "Please be patient while data loads..."
            }
        }


        return (

            <div className="content-wrapper">
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0">History Traveling</h1>
                            </div>
                            {/* /.col */}

                        </div>
                        {/* /.row */}
                    </div>
                    {/* /.container-fluid */}
                </div>
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                {/* card start */}
                                <div className="card card-success shadow-lg" style={{"minHeight": "800px"}}>
                                    <div className="card-header card-header-content">


                                    </div>
                                    <div className="card-body">
                                        {dataHistory.data ? (
                                            <ReactDatatable
                                                config={config}
                                                records={dataHistory}
                                                columns={columns}
                                                dynamic={true}
                                                onChange={this.tableChangeHandler}
                                                loading={this.props.isLoading}
                                                total_record={this.props.totalData}
                                            />
                                        ) : <ReactDatatable
                                            config={config}
                                            records={dataHistory}
                                            columns={columns}
                                            dynamic={true}
                                            onChange={this.tableChangeHandler}
                                            loading={this.props.isLoading}
                                            total_record={"0"}
                                        />}

                                        <div className="card-footer clearfix">
                                            <button type="button" onClick={() => this.props.history.goBack()}
                                                    className="btn btn-flat btn-danger btn-sm">Back
                                            </button>


                                        </div>


                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </section>


            </div>


        )
    }
}

const mapStateToProps = (state) => ({
    data: state.pasiens.data || [],
    dataDokter: state.usersAdm.data || [],
    dataHistory: state.pasiens.dataHistory || [],
    totalData: state.pasiens.totalData,
    isError: state.pasiens.isError,
    isLoading: state.pasiens.isLoading,
    showFormAdd: state.pasiens.showFormAdd,
    isAddLoading: state.pasiens.isAddLoading,
    errorPriority: state.pasiens.errorPriority || null,
    contentMsg: state.pasiens.contentMsg || null,
    showFormSuccess: state.pasiens.showFormSuccess,
    showFormDelete: state.pasiens.showFormDelete,
    tipeSWAL: state.pasiens.tipeSWAL,
    user: state.main.currentUser,
});
const mapDispatchToPros = (dispatch) => {
    return {
        onLoad: (param) => {
            dispatch(fetchDataHistoryTraveling(param));
        },
        showConfirmDel: (data) => {
            dispatch(confirmDel(data));
        },
        showForm: () => {
            dispatch(addForm());
        },
        closeModal: () => {
            dispatch(closeForm());

        },
        closeSwal: () => {
            dispatch(closeForm());
            const patient_id = sessionStorage.getItem('idPasienHDPedia');
            const queryString = {
                sort_order: "DESC",
                sort_column: "created_at",
                patient_id: patient_id,
                per_page: 10
            }
            dispatch(fetchDataHistoryTraveling(queryString));
        },
        resetError: () => {
            dispatch(clearError());
        },

        closeSwalError: () => {
            dispatch(closeForm());
        }
    }
}
export default connect(mapStateToProps, mapDispatchToPros)(ListHistoryTravel);