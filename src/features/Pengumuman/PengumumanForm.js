import React, {Component} from 'react'
import {connect} from 'react-redux';
import {addData, chgProps, clearError, closeForm, fetchDataById, resetForm} from './pengumumanSlice'
import SunEditor from 'suneditor-react';
import 'suneditor/dist/css/suneditor.min.css';
import AppButton from '../../components/button/Button';
import {AppSwalSuccess} from '../../components/modal/SwalSuccess';
import {Alert, Placeholder} from 'rsuite';
import {Col, Form} from 'react-bootstrap';
import Datetime from "react-datetime";
import moment from "moment/moment";
import "react-datetime/css/react-datetime.css";
import axios from "axios";
import {SelectProvMulti} from "../../components/modal/MySelect";

class PengumumanForm extends Component {
    constructor(props) {
        super(props);
        this.initSelected = {
            id_faq: '',
            id_operator: '',
            member: '',
            question: '',
            priority: ''
        }
        this.state = {
            selected: this.initSelected,
            errMsg: this.initSelected,
            loadingForm: false,
            id_operator: '',
            selectOptions: [],
            multiValue: [],
            loadArea: true,
        }

        this.handleChangeStartDate = this.handleChangeStartDate.bind(this);
        this.handleChangeEndDate = this.handleChangeEndDate.bind(this);
    }

    componentDidMount() {
        const selectedId = sessionStorage.getItem('idPengumuman');

        this.fetchProfileAdmin();
        if (selectedId > 0) {
            const param = {id: selectedId}
            this.props.onLoad(param);
        }
        this.getOptions();

    }

    fetchProfileAdmin = () => {
        const token = localStorage.getItem(process.env.REACT_APP_TOKEN_LOGIN);
        if (!token) {
            alert('session invalid');
            window.location.reload();
        }
    }

    handleSubmit() {
        const dt = {key: "id_operator", value: this.props.user.id_operator};
        this.props.changeProps(dt);

        var errors = this.state.errMsg;
        errors.title = !this.props.data.title ? "Title required" : '';
        errors.member = this.state.multiValue.length === 0 ? "Penerima required" : '';
        errors.start_date = !this.props.data.start_date ? "Start Date required" : '';
        errors.end_date = !this.props.data.end_date ? "End Date required" : '';
        errors.body = !this.props.data.content ? "Body required" : '';
        this.setState({errors});
        if (this.validateForm(this.state.errMsg)) {
            let id_level = [];
            Object.values(this.state.multiValue).forEach(
                (val) => id_level.push(val.value)
            );
            const param = {
                id: this.props.data?.id,
                title: this.props.data?.title,
                content: this.props.data?.content,
                id_level: id_level,
                start_date: this.props.data?.start_date,
                end_date: this.props.data?.end_date,
            }
            console.log(param);
            this.props.onAdd(param);
        } else {
            console.error('Invalid Form')
            Object.values(errors).forEach(
                (val) => {
                    if (val) Alert.error(val, 5000);
                }
            );
        }

    }

    validateForm(errors) {
        let valid = true;
        Object.values(errors).forEach(
            (val) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    handleClose = () => {
        this.props.closeSwal();
        this.props.history.goBack();
    };

    handleChangeDesk(name, value) {
        const dt = {key: "id_operator", value: this.props.user.id_operator};
        name === 'answer' && this.props.changeProps(dt);
        dt['key'] = name;
        dt['value'] = value;
        this.props.changeProps(dt);
    }

    handleChange(evt) {
        const name = evt.target.name;
        var value = evt.target.value;
        const dt = {};
        dt['key'] = name;
        dt['value'] = value;
        this.props.changeProps(dt);
    }

    handleChangeStartDate(date) {
        const dt = {};
        if (date) {
            const selectedDate = new Date(date);
            const _date = moment(selectedDate).format('YYYY-MM-DD HH:mm');
            dt['key'] = 'start_date';
            dt['value'] = _date;
        } else {
            dt['key'] = 'start_date';
            dt['value'] = '';
        }
        this.props.changeProps(dt);
    }

    handleChangeEndDate(date) {
        const dt = {};
        if (date) {
            const selectedDate = new Date(date);
            const _date = moment(selectedDate).format('YYYY-MM-DD HH:mm');
            dt['key'] = 'end_date';
            dt['value'] = _date;
        } else {
            dt['key'] = 'end_date';
            dt['value'] = '';
        }
        this.props.changeProps(dt);
    }

    handleMultiChange(option) {

        this.setState(state => {
            return {
                multiValue: option
            };
        });
    }

    async getOptions() {
        const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        this.setState({isLoadingSelected: true})
        const url = process.env.REACT_APP_URL_API + "/list_role_dropdown"
        const res = await axios.post(url, "", {
            headers: {
                'Authorization': 'Bearer ' + token
            }
        });
        const err_code = res.data.err_code
        const mapUsers = new Map();
        if (err_code === '00') {
            const data = res.data.data

            const options = data.map(d => ({
                "value": d.id,
                "label": d.user_type_name
            }))
            data.map(d => {
                mapUsers.set(String(d.id), d.user_type_name);
            });

            setTimeout(() => {
                this.setState({
                    ...this.state,
                    selectOptions: options,
                    isLoadingSelected: false,
                    loadArea: false,
                })
            }, 400);
        } else {
            setTimeout(() => {
                this.setState({
                    ...this.state,
                    selectOptions: null,
                    isLoadingSelected: true,
                    loadArea: true,
                })
            }, 400);

        }

        if(this.props.data && Array.isArray(this.props.data.id_level)){
        const {id_level} = this.props.data;
        let selectOptions = [];
        for (let i = 0; i < id_level.length > 0; i++) {
            selectOptions[i] = {
                "value": id_level[i],
                "label": mapUsers.get(String(id_level[i]))
            }
        }

        this.setState(state => {
            return {
                multiValue: selectOptions
            };
        });

    }
    }

    render() {
        const {data} = this.props;
        const {Paragraph} = Placeholder;

        return (

            <div className="content-wrapper">
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0">Add/Edit Pengumuman</h1>
                            </div>
                            {/* /.col */}

                        </div>
                        {/* /.row */}
                    </div>
                    {/* /.container-fluid */}
                </div>
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                {/* card start */}
                                <div className="card card-success shadow-lg" style={{"minHeight": "800px"}}>

                                    <div className="card-body">
                                        {this.props.isLoading ? (
                                            <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                                       style={{marginTop: 30}}/>) : (

                                            <Form style={{marginTop: 20}}>
                                                <Form.Row>
                                                    <Form.Group as={Col} xs={6} controlId="title">
                                                        <Form.Label>Title</Form.Label>
                                                        <Form.Control
                                                            value={data.title ? data.title : ''}
                                                            autoComplete="off"
                                                            onChange={this.handleChange.bind(this)}
                                                            size="md"
                                                            name="title"
                                                            type="text"
                                                            placeholder="Title"/>
                                                    </Form.Group>

                                                    <Form.Group as={Col} xs={6} controlId="id_level">
                                                        <Form.Label>Penerima</Form.Label>
                                                        <SelectProvMulti
                                                            myVal={this.state.multiValue && this.state.multiValue}
                                                            getData={this.state.selectOptions}
                                                            isLoading={this.state.loadArea}
                                                            onChange={this.handleMultiChange.bind(this)}
                                                        />
                                                    </Form.Group>

                                                </Form.Row>
                                                <br/>
                                                <Form.Row>
                                                    <Form.Group as={Col} xs={6} controlId="start_date">
                                                        <Form.Label>Start Date</Form.Label>
                                                        <Datetime
                                                            closeOnSelect={true}
                                                            timeFormat={true}
                                                            setViewDate={data.start_date ? (new Date(data.start_date)) : new Date()}
                                                            value={data.start_date ? (new Date(data.start_date)) : ''}
                                                            onChange={this.handleChangeStartDate}
                                                            inputProps={{
                                                                value: data.start_date ? (moment(new Date(data.start_date)).format('DD-MM-YYYY HH:mm')) : '',
                                                                readOnly: true,
                                                                autoComplete: "off",
                                                                placeholder: 'Start Date',
                                                                name: 'start_date',
                                                                className: 'form-control form-control-sm'
                                                            }}

                                                            locale="id"
                                                        />
                                                    </Form.Group>

                                                    <Form.Group as={Col} xs={6} controlId="id_level">
                                                        <Form.Label>End Date</Form.Label>
                                                        <Datetime
                                                            closeOnSelect={true}
                                                            timeFormat={true}
                                                            setViewDate={data.end_date ? (new Date(data.end_date)) : new Date()}
                                                            value={data.end_date ? (new Date(data.end_date)) : ''}
                                                            onChange={this.handleChangeEndDate}
                                                            inputProps={{
                                                                value: data.end_date ? (moment(new Date(data.end_date)).format('DD-MM-YYYY HH:mm')) : '',
                                                                readOnly: true,
                                                                autoComplete: "off",
                                                                placeholder: 'End Date',
                                                                name: 'end_date',
                                                                className: 'form-control form-control-sm'
                                                            }}

                                                            locale="id"
                                                        />
                                                    </Form.Group>

                                                </Form.Row>
                                                <br/>

                                                <Form.Row>
                                                    <Form.Label>Body</Form.Label>
                                                    <SunEditor
                                                        defaultValue={data.content}
                                                        setContents={data.content}
                                                        onChange={this.handleChangeDesk.bind(this, 'content')}
                                                        setOptions={{
                                                            placeholder: "Body ...",
                                                            maxHeight: 500,
                                                            height: 450,
                                                            buttonList: [
                                                                ['fontSize', 'formatBlock', 'bold', 'underline', 'italic', 'align', 'horizontalRule', 'list', 'lineHeight', 'link', 'strike', 'subscript', 'superscript', 'codeView', 'undo', 'redo', 'fontColor', 'hiliteColor', 'textStyle', 'paragraphStyle', 'blockquote', 'removeFormat']
                                                            ]
                                                        }}
                                                    />
                                                </Form.Row>
                                                <br/>
                                                <AppButton
                                                    onClick={this.handleSubmit.bind(this)}
                                                    className="float-right"
                                                    isLoading={this.props.isAddLoading}
                                                    type="button"
                                                    theme="success">
                                                    Simpan
                                                </AppButton>
                                                <button style={{marginRight: 5}} type="button"
                                                        onClick={() => this.props.history.goBack()}
                                                        className="float-right btn btn-danger">Back
                                                </button>

                                            </Form>
                                        )}

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                {this.props.showFormSuccess ? (<AppSwalSuccess
                    show={this.props.showFormSuccess}
                    title={<div dangerouslySetInnerHTML={{__html: this.props.contentMsg}}/>}
                    type={this.props.tipeSWAL}
                    handleClose={this.props.isError ? this.props.closeSwalError : this.handleClose.bind(this)}
                >
                </AppSwalSuccess>) : ''}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    data: state.pengumuman.dataId || {},
    isError: state.pengumuman.isError,
    isLoading: state.pengumuman.isLoading,
    isAddLoading: state.pengumuman.isAddLoading,
    errorPriority: state.pengumuman.errorPriority || null,
    contentMsg: state.pengumuman.contentMsg || null,
    showFormSuccess: state.pengumuman.showFormSuccess,
    tipeSWAL: state.pengumuman.tipeSWAL,
    user: state.main.currentUser,
});
const mapDispatchToPros = (dispatch) => {
    return {
        onLoad: (param) => {
            dispatch(fetchDataById(param));
        },
        onAdd: (param) => {
            dispatch(addData(param));
        },
        changeProps: (data) => {
            dispatch(chgProps(data));
        },
        resetError: () => {
            dispatch(clearError());
        },
        closeSwal: () => {
            dispatch(closeForm());
            dispatch(resetForm());
        },
        closeSwalError: () => {
            dispatch(closeForm());
        }
    }
}
export default connect(mapStateToProps, mapDispatchToPros)(PengumumanForm);