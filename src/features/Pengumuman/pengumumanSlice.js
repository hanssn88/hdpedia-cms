import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import axios from 'axios';
import {Alert} from 'rsuite';

const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;
export const fetchData = createAsyncThunk(
    'pengumuman/get',
    async (param, thunkAPI) => {
        try {
            const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
            const response = await axios.post(API_URL + '/announcement_list', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    res = {
                        data: data,
                        totalData: data.total_data
                    }
                    return res;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }
                    return thunkAPI.rejectWithValue(res);
                }
            } else {

                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const fetchDataById = createAsyncThunk(
    'pengumuman/getById',
    async (param, thunkAPI) => {
        try {
            const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
            const response = await axios.post(API_URL + '/announcement_detail', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });

            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    return data;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }
                    return thunkAPI.rejectWithValue(res);
                }
            } else {

                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const addData = createAsyncThunk(
    'pengumuman/storeData',
    async (param, thunkAPI) => {
        let res = {};

        try {
            const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
            const response = await axios.post(API_URL + '/announcement_add_or_update', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });

            //let data = '';
            let _data = response;
            if (response.status === 200) {
                let dataa = _data.data;
                //data = dataa.data;
                if (dataa.err_code === '00') {
                    return dataa;
                } else if (dataa.err_code === '06') {
                    Alert.error('Priority number already exist', 5000);
                    return thunkAPI.rejectWithValue(res);
                } else {
                    res = {
                        isAddLoading: false,
                        showFormSuccess: false,
                        showFormAdd: true,
                        err_msg: dataa.err_msg,
                        contentMsg: null,
                    }
                    return thunkAPI.rejectWithValue(res);
                }

            } else {
                res = {
                    isAddLoading: false,
                    showFormSuccess: true,
                    showFormAdd: false,
                    contentMsg: "<div style='font-size:20px; text-align:center;'><strong>Failed</strong>, Something wrong</div>",
                    err_msg: null
                }
                console.log('Error', _data);
                return thunkAPI.rejectWithValue(res);
            }
        } catch (e) {
            res = {
                isAddLoading: false,
                showFormSuccess: true,
                showFormAdd: false,
                contentMsg: "<div style='font-size:20px; text-align:center;'><strong>Failed</strong>, Something wrong</div>",
                err_msg: null
            }
            console.log('Error catch', e.response.data);
            return thunkAPI.rejectWithValue(res);
        }
    }
);

export const deleteData = createAsyncThunk(
    'pengumuman/delete',
    async (param, thunkAPI) => {
        let res = {};
        try {
            const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
            const response = await axios.post(API_URL + '/announcement_delete', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });

            let data = '';
            let _data = response;
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    return dataa;
                } else {
                    res = {
                        isAddLoading: false,
                        showFormSuccess: true,
                        showFormDelete: false,
                        contentMsg: "<div style='font-size:20px; text-align:center;'><strong>Failed</strong>, Something wrong</div>",
                        err_msg: dataa.err_msg
                    }
                    console.log('Error err_code', data);
                    return thunkAPI.rejectWithValue(res);
                }
            } else {
                res = {
                    isAddLoading: false,
                    showFormSuccess: true,
                    showFormDelete: false,
                    contentMsg: "<div style='font-size:20px; text-align:center;'><strong>Failed</strong>, Something wrong</div>",
                    err_msg: null
                }
                console.log('Error', _data);
                return thunkAPI.rejectWithValue(res);
            }
        } catch (e) {
            res = {
                isAddLoading: false,
                showFormSuccess: true,
                showFormDelete: false,
                contentMsg: "<div style='font-size:20px; text-align:center;'><strong>Failed</strong>, Something wrong</div>",
                err_msg: null
            }
            console.log('Error catch', e.response.data);
            return thunkAPI.rejectWithValue(res);
        }
    }
);

const initialState = {
    data: [],
    dataId: {},
    totalData: 0,
    isError: false,
    isLoading: false,
    isAddLoading: false,
    errorPriority: null,
    contentMsg: null,
    showFormAdd: false,
    showFormSuccess: false,
    showFormDelete: false,
    tipeSWAL: "success"
};


export const pengumumanSlice = createSlice({
    name: 'pengumuman',
    initialState,
    reducers: {
        clearError: (state) => {
            state.isAddLoading = false;
            state.errorPriority = null;
            return state;
        },
        closeForm: (state) => {
            state.showFormAdd = false;
            state.showFormDelete = false;
            state.errorPriority = null;
            state.showFormSuccess = false;
        },
        confirmDel: (state) => {
            state.isAddLoading = false;
            state.showFormDelete = true;
            state.errorPriority = false;
            return state;
        },
        chgProps: (state, {payload}) => {
            state.dataId[payload.key] = payload.value;
        },
        resetForm: (state) => {
            state.dataId = {
                ...state.dataId,
                question: '',
                answer: '',
                priority: 0
            }
        },
    },
    extraReducers: {
        [fetchData.fulfilled]: (state, {payload}) => {
            state.totalData = payload.totalData;
            state.data = payload.data;
            state.isLoading = false;
            state.isError = false;
            state.dataId = {};
            //return state;
        },
        [fetchData.rejected]: (state, {payload}) => {
            state.totalData = 0;
            state.data = [];
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [fetchData.pending]: (state) => {
            state.totalData = 0;
            state.data = [];
            state.isLoading = true;
        },
        [fetchDataById.fulfilled]: (state, {payload}) => {
            state.dataId = payload;
            state.isLoading = false;
            state.isError = false;
            //return state;
        },
        [fetchDataById.rejected]: (state, {payload}) => {
            //console.log('payload', payload);
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [fetchDataById.pending]: (state) => {
            state.isLoading = true;
        },
        [addData.fulfilled]: (state) => {
            state.isError = false;
            state.tipeSWAL = "success";
            state.showFormSuccess = true;
            state.showFormAdd = false;
            state.isAddLoading = false;
            state.contentMsg = "<div style='font-size:20px; text-align:center;'><strong>Success</strong>, Data berhasil disimpan</div>";
        },
        [addData.rejected]: (state, {payload}) => {
            console.log(payload);
            state.isAddLoading = payload !== undefined ? payload.isAddLoading : false;
            state.showFormSuccess = payload !== undefined ? payload.showFormSuccess : true;
            state.showFormAdd = payload !== undefined ? payload.showFormAdd : false;
            state.tipeSWAL = "error";
            state.contentMsg = payload !== undefined ? payload.contentMsg : "<div style='font-size:20px; text-align:center;'><strong>Failed</strong>, Something wrong</div>";
            state.isError = true;
            state.errorPriority = payload !== undefined ? payload.err_msg : null;
        },
        [addData.pending]: (state) => {
            state.isAddLoading = true;
        },
        [deleteData.fulfilled]: (state) => {
            state.showFormDelete = false;
            state.isError = false;
            state.tipeSWAL = "success";
            state.showFormSuccess = true;
            state.isAddLoading = false;
            state.contentMsg = "<div style='font-size:20px; text-align:center;'><strong>Success</strong>, Data berhasil dihapus</div>";
        },
        [deleteData.rejected]: (state, {payload}) => {
            state.isAddLoading = payload !== undefined ? payload.isAddLoading : false;
            state.showFormSuccess = payload !== undefined ? payload.showFormSuccess : true;
            state.showFormDelete = payload !== undefined ? payload.showFormDelete : false;
            state.tipeSWAL = "error";
            state.contentMsg = payload !== undefined ? payload.contentMsg : "<div style='font-size:20px; text-align:center;'><strong>Failed</strong>, Something wrong</div>";
            state.isError = true;
            state.errorPriority = payload !== undefined ? payload.err_msg : null;
        },
        [deleteData.pending]: (state) => {
            state.isAddLoading = true;
        },
    }
})

export const {clearError, confirmDel, closeForm, chgProps, resetForm} = pengumumanSlice.actions;
export const userSelector = (state) => state.pengumuman;