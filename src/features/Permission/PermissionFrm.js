import React, {Component} from 'react'
import {connect} from 'react-redux';
import PermissionService from './permissionService';
import {Form, Table} from 'react-bootstrap';
import Loading from '../../components/loading/MyLoading';
import {setPermission} from "./permissionSlice";

class PermissionFrm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: {},
            errMsg: {},
            konsumen_all: '',
            appsLoading: false,
        }
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.getData();
    }

    getData = async () => {
        const selectedId = sessionStorage.getItem('idLevelHDPedia');
        this.setState({
            ...this.state,
            appsLoading: true,
            id_level: selectedId,
            selected: {...this.state.selected, level_id: selectedId}
        });
        let param = {};
        param['level_id'] = selectedId;
        PermissionService.postData(param, 'VIEW_HAK_AKSES')
            .then(response => {
                setTimeout(() => {
                    if (response.data.err_code === "00" || response.data.err_code === "success") {
                        this.setState({
                            ...this.state,
                            selected: {
                                ...response.data.data,
                                level_id: selectedId
                            }
                        });
                    }
                    this.setState({...this.state, appsLoading: false, loadTbl: false});
                }, 200);
            })
            .catch(e => {
                console.log(e);
                this.setState({...this.state, appsLoading: false, loadTbl: false});
            });
    }


    async handleChange(event) {
        const {name} = event.target;
        var val = this.state.selected[name] > 0 ? 0 : 1;
        await this.setState({
            selected: {
                ...this.state.selected,
                [name]: val,
                showFormSuccess: true
            }
        });

        this.props.onAdd(this.state.selected);
    }

    render() {

        const {selected} = this.state;

        return (
            <div>
                <div className="content-wrapper">
                    {/* Content Header (Page header) */}
                    <div className="content-header">
                        <div className="container-fluid">
                            <div className="row mb-2">
                                <div className="col-sm-6">
                                    <h1 className="m-0">Hak Akses {selected.level_name}</h1>
                                </div>
                                {/* /.col */}

                            </div>
                            {/* /.row */}
                        </div>
                        {/* /.container-fluid */}
                    </div>
                    {/* /.content-header */}
                    {/* Main content */}

                    <section className="content">
                        <div className="col-12">
                            {this.state.appsLoading ? (
                                <Loading/>
                            ) : (

                                <div className="card card-success shadow-lg" style={{"minHeight": "800px"}}>

                                    <div className="card-body">
                                        <Table bordered>
                                            <thead>
                                            <tr>
                                                <th style={{textAlign: "center"}}>Hak Akses</th>
                                                <th style={{textAlign: "center", width: "10%"}}>View</th>
                                                <th style={{textAlign: "center", width: "10%"}}>Add</th>
                                                <th style={{textAlign: "center", width: "10%"}}>Edit</th>
                                                <th style={{textAlign: "center", width: "10%"}}>Delete</th>
                                                <th style={{textAlign: "center", width: "10%"}}>#</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td valign="middle">Member Apps</td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="member_apps_view"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.member_apps_view > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="member_apps_view"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="member_apps_add"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.member_apps_add > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="member_apps_add"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="member_apps_edit"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.member_apps_edit > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="member_apps_edit"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="member_apps_delete"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.member_apps_delete > 0 ? "checked" : ""}
                                                            label=""
                                                            name="member_apps_delete"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">Pasien</td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="patient_view"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.patient_view > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="patient_view"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="patient_add" style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.patient_add > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="patient_add"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="patient_edit" style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.patient_edit > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="patient_edit"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="patient_delete"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.patient_delete > 0 ? "checked" : ""}
                                                            label=""
                                                            name="patient_delete"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="patient_form" style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.patient_form > 0 ? "checked" : ""}
                                                            label="Isi Form"
                                                            name="patient_form"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">Jadwal Treatment</td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="treatment_view"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.treatment_view > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="treatment_view"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="treatment_add"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.treatment_add > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="treatment_add"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="treatment_edit"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.treatment_edit > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="treatment_edit"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="treatment_delete"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.treatment_delete > 0 ? "checked" : ""}
                                                            label=""
                                                            name="treatment_delete"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">Jadwal Dokter</td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="jadwal_dokter_view"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.jadwal_dokter_view > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="jadwal_dokter_view"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="jadwal_dokter_add"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.jadwal_dokter_add > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="jadwal_dokter_add"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="jadwal_dokter_edit"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.jadwal_dokter_edit > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="jadwal_dokter_edit"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="jadwal_dokter_delete"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.jadwal_dokter_delete > 0 ? "checked" : ""}
                                                            label=""
                                                            name="jadwal_dokter_delete"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">Dokter</td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="dokter_view"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.dokter_view > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="dokter_view"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="dokter_add"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.dokter_add > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="dokter_add"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="dokter_edit"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.dokter_edit > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="dokter_edit"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="dokter_delete"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.dokter_delete > 0 ? "checked" : ""}
                                                            label=""
                                                            name="dokter_delete"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">Barang Masuk</td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="bm_view"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.bm_view > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="bm_view"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="bm_add"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.bm_add > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="bm_add"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">

                                                </td>
                                                <td valign="middle" align="center">

                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">Barang Keluar</td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="bk_view"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.bk_view > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="bk_view"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="bk_add"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.bk_add > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="bk_add"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">

                                                </td>
                                                <td valign="middle" align="center">

                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">Virtual Stock</td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="vs_view"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.vs_view > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="vs_view"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="vs_add"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.vs_add > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="vs_add"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">

                                                </td>
                                                <td valign="middle" align="center">

                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">FAQ</td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="faq_view"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.faq_view > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="faq_view"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="faq_add"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.faq_add > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="faq_add"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="faq_edit"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.faq_edit > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="faq_edit"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="faq_delete"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.faq_delete > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="faq_delete"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">Klinik</td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="klinik_view"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.klinik_view > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="klinik_view"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="klinik_add"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.klinik_add > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="klinik_add"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="klinik_edit"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.klinik_edit > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="klinik_edit"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="klinik_delete"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.klinik_delete > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="klinik_delete"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">Vendor</td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="vendor_view"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.vendor_view > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="vendor_view"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="vendor_add"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.vendor_add > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="vendor_add"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="vendor_edit"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.vendor_edit > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="vendor_edit"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="vendor_delete"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.vendor_delete > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="vendor_delete"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">Products</td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="product_view"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.product_view > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="product_view"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="product_add"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.product_add > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="product_add"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="product_edit"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.product_edit > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="product_edit"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="product_delete"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.product_delete > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="product_delete"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">Perawat</td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="perawat_view"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.perawat_view > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="perawat_view"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="perawat_add"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.perawat_add > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="perawat_add"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="perawat_edit"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.perawat_edit > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="perawat_edit"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="perawat_delete"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.perawat_delete > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="perawat_delete"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">Staf Gudang</td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="staff_gudang_view"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.staff_gudang_view > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="staff_gudang_view"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="staff_gudang_add"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.staff_gudang_add > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="staff_gudang_add"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="staff_gudang_edit"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.staff_gudang_edit > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="staff_gudang_edit"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="staff_gudang_delete"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.staff_gudang_delete > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="staff_gudang_delete"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">Staf Rekam Medis</td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="staff_rm_view"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.staff_rm_view > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="staff_rm_view"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="staff_rm_add"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.staff_rm_add > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="staff_rm_add"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="staff_rm_edit"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.staff_rm_edit > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="staff_rm_edit"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="staff_rm_delete"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.staff_rm_delete > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="staff_rm_delete"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">Tim Medis</td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="tim_medis_view"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.tim_medis_view > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="tim_medis_view"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="tim_medis_add"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.tim_medis_add > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="tim_medis_add"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="tim_medis_edit"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.tim_medis_edit > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="tim_medis_edit"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="tim_medis_delete"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.tim_medis_delete > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="tim_medis_delete"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">Staf Admin</td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="staff_admin_view"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.staff_admin_view > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="staff_admin_view"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="staff_admin_add"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.staff_admin_add > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="staff_admin_add"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="staff_admin_edit"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.staff_admin_edit > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="staff_admin_edit"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="staff_admin_delete"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.staff_admin_delete > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="staff_admin_delete"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">Super Admin</td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="superadmin_view"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.superadmin_view > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="superadmin_view"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="superadmin_add"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.superadmin_add > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="superadmin_add"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="superadmin_edit"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.superadmin_edit > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="superadmin_edit"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="superadmin_delete"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.superadmin_delete > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="superadmin_delete"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">Setting</td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="setting_view"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.setting_view > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="setting_view"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">

                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="setting_edit"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.setting_edit > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="setting_edit"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">

                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">Info Medis Pasien</td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="banner_view"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.banner_view > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="banner_view"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="banner_add"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.banner_add > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="banner_add"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="banner_edit"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.banner_edit > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="banner_edit"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td valign="middle" align="center">
                                                    <Form.Group controlId="banner_delete"
                                                                style={{marginBottom: "0rem"}}>
                                                        <Form.Check
                                                            onChange={this.handleChange}
                                                            checked={selected.banner_delete > 0 ? ("checked") : ""}
                                                            label=""
                                                            name="banner_delete"
                                                            custom
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </Table>

                                    </div>

                                </div>

                            )}


                        </div>

                    </section>


                </div>
                <div>

                </div>

            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.main.currentUser,
    }
}

const mapDispatchToPros = (dispatch) => {
    return {
        onLoad: (queryString) => {
            //dispatch(fetchData(queryString));
        },
        onAdd: (data) => {
            console.log(data);
            dispatch(setPermission(data));
        }

    }
}

export default connect(mapStateToProps, mapDispatchToPros)(PermissionFrm);