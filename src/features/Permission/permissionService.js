import axios from "axios";


const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

class PermissionService {
    postData(param, action) {
        console.log(action);
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        switch (action) {
            case "VIEW_HAK_AKSES":
                return axios.post(API_URL + "/menu_access/get_level_permission", param, {
                    headers: {
                        'Authorization': 'Bearer ' + token
                    }
                });

            default:
                return axios.post(API_URL + "/menu_access/get_level_permission", param, {
                    headers: {
                        'Authorization': 'Bearer ' + token
                    }
                });
        }
    }
}

export default new PermissionService()