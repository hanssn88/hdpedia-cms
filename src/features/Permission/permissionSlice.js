import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import axios from 'axios';

const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

export const fetchData = createAsyncThunk(
    'permission/getLevel',
    async (param, thunkAPI) => {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/get_level', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00' || dataa.err_code === 'success') {
                    res = {
                        data: data,
                        totalData: dataa.total_data
                    }
                    return res;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return thunkAPI.rejectWithValue(res);
                }
            } else {
                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const setPermission = createAsyncThunk(
    'permission/setPermission',
    async (param, thunkAPI) => {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/menu_access/post', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00' || dataa.err_code === 'success') {
                    res = {
                        data: data,
                        totalData: dataa.total_data
                    }
                    return res;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return thunkAPI.rejectWithValue(res);
                }
            } else {
                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

const initialState = {
    data: [],
    totalData: 0,
    isError: false,
    isLoading: false,
    isAddLoading: false,
    errorPriority: null,
    contentMsg: null,
    showFormAdd: false,
    showFormSuccess: false,
    showFormDelete: false,
    tipeSWAL: "success"
};


export const permissionSlice = createSlice({
    name: 'klinik',
    initialState,
    reducers: {
        addForm: (state) => {
            state.isAddLoading = false;
            state.showFormAdd = true;
            state.errorPriority = null;
            return state;
        },
        clearError: (state) => {
            state.isAddLoading = false;
            state.errorPriority = null;
            return state;
        },
        closeForm: (state) => {
            state.showFormAdd = false;
            state.showFormDelete = false;
            state.errorPriority = null;
            state.showFormSuccess = false;
        },
        confirmDel: (state) => {
            state.isAddLoading = false;
            state.showFormDelete = true;
            state.errorPriority = false;
            return state;
        }
    },
    extraReducers: {
        [fetchData.fulfilled]: (state, {payload}) => {
            state.totalData = payload.totalData;
            state.data = payload.data;
            state.isLoading = false;
            state.isError = false;

            //return state;
        },
        [fetchData.rejected]: (state, {payload}) => {
            //console.log('payload', payload);
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [fetchData.pending]: (state) => {
            state.isLoading = true;

        },
        [setPermission.fulfilled]: (state, {payload}) => {
            state.isLoading = false;
            state.isError = false;

            //return state;
        },
        [setPermission.rejected]: (state, {payload}) => {
            //console.log('payload', payload);
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [setPermission.pending]: (state) => {
            state.isLoading = true;

        },
    }
})

export const {addForm, clearError, confirmDel, closeForm} = permissionSlice.actions;
export const userSelector = (state) => state.permission;
//export default mainSlice.reducer;