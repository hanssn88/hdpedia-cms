import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux';
import {addData, addForm, clearError, closeForm, confirmDel, deleteData, fetchData} from './productSlice'
import ReactDatatable from '@ashvin27/react-datatable';
import AppModal from '../../components/modal/MyModal';
import AppButton from '../../components/button/Button';
import {AppSwalSuccess} from '../../components/modal/SwalSuccess';
import {Badge, Form} from "react-bootstrap";

class Products extends Component {

    constructor(props) {
        super(props);
        this.initSelected = {
            id: '',
            name: '',
            tipe: '',
            satuan: '',
            validasi_batch: '',
            operator_by: '',
        }
        this.state = {
            sort_order: "ASC",
            sort_column: "name",
            keyword: '',
            page_number: 1,
            per_page: 10,
            selected: this.initSelected,
            errMsg: this.initSelected,
            loadingForm: false,
        }
    }

    componentDidMount() {
        this.fetchProfileAdmin();
        this.props.onLoad(this.state);
    }

    fetchProfileAdmin = async () => {
        const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;
        const token = localStorage.getItem(tokenLogin);
        if (!token) {
            alert('session invalid');
            window.location.reload();
        }
    }

    handleClose = () => {
        this.props.closeModal();
    };

    tableChangeHandler = (data) => {
        let queryString = this.state;
        Object.keys(data).map((key) => {
            if (key === "sort_order" && data[key]) {
                queryString.sort_order = data[key].order;
                queryString.sort_column = data[key].column;
            }
            if (key === "page_number") {
                queryString.page_number = data[key];
            }
            if (key === "page_size") {
                queryString.per_page = data[key];
            }
            if (key === "filter_value") {
                queryString.keyword = data[key];
            }
            return true;
        });
        this.props.onLoad(this.state);
    }

    handleChange(event) {
        const {name, value} = event.target
        var val = value;
        this.props.resetError();
        if (name === "validasi_batch") {
            val = event.target.checked ? 1 : 0;
        }
        this.setState({
            selected: {
                ...this.state.selected,
                [name]: val
            },
            errMsg: {
                ...this.state.errMsg,
                [name]: val ? '' : this.state.errMsg[name]
            },
            loadingForm: false
        });
        if (!this.state.selected.operator_by) this.setState({
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
    }

    discardChanges = () => {
        this.setState({errMsg: {}, selected: this.initSelected, loadingForm: false});
        if (!this.state.selected.operator_by) this.setState({
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
        this.props.showForm();
    }

    editRecord = async (record) => {
        this.setState({
            loadingForm: false,
            errMsg: this.initSelected,
            selected: {...record, operator_by: this.props.user.id_operator}
        });
        this.props.showForm(true);
    }

    deleteRecord = (record) => {
        this.setState({
            selected: {...record, operator_by: this.props.user.id_operator}
        });
        this.props.showConfirmDel(true);
    }

    handleSubmit() {
        var errors = this.state.errMsg;
        this.setState({
            ...this.state,
            loadingForm: true,
        });
        errors.name = !this.state.selected.name ? "Required" : '';

        if (!this.state.selected.operator_by) this.setState({
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
        this.setState({errors});

        if (this.validateForm(this.state.errMsg)) {
            this.props.onAdd(this.state.selected);
        } else {
            console.error('Invalid Form')
        }

        this.setState({
            ...this.state,
            loadingForm: false,
        });
    }

    validateForm(errors) {
        let valid = true;
        Object.values(errors).forEach(
            (val) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    handleDelete() {
        this.props.onDelete(this.state.selected)
    }

    render() {
        const {data, user} = this.props;
        const {selected, errMsg} = this.state;
        const permission = user?.permission;

        const columns = [
            {
                key: "no",
                text: "No.",
                width: 20,
                align: "center",
                sortable: false,
                cell: (row, index) => <div
                    style={{textAlign: "center"}}>{((this.state.page_number - 1) * this.state.per_page) + index + 1 + '.'}</div>,
                row: 0
            },
            {
                key: "name",
                text: "Name",
                align: "center",
                sortable: true,
            },
            {
                key: "tipe",
                text: "Tipe",
                align: "center",
                sortable: true,
            },
            {
                key: "satuan",
                text: "Satuan",
                align: "center",
                sortable: true,
            },
            {
                key: "validasi_batch",
                text: "Validasi Batch",
                align: "center",
                sortable: true,
                width: 150,
                cell: record => {
                    return (
                        <Fragment>
                            <div style={{textAlign: "center"}}>
                                {record.validasi_batch === 1 ? (
                                    <Badge variant="primary"
                                           style={{padding: "0.5em", width: "48px"}}>Active</Badge>) : (
                                    <Badge variant="danger" style={{padding: "0.5em"}}>Inactive</Badge>)}
                            </div>

                        </Fragment>
                    )
                }
            },
            {
                key: "action",
                text: "Action",
                width: 140,
                align: "center",
                sortable: false,
                cell: record => {

                    return (
                        <div style={{textAlign: "center"}}>
                            <Fragment>
                                <button
                                    disabled={parseInt(permission?.product_edit) > 0 ? false : true}
                                    className="btn btn-xs btn-success"
                                    onClick={e => this.editRecord(record)}
                                    style={{marginRight: '5px'}}>
                                    <i className="fa fa-edit"></i> Edit
                                </button>
                                <button
                                    disabled={parseInt(permission?.product_delete) > 0 ? false : true}
                                    className="btn btn-danger btn-xs"
                                    onClick={() => this.deleteRecord(record)}>
                                    <i className="fa fa-trash"></i> Delete
                                </button>
                            </Fragment>
                        </div>
                    );
                }
            }
        ];
        const config = {
            key_column: 'id_vendor',
            page_size: 10,
            length_menu: [10, 20, 50],
            show_filter: true,
            show_pagination: true,
            pagination: 'advance',
            button: {
                excel: false,
                print: false
            },
            language: {
                loading_text: "Please be patient while data loads..."
            }
        }
        const frmUser = <Form id="myForm">

            <Form.Group controlId="name">
                <Form.Label>Nama Produk</Form.Label>

                {errMsg.name ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.name}
                    </span>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="name"
                    type="text"
                    value={selected.name}
                    onChange={this.handleChange.bind(this)}
                    placeholder="Nama Produk"/>
            </Form.Group>

            <Form.Group controlId="tipe">
                <Form.Label>Tipe</Form.Label>
                {errMsg.tipe ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.tipe}
                    </span>) : ''}
                <Form.Control
                    size="sm"
                    name="tipe"
                    as="select"
                    value={selected.tipe ? selected.tipe : ""}
                    onChange={this.handleChange.bind(this)}>
                    <option value="">- Pilih -</option>
                    <option value="Limbako" selected={selected.tipe === "Limbako" ? true : false}>Limbako</option>
                    <option value="Non Limbako" selected={selected.tipe === "Non Limbako" ? true : false}>Non Limbako
                    </option>
                    <option value="Obat" selected={selected.tipe === "Obat" ? true : false}>Obat</option>
                </Form.Control>
            </Form.Group>

            <Form.Group controlId="satuan">
                <Form.Label>Satuan</Form.Label>

                {errMsg.satuan ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.satuan}
                    </span>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="satuan"
                    type="text"
                    value={selected.satuan}
                    onChange={this.handleChange.bind(this)}
                    placeholder="Satuan"/>
            </Form.Group>

            <Form.Group controlId="validasi_batch">
                <Form.Check
                    checked={Number(selected.validasi_batch) === 1 ? true : false}
                    name="validasi_batch"
                    value="1"
                    type="checkbox"
                    onChange={this.handleChange.bind(this)}
                    style={{fontWeight: "bold"}}
                    label="Validasi Batch"
                />
            </Form.Group>


        </Form>;

        const contentDelete = <div
            dangerouslySetInnerHTML={{__html: '<div id="caption" style="padding-bottom:20px;">Apakah anda yakin <br/>akan menghapus data ini ?</div>'}}/>;
        return (

            <div className="content-wrapper">
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0">Master Produk</h1>
                            </div>
                            {/* /.col */}

                        </div>
                        {/* /.row */}
                    </div>
                    {/* /.container-fluid */}
                </div>
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                {/* card start */}
                                <div className="card card-success shadow-lg" style={{"minHeight": "800px"}}>
                                    <div className="card-header card-header-content">
                                        <AppButton
                                            disabled={parseInt(permission?.product_add) > 0 ? false : true}
                                            isLoading={this.props.isLoading}
                                            theme="info"
                                            onClick={this.discardChanges}
                                            icon='add'
                                        >
                                            Add
                                        </AppButton>

                                    </div>
                                    <div className="card-body">
                                        {data ? (
                                            <ReactDatatable
                                                config={config}
                                                records={data}
                                                columns={columns}
                                                dynamic={true}
                                                onChange={this.tableChangeHandler}
                                                loading={this.props.isLoading}
                                                total_record={this.props.totalData}
                                            />
                                        ) : (<p>No Data ...</p>)}

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <AppModal
                    show={this.props.showFormAdd}
                    form={frmUser}
                    size="xs"
                    backdrop={false}
                    keyboard={false}
                    title="Add/Edit Produk"
                    titleButton="Save change"
                    themeButton="success"
                    handleClose={this.handleClose}
                    isLoading={this.props.isAddLoading ? this.props.isAddLoading : this.state.loadingForm}
                    formSubmit={this.handleSubmit.bind(this)}
                ></AppModal>
                <AppModal
                    show={this.props.showFormDelete}
                    size="xs"
                    form={contentDelete}
                    handleClose={this.handleClose}
                    backdrop="static"
                    keyboard={false}
                    title="Delete"
                    titleButton="Delete"
                    themeButton="danger"
                    isLoading={this.props.isAddLoading}
                    formSubmit={this.handleDelete.bind(this)}
                ></AppModal>
                {this.props.showFormSuccess ? (<AppSwalSuccess
                    show={this.props.showFormSuccess}
                    title={<div dangerouslySetInnerHTML={{__html: this.props.contentMsg}}/>}
                    type={this.props.tipeSWAL}
                    handleClose={this.props.closeSwal}
                >
                </AppSwalSuccess>) : ''}
            </div>


        )
    }
}

const mapStateToProps = (state) => ({
    data: state.products.data || [],
    totalData: state.products.totalData,
    isError: state.products.isError,
    isLoading: state.products.isLoading,
    showFormAdd: state.products.showFormAdd,
    isAddLoading: state.products.isAddLoading,
    errorPriority: state.products.errorPriority || null,
    contentMsg: state.products.contentMsg || null,
    showFormSuccess: state.products.showFormSuccess,
    showFormDelete: state.products.showFormDelete,
    tipeSWAL: state.products.tipeSWAL,
    user: state.main.currentUser,
});
const mapDispatchToPros = (dispatch) => {
    return {
        onLoad: (param) => {
            dispatch(fetchData(param));
        },
        onAdd: (param) => {
            dispatch(addData(param));
        },
        showConfirmDel: (data) => {
            dispatch(confirmDel(data));
        },
        showForm: () => {
            dispatch(addForm());
        },
        onDelete: (param) => {
            dispatch(deleteData(param));
        },
        closeModal: () => {
            dispatch(closeForm());

        },
        closeSwal: () => {
            dispatch(closeForm());
            const queryString = {
                sort_order: "ASC",
                sort_column: "name",
                per_page: 10
            }
            dispatch(fetchData(queryString));
        },
        resetError: () => {
            dispatch(clearError());
        },

        closeSwalError: () => {
            dispatch(closeForm());
        }
    }
}
export default connect(mapStateToProps, mapDispatchToPros)(Products);