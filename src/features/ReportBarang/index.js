import moment from 'moment';
import "moment/locale/id";
import React, {Component} from 'react';
import {Form} from "react-bootstrap";
import Datetime from 'react-datetime';
import "react-datetime/css/react-datetime.css";
import {connect} from 'react-redux';
import AppButton from '../../components/button/Button';


var yesterday = moment();
var valid_startDate = function (current) {
    return current.isBefore(yesterday);
};

class ReportBarang extends Component {

    constructor(props) {
        super(props);
        this.initSelected = {
            klinik_id: '',
            user_id: '',
            date: '',
            shift: '',
            end_date: '',
            start_date: '',
        }
        this.state = {
            validSd: valid_startDate,
            validEd: valid_startDate,
            sort_order: "ASC",
            sort_column: "name",
            keyword: '',
            page_number: 1,
            per_page: 10,
            selected: this.initSelected,
            errMsg: this.initSelected,
            loadingForm: false,
            klinik_id: sessionStorage.getItem('klinikHD'),
            end_date: '',
            start_date: '',
        }

    }


    render() {





        return (

            <div className="content-wrapper">
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0">Report Barang</h1>
                            </div>
                            {/* /.col */}

                        </div>
                        {/* /.row */}
                    </div>
                    {/* /.container-fluid */}
                </div>
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                {/* card start */}
                                <div className="card card-success shadow-lg" style={{"minHeight": "800px"}}>

                                    <div className="card-header card-header-content">

                                    </div>


                                    <div className="card-body">
                                        <Form>


                                            <Form.Group controlId="start_date">
                                                <Form.Label>Filter Date</Form.Label>
                                                <Datetime
                                                    setViewDate={this.state.start_date ? (new Date(this.state.start_date)) : ''}
                                                    onChange={this.handleChangeStartDate}
                                                    timeFormat={false}
                                                    closeOnSelect={true}
                                                    inputProps={{
                                                        value: this.state.start_date ? (moment(new Date(this.state.start_date)).format('DD-MM-YYYY')) : '',
                                                        readOnly: true,
                                                        autoComplete: "off",
                                                        placeholder: 'Filter Date',
                                                        name: 'start_date',
                                                        className: 'form-control form-control-sm'
                                                    }}

                                                    locale="id"
                                                />
                                            </Form.Group>

                                        </Form>
                                        <AppButton
                                            // onClick={this.handleSearch.bind(this)}
                                            // isLoading={this.props.isLoading}
                                            type="button"
                                            style={{marginRight: 5}}
                                            theme="success">
                                            Search
                                        </AppButton>
                                        <AppButton
                                            // onClick={this.handleReset.bind(this)}
                                            // isLoading={this.props.isLoading}
                                            type="button"
                                            style={{marginRight: 5}}
                                            theme="warning">
                                            Reset
                                        </AppButton>
                                        <hr/>
                                        <br/>
                                        <p>No Data ...</p>
                                        {/* {data ? (
                                            <ReactDatatable
                                                // config={config}
                                                // records={data}
                                                columns={columns}
                                                dynamic={true}
                                                // onChange={this.tableChangeHandler}
                                                loading={this.props.isLoading}
                                                total_record={this.props.totalData}
                                            />
                                        ) : (<p>No Data ...</p>)} */}

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>


            </div>


        )
    }
}

const mapStateToProps = (state) => ({});
const mapDispatchToPros = (dispatch) => {
    return {}
}
export default connect(mapStateToProps, mapDispatchToPros)(ReportBarang);