import ReactDatatable from '@ashvin27/react-datatable';
import moment from 'moment';
import "moment/locale/id";
import React, {Component, Fragment} from 'react';
import {Col, Form} from "react-bootstrap";
import "react-datetime/css/react-datetime.css";
import NumberFormat from 'react-number-format';
import {connect} from 'react-redux';
import AppButton from '../../components/button/Button';
import AppModal from '../../components/modal/MyModal';
import {SelectBm, SelectKlinik} from "../../components/modal/MySelect";
import {AppSwalSuccess} from '../../components/modal/SwalSuccess';
import {addData, addForm, clearError, closeForm, confirmDel, fetchData, fetchDataReport} from './soSlice';
import ReactExport from 'react-data-export';

var yesterday = moment().subtract(1, 'day');
var valid_startDate = function (current) {
    return current.isAfter(yesterday);
};

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;

class StockOpname extends Component {

    constructor(props) {
        super(props);
        this.initSelected = {
            id_bm: '',
            name: '',
            qty_difference: '',
            type: '',
            shift: '',
            satuan: '',
            klinik_id: '',
            id_kliniks: '',
            nama_kliniks: '',
            operator_by: '',
        }
        this.state = {
            validSd: valid_startDate,
            validEd: valid_startDate,
            sort_order: "DESC",
            sort_column: "name",
            keyword: '',
            page_number: 1,
            per_page: 10,
            klinik_id: '',
            id_kliniks: '',
            nama_kliniks: '',
            selected: this.initSelected,
            errMsg: this.initSelected,
            loadingForm: false,
        }
    }

    componentDidMount() {
        this.fetchProfileAdmin();
    }

    fetchProfileAdmin = async () => {
        const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;
        const token = localStorage.getItem(tokenLogin);
        if (!token) {
            alert('session invalid');
            window.location.reload();
        }
        this.props.onLoad(this.state);


    }

    handleClose = () => {
        this.props.closeModal();
    };

    handleCloseSwal = () => {
        this.props.closeSwal(this.state.klinik_id);
    };

    tableChangeHandler = (data) => {
        let queryString = this.state;
        Object.keys(data).map((key) => {
            if (key === "sort_order" && data[key]) {
                queryString.sort_order = data[key].order;
                queryString.sort_column = data[key].column;
            }
            if (key === "page_number") {
                queryString.page_number = data[key];
            }
            if (key === "page_size") {
                queryString.per_page = data[key];
            }
            if (key === "filter_value") {
                queryString.keyword = data[key];
            }
            return true;
        });
        this.props.onLoad(this.state);
    }

    handleChange(event) {
        const {name, value} = event.target
        var val = value;
        this.props.resetError();
        this.setState({
            selected: {
                ...this.state.selected,
                [name]: val
            },
            errMsg: {
                ...this.state.errMsg,
                [name]: val ? '' : this.state.errMsg[name]
            },
            loadingForm: false
        });
        if (!this.state.selected.operator_by) this.setState({
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
    }


    discardChanges = () => {
        this.setState({errMsg: {}, selected: this.initSelected, loadingForm: false});
        if (this.props.user.id_level !== 1) {
            this.setState({
                id_kliniks: this.props.user.id_klinik,
                selected: {
                    ...this.state.selected,
                    id_kliniks: this.props.user.id_klinik
                }
            });
        } else {
            this.setState({
                selected: {
                    ...this.state.selected,
                    id_kliniks: this.state.id_kliniks, nama_kliniks: this.state.nama_kliniks,
                    klinik_id: this.state.id_kliniks,
                }
            });
        }
        this.props.showForm();
    }


    handleSubmit() {
        var errors = this.state.errMsg;
        this.setState({
            ...this.state,
            loadingForm: true,
        });
        errors.id_bm = !this.state.selected.id_bm ? "Required" : '';
        errors.qty_difference = !this.state.selected.qty_difference ? "Required" : '';
        errors.type = !this.state.selected.type ? "Required" : '';
        // if (this.state.selected.qty_difference) {
        //     const dt = this.state.selected.qty_difference.split(',');
        //     let qty_difference = '';
        //     for (let i = 0; i < dt.length; i++) {
        //         qty_difference += dt[i];
        //     }
        //     if (Number(qty_difference) > Number(this.state.selected.stok)) errors.qty_difference = 'Stok tidak cukup';
        // }
        if (!this.state.selected.operator_by) this.setState({
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
        this.setState({errors});

        if (this.validateForm(this.state.errMsg)) {
            this.props.onAdd(this.state.selected);
        } else {
            console.error('Invalid Form')
        }

        this.setState({
            ...this.state,
            loadingForm: false,
        });
    }

    validateForm(errors) {
        let valid = true;
        Object.values(errors).forEach(
            (val) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    async onchangeSelect(evt) {
        const dt = evt.value.split('_');
        const dtId = evt.value;
        await this.setState({
            ...this.setState,
            selected: {
                ...this.state.selected,
                id_bm: dtId, name: evt.label, stok: dt[1], satuan: dt[2]
            }
        });
        if (!this.state.selected.operator_by) this.setState({
            ...this.setState,
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
    };

    async onchangeSelectKlinik(evt) {
        await this.setState({
            ...this.setState,
            klinik_id: evt.value,
            id_kliniks: evt.value,
            nama_kliniks: evt.label,
            selected: {
                ...this.state.selected,
                id_kliniks: evt.value,
                nama_kliniks: evt.label,
                klinik_id: evt.value,
            }
        });
        if (!this.state.selected.operator_by) this.setState({
            ...this.setState,
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
        this.props.onLoad(this.state);
    };

    render() {
        const {data, user, data_report} = this.props;
        const {selected, errMsg} = this.state;
        const permission = user?.permission;

        const columns = [
            {
                key: "no",
                text: "No.",
                width: 20,
                align: "center",
                sortable: false,
                cell: (row, index) => <div
                    style={{textAlign: "center"}}>{((this.state.page_number - 1) * this.state.per_page) + index + 1 + '.'}</div>,
                row: 0
            },
            // {
            //     key: "created_at",
            //     text: "Tgl",
            //     align: "center",
            //     sortable: true,
            //     width: 110,
            //     cell: record => {
            //         return (moment(new Date(record.created_at)).format('DD-MM-YYYY'))
            //     }
            // },

            {
                key: "name",
                text: "Produk",
                align: "center",
                sortable: true,
            },
            {
                key: "batch_no",
                text: "Batch",
                align: "center",
                width: 150,
            },
            {
                key: "type",
                text: "Type",
                align: "center",
                width: 150,
            },
            // {
            //     key: "expired_date",
            //     text: "Tgl. Expired",
            //     align: "center",
            //     sortable: true,
            //     width: 150,
            //     cell: record => {
            //         return (moment(new Date(record.expired_date)).format('DD-MM-YYYY'))
            //     }
            // },

            {
                key: "qty_before",
                text: "Quantity Before",
                align: "center",
                width: 110,
                sortable: true,
                cell: record => {
                    return (<div style={{textAlign: "right"}}><Fragment>
                        <NumberFormat
                            value={record.qty_before}
                            thousandSeparator={true}
                            decimalScale={2}
                            displayType={'text'}
                        />
                    </Fragment></div>)
                }
            },

            {
                key: "qty_after",
                text: "Quantity After",
                align: "center",
                width: 110,
                sortable: true,
                cell: record => {
                    return (<div style={{textAlign: "right"}}><Fragment>
                        <NumberFormat
                            value={record.qty_after}
                            thousandSeparator={true}
                            decimalScale={2}
                            displayType={'text'}
                        />
                    </Fragment></div>)
                }
            },

            {
                key: "qty_difference",
                text: "Quantity Difference",
                align: "center",
                width: 110,
                sortable: true,
                cell: record => {
                    return (<div style={{textAlign: "right"}}><Fragment>
                        <NumberFormat
                            value={record.qty_difference}
                            thousandSeparator={true}
                            decimalScale={2}
                            displayType={'text'}
                        />
                    </Fragment></div>)
                }
            },
            {
                key: "notes",
                text: "Note",
                align: "center",
                width: 150,
            },

        ];
        const config = {
            key_column: 'id',
            page_size: 10,
            length_menu: [10, 20, 50],
            show_filter: true,
            show_pagination: true,
            pagination: 'advance',
            button: {
                excel: false,
                print: false
            },
            language: {
                loading_text: "Please be patient while data loads..."
            }
        }

        const multiDataSet = [
            {
                columns: [

                    {
                        title: "Produk", width: {wpx: 150},
                        style: {
                            border: {
                                top: {style: "thin", color: "FFFFAA00"},
                                bottom: {style: "thin", color: "FFFFAA00"},
                                left: {style: "thin", color: "FFFFAA00"},
                                right: {style: "thin", color: "FFFFAA00"}
                            },

                            font: {bold: true}
                        }

                    },
                    {
                        title: "Batch", width: {wpx: 100},
                        style: {
                            border: {
                                top: {style: "thin", color: "FFFFAA00"},
                                bottom: {style: "thin", color: "FFFFAA00"},
                                left: {style: "thin", color: "FFFFAA00"},
                                right: {style: "thin", color: "FFFFAA00"}
                            },

                            font: {bold: true}
                        }
                    },//char width
                    {
                        title: "Type", width: {wpx: 90},
                        style: {
                            border: {
                                top: {style: "thin", color: "FFFFAA00"},
                                bottom: {style: "thin", color: "FFFFAA00"},
                                left: {style: "thin", color: "FFFFAA00"},
                                right: {style: "thin", color: "FFFFAA00"}
                            },

                            font: {bold: true}
                        }
                    },
                    {
                        title: "Quantity Before", width: {wpx: 150},
                        style: {
                            border: {
                                top: {style: "thin", color: "FFFFAA00"},
                                bottom: {style: "thin", color: "FFFFAA00"},
                                left: {style: "thin", color: "FFFFAA00"},
                                right: {style: "thin", color: "FFFFAA00"}
                            },

                            font: {bold: true}
                        }
                    },
                    {
                        title: "Quantity After", width: {wpx: 150},
                        style: {
                            border: {
                                top: {style: "thin", color: "FFFFAA00"},
                                bottom: {style: "thin", color: "FFFFAA00"},
                                left: {style: "thin", color: "FFFFAA00"},
                                right: {style: "thin", color: "FFFFAA00"}
                            },

                            font: {bold: true}
                        }
                    },
                    {
                        title: "Quantity Difference", width: {wpx: 150},
                        style: {
                            border: {
                                top: {style: "thin", color: "FFFFAA00"},
                                bottom: {style: "thin", color: "FFFFAA00"},
                                left: {style: "thin", color: "FFFFAA00"},
                                right: {style: "thin", color: "FFFFAA00"}
                            },

                            font: {bold: true}
                        }
                    },
                    {
                        title: "Note", width: {wpx: 250},
                        style: {
                            border: {
                                top: {style: "thin", color: "FFFFAA00"},
                                bottom: {style: "thin", color: "FFFFAA00"},
                                left: {style: "thin", color: "FFFFAA00"},
                                right: {style: "thin", color: "FFFFAA00"}
                            },

                            font: {bold: true}
                        }
                    },


                ],
                ySteps: 1,
                data: data_report.map((data) => [
                    {
                        value: data.name,
                        style: {
                            border: {
                                top: {style: "thin", color: "FFFFAA00"},
                                bottom: {style: "thin", color: "FFFFAA00"},
                                left: {style: "thin", color: "FFFFAA00"},
                                right: {style: "thin", color: "FFFFAA00"}
                            }

                        }
                    },
                    {
                        value: data.batch_no,
                        style: {
                            border: {
                                top: {style: "thin", color: "FFFFAA00"},
                                bottom: {style: "thin", color: "FFFFAA00"},
                                left: {style: "thin", color: "FFFFAA00"},
                                right: {style: "thin", color: "FFFFAA00"}
                            }

                        }
                    },
                    {
                        value: data.type,
                        style: {
                            border: {
                                top: {style: "thin", color: "FFFFAA00"},
                                bottom: {style: "thin", color: "FFFFAA00"},
                                left: {style: "thin", color: "FFFFAA00"},
                                right: {style: "thin", color: "FFFFAA00"}
                            }

                        }
                    },
                    {
                        value: data.qty_before,
                        style: {
                            border: {
                                top: {style: "thin", color: "FFFFAA00"},
                                bottom: {style: "thin", color: "FFFFAA00"},
                                left: {style: "thin", color: "FFFFAA00"},
                                right: {style: "thin", color: "FFFFAA00"}
                            }

                        }
                    },
                    {
                        value: data.qty_after,
                        style: {
                            border: {
                                top: {style: "thin", color: "FFFFAA00"},
                                bottom: {style: "thin", color: "FFFFAA00"},
                                left: {style: "thin", color: "FFFFAA00"},
                                right: {style: "thin", color: "FFFFAA00"}
                            }

                        }
                    },
                    {
                        value: data.qty_difference,
                        style: {
                            border: {
                                top: {style: "thin", color: "FFFFAA00"},
                                bottom: {style: "thin", color: "FFFFAA00"},
                                left: {style: "thin", color: "FFFFAA00"},
                                right: {style: "thin", color: "FFFFAA00"}
                            }

                        }
                    },
                    {
                        value: data.notes,
                        style: {
                            border: {
                                top: {style: "thin", color: "FFFFAA00"},
                                bottom: {style: "thin", color: "FFFFAA00"},
                                left: {style: "thin", color: "FFFFAA00"},
                                right: {style: "thin", color: "FFFFAA00"}
                            }

                        }
                    },
                ])

            }
        ];

        const frmUser = <Form id="myForm" style={{marginTop: 10, marginBottom: 25}}>
            <Form.Group controlId="id_products">
                <Form.Label>Produk</Form.Label>

                {errMsg.id_bm ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.id_bm}
                    </span>) : ''}
                <SelectBm
                    myVal={selected.id_bm ? ({value: selected.id_bm, label: selected.name}) : ''}
                    onChange={this.onchangeSelect.bind((this))}
                />
            </Form.Group>


            <Form.Group controlId="type">
                <Form.Label>Type</Form.Label>
                {errMsg.type ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.type}
                    </span>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="type"
                    as="select"
                    value={selected.type}
                    onChange={this.handleChange.bind(this)}>
                    <option value="">- Pilih -</option>
                    <option value="kurang">
                        Kurang
                    </option>
                    <option value="tambah">
                        Tambah
                    </option>


                </Form.Control>
            </Form.Group>

            <Form.Group controlId="qty_difference">
                <Form.Label>Quantity</Form.Label>
                {errMsg.qty_difference ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.qty_difference}
                    </span>) : ''}
                <NumberFormat
                    onChange={this.handleChange.bind(this)}
                    name="qty_difference"
                    className="form-control form-control-sm"
                    value={selected.qty_difference ? selected.qty_difference : ''}
                    thousandSeparator={true}
                    decimalScale={2}
                    inputMode="numeric"
                    autoComplete="off"
                    placeholder="Quantity"/>
            </Form.Group>

            <Form.Group controlId="notes" style={{marginTop: "30px"}}>
                <Form.Label>Note</Form.Label>
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="notes"
                    type="text"
                    value={selected?.note}
                    placeholder="Notes"
                    onChange={this.handleChange.bind(this)}/>
            </Form.Group>
        </Form>;


        return (

            <div className="content-wrapper">
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0">Stock Opname</h1>
                            </div>
                            {/* /.col */}

                        </div>
                        {/* /.row */}
                    </div>
                    {/* /.container-fluid */}
                </div>
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                {/* card start */}
                                <div className="card card-success shadow-lg" style={{"minHeight": "800px"}}>
                                    <div className="card-header card-header-content" style={{"color": "#000"}}>

                                        <div className="row">
                                            <div className="col-4">
                                                <AppButton
                                                    disabled={(selected.id_kliniks || this.props?.user?.id_klinik) && parseInt(permission?.bk_add) > 0 ? false : true}
                                                    isLoading={this.props.isLoading}
                                                    theme="info"
                                                    onClick={this.discardChanges}
                                                    icon='add'> Add
                                                </AppButton>
                                                <ExcelFile filename="Stock Opname" element={
                                                    <AppButton
                                                        disabled={data_report.length > 0 ? false : true}
                                                        isLoading={this.props.isLoading}
                                                        type="button"
                                                        style={{marginLeft: 5}}
                                                        theme="warning">
                                                        Export
                                                    </AppButton>}>
                                                    <ExcelSheet dataSet={multiDataSet} name="Report Header"/>
                                                </ExcelFile>
                                            </div>
                                            <div className="pull-right col-2 offset-md-6">
                                                {this.props?.user?.id_level === 1 &&
                                                    <Form>
                                                        <Form.Row>
                                                            <Form.Group as={Col} sm={12} controlId="id_kliniks">
                                                                <SelectKlinik
                                                                    myVal={selected.id_kliniks ? ({
                                                                        value: selected.id_kliniks,
                                                                        label: selected.nama_kliniks
                                                                    }) : ''}
                                                                    onChange={this.onchangeSelectKlinik.bind((this))}
                                                                />

                                                            </Form.Group>
                                                        </Form.Row>
                                                    </Form>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body">
                                        {data ? (
                                            <ReactDatatable
                                                config={config}
                                                records={data}
                                                columns={columns}
                                                dynamic={true}
                                                onChange={this.tableChangeHandler}
                                                loading={this.props.isLoading}
                                                total_record={this.props.totalData}
                                            />
                                        ) : (<p>No Data ...</p>)}

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <AppModal
                    show={this.props.showFormAdd}
                    form={frmUser}
                    size="xs"
                    backdrop={false}
                    keyboard={false}
                    title="Add Stock Opname"
                    titleButton="Save change"
                    themeButton="success"
                    handleClose={this.handleClose}
                    isLoading={this.props.isAddLoading ? this.props.isAddLoading : this.state.loadingForm}
                    formSubmit={this.handleSubmit.bind(this)}
                ></AppModal>

                {this.props.showFormSuccess ? (<AppSwalSuccess
                    show={this.props.showFormSuccess}
                    title={<div dangerouslySetInnerHTML={{__html: this.props.contentMsg}}/>}
                    type={this.props.tipeSWAL}
                    handleClose={this.handleCloseSwal}
                >
                </AppSwalSuccess>) : ''}
            </div>


        )
    }
}

const mapStateToProps = (state) => ({
    data: state.stockOpname.data || [],
    data_report: state.stockOpname.dataReport || [],
    totalData: state.stockOpname.totalData,
    isError: state.stockOpname.isError,
    isLoading: state.stockOpname.isLoading,
    showFormAdd: state.stockOpname.showFormAdd,
    isAddLoading: state.stockOpname.isAddLoading,
    errorPriority: state.stockOpname.errorPriority || null,
    contentMsg: state.stockOpname.contentMsg || null,
    showFormSuccess: state.stockOpname.showFormSuccess,
    showFormDelete: state.stockOpname.showFormDelete,
    tipeSWAL: state.stockOpname.tipeSWAL,
    user: state.main.currentUser,
});
const mapDispatchToPros = (dispatch) => {
    return {
        onLoad: (param) => {
            dispatch(fetchData(param));
            dispatch(fetchDataReport({
                klinik_id: param?.klinik_id,
                id_klinik: param?.klinik_id,
                sort_order: "DESC",
                sort_column: "name"
            }));
        },
        onAdd: (param) => {
            dispatch(addData(param));
        },
        showConfirmDel: (data) => {
            dispatch(confirmDel(data));
        },
        showForm: () => {
            dispatch(addForm());
        },

        closeModal: () => {
            dispatch(closeForm());

        },
        closeSwal: (klinik_id) => {
            dispatch(closeForm());
            const queryString = {
                sort_order: "DESC",
                sort_column: "name",
                per_page: 10,
                klinik_id: klinik_id,
                id_klinik: klinik_id
            }
            dispatch(fetchData(queryString));
            dispatch(fetchDataReport({
                klinik_id: klinik_id,
                id_klinik: klinik_id,
                sort_order: "DESC",
                sort_column: "name",
            }));
        },
        resetError: () => {
            dispatch(clearError());
        },

        closeSwalError: () => {
            dispatch(closeForm());
        }
    }

}

export default connect(mapStateToProps, mapDispatchToPros)(StockOpname);