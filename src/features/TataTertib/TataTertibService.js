import axios from "axios";


const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

class TataTertibService {
    postData(param, action) {
        console.log(action);
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        switch (action) {
            case "GET_TATATERTIB":
                return axios.post(API_URL + "/get_syarat_tatib", param, {
                    headers: {
                        'Authorization': 'Bearer ' + token
                    }
                });

            case "UPLOAD_IMAGE":
                return axios.post(API_URL + "/simpan_syarat_tatib", param, {
                    headers: {
                        'Authorization': 'Bearer ' + token
                    }
                });

        }
    }
}

export default new TataTertibService()