import React, {Component} from 'react';
import {Alert, Button, Figure, Image, ListGroup} from 'react-bootstrap';
import Dropzone from "react-dropzone";
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {Placeholder} from 'rsuite';
import {AppSwalSuccess} from '../../components/modal/SwalSuccess';
import TataTertibService from "./TataTertibService";
import noImg from '../../assets/PDF-en-PNG.png';


class TataTertib extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //loadingPage: true,
            appsLoading: false,
            isLoading: false,
            deleteForm: false,
            showSwalSuccess: false,
            contentSwal: '',
            id: '',
            selectedImg: '',
            id_klinik: '',
            operator_by: '',
            dtRes: {},
            fileNames: []
        }
        this.handleDrop = this.handleDrop.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.closeSwal = this.closeSwal.bind(this);
    }

    componentDidMount() {
        this.setState({appsLoading: true})
        setTimeout(() => {
            this.getData();
        }, 300);
    }

    getData = async () => {
        const queryString = {"": ""};
        await TataTertibService.postData(queryString, 'GET_TATATERTIB')
            .then(response => {
                if (response.data.err_code === "00") {
                    const dtRes = response.data.data;
                    this.setState({dtRes: dtRes, appsLoading: false});
                }
                if (response.data.err_code === "04") {
                    this.setState({isLoading: false, dtRes: {}, appsLoading: false});
                }
                if (response.data.err_code === "401") {
                    this.setState({isLoading: false, dtRes: {}, appsLoading: false});
                }
            })
            .catch(e => {
                console.log(e);
                this.setState({isLoading: false});
            });
        //this.setState({loadingPage:false})
    };


    handleClose() {
        this.setState({isLoading: false, deleteForm: false});
    }

    closeSwal() {
        this.setState({showSwalSuccess: false, contentSwal: ''});
        setTimeout(() => {
            this.getData();
        }, 300);
    }

    handleDrop(acceptedFiles, fileRejections) {

        let fd = new FormData();
        acceptedFiles.map(file => {
            fd.append('file', file);
            return true;
        })
        TataTertibService.postData(fd, 'UPLOAD_IMAGE').then((res) => {
            //this.setState({loadingPage:true})
            const err_code = res.data.err_code;
            if (err_code === '00') {
                const contentSwal = <div
                    dangerouslySetInnerHTML={{__html: '<div style="font-size:20px; text-align:center;"><strong>Success</strong>, Data berhasil di Simpan</div>'}}/>;
                this.setState({
                    ...this.state,
                    showSwalSuccess: true,
                    deleteForm: false,
                    isLoading: false,
                    contentSwal: contentSwal
                })

                this.getData();
            } else {
                const contentSwal = <div
                    dangerouslySetInnerHTML={{__html: '<div style="font-size:20px; text-align:center;"><strong>Failed</strong>, Something wrong</div>'}}/>;
                this.setState({
                    ...this.state,
                    showSwalSuccess: true,
                    deleteForm: false,
                    isLoading: false,
                    contentSwal: contentSwal
                })
                console.log(res.data);
            }
        }).catch((error) => {
            console.log(error);
        });
        const fileReject = fileRejections.map(file => {
            let errFile = [];
            file.errors.map(err => {
                if (err.code === "file-too-large") {
                    errFile.push("File is larger than 1MB")
                } else {
                    errFile.push(err.message)
                }
                return 1;
            })
            var dt = {
                fileName: file.file.name,
                errors: errFile.join(" and ")
            }
            return dt;
        })
        this.setState({fileNames: fileReject})
    }

    render() {
        const {Paragraph} = Placeholder;
        console.log(this.state);
        const deleteContent =
            <div className="container-img">
                <Figure>
                    <Figure.Image thumbnail
                                  className="modal-img"
                                  width={171}
                                  height={180}
                                  alt=""
                                  style={{"maxHeight": "180px"}}
                                  src={this.state.selectedImg}
                    />
                    <Figure.Caption id="caption">
                        Apakah anda yakin <br/>akan menghapus data ini ?
                    </Figure.Caption>
                </Figure>
            </div>
        return (


            <div className="content-wrapper">
                {/* Content Header (Page header) */}
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0">Tata Tertib Pasien</h1>
                            </div>
                            {/* /.col */}

                        </div>
                        {/* /.row */}
                    </div>
                    {/* /.container-fluid */}
                </div>
                {/* /.content-header */}
                {/* Main content */}
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                {this.state.appsLoading ? (
                                    <Paragraph rowHeight={25} rowMargin={30} rows={8} active
                                               style={{marginTop: 30}}/>) : (

                                    <div className="card shadow-lg">

                                        <div className="card-body my-card-body">

                                            <Dropzone onDrop={this.handleDrop}
                                                      accept="image/jpeg, image/png, image/jpg"
                                                      maxSize={5242880}
                                            >
                                                {({isDragActive, isDragReject, getRootProps, getInputProps}) => (
                                                    <div {...getRootProps({className: "dropzone"})}>
                                                        <input {...getInputProps()} />
                                                        <br/>
                                                        {!isDragActive && (
                                                            <p style={{"fontWeight": "600"}}>Drag & drop files, or
                                                                click to select files</p>)}
                                                        {isDragReject && (
                                                            <p style={{"color": "red", "fontWeight": "800"}}>File
                                                                not accepted, sorry!</p>)}
                                                        {isDragActive && !isDragReject && (<p style={{
                                                            "color": "lightgreen",
                                                            "fontWeight": "800"
                                                        }}>Drop it like it's hot!</p>)}
                                                        <em>(Only *.jpg, *.jpeg, *.png Images, *.Pdf
                                                            Maks. size 1MB will be accepted)</em>
                                                    </div>
                                                )}
                                            </Dropzone>
                                            {this.state.fileNames.length > 0 ? (
                                                <Alert variant="danger"
                                                       onClose={() => this.setState({fileNames: []})} dismissible>
                                                    <Alert.Heading>You got an error!</Alert.Heading>
                                                    <ListGroup variant="flush">
                                                        {this.state.fileNames.map(fileName => (
                                                            <ListGroup.Item
                                                                key={fileName.fileName}
                                                                style={{
                                                                    "backgroundColor": "transparent",
                                                                    "paddingBottom": "4px",
                                                                    "paddingTop": "4px"
                                                                }}>
                                                                - {fileName.fileName + " : " + fileName.errors}</ListGroup.Item>
                                                        ))}
                                                    </ListGroup>
                                                </Alert>) : ''}


                                            <Figure key={this.state.dtRes.id} className="img-product-pic"
                                                    style={{"marginRight": "10px", "marginLeft": "5px"}}>
                                                {
                                                    this.state.dtRes.image != null ?
                                                        this.state.dtRes.image.includes(".pdf") ?
                                                            <a href={this.state.dtRes.image} target="_blank">
                                                                <Image src={noImg} thumbnail width={170}
                                                                       height={180} style={{"maxHeight": "180px"}}/>
                                                            </a>
                                                            :
                                                            <Image src={this.state.dtRes.image} thumbnail width={170}
                                                                   height={180} style={{"maxHeight": "180px"}}/>
                                                        :
                                                        null
                                                }

                                            </Figure>


                                        </div>

                                        <div className="card-footer">
                                            <Link to="/klinik">
                                                <Button variant="danger">Back</Button>
                                            </Link>

                                        </div>

                                    </div>

                                )}

                            </div>

                        </div>
                    </div>
                </section>
                {/* <AppModal
                    show={this.state.deleteForm}
                    size="xs"
                    form={deleteContent}
                    handleClose={this.handleClose}
                    backdrop="static"
                    keyboard={false}
                    title="Delete Image"
                    titleButton="Delete Image"
                    themeButton="danger"
                    isLoading={this.state.isLoading}
                    formSubmit={this.handleDelete}
                ></AppModal> */}

                {this.state.showSwalSuccess ? (<AppSwalSuccess
                    show={this.state.showSwalSuccess}
                    title={this.state.contentSwal}
                    type="success"
                    handleClose={this.closeSwal}/>) : ''}

            </div>


        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.main.currentUser,

    }
}

const mapDispatchToPros = (dispatch) => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToPros)(TataTertib);