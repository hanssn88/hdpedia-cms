import React, {useEffect, useState} from "react";
import {Form, Image} from 'react-bootstrap';
import {Placeholder} from "rsuite";
import noImg from '../../assets/noPhoto.jpg';


const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

/*const today = moment();
const disableFutureDt = (current) => {
    return current.isBefore(today)
}*/

export const DaftarTindakanObat = (data) => {
    const [datas, setDatas] = useState(data?.data);
    const [isLoading, setIsLoading] = useState(true);
    /*const [validSd, setValidSd] = useState(disableFutureDt);*/

    useEffect(() => {
        async function setData() {

            await setDatas(data?.data);
            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);
        }

        setData();
    }, [data])


    const {Paragraph} = Placeholder;

    var dataObat = [];

    if (datas.obat != null && datas.obat.length > 0) {
        dataObat = datas.obat;
    }
    return (

        <div>
            {isLoading ? (
                <div className="card shadow-lg">
                    <div className="card-body">
                        <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                   style={{marginTop: 30}}/>
                    </div>
                </div>

            ) : (
                <Form>
                    <h3>Daftar Tindakan Dan Obat Pasien</h3>
                    <table className="table table-condensed ">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="12" align="center">
                                Daftar Tindakan Dan Obat Pasien
                            </td>
                        </tr>
                        <tr>
                            <td width="8%">
                                <strong>Jaminan</strong>
                            </td>
                            <td width="1%">:</td>
                            <td width="28%">{datas?.jaminan ? datas?.jaminan : '-'}</td>

                            <td width="8%"><strong>Jenis Tindakan Hemodialisis</strong>
                            </td>
                            <td width="1%">:</td>
                            <td width="28%">{datas?.hemodialis ? datas?.hemodialis : '-'}</td>

                            <td width="8%"><strong>Akses</strong>
                            </td>
                            <td width="1%">:</td>

                            <td width="28%">{datas?.akses ? datas?.akses : '-'}</td>

                            <td width="%"><strong>Penunjang</strong>

                            </td>
                            <td width="1%">:</td>

                            <td>{datas?.penunjang_medis ? datas?.penunjang_medis : '-'}</td>
                        </tr>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="12" align="center">
                                Pemakian Obat
                            </td>
                        </tr>
                        <tr>
                            {
                                dataObat.map((item) =>
                                    <>
                                        {
                                            <td colSpan="3">
                                                <td>
                                                    <Form.Control
                                                        size="sm"
                                                        autoComplete="off"
                                                        name="obat"
                                                        type="text"
                                                        value={item.name ? item.name : ''}
                                                    />
                                                </td>
                                            </td>
                                        }
                                    </>
                                )
                            }

                        </tr>

                        <tr>
                            <td width="8%">
                                <strong>Perawat</strong>
                            </td>
                            <td width="1%">:</td>
                            <td>
                                <Image
                                    src={datas?.image_ttd_perawat && datas?.image_ttd_perawat !== null ? datas?.image_ttd_perawat : noImg}
                                    thumbnail
                                    style={{maxWidth: "100px", maxHeight: "100px"}}/>
                            </td>

                            <td width="8%">
                                <strong>Dr. Pelaksana</strong>
                            </td>
                            <td width="1%">:</td>
                            <td>
                                <Image
                                    src={datas?.image_ttd_dokter && datas?.image_ttd_dokter !== null ? datas?.image_ttd_dokter : noImg}
                                    thumbnail
                                    style={{maxWidth: "100px", maxHeight: "100px"}}/>
                            </td>

                        </tr>

                        </tbody>
                    </table>

                </Form>
            )}


        </div>
    )
}
