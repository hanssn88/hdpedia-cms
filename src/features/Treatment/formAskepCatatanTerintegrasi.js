import React, {useEffect, useState} from "react";
import {Form} from "react-bootstrap";
import "moment/locale/id";
import "react-datetime/css/react-datetime.css";
import axios from "axios";
import Swal from 'sweetalert2'
import {Placeholder} from "rsuite";

const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

/*const today = moment();
const disableFutureDt = (current) => {
    return current.isBefore(today)
}*/

export const FormAskepCatatanTerintegrasi = (data) => {
    const [datas, setDatas] = useState(data?.data);
    const [isLoading, setIsLoading] = useState(true);
    /*const [validSd, setValidSd] = useState(disableFutureDt);*/

    useEffect(() => {
        async function setData() {

            await setDatas(data?.data);
            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);
        }

        setData();
    }, [data])

    const setOnChange = async (key, val) => {
        await setDatas({...datas, [key]: val});
    };

    const handleSubmit = async (e) => {
        const {id, created_at, updated_at, ...params} = datas;
        if (typeof params.treatment_id === "undefined" || params.treatment_id === "") params.treatment_id = sessionStorage.getItem('idPasienHDPedia');
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            await axios.post(API_URL + '/dokter/add_catatan_terintegrasi', params, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            return Swal.fire({
                title: 'Informasi',
                text: "Success add catatan terintegrasi",
                icon: 'success',
                confirmButtonText: 'Ok',
                allowOutsideClick: true
            })
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    const {Paragraph} = Placeholder;
    return (

        <div>
            {isLoading ? (
                <div className="card shadow-lg">
                    <div className="card-body">
                        <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                   style={{marginTop: 30}}/>
                    </div>
                </div>

            ) : (
                <Form>
                    <h3>Form Askep</h3>
                    <table className="table table-condensed table-form-askep">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="3" align="center">
                                Catatan Terintegrasi
                            </td>
                        </tr>


                        <tr>
                            <td width="10%"><strong>Nefrolog</strong></td>
                            <td width="1%">:</td>

                            <td>
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    id="nefrolog"
                                    name="nefrolog"
                                    type="text"
                                    value={datas ? datas.nefrolog : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="nefrolog"/>
                            </td>
                        </tr>

                        <tr>
                            <td width="10%"><strong>Tanggal/Jam</strong></td>
                            <td width="1%">:</td>

                            <td>
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    id="datetime"
                                    name="datetime"
                                    type="text"
                                    value={datas ? datas.datetime : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Tanggal/Jam"/>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%"><strong>Catatan Terintegrasi</strong></td>
                            <td width="1%">:</td>

                            <td>
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    id="catatan_terintegrasi"
                                    name="catatan_terintegrasi"
                                    type="text"
                                    value={datas ? datas.catatan_terintegrasi : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Catatan Terintegrasi"/>
                            </td>
                        </tr>

                        </tbody>
                    </table>


                    <button
                        id="submitFormCatatanTerintegrasi"
                        type="button" onClick={(e) => handleSubmit()}
                        className="btn btn-success btn-sm">Submit Form Catatan Terintegrasi
                    </button>
                </Form>
            )}


        </div>
    )
}
