import React, {useEffect, useState} from "react";
import {Col, Form, Row} from "react-bootstrap";
import axios from "axios";
import Swal from "sweetalert2";
import {Placeholder} from "rsuite";

const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

export const FormAskepDiagnosaKeperawatan = (data) => {
    const [datas, setDatas] = useState(data?.data);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        async function setData() {
            await setDatas(data?.data);
            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);
        }

        setData();
    }, [data])

    const setOnChange = async (key, val) => {
        if (key === "diagnosa") {
            const diagnosa = [];
            if (datas && typeof datas.diagnosa !== "undefined" && datas.diagnosa instanceof Array) {
                for (let i = 0; i < datas.diagnosa.length; i++) {
                    await diagnosa.push(datas.diagnosa[i]);
                }
            }

            const index = diagnosa.indexOf(val);
            if (index > -1) {
                await diagnosa.splice(index, 1);
            } else {
                await diagnosa.push(val);
            }
            await setDatas({...datas, diagnosa: diagnosa});
        } else {
            await setDatas({...datas, [key]: val});
        }
    };

    const handleSubmit = async (e) => {
        const {id, created_at, updated_at, ...params} = datas;
        if (typeof params.treatment_id === "undefined" || params.treatment_id === "") params.treatment_id = sessionStorage.getItem('idPasienHDPedia');
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            await axios.post(API_URL + '/dokter/add_diagnosa_keperawatan', params, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            return Swal.fire({
                title: 'Informasi',
                text: "Success add diagnosa keperawatan",
                icon: 'success',
                confirmButtonText: 'Ok',
                allowOutsideClick: true
            })
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    const {Paragraph} = Placeholder;

    return (
        <div>
            {isLoading ? (
                <div className="card shadow-lg">
                    <div className="card-body">
                        <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                   style={{marginTop: 30}}/>
                    </div>
                </div>

            ) : (
                <Form>
                    <h3>Form Askep</h3>
                    <table className="table table-condensed table-form-askep">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="6" align="center">
                                Diagnosa Keperawatan
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="diagnosa"
                                    id="diagnosa"
                                    checked={datas ? datas.diagnosa?.includes("Kelebihan Volume Cairan") : ''}
                                    onChange={e => setOnChange("diagnosa", "Kelebihan Volume Cairan")}
                                    label="1. Kelebihan Volume Cairan"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="diagnosa"
                                    id="diagnosa5"
                                    checked={datas ? datas.diagnosa?.includes("Nutrisi Kurang Dari Kebutuhan Tubuh") : ''}
                                    onChange={e => setOnChange("diagnosa", "Nutrisi Kurang Dari Kebutuhan Tubuh")}
                                    label="5. Nutrisi Kurang Dari Kebutuhan Tubuh"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="diagnosa"
                                    id="diagnosa2"
                                    checked={datas ? datas.diagnosa?.includes("Gangguan Perfusi Jaringan") : ''}
                                    onChange={e => setOnChange("diagnosa", "Gangguan Perfusi Jaringan")}
                                    label="2. Gangguan Perfusi Jaringan"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="diagnosa"
                                    id="diagnosa6"
                                    checked={datas ? datas.diagnosa?.includes("Ketidakpatuhan Terhadap Diit") : ''}
                                    onChange={e => setOnChange("diagnosa", "Ketidakpatuhan Terhadap Diit")}
                                    label="6. Ketidakpatuhan Terhadap Diit"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="diagnosa"
                                    id="diagnosa3"
                                    checked={datas ? datas.diagnosa?.includes("Risiko Ketidakseimbangan Elektrolit") : ''}
                                    onChange={e => setOnChange("diagnosa", "Risiko Ketidakseimbangan Elektrolit")}
                                    label="3. Risiko Ketidakseimbangan Elektrolit"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="diagnosa"
                                    id="diagnosa7"
                                    checked={datas ? datas.diagnosa?.includes("Gangguan Rasa Nyaman : Nyeri") : ''}
                                    onChange={e => setOnChange("diagnosa", "Gangguan Rasa Nyaman : Nyeri")}
                                    label="7. Gangguan Rasa Nyaman : Nyeri"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="diagnosa"
                                    id="diagnosa4"
                                    checked={datas ? datas.diagnosa?.includes("Risiko Penurunan Curah Jantung") : ''}
                                    onChange={e => setOnChange("diagnosa", "Risiko Penurunan Curah Jantung")}
                                    label="4. Risiko Penurunan Curah Jantung"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="diagnosa"
                                    id="diagnosa8"
                                    checked={datas ? datas.diagnosa?.includes("Lain-lain") : ''}
                                    onChange={e => setOnChange("diagnosa", "Lain-lain")}
                                    label="8. Lain-lain"/>
                            </td>
                        </tr>

                        <tr>
                            <td colSpan="6">
                                <Form.Group as={Row} className="mb-3" controlId="jelaskan">
                                    <Form.Label column sm={1}>
                                        Jelaskan :
                                    </Form.Label>
                                    <Col sm={11}>
                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="jelaskan"
                                            id="jelaskan"
                                            type="text"
                                            value={datas?.jelaskan}
                                            placeholder="Jelaskan"
                                            onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                    </Col>
                                </Form.Group>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <button
                        type="button"
                        id="submitFormDiagnosaKeperawatan"
                        onClick={(e) => handleSubmit()}
                        className="btn btn-success btn-sm">Submit Form Diagnosa Keperawatan
                    </button>

                </Form>
            )}
        </div>
    )
}
