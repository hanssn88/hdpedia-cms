import React, {useEffect, useState} from "react";
import {Form} from "react-bootstrap";
import axios from "axios";
import Swal from "sweetalert2";
import {Placeholder} from "rsuite";


const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;
export const FormAskepHeparinisasi = (data) => {
    const [datas, setDatas] = useState(data?.data);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        async function setData() {
            await setDatas(data?.data);
            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);
        }

        setData();
    }, [data])

    const setOnChange = async (key, val) => {
        if (key === "akses_vaskuler") {
            const akses_vaskuler = [];
            if (datas && typeof datas.akses_vaskuler !== "undefined" && datas.akses_vaskuler instanceof Array) {
                for (let i = 0; i < datas.akses_vaskuler.length; i++) {
                    await akses_vaskuler.push(datas.akses_vaskuler[i]);
                }

            }

            if (val === 'jugular_kanan') {
                for (let i = 0; i < datas.akses_vaskuler.length; i++) {
                    const removeData = ['jugular_kiri', 'subclavia_kiri', 'subclavia_kanan', 'femoral_kiri', 'femoral_kanan'];
                    if (removeData.includes(datas.akses_vaskuler[i])) {
                        await akses_vaskuler.splice(i, 1);
                    }
                }
                const index1 = akses_vaskuler.indexOf('femoral_kiri');
                if (index1 > -1) {
                    await akses_vaskuler.splice(index1, 1);
                }
                const index2 = akses_vaskuler.indexOf('femoral_kanan');
                if (index2 > -1) {
                    await akses_vaskuler.splice(index1, 1);
                }
            }
            if (val === 'jugular_kiri') {
                const removeData = ['jugular_kanan', 'subclavia_kiri', 'subclavia_kanan', 'femoral_kiri', 'femoral_kanan'];
                for (let i = 0; i < datas.akses_vaskuler.length; i++) {
                    if (removeData.includes(datas.akses_vaskuler[i])) {
                        await akses_vaskuler.splice(i, 1);
                    }
                }
                const index1 = akses_vaskuler.indexOf('femoral_kiri');
                if (index1 > -1) {
                    await akses_vaskuler.splice(index1, 1);
                }
                const index2 = akses_vaskuler.indexOf('femoral_kanan');
                if (index2 > -1) {
                    await akses_vaskuler.splice(index1, 1);
                }
            }

            if (val === 'subclavia_kiri') {
                const removeData = ['jugular_kanan', 'jugular_kiri', 'subclavia_kanan', 'femoral_kiri', 'femoral_kanan'];
                for (let i = 0; i < datas.akses_vaskuler.length; i++) {
                    if (removeData.includes(datas.akses_vaskuler[i])) {
                        await akses_vaskuler.splice(i, 1);
                    }
                }
                const index1 = akses_vaskuler.indexOf('femoral_kiri');
                if (index1 > -1) {
                    await akses_vaskuler.splice(index1, 1);
                }
                const index2 = akses_vaskuler.indexOf('femoral_kanan');
                if (index2 > -1) {
                    await akses_vaskuler.splice(index1, 1);
                }
            }

            if (val === 'subclavia_kanan') {
                const removeData = ['jugular_kanan', 'jugular_kiri', 'subclavia_kiri', 'femoral_kiri', 'femoral_kanan'];
                for (let i = 0; i < datas.akses_vaskuler.length; i++) {
                    if (removeData.includes(datas.akses_vaskuler[i])) {
                        await akses_vaskuler.splice(i, 1);
                    }
                }
                const index1 = akses_vaskuler.indexOf('femoral_kiri');
                if (index1 > -1) {
                    await akses_vaskuler.splice(index1, 1);
                }
                const index2 = akses_vaskuler.indexOf('femoral_kanan');
                if (index2 > -1) {
                    await akses_vaskuler.splice(index1, 1);
                }
            }

            if (val === 'femoral_kiri') {
                const removeData = ['jugular_kanan', 'jugular_kiri', 'subclavia_kiri', 'subclavia_kanan', 'femoral_kanan'];
                for (let i = 0; i < datas.akses_vaskuler.length; i++) {
                    if (removeData.includes(datas.akses_vaskuler[i])) {
                        await akses_vaskuler.splice(i, 1);
                    }
                }
            }

            if (val === 'femoral_kanan') {
                const removeData = ['jugular_kanan', 'jugular_kiri', 'subclavia_kiri', 'subclavia_kanan', 'femoral_kiri'];
                for (let i = 0; i < datas.akses_vaskuler.length; i++) {
                    if (removeData.includes(datas.akses_vaskuler[i])) {
                        await akses_vaskuler.splice(i, 1);
                    }
                }
            }

            if (val === 'lengan_kiri') {
                const index1 = akses_vaskuler.indexOf('lengan_kanan');
                if (index1 > -1) {
                    await akses_vaskuler.splice(index1, 1);
                }
            }

            if (val === 'lengan_kanan') {
                const index1 = akses_vaskuler.indexOf('lengan_kiri');
                if (index1 > -1) {
                    await akses_vaskuler.splice(index1, 1);
                }
            }

            if (val === 'tungkai_kiri') {
                const index1 = akses_vaskuler.indexOf('tungkai_kanan');
                if (index1 > -1) {
                    await akses_vaskuler.splice(index1, 1);
                }
            }

            if (val === 'tungkai_kanan') {
                const index1 = akses_vaskuler.indexOf('tungkai_kiri');
                if (index1 > -1) {
                    await akses_vaskuler.splice(index1, 1);
                }
            }

            if (val === 'av_shunt_kiri') {
                const index1 = akses_vaskuler.indexOf('av_shunt_kanan');
                if (index1 > -1) {
                    await akses_vaskuler.splice(index1, 1);
                }
            }

            if (val === 'av_shunt_kanan') {
                const index1 = akses_vaskuler.indexOf('av_shunt_kiri');
                if (index1 > -1) {
                    await akses_vaskuler.splice(index1, 1);
                }
            }

            const index = akses_vaskuler.indexOf(val);
            if (index > -1) {
                await akses_vaskuler.splice(index, 1);
            } else {
                await akses_vaskuler.push(val);
            }
            await setDatas({...datas, akses_vaskuler: akses_vaskuler});
        } else {
            let totalBalance = 0;
            if (key === 'total_cairan_keluar') {
                totalBalance = datas.total_cairan_masuk - val;
            }
            if (key === 'total_cairan_masuk') {
                totalBalance = val - datas.total_cairan_keluar;
            }
            console.log(totalBalance);
            console.log(datas);
            await setDatas({...datas, [key]: val, total_balance: totalBalance});
        }
    };

    const handleSubmit = async (e) => {
        const {id, created_at, updated_at, ...params} = datas;
        if (typeof params.treatment_id === "undefined" || params.treatment_id === "") params.treatment_id = sessionStorage.getItem('idPasienHDPedia');
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            await axios.post(API_URL + '/dokter/add_heparinisasi', params, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            return Swal.fire({
                title: 'Informasi',
                text: "Success add heparinisasi",
                icon: 'success',
                confirmButtonText: 'Ok',
                allowOutsideClick: true
            })
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    const {Paragraph} = Placeholder;
    return (
        <div>
            {isLoading ? (
                <div className="card shadow-lg">
                    <div className="card-body">
                        <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                   style={{marginTop: 30}}/>
                    </div>
                </div>

            ) : (
                <Form>
                    <h3>Form Askep</h3>
                    <table className="table table-condensed table-form-askep">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="7" align="center">
                                Heparinisasi
                            </td>
                        </tr>
                        <tr>
                            <td width="10%"><strong>Dosis Sirkulasi (in)</strong></td>
                            <td width="1%">:</td>
                            <td width="14%">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="dosis_sirkulasi"
                                    style={{width: "90%"}}
                                    type="text"
                                    value={datas ? datas.dosis_sirkulasi : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Dosis Sirkulasi (in)"/>
                            </td>
                            <td colSpan="2" align="right"><strong>Dosis Awal (in) : </strong></td>

                            <td width="19%">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="dosis_awal"
                                    style={{width: "90%"}}
                                    type="text"
                                    value={datas ? datas.map : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Dosis Awal (in)"/>
                            </td>


                            <td></td>


                        </tr>


                        <tr>
                            <td><strong>Dosis Maintenance</strong></td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Group className="mb-3" controlId="kontinu">
                                    <Form.Label>Kontinu(IU)</Form.Label>

                                    <Form.Control
                                        size="sm"
                                        style={{width: "90%"}}
                                        autoComplete="off"
                                        name="kontinu"
                                        id="kontinu"
                                        type="text"
                                        value={datas ? datas.bb_pre_hd : ''}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                        placeholder="Kontinu(IU)"/>
                                </Form.Group>
                            </td>
                            <td width="20%">
                                <Form.Group className="mb-3" controlId="intermiten">
                                    <Form.Label>Intermiten(IU)</Form.Label>
                                    <Form.Control
                                        size="sm"
                                        style={{width: "93%"}}
                                        autoComplete="off"
                                        id="intermiten"
                                        name="intermiten"
                                        type="text"
                                        value={datas ? datas.intermiten : ''}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                        placeholder="Intermiten(IU)"/>
                                </Form.Group>
                            </td>
                            <td width="20%">
                                <Form.Group className="mb-3" controlId="lmwh">
                                    <Form.Label>LMWH</Form.Label>
                                    <Form.Control
                                        size="sm"
                                        style={{width: "93%"}}
                                        autoComplete="off"
                                        id="lmwh"
                                        name="lmwh"
                                        type="text"
                                        value={datas ? datas.lmwh : ''}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                        placeholder="LMWH"/>
                                </Form.Group>
                            </td>
                            <td>
                                <Form.Group className="mb-3" controlId="tampa_heparin_etiologi">
                                    <Form.Label>Tampa Heparin, Etiologi</Form.Label>
                                    <Form.Control
                                        size="sm"
                                        autoComplete="off"
                                        id="tampa_heparin_etiologi"
                                        name="tampa_heparin_etiologi"
                                        type="text"
                                        style={{width: "90%"}}
                                        value={datas ? datas.tampa_heparin_etiologi : ''}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                        placeholder="Tampa Heparin, Etiologi"/>
                                </Form.Group>
                            </td>

                            <td width="20%">
                                <Form.Group controlId="program_bilas_nac1">
                                    <Form.Label>Program Bilas NaC1 0,9% (Jam)</Form.Label>
                                    <Form.Control
                                        size="sm"
                                        autoComplete="off"
                                        id="program_bilas_nac1"
                                        name="program_bilas_nac1"
                                        type="text"
                                        value={datas ? datas.program_bilas_nac1 : ''}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                        placeholder="Program Bilas NaC1 0,9% (Jam)"/>
                                </Form.Group>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                    <table className="table table-condensed table-form-askep">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="6" align="center">
                                Akses Vaskuler
                            </td>
                        </tr>
                        <tr>
                            <td width="5%"><strong>AV Shunt</strong></td>
                            <td width="3%">:</td>
                            <td width="7%">
                                <Form.Check
                                    type="checkbox"
                                    name="akses_vaskuler"
                                    id="akses_vaskuler3"
                                    checked={datas ? datas.akses_vaskuler?.includes("av_shunt_kiri") : ''}
                                    onChange={e => setOnChange("akses_vaskuler", "av_shunt_kiri")}
                                    label="Kiri"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="akses_vaskuler"
                                    id="akses_vaskuler5"
                                    checked={datas ? datas.akses_vaskuler?.includes("av_shunt_kanan") : ''}
                                    onChange={e => setOnChange("akses_vaskuler", "av_shunt_kanan")}
                                    label="Kanan"/>
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="4" style={{color: "#fff"}}>.</td>
                        </tr>
                        <tr>
                            <td colSpan="4"><strong>CDL</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Jugular</strong></td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="akses_vaskuler"
                                    id="akses_vaskuler11"
                                    checked={datas ? datas.akses_vaskuler?.includes("jugular_kiri") : ''}
                                    onChange={e => setOnChange("akses_vaskuler", "jugular_kiri")}
                                    label="Kiri"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="akses_vaskuler"
                                    id="akses_vaskuler13"
                                    checked={datas ? datas.akses_vaskuler?.includes("jugular_kanan") : ''}
                                    onChange={e => setOnChange("akses_vaskuler", "jugular_kanan")}
                                    label="Kanan"/>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Subclavia</strong></td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="akses_vaskuler"
                                    id="akses_vaskuler17"
                                    checked={datas ? datas.akses_vaskuler?.includes("subclavia_kiri") : ''}
                                    onChange={e => setOnChange("akses_vaskuler", "subclavia_kiri")}
                                    label="Kiri"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="akses_vaskuler"
                                    id="akses_vaskuler19"
                                    checked={datas ? datas.akses_vaskuler?.includes("subclavia_kanan") : ''}
                                    onChange={e => setOnChange("akses_vaskuler", "subclavia_kanan")}
                                    label="Kanan"/>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Femoral</strong></td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="akses_vaskuler"
                                    id="akses_vaskuler22"
                                    checked={datas ? datas.akses_vaskuler?.includes("femoral_kiri") : ''}
                                    onChange={e => setOnChange("akses_vaskuler", "femoral_kiri")}
                                    label="Kiri"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="akses_vaskuler"
                                    id="akses_vaskuler23"
                                    checked={datas ? datas.akses_vaskuler?.includes("femoral_kanan") : ''}
                                    onChange={e => setOnChange("akses_vaskuler", "femoral_kanan")}
                                    label="Kanan"/>
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="4" style={{color: "#fff"}}>.</td>
                        </tr>
                        <tr>
                            <td><strong>AV Graft</strong></td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="akses_vaskuler"
                                    id="akses_vaskuler4"
                                    checked={datas ? datas.akses_vaskuler?.includes("av_graft_kiri") : ''}
                                    onChange={e => setOnChange("akses_vaskuler", "av_graft_kiri")}
                                    label="Kiri"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="akses_vaskuler"
                                    id="akses_vaskuler6"
                                    checked={datas ? datas.akses_vaskuler?.includes("av_graft_kanan") : ''}
                                    onChange={e => setOnChange("akses_vaskuler", "av_graft_kanan")}
                                    label="Kanan"/>
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="4" style={{color: "#fff"}}>.</td>
                        </tr>
                        <tr>
                            <td colSpan="4"><strong>Akses Vena</strong></td>

                        </tr>
                        <tr>
                            <td><strong>Lengan</strong></td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="akses_vaskuler"
                                    id="akses_vaskuler12"
                                    checked={datas ? datas.akses_vaskuler?.includes("lengan_kiri") : ''}
                                    onChange={e => setOnChange("akses_vaskuler", "lengan_kiri")}
                                    label="Kiri"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="akses_vaskuler"
                                    id="akses_vaskuler14"
                                    checked={datas ? datas.akses_vaskuler?.includes("lengan_kanan") : ''}
                                    onChange={e => setOnChange("akses_vaskuler", "lengan_kanan")}
                                    label="Kanan"/>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Tungkai</strong></td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="akses_vaskuler"
                                    id="akses_vaskuler18"
                                    checked={datas ? datas.akses_vaskuler?.includes("tungkai_kiri") : ''}
                                    onChange={e => setOnChange("akses_vaskuler", "tungkai_kiri")}
                                    label="Kiri"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="akses_vaskuler"
                                    id="akses_vaskuler20"
                                    checked={datas ? datas.akses_vaskuler?.includes("tungkai_kanan") : ''}
                                    onChange={e => setOnChange("akses_vaskuler", "tungkai_kanan")}
                                    label="Kanan"/>
                            </td>
                        </tr>

                        <tr>
                            <td><strong>Femoral</strong></td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="akses_vaskuler"
                                    id="akses_vaskuler24"
                                    checked={datas ? datas.akses_vaskuler?.includes("akses_vena_femoral_kiri") : ''}
                                    onChange={e => setOnChange("akses_vaskuler", "akses_vena_femoral_kiri")}
                                    label="Kiri"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="akses_vaskuler"
                                    id="akses_vaskuler25"
                                    checked={datas ? datas.akses_vaskuler?.includes("akses_vena_femoral_kanan") : ''}
                                    onChange={e => setOnChange("akses_vaskuler", "akses_vena_femoral_kanan")}
                                    label="Kanan"/>
                            </td>
                        </tr>


                        </tbody>
                    </table>
                    <table className="table table-condensed">
                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="9" align="center">
                                Cairan
                            </td>
                        </tr>
                        <tr>
                            <td width="12%" colSpan="3">
                                <strong>Cairan Masuk</strong>
                            </td>

                            <td width="12%" colSpan="3"><strong>Cairan Keluar</strong>
                            </td>


                            <td width="10%"><strong>Balance Cairan</strong>

                            </td>
                            <td width="1%"></td>

                            <td></td>
                        </tr>

                        <tr>
                            <td width="11%">
                                Sisa Priming (ml)
                            </td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="sisa_priming"
                                    style={{width: "50%"}}
                                    type="text"
                                    maxLength={5}
                                    value={datas ? datas.sisa_priming : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Sisa Priming (ml)"/>
                            </td>
                            <td width="11%">Urin (ml)
                            </td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="urin"
                                    style={{width: "50%"}}
                                    type="text"
                                    value={datas ? datas.urin : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Urin (ml)"/>
                            </td>

                            <td>Volume Priming (ml)

                            </td>
                            <td width="1%">:</td>

                            <td>
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="volume_priming"
                                    style={{width: "60%"}}
                                    type="text"
                                    maxLength={5}
                                    value={datas ? datas.volume_priming : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Volume Priming (ml)"/>
                            </td>
                        </tr>

                        <tr>
                            <td width="8%">
                                Cairan Drip (ml)
                            </td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="cairan_drip"
                                    style={{width: "50%"}}
                                    type="text"
                                    value={datas ? datas.cairan_drip : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Cairan Drip (ml)"/>
                            </td>
                            <td width="8%">Muntah (ml)
                            </td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="muntah"
                                    style={{width: "50%"}}
                                    type="text"
                                    value={datas ? datas.muntah : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Muntah (ml)"/>
                            </td>

                            <td width="8%"><strong>Total balance (ml)</strong></td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Control
                                    readOnly
                                    size="sm"
                                    autoComplete="off"
                                    name="total_balance"
                                    style={{width: "60%"}}
                                    type="text"
                                    value={datas ? datas.total_balance : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Total balance (ml)"/>
                            </td>
                        </tr>

                        <tr>
                            <td width="8%">
                                Transfusi (ml)
                            </td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="transfusi"
                                    style={{width: "50%"}}
                                    type="text"
                                    value={datas ? datas.transfusi : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Transfusi (ml)"/>
                            </td>
                            <td width="8%">
                                Ultrafiltrat (ml)
                            </td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="ultrafiltrat"
                                    style={{width: "50%"}}
                                    type="text"
                                    value={datas ? datas.ultrafiltrat : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Ultrafiltrat (ml)"/>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td width="8%">
                                Sonde (ml)
                            </td>
                            <td width="1%">:</td>
                            <td width="23%">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="sonde"
                                    style={{width: "50%"}}
                                    type="text"
                                    value={datas ? datas.sonde : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Sonde (ml)"/>
                            </td>

                            <td width="8%">
                                <strong>Total cairan keluar (ml)</strong>
                            </td>
                            <td width="1%">:</td>
                            <td width="23%">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="total_cairan_keluar"
                                    style={{width: "50%"}}
                                    type="text"
                                    value={datas ? datas.total_cairan_keluar : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Total cairan keluar (ml)"/>
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td width="8%">
                                Minum (ml)
                            </td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="minum"
                                    style={{width: "50%"}}
                                    type="text"
                                    value={datas ? datas.minum : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Minum (ml)"/>
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>

                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td width="8%">
                                Wash Out (ml)
                            </td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="wash_out"
                                    style={{width: "50%"}}
                                    type="text"
                                    value={datas ? datas.wash_out : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Wash Out (ml)"/>
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>

                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td width="8%">
                                <strong>Total cairan masuk (ml)</strong>
                            </td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="total_cairan_masuk"
                                    style={{width: "50%"}}
                                    type="text"
                                    value={datas ? datas.total_cairan_masuk : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Total cairan masuk (ml)"/>
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>

                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>

                        </tbody>
                    </table>


                    <button
                        id="submitFormAskepHeparinisasi"
                        type="button" onClick={(e) => handleSubmit()}
                        className="btn btn-success btn-sm">Submit Form Heparinisasi
                    </button>
                </Form>
            )}
        </div>
    )
}
