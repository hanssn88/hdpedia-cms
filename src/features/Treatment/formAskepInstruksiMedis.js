import axios from "axios";
import moment from 'moment';
import "moment/locale/id";
import React, {useEffect, useState} from "react";
import {Col, Form, Row} from "react-bootstrap";
import NumberFormat from "react-number-format";
import {Placeholder} from "rsuite";
import Swal from 'sweetalert2';

const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

export const FormAskepInstruksiMedis = (data) => {
    const [datas, setDatas] = useState(data?.data);

    const [datt, setDatt] = useState(data?.data);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        async function setData() {

            await setDatas(data?.data);
            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);
        }

        async function setDatts() {

            await setDatt(data?.data);


        }

        setData();
        setDatts();
    }, [data])

    const setOnChange = async (key, val) => {
        //moment.utc(moment(now,"DD/MM/YYYY HH:mm:ss").diff(moment(then,"DD/MM/YYYY HH:mm:ss"))).format("HH:mm:ss")
        if (key === 'na_text' || key === 'uf_text' || key === 'bikarbonat_text' || key === 'conductivity' || key === 'temperature_mesin') {
            if (parseInt(val) >= 1000) {
                await setDatas({...datas});
            } else {
                await setDatas({...datas, [key]: val});
            }
        } else if (key === "resep_hd") {
            const resep_hd = [];

            await resep_hd.push(val);

            if (val !== 'SLED') {
                await setDatas({...datas, keterangan_sled: "", resep_hd: resep_hd});
            } else {
                await setDatas({...datas, resep_hd: resep_hd});
            }
        } else if (key === "jam_mulai" || key === "jam_selesai") {
            let lama_hd;
            let jam_mulai;
            let jam_selesai;
            if (key === "jam_mulai") {
                jam_mulai = moment.utc(val, "HH:mm");
                jam_selesai = datas.jam_selesai !== "" && datas.jam_selesai !== undefined ? moment.utc(datas.jam_selesai, "HH:mm") : '';
                if (jam_selesai !== "" && jam_selesai !== undefined) {
                    const diff = jam_selesai.diff(jam_mulai);
                    const diffDuration = moment.duration(diff);
                    const durationHours = parseInt(diffDuration.hours()) > 9 ? diffDuration.hours() : `0${diffDuration.hours()}`;
                    const durationMinutes = parseInt(diffDuration.minutes()) > 9 ? diffDuration.minutes() : `0${diffDuration.minutes()}`;
                    lama_hd = `${durationHours}:${durationMinutes}`;
                }
            }
            if (key === "jam_selesai") {
                jam_mulai = datas.jam_mulai !== "" && datas.jam_mulai !== undefined ? moment.utc(datas.jam_mulai, "HH:mm") : '';
                jam_selesai = moment.utc(val, "HH:mm");

                if (jam_mulai !== "" && jam_mulai !== undefined) {
                    const diff = jam_selesai.diff(jam_mulai);
                    const diffDuration = moment.duration(diff);
                    const durationHours = parseInt(diffDuration.hours()) > 9 ? diffDuration.hours() : `0${diffDuration.hours()}`;
                    const durationMinutes = parseInt(diffDuration.minutes()) > 9 ? diffDuration.minutes() : `0${diffDuration.minutes()}`;
                    lama_hd = `${durationHours}:${durationMinutes}`;
                }
            }
            await setDatas({...datas, [key]: val, lama_hd: lama_hd});
        } else {
            await setDatas({...datas, [key]: val});
        }
    };

    const handleSubmit = async (e) => {
        const {id, created_at, updated_at, ...params} = datas;
        if (typeof params.treatment_id === "undefined" || params.treatment_id === "")
            params.treatment_id = sessionStorage.getItem('idPasienHDPedia');
        if (params.resep_hd == "Rutin" || params.resep_hd == "Pre Op" || params.resep_hd == "Inisiasi") {

            params.resep_hd = params.resep_hd;
        } else {
            params.resep_hd = params.keterangan_sled;
        }
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            await axios.post(API_URL + '/dokter/add_instruksi_medis', params, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            return Swal.fire({
                title: 'Informasi',
                text: "Success add instruksi medis",
                icon: 'success',
                confirmButtonText: 'Ok',
                allowOutsideClick: true
            })
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    const {Paragraph} = Placeholder;
    return (
        <div>
            {isLoading ? (
                <div className="card shadow-lg">
                    <div className="card-body">
                        <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                   style={{marginTop: 30}}/>
                    </div>
                </div>

            ) : (
                <Form>
                    <h3>Form Askep</h3>
                    <table className="table table-condensed table-form-askep">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="9" align="center">
                                Instruksi Medis
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="9">
                                <Form.Group as={Row} className="mb-3" controlId="instruksi_medis">
                                    <Form.Label column sm={2}>
                                        Resep HD
                                    </Form.Label>
                                    <Col sm={10}>
                                        <Form.Check
                                            type="radio"
                                            name="resep_hd"
                                            id="resep_hd"
                                            value="Inisiasi"
                                            label="Inisiasi"
                                            checked={datas ? datas.resep_hd?.includes("Inisiasi") : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                        <Form.Check
                                            type="radio"
                                            name="resep_hd"
                                            id="resep_hd2"
                                            value="Rutin"
                                            label="Rutin"
                                            checked={datas ? datas.resep_hd?.includes("Rutin") : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                        <Form.Check
                                            type="radio"
                                            name="resep_hd"
                                            id="resep_hd3"
                                            value="Pre Op"
                                            label="Pre Op"
                                            checked={datas ? datas.resep_hd?.includes("Pre Op") : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                        <Form.Check
                                            type="radio"
                                            name="resep_hd"
                                            id="resep_hd4"
                                            value={datas && datas.resep_hd != "Pre Op" && datas.resep_hd != "Rutin" && datas.resep_hd != "Inisiasi" ? datas.resep_hd : "SLED"}
                                            label="SLED"
                                            checked={datas && (datas.resep_hd != "Rutin" && datas.resep_hd != "Pre Op" && datas.resep_hd != "Inisiasi") ? datas.resep_hd == "SLED" ? datas.keterangan_sled : datt.resep_hd : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                        {datas && (datas.resep_hd != "Pre Op" && datas.resep_hd != "Rutin" && datas.resep_hd != "Inisiasi") || (datas.resep_hd == "SLED") ? (
                                            <Form.Control
                                                size="sm"
                                                autoComplete="off"
                                                name="keterangan_sled"
                                                style={{width: "15%", marginTop: 5}}
                                                type="text"
                                                value={datas && (datas.resep_hd != "Rutin" && datas.resep_hd != "Pre Op" && datas.resep_hd != "Inisiasi") ? datas.resep_hd == "SLED" ? datas.keterangan_sled : datt.resep_hd : ''}
                                                onChange={e => setOnChange(e.target.name, e.target.value)}
                                                placeholder="Keterangan"/>) : ''}
                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="resep_hd_jam"
                                            style={{width: "15%", marginTop: 5}}
                                            type="text"
                                            maxLength={2}
                                            value={datas?.resep_hd_jam}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="Time Dialysis (jam)"/>

                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="qb_mnt"
                                            style={{width: "15%", marginTop: 5}}
                                            type="text"
                                            maxLength={3}
                                            value={datas?.qb_mnt}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="QB (ml/mnt)"/>

                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="qd_mnt"
                                            style={{width: "15%", marginTop: 5}}
                                            type="text"
                                            maxLength={3}
                                            value={datas?.qd_mnt}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="QD (ml/mnt)"/>

                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="ufg_ml"
                                            style={{width: "15%", marginTop: 5}}
                                            type="text"
                                            maxLength={5}
                                            value={datas?.ufg_ml}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="UFG (ml)"/>

                                    </Col>
                                </Form.Group>

                            </td>
                        </tr>

                        <tr>
                            <td colSpan="9" className="td-valign-top">
                                <strong>Program Profiling</strong>

                            </td>

                        </tr>


                        <tr>
                            <td colSpan="3">
                                <Form.Check
                                    type="checkbox"
                                    name="na"
                                    id="na"
                                    value="Na"
                                    label="Na"
                                    checked={datas?.na || datas?.na_text ? true : false}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}/>

                                <NumberFormat
                                    name="na_text"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    className="form-control form-control-sm"
                                    value={datas ? datas.na_text : ''}
                                    thousandSeparator={false}
                                    decimalScale={2}
                                    inputMode="numeric"
                                    autoComplete="off"
                                    id="na_text"
                                    decimalSeparator=","
                                    placeholder="Na"/>
                            </td>

                            <td colSpan="3">
                                <Form.Check
                                    type="checkbox"
                                    name="uf"
                                    id="uf"
                                    value="UF"
                                    label="UF"
                                    checked={datas?.uf || datas?.uf_text ? true : false}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                <NumberFormat
                                    name="uf_text"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    className="form-control form-control-sm"
                                    value={datas ? datas.uf_text : ''}
                                    thousandSeparator={false}
                                    decimalScale={2}
                                    inputMode="numeric"
                                    autoComplete="off"
                                    id="uf_text"
                                    decimalSeparator=","
                                    placeholder="UF"/>
                            </td>

                            <td colSpan="3">
                                <Form.Check
                                    type="checkbox"
                                    name="bikarbonat"
                                    id="bikarbonat"
                                    value="Bikarbonat"
                                    label="Bikarbonat"
                                    checked={datas?.bikarbonat || datas?.bikarbonat_text ? true : false}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                <NumberFormat
                                    name="bikarbonat_text"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    className="form-control form-control-sm"
                                    value={datas ? datas.bikarbonat_text : ''}
                                    thousandSeparator={false}
                                    decimalScale={2}
                                    inputMode="numeric"
                                    autoComplete="off"
                                    id="bikarbonat_text"
                                    decimalSeparator=","
                                    placeholder="Bikarbonat"/>
                            </td>
                        </tr>

                        <tr>
                            <td width="6%">
                                <strong>Jam Mulai</strong>
                            </td>
                            <td width="1%">:</td>
                            <td width="18%">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="jam_mulai"
                                    style={{width: "30%"}}
                                    type="time"
                                    value={datas?.jam_mulai}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Jam Mulai"/>
                            </td>
                            <td width="7%"><strong>Jam Selesai</strong>
                            </td>
                            <td width="1%">:</td>
                            <td width="19%">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="jam_selesai"
                                    style={{width: "30%"}}
                                    type="time"
                                    value={datas?.jam_selesai}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Jam Selesai"/>
                            </td>

                            <td width="6%"><strong>Lama HD</strong>

                            </td>
                            <td width="1%">:</td>

                            <td width="18%">
                                <Form.Control
                                    readOnly
                                    size="sm"
                                    autoComplete="off"
                                    name="lama_hd"
                                    type="text"
                                    value={datas?.lama_hd}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Lama HD (jam & menit)"/>
                            </td>

                        </tr>
                        <tr>
                            <td width="6%">
                                <strong>Jenis Dialisat</strong>
                            </td>
                            <td width="1%">:</td>
                            <td width="18%">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="jenis_dialisat"
                                    style={{width: "85%"}}
                                    type="text"
                                    value={datas?.jenis_dialisat}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Jenis Dialisat"/>
                            </td>
                            <td width="7%"><strong>Conductivity</strong>
                            </td>
                            <td width="1%">:</td>
                            <td width="19%">
                                <NumberFormat
                                    name="conductivity"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    className="form-control form-control-sm"
                                    value={datas ? datas.conductivity : ''}
                                    thousandSeparator={false}
                                    decimalScale={2}
                                    inputMode="numeric"
                                    autoComplete="off"
                                    id="conductivity"
                                    decimalSeparator=","
                                    placeholder="Conductivity"/>
                            </td>

                            <td width="6%"><strong>Temperatur Mesin (  &#8451; )</strong>

                            </td>
                            <td width="1%">:</td>

                            <td width="18%">
                                <NumberFormat
                                    name="temperature_mesin"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    className="form-control form-control-sm"
                                    value={datas ? datas.temperature_mesin : ''}
                                    thousandSeparator={false}
                                    decimalScale={2}
                                    inputMode="numeric"
                                    autoComplete="off"
                                    id="temperature_mesin"
                                    decimalSeparator=","
                                    placeholder="Temperatur ( &#8451;)"/>
                            </td>

                        </tr>
                        </tbody>
                    </table>

                    <button
                        type="button"
                        id="submitFormInstruksiMedis"
                        onClick={(e) => handleSubmit()}
                        className="btn btn-success btn-sm">Submit Form Instruksi Medis
                    </button>
                </Form>
            )}
        </div>
    )
}
