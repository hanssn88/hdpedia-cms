import axios from "axios";
import React, {useEffect, useState} from "react";
import {Col, Form, Row} from "react-bootstrap";
import {Placeholder} from "rsuite";
import Swal from "sweetalert2";

const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;
export const FormAskepIntervensiKeperawatan = (data) => {
    const [datas, setDatas] = useState(data?.data);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        async function setData() {
            await setDatas(data?.data);
            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);
        }

        setData();
    }, [data])

    const setOnChange = async (key, val) => {
        if (key === "intervensi") {
            const intervensi = [];

            if (datas && typeof datas.intervensi !== "undefined" && datas.intervensi instanceof Array) {
                for (let i = 0; i < datas.intervensi.length; i++) {
                    await intervensi.push(datas.intervensi[i]);
                }
            }

            const index = intervensi.indexOf(val);
            if (index > -1) {
                await intervensi.splice(index, 1);
            } else {
                await intervensi.push(val);
            }

            await setDatas({...datas, intervensi: intervensi});
        } else {
            await setDatas({...datas, [key]: val});
        }
    };

    const handleSubmit = async (e) => {
        const {id, created_at, updated_at, ...params} = datas;
        if (typeof params.treatment_id === "undefined" || params.treatment_id === "") params.treatment_id = sessionStorage.getItem('idPasienHDPedia');
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            await axios.post(API_URL + '/dokter/add_intervensi_keperawatan', params, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            return Swal.fire({
                title: 'Informasi',
                text: "Success add intervensi keperawatan",
                icon: 'success',
                confirmButtonText: 'Ok',
                allowOutsideClick: true
            })
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    const {Paragraph} = Placeholder;
    return (
        <div>
            {isLoading ? (
                <div className="card shadow-lg">
                    <div className="card-body">
                        <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                   style={{marginTop: 30}}/>
                    </div>
                </div>

            ) : (
                <Form>
                    <h3>Form Askep</h3>
                    <table className="table table-condensed table-form-askep">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} align="center">
                                Intervensi Keperawatan
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi"
                                    checked={datas ? datas.intervensi?.includes("Observasi Keadaan Umum dan Tanda Tanda Vital Pasien (TD, Nadi, RR, Suhu)") : ''}
                                    onChange={e => setOnChange("intervensi", "Observasi Keadaan Umum dan Tanda Tanda Vital Pasien (TD, Nadi, RR, Suhu)")}
                                    label="Observasi Keadaan Umum dan Tanda Tanda Vital Pasien (TD, Nadi, RR, Suhu)"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi2"
                                    checked={datas ? datas.intervensi?.includes("Observasi dan Monitoring Durante HD (Keadaan Umum, TTV, Akses Vaskuler, Mesin)") : ''}
                                    onChange={e => setOnChange("intervensi", "Observasi dan Monitoring Durante HD (Keadaan Umum, TTV, Akses Vaskuler, Mesin)")}
                                    label="Observasi dan Monitoring Durante HD (Keadaan Umum, TTV, Akses Vaskuler, Mesin)"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi3"
                                    checked={datas ? datas.intervensi?.includes("Kaji Status Cairan (Timbang Berat Badan, Input dan Output Cairan, Turgor Kulit, Adanya Edema)") : ''}
                                    onChange={e => setOnChange("intervensi", "Kaji Status Cairan (Timbang Berat Badan, Input dan Output Cairan, Turgor Kulit, Adanya Edema)")}
                                    label="Kaji Status Cairan (Timbang Berat Badan, Input dan Output Cairan, Turgor Kulit, Adanya Edema)"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi4"
                                    checked={datas ? datas.intervensi?.includes("Posisikan Supinasi Dengan Elevasi Kepala 30\u00b0 dan Elevasi Kaki") : ''}
                                    onChange={e => setOnChange("intervensi", "Posisikan Supinasi Dengan Elevasi Kepala 30\u00b0 dan Elevasi Kaki")}
                                    label="Posisikan Supinasi Dengan Elevasi Kepala 30° dan Elevasi Kaki"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi5"
                                    checked={datas ? datas.intervensi?.includes("Kaji Pemahaman Mengenai Penyebab Ginjal, Fungsi Ginjal, Pembatasan Diit, Penanganan (HD)") : ''}
                                    onChange={e => setOnChange("intervensi", "Kaji Pemahaman Mengenai Penyebab Ginjal, Fungsi Ginjal, Pembatasan Diit, Penanganan (HD)")}
                                    label="Kaji Pemahaman Mengenai Penyebab Ginjal, Fungsi Ginjal, Pembatasan Diit, Penanganan (HD)"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi6"
                                    checked={datas ? datas.intervensi?.includes("Hentikan HD Sesuai Indikasi") : ''}
                                    onChange={e => setOnChange("intervensi", "Hentikan HD Sesuai Indikasi")}
                                    label="Hentikan HD Sesuai Indikasi"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi7"
                                    checked={datas ? datas.intervensi?.includes("Kaji Kemampuan Pasien Mendapatkan Nutrisi yang Dibutuhkan") : ''}
                                    onChange={e => setOnChange("intervensi", "Kaji Kemampuan Pasien Mendapatkan Nutrisi yang Dibutuhkan")}
                                    label="Kaji Kemampuan Pasien Mendapatkan Nutrisi yang Dibutuhkan"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi8"
                                    checked={datas ? datas.intervensi?.includes("Monitor Tanda dan Gejala Infeksi (Lokal dan Sistemik)") : ''}
                                    onChange={e => setOnChange("intervensi", "Monitor Tanda dan Gejala Infeksi (Lokal dan Sistemik)")}
                                    label="Monitor Tanda dan Gejala Infeksi (Lokal dan Sistemik)"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi9"
                                    checked={datas ? datas.intervensi?.includes("Observasi dan Kaji Akses Vaskuler (Bila Adanya Tanda Tanda Infeksi)") : ''}
                                    onChange={e => setOnChange("intervensi", "Observasi dan Kaji Akses Vaskuler (Bila Adanya Tanda Tanda Infeksi)")}
                                    label="Observasi dan Kaji Akses Vaskuler (Bila Adanya Tanda Tanda Infeksi)"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi10"
                                    checked={datas ? datas.intervensi?.includes("Ganti Balutan Luka Sesuai Dengan Prosedur") : ''}
                                    onChange={e => setOnChange("intervensi", "Ganti Balutan Luka Sesuai Dengan Prosedur")}
                                    label="Ganti Balutan Luka Sesuai Dengan Prosedur"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi11"
                                    checked={datas ? datas.intervensi?.includes("Berikan Rasa Nyaman Terhadap Pasien") : ''}
                                    onChange={e => setOnChange("intervensi", "Berikan Rasa Nyaman Terhadap Pasien")}
                                    label="Berikan Rasa Nyaman Terhadap Pasien"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi12"
                                    checked={datas ? datas.intervensi?.includes("Berikan Tanda Risiko Jatuh") : ''}
                                    onChange={e => setOnChange("intervensi", "Berikan Tanda Risiko Jatuh")}
                                    label="Berikan Tanda Risiko Jatuh"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi13"
                                    checked={datas ? datas.intervensi?.includes("Atur Posisi Pasien Agar Ventilasi Adekuat") : ''}
                                    onChange={e => setOnChange("intervensi", "Atur Posisi Pasien Agar Ventilasi Adekuat")}
                                    label="Atur Posisi Pasien Agar Ventilasi Adekuat"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi14"
                                    checked={datas ? datas.intervensi?.includes("Tinggikan \/ Pasang Bedsite") : ''}
                                    onChange={e => setOnChange("intervensi", "Tinggikan \/ Pasang Bedsite")}
                                    label="Tinggikan / Pasang Bedsite"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi15"
                                    checked={datas ? datas.intervensi?.includes("Monitor Berat Badan, Intake dan Out Put") : ''}
                                    onChange={e => setOnChange("intervensi", "Monitor Berat Badan, Intake dan Out Put")}
                                    label="Monitor Berat Badan, Intake dan Out Put"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi16"
                                    checked={datas ? datas.intervensi?.includes("Bantu Pasien Dalam Pemenuhan Kebutuhan Mobilisasi") : ''}
                                    onChange={e => setOnChange("intervensi", "Bantu Pasien Dalam Pemenuhan Kebutuhan Mobilisasi")}
                                    label="Bantu Pasien Dalam Pemenuhan Kebutuhan Mobilisasi"/>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi17"
                                    checked={datas ? datas.intervensi?.includes("Monitor Tanda dan Gejala Hipoglikemi") : ''}
                                    onChange={e => setOnChange("intervensi", "Monitor Tanda dan Gejala Hipoglikemi")}
                                    label="Monitor Tanda dan Gejala Hipoglikemi"/>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi18"
                                    checked={datas ? datas.intervensi?.includes("Berikan PENKES : diit AV-Shunt") : ''}
                                    onChange={e => setOnChange("intervensi", "Berikan PENKES : diit AV-Shunt")}
                                    label="Berikan PENKES : diit AV-Shunt"/>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi19"
                                    checked={datas ? datas.intervensi?.includes("Monitor Area Akses Vaskuler Terhadap, Kemerahan, Bengkak, Nyeri") : ''}
                                    onChange={e => setOnChange("intervensi", "Monitor Area Akses Vaskuler Terhadap, Kemerahan, Bengkak, Nyeri")}
                                    label="Monitor Area Akses Vaskuler Terhadap, Kemerahan, Bengkak, Nyeri"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi20"
                                    checked={datas ? datas.intervensi?.includes("Kolaborasi Dengan Dokter Dalam Memberikan Terapi Oksigen Sesuai Kebutuhan") : ''}
                                    onChange={e => setOnChange("intervensi", "Kolaborasi Dengan Dokter Dalam Memberikan Terapi Oksigen Sesuai Kebutuhan")}
                                    label="Kolaborasi Dengan Dokter Dalam Memberikan Terapi Oksigen Sesuai Kebutuhan"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi21"
                                    checked={datas ? datas.intervensi?.includes("Pertahankan Teknik Steril Selama Kontak Dengan Area Vaskuler (Inisiasi dan Terminasi)") : ''}
                                    onChange={e => setOnChange("intervensi", "Pertahankan Teknik Steril Selama Kontak Dengan Area Vaskuler (Inisiasi dan Terminasi)")}
                                    label="Pertahankan Teknik Steril Selama Kontak Dengan Area Vaskuler (Inisiasi dan Terminasi)"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi22"
                                    checked={datas ? datas.intervensi?.includes("Kolaborasi Dengan Dokter Dalam Pemberian Terapi Medis (Hemodialisis, Obat, Drip Cairan)") : ''}
                                    onChange={e => setOnChange("intervensi", "Kolaborasi Dengan Dokter Dalam Pemberian Terapi Medis (Hemodialisis, Obat, Drip Cairan)")}
                                    label="Kolaborasi Dengan Dokter Dalam Pemberian Terapi Medis (Hemodialisis, Obat, Drip Cairan)"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi23"
                                    checked={datas ? datas.intervensi?.includes("Lakukan Teknik Distraksi Relaksasi") : ''}
                                    onChange={e => setOnChange("intervensi", "Lakukan Teknik Distraksi Relaksasi")}
                                    label="Lakukan Teknik Distraksi Relaksasi"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi24"
                                    checked={datas ? datas.intervensi?.includes("Lain-lain") : ''}
                                    onChange={e => setOnChange("intervensi", "Lain-lain")}
                                    label="Lain-lain"/>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Form.Group as={Row} className="mb-3" controlId="keterangan">
                                    <Form.Label column sm={1}>
                                        Keterangan :
                                    </Form.Label>
                                    <Col sm={11}>
                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="keterangan"
                                            type="text"
                                            value={datas?.keterangan}
                                            placeholder="Keterangan"
                                            onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                    </Col>
                                </Form.Group>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <button
                        type="button"
                        id="submitFormIntervensiKeperawatan"
                        onClick={(e) => handleSubmit()}
                        className="btn btn-success btn-sm">Submit Form Intervensi Keperawatan
                    </button>
                </Form>

            )}
        </div>
    )
}
