import React, {useEffect, useState} from "react";
import {Col, Form, Row} from "react-bootstrap";
import axios from "axios";
import Swal from "sweetalert2";
import {Placeholder} from "rsuite";

const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

export const FormAskepIntervensiKolaborasi = (data) => {
    const [datas, setDatas] = useState(data?.data);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        async function setData() {
            await setDatas(data?.data);
            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);
        }

        setData();
    }, [data])

    const setOnChange = async (key, val) => {
        if (key === "intervensi") {
            const intervensi = [];

            if (datas && typeof datas.intervensi !== "undefined" && datas.intervensi instanceof Array) {
                for (let i = 0; i < datas.intervensi.length; i++) {
                    await intervensi.push(datas.intervensi[i]);
                }
            }

            const index = intervensi.indexOf(val);
            if (index > -1) {
                await intervensi.splice(index, 1);
            } else {
                await intervensi.push(val);
            }

            await setDatas({...datas, intervensi: intervensi});
        } else {
            await setDatas({...datas, [key]: val});
        }
    };

    const handleSubmit = async (e) => {
        const {id, created_at, updated_at, ...params} = datas;
        if (typeof params.treatment_id === "undefined" || params.treatment_id === "") params.treatment_id = sessionStorage.getItem('idPasienHDPedia');
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            await axios.post(API_URL + '/dokter/add_intervensi_kolaborasi', params, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            return Swal.fire({
                title: 'Informasi',
                text: "Success add intervensi kolaborasi",
                icon: 'success',
                confirmButtonText: 'Ok',
                allowOutsideClick: true
            })
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    const {Paragraph} = Placeholder;

    return (
        <div>
            {isLoading ? (
                <div className="card shadow-lg">
                    <div className="card-body">
                        <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                   style={{marginTop: 30}}/>
                    </div>
                </div>

            ) : (
                <Form>
                    <h3>Form Askep</h3>
                    <table className="table table-condensed table-form-askep">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="6" align="center">
                                Intervensi Kolaborasi
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi"
                                    checked={datas ? datas.intervensi?.includes("Program HD") : ''}
                                    onChange={e => setOnChange("intervensi", "Program HD")}
                                    label="Program HD"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi2"
                                    checked={datas ? datas.intervensi?.includes("Pemberian Analgetik") : ''}
                                    onChange={e => setOnChange("intervensi", "Pemberian Analgetik")}
                                    label="Pemberian Analgetik"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi3"
                                    checked={datas ? datas.intervensi?.includes("Transfusi Darah") : ''}
                                    onChange={e => setOnChange("intervensi", "Transfusi Darah")}
                                    label="Transfusi Darah"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi4"
                                    checked={datas ? datas.intervensi?.includes("Pemberian Erytropoetin") : ''}
                                    onChange={e => setOnChange("intervensi", "Pemberian Erytropoetin")}
                                    label="Pemberian Erytropoetin"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi5"
                                    checked={datas ? datas.intervensi?.includes("Kolaborasi Diit") : ''}
                                    onChange={e => setOnChange("intervensi", "Kolaborasi Diit")}
                                    label="Kolaborasi Diit"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi6"
                                    checked={datas ? datas.intervensi?.includes("Pemberian Preparat Besi") : ''}
                                    onChange={e => setOnChange("intervensi", "Pemberian Preparat Besi")}
                                    label="Pemberian Preparat Besi"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi7"
                                    checked={datas ? datas.intervensi?.includes("Pemberian Ca Gluconas") : ''}
                                    onChange={e => setOnChange("intervensi", "Pemberian Ca Gluconas")}
                                    label="Pemberian Ca Gluconas"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi8"
                                    checked={datas ? datas.intervensi?.includes("Obat-obat Emergensi") : ''}
                                    onChange={e => setOnChange("intervensi", "Obat-obat Emergensi")}
                                    label="Obat-obat Emergensi"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi9"
                                    checked={datas ? datas.intervensi?.includes("Pemberian Antipiretik") : ''}
                                    onChange={e => setOnChange("intervensi", "Pemberian Antipiretik")}
                                    label="Pemberian Antipiretik"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi10"
                                    checked={datas ? datas.intervensi?.includes("Pemberian Antibiotik") : ''}
                                    onChange={e => setOnChange("intervensi", "Pemberian Antibiotik")}
                                    label="Pemberian Antibiotik"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="intervensi"
                                    id="intervensi11"
                                    checked={datas ? datas.intervensi?.includes("Lain-lain") : ''}
                                    onChange={e => setOnChange("intervensi", "Lain-lain")}
                                    label="Lain-lain"/>
                            </td>
                            <td></td>
                        </tr>

                        <tr>
                            <td colSpan="6">
                                <Form.Group as={Row} className="mb-3" controlId="keterangan">
                                    <Form.Label column sm={1}>
                                        Keterangan :
                                    </Form.Label>
                                    <Col sm={11}>
                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="keterangan"
                                            type="text"
                                            value={datas?.keterangan}
                                            placeholder="Keterangan"
                                            onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                    </Col>
                                </Form.Group>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <button
                        type="button"
                        id="submitFormIntervensiKolaborasi"
                        onClick={(e) => handleSubmit()}
                        className="btn btn-success btn-sm">Submit Form Intervensi Kolaborasi
                    </button>
                </Form>
            )}
        </div>
    )
}
