import axios from "axios";
import React, {useEffect, useState} from "react";
import {Form} from "react-bootstrap";
import NumberFormat from 'react-number-format';
import {Placeholder} from "rsuite";
import Swal from "sweetalert2";


const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;
export const FormAskepPemeriksaanFisik = (data) => {
    const [datas, setDatas] = useState(data?.data);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        async function setData() {
            await setDatas(data?.data);
            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);
        }

        setData();
    }, [data])

    const setOnChange = async (key, val) => {
        if (key === 'bb_pre_hd' || key === 'bb_post_hd_yang_lalu' || key === 'bb_kering' || key === 'bb_post_hd') {
            if (parseInt(val) >= 1000) {
                await setDatas({...datas});
            } else {
                await setDatas({...datas, [key]: val});
            }
        } else if (key === "kesadaran") {
            const kesadaran = [];
            if (datas && typeof datas.kesadaran !== "undefined" && datas.kesadaran instanceof Array) {
                for (let i = 0; i < datas.kesadaran.length; i++) {
                    await kesadaran.push(datas.kesadaran[i]);
                }
            }

            const index = kesadaran.indexOf(val);
            if (index > -1) {
                await kesadaran.splice(index, 1);
            } else {
                await kesadaran.push(val);
            }
            await setDatas({...datas, kesadaran: kesadaran});
        } else {
            await setDatas({...datas, [key]: val});
        }
    };

    const handleSubmit = async (e) => {
        const {id, created_at, updated_at, ...params} = datas;
        if (typeof params.treatment_id === "undefined" || params.treatment_id === "") params.treatment_id = sessionStorage.getItem('idPasienHDPedia');
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            await axios.post(API_URL + '/dokter/add_pemeriksaan_fisik', params, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            return Swal.fire({
                title: 'Informasi',
                text: "Success add pemeriksaan fisik",
                icon: 'success',
                confirmButtonText: 'Ok',
                allowOutsideClick: true
            })
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    const {Paragraph} = Placeholder;
    return (
        <div>
            {isLoading ? (
                <div className="card shadow-lg">
                    <div className="card-body">
                        <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                   style={{marginTop: 30}}/>
                    </div>
                </div>

            ) : (
                <Form>
                    <h3>Form Askep</h3>
                    <table className="table table-condensed table-form-askep">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="12" align="center">
                                Pemeriksaan Fisik
                            </td>
                        </tr>
                        <tr>
                            <td width="10%"><strong>Tekanan Darah (mmHg)</strong></td>
                            <td width="1%">:</td>
                            <td width="14%">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="tekanan_darah"
                                    style={{width: "90%"}}
                                    type="text"
                                    value={datas ? datas.tekanan_darah : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Tekanan Darah(mmHg)"/>
                            </td>
                            <td width="5%"><strong>MAP</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="map"
                                    style={{width: "90%"}}
                                    type="text"
                                    value={datas ? datas.map : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="MAP"/>
                            </td>

                            <td width="6%"><strong>Ekstremitas</strong></td>
                            <td width="1%">:</td>
                            <td width="16%">
                                <Form.Control
                                    style={{width: "90%"}}
                                    size="sm"
                                    autoComplete="off"
                                    name="ekstremitas"
                                    as="select"
                                    value={datas ? datas.ekstremitas : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                >
                                    <option value="">- Pilih -</option>
                                    <option value="Tidak Edema / tidak Dehidrasi">Tidak Edema / tidak Dehidrasi</option>
                                    <option value="Dehidrasi">Dehidrasi</option>
                                    <option value="Edema">Edema</option>
                                    <option value="Edema Anasarka">Edema Anasarka</option>
                                    <option value="Pucat & Dingin">Pucat & Dingin</option>
                                </Form.Control>
                            </td>


                            <td width="9%"><strong>Keadaan Umum</strong></td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="keadaan_umum"
                                    as="select"
                                    value={datas ? datas.keadaan_umum : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                >
                                    <option value="">- Pilih -</option>
                                    <option value="Baik">Baik</option>
                                    <option value="Sedang">Sedang</option>
                                    <option value="Buruk">Buruk</option>

                                </Form.Control>
                            </td>
                        </tr>


                        <tr>
                            <td><strong>Nadi</strong></td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Group className="mb-3" controlId="nadi">
                                    <Form.Control
                                        style={{width: "90%"}}
                                        size="sm"
                                        autoComplete="off"
                                        name="nadi"
                                        as="select"
                                        value={datas ? datas.nadi : ''}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                    >
                                        <option value="">- Pilih -</option>
                                        <option value="Reguler">Reguler</option>
                                        <option value="Ireguler">Ireguler</option>
                                    </Form.Control>
                                    <Form.Control
                                        size="sm"
                                        style={{width: "90%", marginTop: "5px"}}
                                        autoComplete="off"
                                        name="frekuensi_nadi"
                                        type="text"
                                        maxLength={3}
                                        value={datas ? datas.frekuensi_nadi : ''}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                        placeholder="Frekuensi (x/mnt)"/>
                                </Form.Group>

                            </td>
                            <td width="6%"><strong>Konjungtiva</strong></td>
                            <td width="1%">:</td>
                            <td width="18%">
                                <Form.Group className="mb-3" controlId="koningtiya">
                                    <Form.Control
                                        style={{width: "90%"}}
                                        size="sm"
                                        autoComplete="off"
                                        name="koningtiya"
                                        as="select"
                                        value={datas ? datas.koningtiya : ''}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                    >
                                        <option value="">- Pilih -</option>
                                        <option value="Tidak Anemis">Tidak Anemis</option>
                                        <option value="Anemis">Anemis</option>
                                        <option value="Lain lain">Lain lain</option>
                                    </Form.Control>
                                    <Form.Control
                                        size="sm"
                                        style={{width: "90%", marginTop: "5px"}}
                                        autoComplete="off"
                                        name="keterangan"
                                        type="text"
                                        value={datas ? datas.keterangan : ''}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                        placeholder="Keterangan"/>
                                </Form.Group>

                            </td>
                            <td width="6%"><strong>Respirasi</strong></td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Group className="mb-3" controlId="respirasi">
                                    <Form.Control
                                        style={{width: "90%"}}
                                        size="sm"
                                        autoComplete="off"
                                        name="respirasi"
                                        as="select"
                                        value={datas ? datas.respirasi : ''}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                    >
                                        <option value="">- Pilih -</option>
                                        <option value="Normal">Normal</option>
                                        <option value="Kusmaul">Kusmaul</option>
                                        <option value="Dispnea">Dispnea</option>
                                        <option value="Edema Paru / Ronchi">Edema Paru / Ronchi</option>
                                    </Form.Control>
                                    <Form.Control
                                        size="sm"
                                        style={{width: "90%", marginTop: "5px"}}
                                        autoComplete="off"
                                        name="frekuensi_respirasi"
                                        type="text"
                                        maxLength={3}
                                        value={datas ? datas.frekuensi_respirasi : ''}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                        placeholder="(x/mnt)"/>
                                </Form.Group>

                            </td>

                            <td><strong>TD Post HD yang lalu (mmHg)</strong></td>
                            <td width="1%">:</td>

                            <td>
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="td_post_hd_yang_lalu"
                                    id="td_post_hd_yang_lalu"
                                    type="text"
                                    value={datas ? datas.td_post_hd_yang_lalu : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="TD Post HD yang lalu"/>
                            </td>
                        </tr>

                        <tr>
                            <td><strong>Berat Badan</strong></td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Group className="mb-3" controlId="bb_pre_hd">
                                    <Form.Label>Pre HD(Kg)</Form.Label>

                                    <NumberFormat
                                        name="bb_pre_hd"
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                        className="form-control form-control-sm"
                                        value={datas ? datas.bb_pre_hd : ''}
                                        thousandSeparator={false}
                                        decimalScale={2}
                                        inputMode="numeric"
                                        autoComplete="off"
                                        id="bb_pre_hd"
                                        decimalSeparator=","
                                        placeholder="BB Pre HD(Kg)"/>


                                </Form.Group>
                            </td>
                            <td width="25%" colSpan="3">
                                <Form.Group className="mb-3" controlId="bb_post_hd_yang_lalu">
                                    <Form.Label>BB Post HD yang lalu(Kg)</Form.Label>
                                    <NumberFormat
                                        name="bb_post_hd_yang_lalu"
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                        className="form-control form-control-sm"
                                        value={datas ? datas.bb_post_hd_yang_lalu : ''}
                                        thousandSeparator={false}
                                        decimalScale={2}
                                        inputMode="numeric"
                                        autoComplete="off"
                                        id="bb_post_hd_yang_lalu"
                                        decimalSeparator=","
                                        placeholder="BB Post HD yang lalu(Kg)"/>

                                </Form.Group>
                            </td>
                            <td width="25%" colSpan="3">
                                <Form.Group className="mb-3" controlId="bb_kering">
                                    <Form.Label>BB Kering(Kg)</Form.Label>

                                    <NumberFormat
                                        name="bb_kering"
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                        className="form-control form-control-sm"
                                        value={datas ? datas.bb_kering : ''}
                                        thousandSeparator={false}
                                        decimalScale={2}
                                        inputMode="numeric"
                                        autoComplete="off"
                                        id="bb_kering"
                                        decimalSeparator=","
                                        placeholder="BB Kering(Kg)"/>


                                </Form.Group>
                            </td>
                            <td width="25%" colSpan="3">
                                <Form.Group className="mb-3" controlId="bb_post_hd">
                                    <Form.Label>BB Post HD(Kg)</Form.Label>

                                    <NumberFormat
                                        name="bb_post_hd"
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                        className="form-control form-control-sm"
                                        value={datas ? datas.bb_post_hd : ''}
                                        thousandSeparator={false}
                                        decimalScale={2}
                                        inputMode="numeric"
                                        autoComplete="off"
                                        id="bb_post_hd"
                                        decimalSeparator=","
                                        placeholder="BB Post HD(Kg)"/>


                                </Form.Group>
                            </td>

                        </tr>
                        <tr>
                            <td width="6%"><strong>Kesadaran</strong></td>
                            <td width="1%">:</td>
                            <td width="18%">
                                <Form.Group className="mb-3" controlId="kesadaran1">
                                    <Form.Check
                                        id="kesadaran1"
                                        name="kesadaran1"
                                        className="mb-2"
                                        value="Compos Mentis"
                                        type="checkbox"
                                        checked={datas ? datas.kesadaran?.includes("Compos Mentis") : false}
                                        onChange={e => setOnChange("kesadaran", "Compos Mentis")}
                                        label="Compos Mentis"/>
                                    <Form.Check
                                        id="kesadaran2"
                                        name="kesadaran2"
                                        className="mb-2"
                                        value="Apatis"
                                        type="checkbox"
                                        checked={datas ? datas.kesadaran?.includes("Apatis") : ''}
                                        onChange={e => setOnChange("kesadaran", "Apatis")}
                                        label="Apatis"/>
                                    <Form.Check
                                        id="kesadaran3"
                                        name="kesadaran3"
                                        className="mb-2"
                                        value="Delirium"
                                        type="checkbox"
                                        checked={datas ? datas.kesadaran?.includes("Delirium") : ''}
                                        onChange={e => setOnChange("kesadaran", "Delirium")}
                                        label="Delirium"/>
                                    <Form.Check
                                        id="kesadaran4"
                                        name="kesadaran4"
                                        value="Somnolen"
                                        type="checkbox"
                                        className="mb-2"
                                        checked={datas ? datas.kesadaran?.includes("Somnolen") : ''}
                                        onChange={e => setOnChange("kesadaran", "Somnolen")}
                                        label="Somnolen"/>
                                    <Form.Check
                                        id="kesadaran5"
                                        name="kesadaran5"
                                        className="mb-2"
                                        value="Sopor"
                                        type="checkbox"
                                        checked={datas ? datas.kesadaran?.includes("Sopor") : ''}
                                        onChange={e => setOnChange("kesadaran", "Sopor")}
                                        label="Sopor"/>
                                    <Form.Check
                                        id="kesadaran6"
                                        name="kesadaran6"
                                        value="Coma"
                                        type="checkbox"
                                        checked={datas ? datas.kesadaran?.includes("Coma") : ''}
                                        onChange={e => setOnChange("kesadaran", "Coma")}
                                        label="Coma"/>
                                </Form.Group>


                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>


                    <button
                        id="submitFormAskepPemeriksaanFisik"
                        type="button" onClick={(e) => handleSubmit()}
                        className="btn btn-success btn-sm">Submit Form Pemeriksaan Fisik
                    </button>
                </Form>
            )}
        </div>
    )
}
