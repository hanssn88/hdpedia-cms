import axios from "axios";
import moment from 'moment';
import "moment/locale/id";
import React, {useEffect, useState} from "react";
import {Col, Form, Row} from "react-bootstrap";
import Datetime from "react-datetime";
import "react-datetime/css/react-datetime.css";
import {Placeholder} from "rsuite";
import Swal from 'sweetalert2';


const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

export const FormAskepPemeriksaanPenunjang = (data) => {
    const [datas, setDatas] = useState(data?.data);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        async function setData() {

            await setDatas(data?.data);
            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);
        }

        setData();
    }, [data])

    const setOnChange = async (key, val) => {
        if (key === 'tanggal') val = moment(new Date(val)).format('YYYY-MM-DD');
        await setDatas({...datas, [key]: val});
    };

    const handleSubmit = async (e) => {
        const {id, created_at, updated_at, ...params} = datas;
        if (typeof params.treatment_id === "undefined" || params.treatment_id === "") params.treatment_id = sessionStorage.getItem('idPasienHDPedia');
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            await axios.post(API_URL + '/dokter/add_pemeriksaan_penunjang', params, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            return Swal.fire({
                title: 'Informasi',
                text: "Success add pemeriksaan penunjang",
                icon: 'success',
                confirmButtonText: 'Ok',
                allowOutsideClick: true
            })
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    const {Paragraph} = Placeholder;
    return (
        <div>
            {isLoading ? (
                <div className="card shadow-lg">
                    <div className="card-body">
                        <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                   style={{marginTop: 30}}/>
                    </div>
                </div>

            ) : (
                <Form>
                    <h3>Form Askep</h3>
                    <table className="table table-condensed table-form-askep">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="9" align="center">
                                Pemeriksaan Penunjang
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="9">
                                <Form.Group as={Row} className="mb-3" controlId="pemeriksaan_penunjang">
                                    <Form.Label column sm={2}>
                                        Pemeriksaan Penunjang
                                    </Form.Label>
                                    <Col sm={10}>
                                        <Form.Check
                                            type="radio"
                                            name="pemeriksaan_penunjang"
                                            id="pemeriksaan_penunjang"
                                            value="Lab"
                                            label="Lab"
                                            checked={datas?.pemeriksaan_penunjang === "Lab"}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                        <Form.Check
                                            type="radio"
                                            name="pemeriksaan_penunjang"
                                            id="pemeriksaan_penunjang1"
                                            value="Ro"
                                            label="Ro"
                                            checked={datas?.pemeriksaan_penunjang === "Ro"}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                        <Form.Check
                                            type="radio"
                                            name="pemeriksaan_penunjang"
                                            id="pemeriksaan_penunjang2"
                                            value="Lain-lain"
                                            label="Lain-lain"
                                            checked={datas?.pemeriksaan_penunjang === "Lain-lain"}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}/>

                                    </Col>
                                </Form.Group>

                            </td>
                        </tr>
                        <tr>
                            <td colSpan="9" className="td-valign-top">
                                <strong>Gizi (Dikaji Tiap 3 bulan sekali / diulangi jika dianggap terjadi perubahan
                                    asupan
                                    gizi)</strong>
                            </td>

                        </tr>


                        <tr>
                            <td width="6%">
                                <strong>Tanggal</strong>
                            </td>
                            <td width="1%">:</td>
                            <td width="18%">
                                <Datetime
                                    setViewDate={datas && datas.tanggal ? new Date(datas.tanggal) : new Date()}
                                    value={datas && datas.tanggal ? new Date(datas.tanggal) : new Date()}
                                    onChange={e => setOnChange("tanggal", e)}
                                    timeFormat={false}
                                    closeOnSelect={true}
                                    inputProps={{
                                        value: datas && datas.tanggal ? moment(new Date(datas.tanggal)).format('DD-MM-YYYY') : '',
                                        readOnly: true,
                                        autoComplete: "off",
                                        placeholder: 'Tanggal',
                                        name: 'tanggal',
                                        className: 'form-control form-control-sm date-askep'
                                    }}
                                />
                            </td>
                            <td width="7%"><strong>MNA, Score Total</strong>
                            </td>
                            <td width="1%">:</td>
                            <td width="19%">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="mis_score_total"
                                    style={{width: "90%"}}
                                    type="text"
                                    maxLength={3}
                                    value={datas?.mis_score_total}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="MIS, Score Total"/>
                            </td>

                            <td width="6%"><strong>Rekomendasi</strong>

                            </td>
                            <td width="1%">:</td>

                            <td width="18%">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="rekomendasi"
                                    type="text"
                                    value={datas?.rekomendasi}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Rekomendasi"/>
                            </td>

                        </tr>

                        <tr>
                            <td colSpan="9">
                                <strong>Kesimpulan</strong>
                                <Form.Control
                                    size="sm"
                                    style={{marginTop: "5px"}}
                                    autoComplete="off"
                                    name="kesimpulan"
                                    as="select"
                                    value={datas?.kesimpulan}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}>
                                    <option value="">- Pilih -</option>
                                    <option value="Tanpa Malnutrisi < 6">Tanpa malnutrisi &#60; 6</option>
                                    <option value="Malnutrisi > 6">Malnutrisi &#62; 6</option>

                                </Form.Control>

                            </td>


                        </tr>

                        </tbody>
                    </table>

                    <button
                        type="button"
                        id="submitFormPemeriksaanPenunjang"
                        onClick={(e) => handleSubmit()}
                        className="btn btn-success btn-sm">Submit Form Pemeriksaan Penunjang
                    </button>
                </Form>
            )}
        </div>
    )
}
