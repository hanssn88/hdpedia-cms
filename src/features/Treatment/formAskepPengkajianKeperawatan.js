import axios from "axios";
import moment from 'moment';
import "moment/locale/id";
import React, {useEffect, useState} from "react";
import {Form} from "react-bootstrap";
import Datetime from "react-datetime";
import "react-datetime/css/react-datetime.css";
import {Placeholder} from "rsuite";
import Swal from 'sweetalert2';

const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

/*const today = moment();
const disableFutureDt = (current) => {
    return current.isBefore(today)
}*/

export const FormAskepPengkajianKeperawatan = (data) => {
    const [datas, setDatas] = useState(data?.data);
    const [datt, setDatt] = useState(data?.data);
    const [isLoading, setIsLoading] = useState(true);
    /*const [validSd, setValidSd] = useState(disableFutureDt);*/

    useEffect(() => {
        async function setData() {

            await setDatas(data?.data);
            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);

        }

        async function setDatts() {

            await setDatt(data?.data);


        }


        setData();
        setDatts();

    }, [data])

    const setOnChange = async (key, val) => {
        if (key === 'tanggal') val = moment(new Date(val)).format('YYYY-MM-DD');
        if (key === 'ntgl') val = moment(new Date(val)).format('YYYY-MM-DD');
        if (key === "keluhan_utama") {
            const keluhan_utama = [];

            if (datas && typeof datas.keluhan_utama !== "undefined" && datas.keluhan_utama instanceof Array) {
                for (let i = 0; i < datas.keluhan_utama.length; i++) {
                    await keluhan_utama.push(datas.keluhan_utama[i]);
                }
            }

            const index = keluhan_utama.indexOf(val);
            if (index > -1) {
                await keluhan_utama.splice(index, 1);
            } else {
                await keluhan_utama.push(val);
            }
            await setDatas({...datas, keluhan_utama: keluhan_utama});
        } else {
            await setDatas({...datas, [key]: val});
        }
    };

    const handleSubmit = async (e) => {
        const {id, created_at, updated_at, ...params} = datas;
        if (typeof params.treatment_id === "undefined" || params.treatment_id === "")
            params.treatment_id = sessionStorage.getItem('idPasienHDPedia');
        if (params.cara_bayar == "BPJS PBI" || params.cara_bayar == "BPJS Non PBI") {
            params.cara_bayar = params.cara_bayar;
        } else {
            params.cara_bayar = params.cara_bayar_lainnya;
        }
        // if(params.cara_bayar != "BPJS PBI" ||  params.cara_bayar != "BPJS Non PBI"){
        //     params.cara_bayar = params.cara_bayar_lainnya;
        // }else{
        //     params.cara_bayar = params.cara_bayar;
        // }

        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            await axios.post(API_URL + '/dokter/add_pengkajian_keperawatan', params, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            return Swal.fire({
                title: 'Informasi',
                text: "Success add pengkajian keperawatan",
                icon: 'success',
                confirmButtonText: 'Ok',
                allowOutsideClick: true
            })
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    const {Paragraph} = Placeholder;
    return (

        <div>
            {isLoading ? (
                <div className="card shadow-lg">
                    <div className="card-body">
                        <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                   style={{marginTop: 30}}/>
                    </div>
                </div>

            ) : (
                <Form>
                    <h3>Form Askep</h3>
                    <table className="table table-condensed table-form-askep">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="12" align="center">
                                Pengkajian Keperawatan
                            </td>
                        </tr>
                        <tr>
                            <td width="6%">
                                <strong>Tanggal</strong>
                            </td>
                            <td width="1%">:</td>
                            <td width="18%">
                                <Datetime
                                    setViewDate={datas && datas.tanggal ? new Date(datas.tanggal) : new Date()}
                                    value={datas && datas.tanggal ? new Date(datas.tanggal) : new Date()}
                                    onChange={e => setOnChange("tanggal", e)}
                                    timeFormat={false}
                                    closeOnSelect={true}
                                    inputProps={{
                                        value: datas && datas.tanggal ? moment(new Date(datas.tanggal)).format('DD-MM-YYYY') : '',
                                        readOnly: true,
                                        autoComplete: "off",
                                        placeholder: 'Tanggal',
                                        name: 'tanggal',
                                        className: 'form-control form-control-sm date-askep'
                                    }}
                                />

                            </td>
                            <td width="9%"><strong>Jam</strong>
                            </td>
                            <td width="1%">:</td>
                            <td width="15%">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="jam"
                                    id="jam"
                                    style={{width: "90%"}}
                                    type="text"
                                    value={datas ? datas.jam : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Jam"/>
                            </td>

                            <td width="6%"><strong>Hemodialisi</strong></td>
                            <td width="1%">:</td>

                            <td width="13%">
                                <Form.Control
                                    size="sm"
                                    style={{width: "90%"}}
                                    autoComplete="off"
                                    name="hemodialisi_ke"
                                    id="hemodialisi_ke"
                                    type="text"
                                    value={datas ? datas.hemodialisi_ke : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Hemodialisi"/>
                            </td>
                            <td width="10%"><strong>Tipe Dializer</strong></td>
                            <td width="1%">:</td>
                            <td width="18%">
                                <Form.Control
                                    size="sm"

                                    autoComplete="off"
                                    name="tipe_dializer"
                                    id="tipe_dializer"
                                    type="text"
                                    value={datas ? datas.tipe_dializer : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Tipe Dializer"/>
                            </td>
                        </tr>

                        <tr>

                            <td width="5%"><strong>N Tanggal</strong></td>
                            <td width="1%">:</td>
                            <td width="19%">

                                <Datetime
                                    setViewDate={datas && datas.ntgl ? new Date(datas.ntgl) : new Date()}
                                    value={datas && datas.ntgl ? new Date(datas.ntgl) : new Date()}
                                    onChange={e => setOnChange("ntgl", e)}
                                    timeFormat={false}
                                    closeOnSelect={true}
                                    inputProps={{
                                        value: datas && datas.ntgl ? moment(new Date(datas.ntgl)).format('DD-MM-YYYY') : '',
                                        readOnly: true,
                                        autoComplete: "off",
                                        placeholder: 'N Tanggal',
                                        name: 'ntgl',
                                        className: 'form-control form-control-sm date-askep'
                                    }}
                                />


                            </td>
                            <td width="5%"><strong>Dx Medis</strong></td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    id="dx_medis"
                                    name="dx_medis"
                                    style={{width: "90%"}}
                                    type="text"
                                    value={datas ? datas.dx_medis : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Dx Medis"/>
                            </td>
                            <td width="5%"><strong>R Ke</strong></td>
                            <td width="1%">:</td>

                            <td>
                                <Form.Control
                                    size="sm"
                                    style={{width: "90%"}}
                                    autoComplete="off"
                                    id="r_ke"
                                    name="r_ke"
                                    type="text"
                                    value={datas ? datas.r_ke : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="R Ke"/>
                            </td>

                            <td width="6%"><strong>Riwayat Alergi Obat</strong></td>
                            <td width="1%">:</td>
                            <td width="18%">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="riwayat_alergi_obat"
                                    id="riwayat_alergi_obat"
                                    as="select"
                                    value={datas ? datas.riwayat_alergi_obat : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                >
                                    <option value="">- Pilih -</option>
                                    <option value="Ya">Ya</option>
                                    <option value="Tidak">Tidak</option>

                                </Form.Control>
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="2"><strong>Frekuensi HD : (Minggu)</strong></td>

                            <td width="18%">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="frekuensi_hd"
                                    id="frekuensi_hd"
                                    style={{width: "90%"}}
                                    type="text"
                                    value={datas ? datas.frekuensi_hd : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Frekuensi HD (Minggu)"/>
                            </td>
                            <td width="8%">
                                {datas && parseInt(datas.nyeri) >= 1 && parseInt(datas.nyeri) <= 3 ?
                                    <strong>NRS(Ringan)</strong> :
                                    datas && parseInt(datas.nyeri) >= 4 && parseInt(datas.nyeri) <= 6 ?
                                        <strong>NRS(Sedang)</strong> :
                                        datas && parseInt(datas.nyeri) >= 7 && parseInt(datas.nyeri) <= 10 ?
                                            <strong>NRS(Berat)</strong> : <strong>NRS</strong>}

                            </td>
                            <td width="1%">:</td>
                            <td width="16%">
                                <br/>
                                <Form.Control
                                    size="sm"
                                    style={{width: "90%"}}
                                    autoComplete="off"
                                    id="nyeri"
                                    name="nyeri"
                                    as="select"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    value={datas ? datas.nyeri : ''}
                                >
                                    <option value="">- Pilih -</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>

                                </Form.Control>

                                <Form.Group className="mt-2" controlId="tingkat">
                                    <Form.Check
                                        type="radio"
                                        name="tingkat"
                                        id="tingkat"
                                        value="Akut"
                                        label="Akut"
                                        checked={String(datas?.tingkat) === "Akut"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                    <Form.Check
                                        type="radio"
                                        name="tingkat"
                                        id="tingkat1"
                                        value="Kronik"
                                        label="Kronik"
                                        checked={String(datas?.tingkat) === "Kronik"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                </Form.Group>
                            </td>
                            <td width="5%"><strong>Pekerjaan</strong></td>
                            <td width="1%">:</td>
                            <td>

                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    id="pekerjaan"
                                    name="pekerjaan"
                                    style={{width: "90%"}}
                                    type="text"
                                    value={datas ? datas.pekerjaan : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Pekerjaan"/>
                            </td>
                            <td width="5%"><strong>Cara Bayar</strong>
                            </td>
                            <td width="1%">:</td>
                            <td width="19%">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="cara_bayar"
                                    id="cara_bayar"
                                    as="select"
                                    value={datas ? datas.cara_bayar : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                >
                                    <option value="">- Pilih -</option>
                                    <option value="BPJS PBI">BPJS PBI</option>
                                    <option value="BPJS Non PBI">BPJS Non PBI</option>
                                    <option
                                        value={datas && datas.cara_bayar != "BPJS PBI" && datas.cara_bayar != "BPJS Non PBI" ? datas.cara_bayar : "Lainnya"}>Lainnya
                                    </option>

                                </Form.Control>
                                {datas && (datas.cara_bayar != "BPJS PBI" && datas.cara_bayar != "BPJS Non PBI") || (datas.cara_bayar == "Lainnya") ? (
                                    <Form.Control
                                        size="sm"
                                        autoComplete="off"
                                        id="cara_bayar_lainnya"
                                        name="cara_bayar_lainnya"
                                        style={{marginTop: 5}}
                                        type="text"
                                        value={datas && (datas.cara_bayar != "BPJS PBI" && datas.cara_bayar != "BPJS Non PBI") ? datas.cara_bayar == "Lainnya" ? datas.cara_bayar_lainnya : datt.cara_bayar : ''}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                        placeholder="Cara Bayar Lainnya"/>
                                ) : ''}

                            </td>


                        </tr>
                        <tr>
                            <td><strong>Keluhan Utama</strong></td>
                            <td>:</td>
                            <td colSpan="10">
                                <Form.Group className="mb-3" controlId="keluhan_utama">
                                    <Form.Check
                                        id="keluhan_utama1"
                                        name="keluhan_utama1"
                                        className="mb-2"
                                        value="Sesak"
                                        type="checkbox"
                                        checked={datas ? datas.keluhan_utama?.includes("Sesak") : false}
                                        onChange={e => setOnChange("keluhan_utama", "Sesak")}
                                        label="Sesak"/>
                                    <Form.Check
                                        id="keluhan_utama2"
                                        name="keluhan_utama2"
                                        className="mb-2"
                                        value="Mual Muntah"
                                        type="checkbox"
                                        checked={datas ? datas.keluhan_utama?.includes("Mual Muntah") : ''}
                                        onChange={e => setOnChange("keluhan_utama", "Mual Muntah")}
                                        label="Mual Muntah"/>
                                    <Form.Check
                                        id="keluhan_utama3"
                                        name="keluhan_utama3"
                                        className="mb-2"
                                        value="Gatal"
                                        type="checkbox"
                                        checked={datas ? datas.keluhan_utama?.includes("Gatal") : ''}
                                        onChange={e => setOnChange("keluhan_utama", "Gatal")}
                                        label="Gatal"/>
                                    <Form.Check
                                        id="keluhan_utama4"
                                        name="keluhan_utama4"
                                        value="Lainnya"
                                        type="checkbox"
                                        checked={datas ? datas.keluhan_utama?.includes("Lainnya") : ''}
                                        onChange={e => setOnChange("keluhan_utama", "Lainnya")}
                                        label="Lainnya"/>
                                </Form.Group>

                            </td>

                        </tr>
                        <tr>
                            <td><strong>Keterangan</strong></td>
                            <td width="1%">:</td>

                            <td colSpan="10">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    id="keterangan"
                                    name="keterangan"
                                    type="text"
                                    value={datas ? datas.keterangan : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Keterangan"/>
                            </td>
                        </tr>

                        </tbody>
                    </table>


                    <button
                        id="submitFormPengkajianKeperwatan"
                        type="button" onClick={(e) => handleSubmit()}
                        className="btn btn-success btn-sm">Submit Form Pengkajian Keperawatan
                    </button>
                </Form>
            )}


        </div>
    )
}
