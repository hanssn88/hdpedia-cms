import React, {useEffect, useState} from "react";
import {Col, Form, Row} from "react-bootstrap";
import axios from "axios";
import Swal from "sweetalert2";
import {Placeholder} from "rsuite";

const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

export const FormAskepResikoJatuh = (data) => {

    const [datas, setDatas] = useState(data?.data);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        async function setData() {
            await setDatas(data?.data);
            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);
        }

        setData();
    }, [data])

    const setOnChange = async (key, val) => {
        await setDatas({...datas, [key]: val});
        if (key === 'skor_riwayat' || key === 'skor_diagnosis' || key === 'skor_alat_bantu_jalan' || key === 'skor_memakai_terapi_heparin' || key === 'skor_cara_berjalan' || key === 'skor_status_mental') {
            const totalSkor = await calculateSkor(key, val);
            console.log(totalSkor);
            if (totalSkor > 45) {
                await setDatas({...datas, [key]: val, total_skor: totalSkor, kesimpulan: ">45 (risiko tinggi)"});
            } else if (totalSkor >= 24 && totalSkor <= 25) {
                await setDatas({...datas, [key]: val, total_skor: totalSkor, kesimpulan: "24-25 (risiko sedang)"});
            } else if (totalSkor >= 0 && totalSkor < 24) {
                await setDatas({...datas, [key]: val, total_skor: totalSkor, kesimpulan: "0-24 (tidak berisiko)"});
            } else {
                await setDatas({...datas, [key]: val, total_skor: totalSkor, kesimpulan: ""});
            }
        } else {
            await setDatas({...datas, [key]: val});
        }
    };

    const calculateSkor = async (key, val) => {
        const {
            skor_riwayat,
            skor_diagnosis,
            skor_alat_bantu_jalan,
            skor_memakai_terapi_heparin,
            skor_cara_berjalan,
            skor_status_mental
        } = datas;
        let totalSkor = 0;
        let skorRiwayat = skor_riwayat ? Number(skor_riwayat) : 0;
        let skorDiagnosis = skor_diagnosis ? Number(skor_diagnosis) : 0;
        let skorAlatBantuJalan = skor_alat_bantu_jalan ? Number(skor_alat_bantu_jalan) : 0;
        let skorMemakaiTerapiHeparin = skor_memakai_terapi_heparin ? Number(skor_memakai_terapi_heparin) : 0;
        let skorCaraBerjalan = skor_cara_berjalan ? Number(skor_cara_berjalan) : 0;
        let skorStatusMental = skor_status_mental ? Number(skor_status_mental) : 0;

        if (key === 'skor_riwayat') {
            skorRiwayat = Number(val);
        }
        if (key === 'skor_diagnosis') {
            skorDiagnosis = Number(val);
        }
        if (key === 'skor_alat_bantu_jalan') {
            skorAlatBantuJalan = Number(val);
        }
        if (key === 'skor_memakai_terapi_heparin') {
            skorMemakaiTerapiHeparin = Number(val);
        }

        if (key === 'skor_cara_berjalan') {
            skorCaraBerjalan = Number(val);
        }
        if (key === 'skor_status_mental') {
            skorStatusMental = Number(val);
        }
        totalSkor = totalSkor + skorRiwayat + skorDiagnosis + skorAlatBantuJalan + skorMemakaiTerapiHeparin + skorCaraBerjalan + skorStatusMental;
        return totalSkor;
    };

    const handleSubmit = async (e) => {
        const {id, created_at, updated_at, ...params} = datas;
        if (typeof params.treatment_id === "undefined" || params.treatment_id === "") params.treatment_id = sessionStorage.getItem('idPasienHDPedia');
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            await axios.post(API_URL + '/dokter/add_resiko_jatuh', params, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            return Swal.fire({
                title: 'Informasi',
                text: "Success add resiko jatuh",
                icon: 'success',
                confirmButtonText: 'Ok',
                allowOutsideClick: true
            })
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    const {Paragraph} = Placeholder;
    return (
        <div>
            {isLoading ? (
                <div className="card shadow-lg">
                    <div className="card-body">
                        <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                   style={{marginTop: 30}}/>
                    </div>
                </div>

            ) : (
                <Form>
                    <h3>Form Askep</h3>
                    <table className="table table-condensed table-form-askep">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="9" align="center">
                                Resiko Jatuh
                            </td>
                        </tr>
                        <tr>
                            <td width="15%" className="td-valign-top">
                                <strong>Riwayat jatuh yang baru atau dalam 3 bulan terakhir</strong>
                            </td>
                            <td width="1%" className="td-valign-top">:</td>
                            <td width="5%" className="td-valign-top">
                                <Form.Group className="mb-3" controlId="riwayat_jatuh">
                                    <Form.Check
                                        type="radio"
                                        name="riwayat_jatuh"
                                        id="riwayat_jatuh"
                                        value="Ya"
                                        label="Ya"
                                        checked={datas?.riwayat_jatuh === "Ya"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                    <Form.Check
                                        type="radio"
                                        name="riwayat_jatuh"
                                        id="riwayat_jatuh1"
                                        value="Tidak"
                                        label="Tidak"
                                        checked={datas?.riwayat_jatuh === "Tidak"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                </Form.Group>
                                <label>Skor</label>
                                <Form.Group className="mb-3" controlId="skor_riwayat">
                                    <Form.Check
                                        type="radio"
                                        name="skor_riwayat"
                                        id="skor_riwayat"
                                        value="0"
                                        label="0"
                                        checked={String(datas?.skor_riwayat) === "0"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                    <Form.Check
                                        type="radio"
                                        name="skor_riwayat"
                                        id="skor_riwayat1"
                                        value="25"
                                        label="25"
                                        checked={String(datas?.skor_riwayat) === "25"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                </Form.Group>
                            </td>

                            <td width="8%" className="td-valign-top">
                                <strong>Diagnosis medis sekunder >1</strong>
                            </td>
                            <td width="1%" className="td-valign-top">:</td>
                            <td width="12%" className="td-valign-top">
                                <Form.Group className="mb-3" controlId="diagnosis_medis_sekunder">
                                    <Form.Check
                                        type="radio"
                                        name="diagnosis_medis_sekunder"
                                        id="diagnosis_medis_sekunder"
                                        value="Ya"
                                        label="Ya"
                                        checked={datas?.diagnosis_medis_sekunder === "Ya"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                    <Form.Check
                                        type="radio"
                                        name="diagnosis_medis_sekunder"
                                        id="diagnosis_medis_sekunder1"
                                        value="Tidak"
                                        label="Tidak"
                                        checked={datas?.diagnosis_medis_sekunder === "Tidak"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                </Form.Group>
                                <label>Skor</label>
                                <Form.Group className="mb-3" controlId="skor_diagnosis">
                                    <Form.Check
                                        type="radio"
                                        name="skor_diagnosis"
                                        id="skor_diagnosis"
                                        value="0"
                                        label="0"
                                        checked={String(datas?.skor_diagnosis) === "0"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                    <Form.Check
                                        type="radio"
                                        name="skor_diagnosis"
                                        id="skor_diagnosis1"
                                        value="15"
                                        label="15"
                                        checked={String(datas?.skor_diagnosis) === "15"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                </Form.Group>
                            </td>

                            <td width="5%" className="td-valign-top">
                                <strong>Alat bantu jalan</strong>
                            </td>
                            <td width="1%" className="td-valign-top">:</td>
                            <td width="10%" className="td-valign-top">
                                <Form.Group className="mb-3" controlId="alat_bantu_jalan">
                                    <Form.Check
                                        type="radio"
                                        name="alat_bantu_jalan"
                                        id="alat_bantu_jalan"
                                        value="Bed Rest"
                                        label="Bed Rest"
                                        checked={datas?.alat_bantu_jalan === "Bed Rest"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                    <Form.Check
                                        type="radio"
                                        name="alat_bantu_jalan"
                                        id="alat_bantu_jalan1"
                                        value="Penopang Tongkat"
                                        label="Penopang Tongkat"
                                        checked={datas?.alat_bantu_jalan === "Penopang Tongkat"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                    <Form.Check
                                        type="radio"
                                        name="alat_bantu_jalan"
                                        id="alat_bantu_jalan2"
                                        value="Furnitur"
                                        label="Furnitur"
                                        checked={datas?.alat_bantu_jalan === "Furnitur"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                </Form.Group>
                                <label>Skor</label>
                                <Form.Group className="mb-3" controlId="skor_alat_bantu_jalan">
                                    <Form.Check
                                        type="radio"
                                        name="skor_alat_bantu_jalan"
                                        id="skor_alat_bantu_jalan"
                                        value="0"
                                        label="0"
                                        checked={String(datas?.skor_alat_bantu_jalan) === "0"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                    <Form.Check
                                        type="radio"
                                        name="skor_alat_bantu_jalan"
                                        id="skor_alat_bantu_jalan1"
                                        value="30"
                                        label="30"
                                        checked={String(datas?.skor_alat_bantu_jalan) === "30"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                    <Form.Check
                                        type="radio"
                                        name="skor_alat_bantu_jalan"
                                        id="skor_alat_bantu_jalan2"
                                        value="15"
                                        label="15"
                                        checked={String(datas?.skor_alat_bantu_jalan) === "15"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                </Form.Group>
                            </td>

                        </tr>
                        <tr>
                            <td width="15%" className="td-valign-top">
                                <strong>Memakai Terapi Heparin/Iv</strong>
                            </td>
                            <td width="1%" className="td-valign-top">:</td>
                            <td width="9%" className="td-valign-top">
                                <Form.Group className="mb-3" controlId="memakai_terapi_heparin">
                                    <Form.Check
                                        type="radio"
                                        name="memakai_terapi_heparin"
                                        id="memakai_terapi_heparin"
                                        value="Ya"
                                        label="Ya"
                                        checked={datas?.memakai_terapi_heparin === "Ya"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                    <Form.Check
                                        type="radio"
                                        name="memakai_terapi_heparin"
                                        id="memakai_terapi_heparin1"
                                        value="Tidak"
                                        label="Tidak"
                                        checked={datas?.memakai_terapi_heparin === "Tidak"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                </Form.Group>
                                <label>Skor</label>
                                <Form.Group className="mb-3" controlId="skor_memakai_terapi_heparin">
                                    <Form.Check
                                        type="radio"
                                        name="skor_memakai_terapi_heparin"
                                        id="skor_memakai_terapi_heparin"
                                        value="0"
                                        label="0"
                                        checked={String(datas?.skor_memakai_terapi_heparin) === "0"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                    <Form.Check
                                        type="radio"
                                        name="skor_memakai_terapi_heparin"
                                        id="skor_memakai_terapi_heparin1"
                                        value="20"
                                        label="20"
                                        checked={String(datas?.skor_memakai_terapi_heparin) === "20"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                </Form.Group>
                            </td>

                            <td width="8%" className="td-valign-top">
                                <strong>Cara berjalan/Berpindah</strong>
                            </td>
                            <td width="1%" className="td-valign-top">:</td>
                            <td width="9%" className="td-valign-top">
                                <Form.Group className="mb-3" controlId="cara_berjalan">
                                    <Form.Check
                                        type="radio"
                                        name="cara_berjalan"
                                        id="cara_berjalan"
                                        value="Normal bed rest/imobilisasi"
                                        label="Normal bed rest/imobilisasi"
                                        checked={datas?.cara_berjalan === "Normal bed rest/imobilisasi"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                    />
                                    <Form.Check
                                        type="radio"
                                        name="cara_berjalan"
                                        id="cara_berjalan1"
                                        value="Lemah"
                                        label="Lemah"
                                        checked={datas?.cara_berjalan === "Lemah"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                    />
                                    <Form.Check
                                        type="radio"
                                        name="cara_berjalan"
                                        id="cara_berjalan2"
                                        value="Terganggu"
                                        label="Terganggu"
                                        checked={datas?.cara_berjalan === "Terganggu"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                    />
                                </Form.Group>
                                <label>Skor</label>
                                <Form.Group className="mb-3" controlId="skor_cara_berjalan">
                                    <Form.Check
                                        type="radio"
                                        name="skor_cara_berjalan"
                                        id="skor_cara_berjalan"
                                        value="0"
                                        label="0"
                                        checked={String(datas?.skor_cara_berjalan) === "0"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                    <Form.Check
                                        type="radio"
                                        name="skor_cara_berjalan"
                                        id="skor_cara_berjalan1"
                                        value="10"
                                        label="10"
                                        checked={String(datas?.skor_cara_berjalan) === "10"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                    <Form.Check
                                        type="radio"
                                        name="skor_cara_berjalan"
                                        id="skor_cara_berjalan2"
                                        value="20"
                                        label="20"
                                        checked={String(datas?.skor_cara_berjalan) === "20"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                </Form.Group>
                            </td>

                            <td width="5%" className="td-valign-top">
                                <strong>Status Mental</strong>
                            </td>
                            <td width="1%" className="td-valign-top">:</td>
                            <td width="9%" className="td-valign-top">
                                <Form.Group className="mb-3" controlId="status_mental">
                                    <Form.Check
                                        type="radio"
                                        name="status_mental"
                                        id="status_mental"
                                        value="Orientasi sesuai kemampuan"
                                        label="Orientasi sesuai kemampuan"
                                        checked={datas?.status_mental === "Orientasi sesuai kemampuan"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                    <Form.Check
                                        type="radio"
                                        name="status_mental"
                                        id="status_mental1"
                                        value="Lupa keterbatasan"
                                        label="Lupa keterbatasan"
                                        checked={datas?.status_mental === "Lupa keterbatasan"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                </Form.Group>
                                <label>Skor</label>
                                <Form.Group className="mb-3" controlId="skor_status_mental">
                                    <Form.Check
                                        type="radio"
                                        name="skor_status_mental"
                                        id="skor_status_mental"
                                        value="0"
                                        label="0"
                                        checked={String(datas?.skor_status_mental) === "0"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                    <Form.Check
                                        type="radio"
                                        name="skor_status_mental"
                                        id="skor_status_mental1"
                                        value="15"
                                        label="15"
                                        checked={String(datas?.skor_status_mental) === "15"}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                </Form.Group>
                            </td>
                        </tr>

                        <tr>
                            <td colSpan="9">
                                <Form.Group as={Row} controlId="total_skor">
                                    <Form.Label column sm={1}>
                                        Total Skor
                                    </Form.Label>
                                    <Col sm={11}>
                                        <Form.Control
                                            readOnly
                                            size="sm"
                                            style={{width: "15%"}}
                                            autoComplete="off"
                                            name="total_skor"
                                            id="total_skor"
                                            type="text"
                                            value={datas?.total_skor}
                                            placeholder="Total Skor"
                                            onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                    </Col>
                                </Form.Group>

                            </td>
                        </tr>
                        <tr>
                            <td colSpan="9">
                                <Form.Group as={Row} controlId="kesimpulan">
                                    <Form.Label column sm={1}>
                                        Kesimpulan
                                    </Form.Label>
                                    <Col sm={11}>
                                        <Form.Check
                                            type="radio"
                                            name="kesimpulan"
                                            id="kesimpulan"
                                            value="0-24 (tidak berisiko)"
                                            label="0-24 (tidak berisiko)"
                                            checked={datas?.kesimpulan === "0-24 (tidak berisiko)"}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                        <Form.Check
                                            type="radio"
                                            name="kesimpulan"
                                            id="kesimpulan1"
                                            value="24-25 (risiko sedang)"
                                            label="24-25 (risiko sedang)"
                                            checked={datas?.kesimpulan === "24-25 (risiko sedang)"}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                        <Form.Check
                                            type="radio"
                                            name="kesimpulan"
                                            id="kesimpulan"
                                            value=">45 (risiko tinggi)"
                                            label=">45 (risiko tinggi)"
                                            checked={datas?.kesimpulan === ">45 (risiko tinggi)"}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}/>
                                    </Col>
                                </Form.Group>

                            </td>
                        </tr>


                        </tbody>
                    </table>


                    <button
                        type="button"
                        id="submitFormResikoJatuh"
                        onClick={(e) => handleSubmit()}
                        className="btn btn-success btn-sm">Submit Form Resiko Jatuh
                    </button>
                </Form>
            )}
        </div>
    )
}
