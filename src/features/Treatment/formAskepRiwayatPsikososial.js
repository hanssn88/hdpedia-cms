import React, {useEffect, useState} from "react";
import {Form} from 'react-bootstrap';
import axios from "axios";
import Swal from "sweetalert2";
import {Placeholder} from "rsuite";

const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;
export const FormAskepRiwayatPsikososial = (data) => {
    const [datas, setDatas] = useState(data?.data);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        async function setData() {

            await setDatas(data?.data);
            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);
        }

        setData();
    }, [data])

    const setOnChange = async (key, val) => {
        if (key === "kondisi_saat_ini") {
            const kondisi_saat_ini = [];
            if (datas && typeof datas.kondisi_saat_ini !== "undefined" && datas.kondisi_saat_ini instanceof Array) {
                for (let i = 0; i < datas.kondisi_saat_ini.length; i++) {
                    await kondisi_saat_ini.push(datas.kondisi_saat_ini[i]);
                }
            }

            const index = kondisi_saat_ini.indexOf(val);
            if (index > -1) {
                await kondisi_saat_ini.splice(index, 1);
            } else {
                await kondisi_saat_ini.push(val);
            }
            await setDatas({...datas, kondisi_saat_ini: kondisi_saat_ini});
        } else {
            await setDatas({...datas, [key]: val});
        }
    };

    const handleSubmit = async (e) => {
        const {id, created_at, updated_at, ...params} = datas;
        if (typeof params.treatment_id === "undefined" || params.treatment_id === "") params.treatment_id = sessionStorage.getItem('idPasienHDPedia');
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            await axios.post(API_URL + '/dokter/add_riwayat_psikososial', params, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            return Swal.fire({
                title: 'Informasi',
                text: "Success add riwayat psikososial",
                icon: 'success',
                confirmButtonText: 'Ok',
                allowOutsideClick: true
            })
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    const {Paragraph} = Placeholder;
    return (
        <div>
            {isLoading ? (
                <div className="card shadow-lg">
                    <div className="card-body">
                        <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                   style={{marginTop: 30}}/>
                    </div>
                </div>

            ) : (
                <Form>
                    <h3>Form Askep</h3>
                    <table className="table table-condensed table-form-askep">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="9" align="center">
                                Riwayat Psikososial
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="4">
                                <strong>a. Adakah keyakinan/tradisi/budaya yang berkaitan dengan pelayanan kesehatan
                                    yang
                                    akan diberikan</strong>
                            </td>
                            <td width="1%">:</td>
                            <td colSpan="4">
                                <Form.Control
                                    size="sm"
                                    style={{width: "30%"}}
                                    autoComplete="off"
                                    name="keyakinan_pelayanan_kesehatan"
                                    as="select"
                                    value={datas?.keyakinan_pelayanan_kesehatan}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}>
                                    <option value="">- Pilih -</option>
                                    <option value="Ya">Ya</option>
                                    <option value="Tidak">Tidak</option>

                                </Form.Control>
                            </td>

                        </tr>
                        <tr>
                            <td width="13%"><strong>b. Kendala Komunikasi</strong></td>
                            <td>:</td>
                            <td width="22%">
                                <Form.Control
                                    size="sm"
                                    style={{width: "90%"}}
                                    autoComplete="off"
                                    name="kendala_komunikasi"
                                    as="select"
                                    value={datas?.kendala_komunikasi}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}>
                                    <option value="">- Pilih -</option>
                                    <option value="Ada">Ada</option>
                                    <option value="Tidak Ada">Tidak Ada</option>

                                </Form.Control>
                            </td>
                            <td width="7%" style={{textAlign: "right"}}><strong>Jelaskan</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="4">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="penjelasan_kendala_komunikasi"
                                    type="text"
                                    value={datas?.penjelasan_kendala_komunikasi}
                                    placeholder="Jelaskan Kendala Komunikasi"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}/>
                            </td>
                        </tr>

                        <tr>

                            <td><strong>c. Yang merawat dirumah</strong></td>
                            <td width="1%">:</td>
                            <td>
                                <Form.Control
                                    size="sm"
                                    style={{width: "90%"}}
                                    autoComplete="off"
                                    name="yang_merawat_dirumah"
                                    as="select"
                                    value={datas?.yang_merawat_dirumah}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}>

                                    <option value="">- Pilih -</option>
                                    <option value="Ada">Ada</option>
                                    <option value="Tidak Ada">Tidak Ada</option>

                                </Form.Control>
                            </td>
                            <td style={{textAlign: "right"}}><strong>Jelaskan</strong></td>
                            <td width="1%">:</td>
                            <td colSpan="4">
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="penjelasan_yang_merawat_dirumah"
                                    type="text"
                                    value={datas?.penjelasan_yang_merawat_dirumah}
                                    placeholder="Penjelasan yang merawat di rumah"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}/>
                            </td>

                        </tr>

                        <tr>
                            <td><strong>d. Kondisi Saat ini</strong></td>
                            <td width="1%">:</td>

                            <td colSpan="4">
                                <Form.Group className="mt-3" controlId="kondisi_saat_ini">
                                    <Form.Check
                                        id="kondisi_saat_ini"
                                        name="kondisi_saat_ini"
                                        className="mb-2"
                                        value="Tenang"
                                        type="checkbox"
                                        checked={datas ? datas.kondisi_saat_ini?.includes("Tenang") : false}
                                        onChange={e => setOnChange("kondisi_saat_ini", "Tenang")}
                                        label="Tenang"/>
                                    <Form.Check
                                        id="kondisi_saat_ini2"
                                        name="kondisi_saat_ini2"
                                        className="mb-2"
                                        value="Gelisah"
                                        type="checkbox"
                                        checked={datas ? datas.kondisi_saat_ini?.includes("Gelisah") : ''}
                                        onChange={e => setOnChange("kondisi_saat_ini", "Gelisah")}
                                        label="Gelisah"/>
                                    <Form.Check
                                        id="kondisi_saat_ini3"
                                        name="kondisi_saat_ini3"
                                        className="mb-2"
                                        value="Takut terhadap tindakan"
                                        type="checkbox"
                                        checked={datas ? datas.kondisi_saat_ini?.includes("Takut Terhadap Tindakan") : ''}
                                        onChange={e => setOnChange("kondisi_saat_ini", "Takut Terhadap Tindakan")}
                                        label="Takut Terhadap Tindakan"/>
                                    <Form.Check
                                        id="kondisi_saat_ini4"
                                        name="kondisi_saat_ini4"
                                        className="mb-2"
                                        value="Marah"
                                        type="checkbox"
                                        checked={datas ? datas.kondisi_saat_ini?.includes("Marah") : ''}
                                        onChange={e => setOnChange("kondisi_saat_ini", "Marah")}
                                        label="Marah"/>
                                    <Form.Check
                                        id="kondisi_saat_ini5"
                                        name="kondisi_saat_ini5"
                                        className="mb-2"
                                        value="Mudah Tersinggung"
                                        type="checkbox"
                                        checked={datas ? datas.kondisi_saat_ini?.includes("Mudah Tersinggung") : ''}
                                        onChange={e => setOnChange("kondisi_saat_ini", "Mudah Tersinggung")}
                                        label="Mudah Tersinggung"/>
                                </Form.Group>

                            </td>


                        </tr>


                        </tbody>
                    </table>
                    <button
                        type="button"
                        id="submitFormRiwayatPsikososial"
                        onClick={(e) => handleSubmit()}
                        className="btn btn-success btn-sm">Submit Form Riwayat Psikososial
                    </button>
                </Form>
            )}
        </div>
    )
}
