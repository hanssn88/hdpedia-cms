import axios from "axios";
import React, {useEffect, useState} from "react";
import {Card, CardColumns, Col, Dropdown, Form, ListGroup} from "react-bootstrap";
import Button from "react-bootstrap/Button";
import NumberFormat from "react-number-format";
import {Placeholder} from "rsuite";
import Swal from 'sweetalert2';
import AppModal from "../../components/modal/MyModal";


const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

export const FormAskepTindakanKeperawatan = (data) => {
    const [datas, setDatas] = useState(data?.data?.dataTindakanKeperawatanHD);
    const [dataKeperawatan, setDataKeperawatan] = useState(data?.data?.dataTindakanKeperawatan);
    const [dataHD, setDataHD] = useState({});
    const [tipeHD, setTipeHD] = useState('');
    const [isLoading, setIsLoading] = useState(true);
    const [addForm, setAddForm] = useState(false);
    const [addFormPostHD, setAddFormPostHD] = useState(false);
    const [confirmDel, setConfirmDel] = useState(false);
    const [addFormTransfusi, setAddFormTransfusi] = useState(false);
    const [dataTransfusi, setDataTransfusi] = useState({});
    const [errMsg, setErrMsg] = useState({});


    useEffect(() => {

        async function setData() {


            await sessionStorage.setItem('reload211', false);
            await setDatas(data?.data.dataTindakanKeperawatanHD);
            await setDataKeperawatan(data?.data.dataTindakanKeperawatan);
            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);


        }

        setData();
    }, [data])


    const setOnChange = async (key, val) => {
        if (key === "penyulit_selama_hd") {
            const penyulit_selama_hd = [];
            if (dataKeperawatan && typeof dataKeperawatan.penyulit_selama_hd !== "undefined" && dataKeperawatan.penyulit_selama_hd instanceof Array) {
                for (let i = 0; i < dataKeperawatan.penyulit_selama_hd.length; i++) {
                    await penyulit_selama_hd.push(dataKeperawatan.penyulit_selama_hd[i]);
                }
            }
            const index = penyulit_selama_hd.indexOf(val);
            if (index > -1) {
                await penyulit_selama_hd.splice(index, 1);
            } else {
                await penyulit_selama_hd.push(val);
            }
            await setDataKeperawatan({...dataKeperawatan, penyulit_selama_hd: penyulit_selama_hd});
        } else {
            await setDataKeperawatan({...dataKeperawatan, [key]: val});
        }
    };

    const setOnChangeHD = async (key, val) => {
        await setDataHD({...dataHD, [key]: val});
    };

    const setOnChangeTransfusi = async (key, val) => {
        await setDataTransfusi({...dataTransfusi, [key]: val});
    };


    const handleSubmit = async (e) => {
        const {id, created_at, updated_at, ...params} = dataKeperawatan;

        if (typeof params.treatment_id === "undefined" || params.treatment_id === "") params.treatment_id = sessionStorage.getItem('idPasienHDPedia');
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            await axios.post(API_URL + '/dokter/add_tindakan_keperawatan', params, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            await sessionStorage.setItem('reload211', true);
            return Swal.fire({
                title: 'Informasi',
                text: "Success add tindakan keperawatan",
                icon: 'success',
                confirmButtonText: 'Ok',
                allowOutsideClick: true
            })

        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    const handleValidHD = async (e) => {


        const paramValid = {
            "jam": dataHD.jam === '' ? "Required" : "",
            "td_mmhg": dataHD.td_mmhg === '' ? "Required" : "",
            "nadi_x_menit": dataHD.nadi_x_menit === '' ? "Required" : "",
            "suhu": dataHD.suhu === '' ? "Required" : "",
            "rr_x_menit": dataHD.rr_x_menit === '' ? "Required" : "",
            "ufg_ml": dataHD.ufg_ml === '' ? "Required" : "",
            "ufr_ml_menit": dataHD.ufr_ml_menit === '' ? "Required" : "",
            "vp_mmhg": dataHD.vp_mmhg === '' ? "Required" : "",
            "tmp_mmhg": dataHD.tmp_mmhg === '' ? "Required" : "",
            "heparin_cc": dataHD.heparin_cc === '' ? "Required" : "",
            "ufr_ml_jam": dataHD.ufr_ml_jam === '' ? "Required" : "",
            "qb_ml_menit": dataHD.qb_ml_menit === '' ? "Required" : "",


        };

        await setErrMsg(paramValid);

        if (dataHD.jam != '' && dataHD.nadi_x_menit != '' && dataHD.td_mmhg != ''
            && dataHD.suhu != '' && dataHD.rr_x_menit != '' && dataHD.ufg_ml != ''
            && dataHD.vp_mmhg != '' && dataHD.tmp_mmhg != '' && dataHD.heparin_cc != ''
            && dataHD.ufr_ml_jam != '' && dataHD.qb_ml_menit != ''
        ) {

            handleSubmitHD();

        } else {

            console.error('Invalid Form');
        }

    }

    const handleValidPostHD = async (e) => {


        const paramValid = {
            "jam": dataHD.jam == '' ? "Required" : "",
            "td_mmhg": dataHD.td_mmhg == '' ? "Required" : "",
            "nadi_x_menit": dataHD.nadi_x_menit == '' ? "Required" : "",
            "ufg_ml": dataHD.ufg_ml == '' ? "Required" : "",
            "vp_mmhg": dataHD.vp_mmhg == '' ? "Required" : "",
            "tmp_mmhg": dataHD.tmp_mmhg == '' ? "Required" : "",
        };

        await setErrMsg(paramValid);

        if (dataHD.jam != '' && dataHD.td_mmhg != ''
            && dataHD.nadi_x_menit != '' && dataHD.ufg_ml != '' && dataHD.vp_mmhg != ''
            && dataHD.tmp_mmhg != ''
        ) {

            handleSubmitPostHD();

        } else {

            console.error('Invalid Form');
        }

    }

    const handleSubmitHD = async (e) => {
        const {created_at, updated_at, ...params} = dataHD;
        if (typeof params.treatment_id === "undefined" || params.treatment_id === "")
            params.treatment_id = sessionStorage.getItem('idPasienHDPedia');
        params.nama_user = sessionStorage.getItem('idOP');
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const url = tipeHD === 'pre-hd' ? API_URL + '/dokter/add_pre_hd' : API_URL + '/dokter/add_or_update_intra_hd';
            await axios.post(url, params, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            await setAddForm(false);
            await setDataHD({});
            await sessionStorage.setItem('reload211', true);
            return Swal.fire({
                title: 'Informasi',
                text: "Success add data",
                icon: 'success',
                confirmButtonText: 'Ok',
                allowOutsideClick: true
            }).then(okay => {
                if (okay) window.location.reload();
            });
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    const handleSubmitPostHD = async (e) => {
        const {created_at, updated_at, ...params} = dataHD;
        if (typeof params.treatment_id === "undefined" || params.treatment_id === "")
            params.treatment_id = sessionStorage.getItem('idPasienHDPedia');
        params.nama_user = sessionStorage.getItem('idOP');
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const url = API_URL + '/dokter/add_or_update_post_hd';
            await axios.post(url, params, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            await setAddForm(false);
            await setDataHD({});
            await sessionStorage.setItem('reload211', true);
            return Swal.fire({
                title: 'Informasi',
                text: "Success add data",
                icon: 'success',
                confirmButtonText: 'Ok',
                allowOutsideClick: true
            }).then(okay => {
                if (okay) window.location.reload();
            });
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    const handleSubmitTransfusi = async (e) => {
        const {created_at, updated_at, ...params} = dataTransfusi;
        if (typeof params.treatment_id === "undefined" || params.treatment_id === "") params.treatment_id = sessionStorage.getItem('idPasienHDPedia');
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const url = API_URL + '/dokter/add_transfusi';
            await axios.post(url, params, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            await setAddFormTransfusi(false);
            await setDataTransfusi({});
            await sessionStorage.setItem('reload211', true);
            return Swal.fire({
                title: 'Informasi',
                text: "Success add data",
                icon: 'success',
                confirmButtonText: 'Ok',
                allowOutsideClick: true
            }).then(okay => {
                if (okay) window.location.reload();
            });
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    const handleDeleteTransfusi = async (e) => {
        const params = {transfusi_id: dataTransfusi.transfusi_id}
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const url = API_URL + '/dokter/delete_transfusi';
            await axios.post(url, params, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            await setConfirmDel(false);
            await setDataTransfusi({});
            await sessionStorage.setItem('reload211', true);
            return Swal.fire({
                title: 'Informasi',
                text: "Success delete data",
                icon: 'success',
                confirmButtonText: 'Ok',
                allowOutsideClick: true
            }).then(okay => {
                if (okay) window.location.reload();
            });
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    async function addFormHD(e) {
        e.preventDefault();

        const paramDatas = {
            "jam": "",
            "td_mmhg": "",
            "nadi_x_menit": "",
            "suhu": "",
            "rr_x_menit": "",
            "ufg_ml": "",
            "ufr_ml_menit": "",
            "vp_mmhg": "",
            "tmp_mmhg": "",
            "heparin_cc": "",
            "ufr_ml_jam": "",
            "qb_ml_menit": "",
        };

        await setDataHD(paramDatas);

        const paramValid = {
            "jam": "",
            "td_mmhg": "",
            "nadi_x_menit": "",
            "suhu": "",
            "rr_x_menit": "",
            "ufg_ml": "",
            "ufr_ml_menit": "",
            "vp_mmhg": "",
            "tmp_mmhg": "",
            "heparin_cc": "",
            "ufr_ml_jam": "",
            "qb_ml_menit": "",
        };


        await setErrMsg(paramValid);


        await setTipeHD('pre-hd');
        await setAddForm(true);
        // await setDataHD({});
    }

    async function addFormHD2(e) {
        e.preventDefault();
        const paramDatas = {
            "jam": "",
            "td_mmhg": "",
            "nadi_x_menit": "",
            "suhu": "",
            "rr_x_menit": "",
            "ufg_ml": "",
            "ufr_ml_menit": "",
            "vp_mmhg": "",
            "tmp_mmhg": "",
            "heparin_cc": "",
            "ufr_ml_jam": "",
            "qb_ml_menit": "",
        };

        await setDataHD(paramDatas);

        const paramValid = {
            "jam": "",
            "td_mmhg": "",
            "nadi_x_menit": "",
            "suhu": "",
            "rr_x_menit": "",
            "ufg_ml": "",
            "ufr_ml_menit": "",
            "vp_mmhg": "",
            "tmp_mmhg": "",
            "heparin_cc": "",
            "ufr_ml_jam": "",
            "qb_ml_menit": "",
        };

        await setErrMsg(paramValid);

        await setTipeHD('intra-hd');
        await setAddForm(true);
        // await setDataHD({});
    }

    async function addFormPOSTHD(e) {
        const paramDatas = {
            "jam": "",
            "td_mmhg": "",
            "nadi_x_menit": "",
            "ufg_ml": "",
            "vp_mmhg": "",
            "tmp_mmhg": "",

        };
        await setDataHD(paramDatas);

        const paramValid = {
            "jam": "",
            "td_mmhg": "",
            "nadi_x_menit": "",
            "ufg_ml": "",
            "vp_mmhg": "",
            "tmp_mmhg": "",
        };

        await setErrMsg(paramValid);

        e.preventDefault();
        await setAddFormPostHD(true);
        // await setDataHD({});
    }

    async function closeFormHD(e) {
        e.preventDefault();
        await setAddForm(false);
        await setAddFormTransfusi(false);
        await setConfirmDel(false);
        await setAddFormPostHD(false);
        await setDataHD({});
        await setDataTransfusi({});
    }

    async function addTransfusi(e) {
        e.preventDefault();
        await setAddFormTransfusi(true);
        await setDataTransfusi({});
    }

    const editFormHD = async (e, record) => {
        e.preventDefault();
        await setAddForm(true);
        await setDataHD(record);
    };

    const editFormPostHD = async (e, record) => {
        e.preventDefault();
        await setAddFormPostHD(true);
        await setDataHD(record);
    };

    const confirmDelTransfusi = async (e, record) => {
        e.preventDefault();
        await setConfirmDel(true);
        await setDataTransfusi(record);
    };


    const {Paragraph} = Placeholder;
    const {pre_hd, intra_hd, post_hd} = datas;

    const frmUser = <Form id="myFormHD">
        <Form.Row>

            <Form.Group as={Col} controlId="jam">
                <Form.Label>Jam</Form.Label>
                {errMsg.jam === "Required" ? (<React.Fragment><span
                    className="text-error badge badge-danger mb-1">{errMsg.jam}</span></React.Fragment>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="jam"
                    type="text"
                    value={dataHD?.jam}
                    onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                    placeholder="Jam"/>
            </Form.Group>
            <Form.Group as={Col} controlId="td_mmhg">
                <Form.Label>Tekanan darah mmHg</Form.Label>
                {errMsg.td_mmhg === "Required" ? (<React.Fragment><span
                    className="text-error badge badge-danger mb-1">{errMsg.td_mmhg}</span></React.Fragment>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="td_mmhg"
                    type="text"
                    value={dataHD?.td_mmhg}
                    onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                    placeholder="Tekanan darah mmHg"/>
            </Form.Group>
        </Form.Row>
        <Form.Row>
            <Form.Group as={Col} controlId="nadi_x_menit">
                <Form.Label>Nadi x/m</Form.Label>
                {errMsg.nadi_x_menit === "Required" ? (<React.Fragment><span
                    className="text-error badge badge-danger mb-1">{errMsg.nadi_x_menit}</span></React.Fragment>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="nadi_x_menit"
                    type="text"
                    value={dataHD?.nadi_x_menit}
                    onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                    placeholder="Nadi x/m"/>
            </Form.Group>
            <Form.Group as={Col} controlId="suhu">
                <Form.Label>Suhu (&#8451;)</Form.Label>
                {errMsg.suhu === "Required" ? (<React.Fragment><span
                    className="text-error badge badge-danger mb-1">{errMsg.suhu}</span></React.Fragment>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="suhu"
                    type="text"
                    value={dataHD?.suhu}
                    onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                    placeholder="Suhu (&#8451;)"/>
            </Form.Group>
        </Form.Row>

        <Form.Row>
            <Form.Group as={Col} controlId="rr_x_menit">
                <Form.Label>RR x/m</Form.Label>
                {errMsg.rr_x_menit === "Required" ? (<React.Fragment><span
                    className="text-error badge badge-danger mb-1">{errMsg.rr_x_menit}</span></React.Fragment>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="rr_x_menit"
                    type="text"
                    value={dataHD?.rr_x_menit}
                    onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                    placeholder="RR x/m"/>
            </Form.Group>
            <Form.Group as={Col} controlId="qb_ml_menit">
                <Form.Label>QB ml/mnt</Form.Label>
                {errMsg.qb_ml_menit === "Required" ? (<React.Fragment><span
                    className="text-error badge badge-danger mb-1">{errMsg.qb_ml_menit}</span></React.Fragment>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="qb_ml_menit"
                    type="text"
                    value={dataHD?.qb_ml_menit}
                    onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                    placeholder="QB ml/mnt"/>
            </Form.Group>
        </Form.Row>

        <Form.Row>
            <Form.Group as={Col} controlId="ufg_ml">
                <Form.Label>UFG ml</Form.Label>
                {errMsg.ufg_ml === "Required" ? (<React.Fragment><span
                    className="text-error badge badge-danger mb-1">{errMsg.ufg_ml}</span></React.Fragment>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="ufg_ml"
                    type="text"
                    value={dataHD?.ufg_ml}
                    onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                    placeholder="UFG ml"/>
            </Form.Group>
            <Form.Group as={Col} controlId="ufr_ml_jam">
                <Form.Label>UFR ml/jam</Form.Label>
                {errMsg.ufr_ml_jam === "Required" ? (<React.Fragment><span
                    className="text-error badge badge-danger mb-1">{errMsg.ufr_ml_jam}</span></React.Fragment>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="ufr_ml_jam"
                    type="ufr_ml_jam"
                    value={dataHD?.ufr_ml_jam}
                    onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                    placeholder="UFR ml/jam"/>
            </Form.Group>
        </Form.Row>

        <Form.Row>
            <Form.Group as={Col} controlId="ufr_ml_menit">
                <Form.Label>UFR ml/mnt</Form.Label>
                {errMsg.ufr_ml_menit === "Required" ? (<React.Fragment><span
                    className="text-error badge badge-danger mb-1">{errMsg.ufr_ml_menit}</span></React.Fragment>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="ufr_ml_menit"
                    type="text"
                    value={dataHD?.ufr_ml_menit}
                    onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                    placeholder="UFR ml/mnt"/>
            </Form.Group>
            <Form.Group as={Col} controlId="vp_mmhg">
                <Form.Label>VP mmHg</Form.Label>
                {errMsg.vp_mmhg === "Required" ? (<React.Fragment><span
                    className="text-error badge badge-danger mb-1">{errMsg.vp_mmhg}</span></React.Fragment>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="vp_mmhg"
                    type="text"
                    value={dataHD?.vp_mmhg}
                    onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                    placeholder="VP mmHg"/>
            </Form.Group>
        </Form.Row>

        <Form.Row>
            <Form.Group as={Col} controlId="tmp_mmhg">
                <Form.Label>TMP mmHg</Form.Label>
                {errMsg.tmp_mmhg === "Required" ? (<React.Fragment><span
                    className="text-error badge badge-danger mb-1">{errMsg.tmp_mmhg}</span></React.Fragment>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="tmp_mmhg"
                    type="text"
                    value={dataHD?.tmp_mmhg}
                    onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                    placeholder="TMP mmHg"/>
            </Form.Group>
            <Form.Group as={Col} controlId="heparin_cc">
                <Form.Label>Heparin (ml)</Form.Label>
                {errMsg.heparin_cc === "Required" ? (<React.Fragment><span
                    className="text-error badge badge-danger mb-1">{errMsg.heparin_cc}</span></React.Fragment>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="heparin_cc"
                    type="text"
                    value={dataHD?.heparin_cc}
                    onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                    placeholder="Heparin (ml)"/>
            </Form.Group>
        </Form.Row>
        <Form.Group controlId="nama_user">
            {/* <Form.Label>Nama petugas</Form.Label> */}
            {/* {errMsg.nama_user === "Required" ?  (<React.Fragment><span className="text-error badge badge-danger mb-1">{errMsg.nama_user }</span></React.Fragment>) : ''} */}
            <Form.Control
                size="sm"
                autoComplete="off"
                name="nama_user"
                type="text"
                hidden={true}
                // value={this.props.user.id_operator}
                onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                placeholder="Nama petugas"/>
        </Form.Group>
        <Form.Group controlId="keterangan">
            <Form.Label>Keterangan</Form.Label>
            <Form.Control
                style={{"overflow-y": "scroll"}}
                as="textarea" rows={3}
                size="sm"
                autoComplete="off"
                name="keterangan"
                type="text"
                value={dataHD?.keterangan}
                onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                placeholder="Keterangan"/>
        </Form.Group>


    </Form>;

    const frmPostHD = <Form id="myFormPostHD">
        <Form.Row>
            <Form.Group as={Col} controlId="jam">
                <Form.Label>Jam</Form.Label>
                {errMsg.jam === "Required" ? (<React.Fragment><span
                    className="text-error badge badge-danger mb-1">{errMsg.jam}</span></React.Fragment>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="jam"
                    type="text"
                    value={dataHD?.jam}
                    onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                    placeholder="Jam"/>
            </Form.Group>
            <Form.Group as={Col} controlId="td_mmhg">
                <Form.Label>Tekanan darah Hg</Form.Label>
                {errMsg.td_mmhg === "Required" ? (<React.Fragment><span
                    className="text-error badge badge-danger mb-1">{errMsg.td_mmhg}</span></React.Fragment>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="td_mmhg"
                    type="text"
                    value={dataHD?.td_mmhg}
                    onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                    placeholder="Tekanan darah mmHg"/>
            </Form.Group>
        </Form.Row>
        <Form.Row>
            <Form.Group as={Col} controlId="nadi_x_menit">
                <Form.Label>Nadi x/m</Form.Label>
                {errMsg.nadi_x_menit === "Required" ? (<React.Fragment><span
                    className="text-error badge badge-danger mb-1">{errMsg.nadi_x_menit}</span></React.Fragment>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="nadi_x_menit"
                    type="text"
                    value={dataHD?.nadi_x_menit}
                    onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                    placeholder="Nadi x/m"/>
            </Form.Group>
            <Form.Group as={Col} controlId="ufg_ml">
                <Form.Label>Total UFG Tercapai(ml)</Form.Label>
                {errMsg.ufg_ml === "Required" ? (<React.Fragment><span
                    className="text-error badge badge-danger mb-1">{errMsg.ufg_ml}</span></React.Fragment>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="ufg_ml"
                    type="text"
                    value={dataHD?.ufg_ml}
                    onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                    placeholder="Total UFG Tercapai(ml)"/>
            </Form.Group>
        </Form.Row>


        <Form.Row>

            <Form.Group as={Col} controlId="vp_mmhg">
                <Form.Label>VP mmHg</Form.Label>
                {errMsg.vp_mmhg === "Required" ? (<React.Fragment><span
                    className="text-error badge badge-danger mb-1">{errMsg.vp_mmhg}</span></React.Fragment>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="vp_mmhg"
                    type="text"
                    value={dataHD?.vp_mmhg}
                    onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                    placeholder="VP mmHg"/>
            </Form.Group>
            <Form.Group as={Col} controlId="tmp_mmhg">
                <Form.Label>TMP mmHg</Form.Label>
                {errMsg.tmp_mmhg === "Required" ? (<React.Fragment><span
                    className="text-error badge badge-danger mb-1">{errMsg.tmp_mmhg}</span></React.Fragment>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="tmp_mmhg"
                    type="text"
                    value={dataHD?.tmp_mmhg}
                    onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                    placeholder="TMP mmHg"/>
            </Form.Group>
        </Form.Row>
        <Form.Group controlId="keterangan">
            <Form.Label>Keterangan</Form.Label>
            <Form.Control
                style={{"overflow-y": "scroll"}}
                as="textarea" rows={3}
                size="sm"
                autoComplete="off"
                name="keterangan"
                type="text"
                value={dataHD?.keterangan}
                onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                placeholder="Keterangan"/>
        </Form.Group>

        <Form.Group controlId="nama_user">
            {/* <Form.Label>Nama petugas</Form.Label> */}
            <Form.Control
                size="sm"
                autoComplete="off"
                name="nama_user"
                type="text"
                hidden={true}
                // value={this.props.user.id_operator}
                onChange={e => setOnChangeHD(e.target.name, e.target.value)}
                placeholder="Nama petugas"/>
        </Form.Group>


    </Form>;

    const frmTransfusi = <Form id="myFormTransfusi">
        <Form.Row>
            <Form.Group as={Col} controlId="batch">
                <Form.Label>Batch</Form.Label>
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="batch"
                    type="text"
                    value={dataTransfusi?.batch}
                    onChange={e => setOnChangeTransfusi(e.target.name, e.target.value)}
                    placeholder="Batch"/>
            </Form.Group>
            <Form.Group as={Col} controlId="ml">
                <Form.Label>Ml</Form.Label>
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="ml"
                    type="text"
                    value={dataTransfusi?.ml}
                    onChange={e => setOnChangeTransfusi(e.target.name, e.target.value)}
                    placeholder="Ml"/>
            </Form.Group>
        </Form.Row>
        <Form.Row>
            <Form.Group as={Col} controlId="golongan_darah">
                <Form.Label>Golongan Darah</Form.Label>
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="golongan_darah"
                    as="select"
                    value={dataTransfusi?.golongan_darah}
                    onChange={e => setOnChangeTransfusi(e.target.name, e.target.value)}
                >
                    <option value="">- Pilih -</option>
                    <option
                        value="A"
                        selected={dataTransfusi?.golongan_darah === "A" ? true : false}>
                        A
                    </option>
                    <option
                        value="B"
                        selected={dataTransfusi?.golongan_darah === "B" ? true : false}>
                        B
                    </option>
                    <option
                        value="O"
                        selected={dataTransfusi?.golongan_darah === "O" ? true : false}>
                        O
                    </option>
                    <option
                        value="AB"
                        selected={dataTransfusi?.golongan_darah === "AB" ? true : false}>
                        AB
                    </option>
                </Form.Control>
            </Form.Group>
            <Form.Group as={Col} controlId="rhesus">
                <Form.Label>Rhesus</Form.Label>
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="rhesus"
                    as="select"
                    value={dataTransfusi?.rhesus}
                    onChange={e => setOnChangeTransfusi(e.target.name, e.target.value)}
                >
                    <option value="">- Pilih -</option>
                    <option
                        value="Positif"
                        selected={dataTransfusi?.rhesus === "Positif" ? true : false}>
                        Positif(+)
                    </option>
                    <option
                        value="Negatif"
                        selected={dataTransfusi?.rhesus === "Negatif" ? true : false}>
                        Negatif(-)
                    </option>

                </Form.Control>
            </Form.Group>
        </Form.Row> </Form>;

    const renderDataTransfusi = () => {
        let cards = [];
        if (!isLoading && dataKeperawatan.transfusi) {
            for (let i = 0; i < dataKeperawatan.transfusi.length; i++) {
                cards.push(
                    <Card key={dataKeperawatan.transfusi[i]?.transfusi_id} className="mb-2">
                        <Card.Body>
                            <Card.Text>
                                <ListGroup variant="flush">
                                    <ListGroup.Item><strong>Batch</strong> : {dataKeperawatan.transfusi[i]?.batch}
                                    </ListGroup.Item>
                                    <ListGroup.Item><strong>Ml</strong> : {dataKeperawatan.transfusi[i]?.ml}
                                    </ListGroup.Item>
                                    <ListGroup.Item><strong>Gol. Darah</strong>
                                        : {dataKeperawatan.transfusi[i]?.golongan_darah}</ListGroup.Item>
                                    <ListGroup.Item><strong>Rhesus</strong> : {dataKeperawatan.transfusi[i]?.rhesus}
                                    </ListGroup.Item>
                                </ListGroup>
                            </Card.Text>
                            <Button onClick={e => confirmDelTransfusi(e, dataKeperawatan.transfusi[i])} variant="danger"
                                    style={{float: "right", marginTop: 2}}>
                                <i className="fa fa-trash"></i> Hapus</Button>
                        </Card.Body>
                    </Card>
                );
            }
        }
        return cards;
    };

    const contentDelete = <div
        dangerouslySetInnerHTML={{__html: '<div id="caption" style="padding-bottom:20px;">Apakah anda yakin <br/>akan menghapus data ini ?</div>'}}/>;


    return (
        <div>
            {isLoading ? (
                <div className="card shadow-lg">
                    <div className="card-body">
                        <Paragraph rowHeight={25} rowMargin={30} rows={10} active
                                   style={{marginTop: 30}}/>
                    </div>
                </div>

            ) : (
                <Form>
                    <h3>Form Askep</h3>
                    <table className="table table-condensed table-form-askep">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} align="center">
                                Tindakan Keperawatan
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Dropdown>
                                    <Dropdown.Toggle variant="info" id="dropdown-basic">
                                        <i className="fa fa-plus mr-2"></i> Tambah Form
                                    </Dropdown.Toggle>

                                    <Dropdown.Menu className="my-dropdown-menu">
                                        <Dropdown.Item as="button" onClick={addFormHD}>Pre HD </Dropdown.Item>
                                        <Dropdown.Item as="button" onClick={addFormHD2}>Intra HD </Dropdown.Item>
                                        <Dropdown.Item as="button" onClick={addFormPOSTHD}>Post HD </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table className="table table-bordered">
                                    <thead>

                                    <tr>
                                        <th style={{textAlign: 'center'}}>Observasi</th>
                                        <th style={{textAlign: 'center'}}>Jam</th>
                                        <th style={{textAlign: 'center'}}>Tekanan darah (mmHg)</th>
                                        <th style={{textAlign: "center"}}>Nadi (x/m)</th>

                                        <th style={{textAlign: 'center'}}>Suhu (&#8451;)</th>
                                        <th style={{textAlign: 'center'}}>RR (x/m)</th>
                                        <th style={{textAlign: 'center'}}>QB (ml/mnt)</th>
                                        <th style={{textAlign: "center"}}>UFG (ml)</th>

                                        <th style={{textAlign: 'center'}}>UFR (ml/jam)</th>
                                        <th style={{textAlign: 'center'}}>UFR (ml/mnt)</th>
                                        <th style={{textAlign: 'center'}}>UFV (ml)</th>
                                        <th style={{textAlign: 'center'}}>VP (mmHg)</th>
                                        <th style={{textAlign: "center"}}>TMP (mmHg)</th>

                                        <th style={{textAlign: 'center'}}>Heparin (ml)</th>
                                        <th style={{textAlign: 'center'}}>Keterangan</th>
                                        <th style={{textAlign: 'center'}}>Nama Petugas</th>
                                        <th style={{textAlign: "center", width: "75px"}}>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Pre HD</td>
                                        <td>{pre_hd?.jam}</td>
                                        <td>{pre_hd?.td_mmhg}</td>
                                        <td>{pre_hd?.nadi_x_menit}</td>
                                        <td>{pre_hd?.suhu}</td>
                                        <td>{pre_hd?.rr_x_menit}</td>
                                        <td>{pre_hd?.qb_ml_menit}</td>
                                        <td>{pre_hd?.ufg_ml}</td>
                                        <td>{pre_hd?.ufr_ml_jam}</td>
                                        <td>{pre_hd?.ufr_ml_menit}</td>
                                        <td>{pre_hd?.ufv_ml}</td>
                                        <td>{pre_hd?.vp_mmhg}</td>
                                        <td>{pre_hd?.tmp_mmhg}</td>
                                        <td>{pre_hd?.heparin_cc}</td>
                                        <td>{pre_hd?.keterangan}</td>
                                        <td>{pre_hd?.nama_user}</td>

                                        <td>

                                        </td>
                                    </tr>

                                    {intra_hd && intra_hd.length > 0 ? (
                                        intra_hd.map(function (dt, i) {
                                            let td = '';
                                            if (i === 0) td =
                                                <td rowSpan={intra_hd && intra_hd.length > 0 ? intra_hd.length : -1}>Intra
                                                    HD</td>;
                                            return <tr>
                                                {td}
                                                <td>{dt.jam}</td>
                                                <td>{dt.td_mmhg}</td>
                                                <td>{dt?.nadi_x_menit}</td>
                                                <td>{dt?.suhu}</td>
                                                <td>{dt?.rr_x_menit}</td>
                                                <td>{dt?.qb_ml_menit}</td>
                                                <td>{dt?.ufg_ml}</td>
                                                <td>{dt?.ufr_ml_jam}</td>
                                                <td>{dt?.ufr_ml_menit}</td>
                                                <td>{dt?.vp_mmhg}</td>
                                                <td>{dt?.ufv_ml}</td>
                                                <td>{dt?.tmp_mmhg}</td>
                                                <td>{dt?.heparin_cc}</td>
                                                <td>{dt?.keterangan}</td>
                                                <td>{dt?.nama_user}</td>
                                                <td>
                                                    <button
                                                        onClick={e => editFormHD(e, dt)}
                                                        className="btn btn-xs btn-success">
                                                        <i className="fa fa-edit"></i> Edit
                                                    </button>
                                                </td>
                                            </tr>
                                        })
                                    ) : (
                                        <tr>
                                            <td>Intra HD</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    )}


                                    <tr>
                                        <td>Post HD</td>
                                        <td>{post_hd?.jam}</td>
                                        <td>{post_hd?.td_mmhg}</td>
                                        <td>{post_hd?.nadi_x_menit}</td>
                                        <td colSpan="7" align="center">
                                            <Form.Group controlId="ufg_ml">
                                                <Form.Label>Total UFG Tercapai (ml) </Form.Label>
                                                <Form.Control
                                                    readOnly
                                                    size="sm"
                                                    style={{width: "40%"}}
                                                    autoComplete="off"
                                                    name="kontinu"
                                                    id="kontinu"
                                                    type="text"
                                                    value={post_hd ? post_hd.ufg_ml : ''}
                                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                                    placeholder="Total UFG Tercapai (ml)"/>
                                            </Form.Group>

                                        </td>

                                        <td>{post_hd?.vp_mmhg}</td>
                                        <td>{post_hd?.tmp_mmhg}</td>
                                        <td></td>
                                        <td>{post_hd?.keterangan}</td>
                                        <td>{post_hd?.nama_user}</td>
                                        <td>
                                            <button
                                                onClick={e => editFormPostHD(e, post_hd)}
                                                className="btn btn-xs btn-success">
                                                <i className="fa fa-edit"></i> Edit
                                            </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>

                        </tbody>
                    </table>

                    <table className="table table-condensed table-form-askep">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "rgba(0,0,0,.1)",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="6" align="center">
                                Penyulit selama HD
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="penyulit_selama_hd"
                                    id="penyulit_selama_hd"
                                    checked={dataKeperawatan ? dataKeperawatan.penyulit_selama_hd?.includes("Masalah Akses") : ''}
                                    onChange={e => setOnChange("penyulit_selama_hd", "Masalah Akses")}
                                    label="Masalah Akses"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="penyulit_selama_hd"
                                    id="penyulit_selama_hd2"
                                    checked={dataKeperawatan ? dataKeperawatan.penyulit_selama_hd?.includes("Kram Otot") : ''}
                                    onChange={e => setOnChange("penyulit_selama_hd", "Kram Otot")}
                                    label="Kram Otot"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="penyulit_selama_hd"
                                    id="penyulit_selama_hd3"
                                    checked={dataKeperawatan ? dataKeperawatan.penyulit_selama_hd?.includes("Nyeri Dada") : ''}
                                    onChange={e => setOnChange("penyulit_selama_hd", "Nyeri Dada")}
                                    label="Nyeri Dada"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="penyulit_selama_hd"
                                    id="penyulit_selama_hd4"
                                    checked={dataKeperawatan ? dataKeperawatan.penyulit_selama_hd?.includes("Pendarahan") : ''}
                                    onChange={e => setOnChange("penyulit_selama_hd", "Pendarahan")}
                                    label="Pendarahan"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="penyulit_selama_hd"
                                    id="penyulit_selama_hd5"
                                    checked={dataKeperawatan ? dataKeperawatan.penyulit_selama_hd?.includes("Hiperkalemia") : ''}
                                    onChange={e => setOnChange("penyulit_selama_hd", "Hiperkalemia")}
                                    label="Hiperkalemia"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="penyulit_selama_hd"
                                    id="penyulit_selama_hd6"
                                    checked={dataKeperawatan ? dataKeperawatan.penyulit_selama_hd?.includes("Aritmia") : ''}
                                    onChange={e => setOnChange("penyulit_selama_hd", "Aritmia")}
                                    label="Aritmia"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="penyulit_selama_hd"
                                    id="penyulit_selama_hd7"
                                    checked={dataKeperawatan ? dataKeperawatan.penyulit_selama_hd?.includes("First Use Syndrome") : ''}
                                    onChange={e => setOnChange("penyulit_selama_hd", "First Use Syndrome")}
                                    label="First Use Syndrome"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="penyulit_selama_hd"
                                    id="penyulit_selama_hd8"
                                    checked={dataKeperawatan ? dataKeperawatan.penyulit_selama_hd?.includes("Demam") : ''}
                                    onChange={e => setOnChange("penyulit_selama_hd", "Demam")}
                                    label="Demam"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="penyulit_selama_hd"
                                    id="penyulit_selama_hd9"
                                    checked={dataKeperawatan ? dataKeperawatan.penyulit_selama_hd?.includes("Gatal-gatal") : ''}
                                    onChange={e => setOnChange("penyulit_selama_hd", "Gatal-gatal")}
                                    label="Gatal-gatal"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="penyulit_selama_hd"
                                    id="penyulit_selama_hd10"
                                    checked={dataKeperawatan ? dataKeperawatan.penyulit_selama_hd?.includes("Sakit Kepala") : ''}
                                    onChange={e => setOnChange("penyulit_selama_hd", "Sakit Kepala")}
                                    label="Sakit Kepala"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="penyulit_selama_hd"
                                    id="penyulit_selama_hd11"
                                    checked={dataKeperawatan ? dataKeperawatan.penyulit_selama_hd?.includes("Hipotensi") : ''}
                                    onChange={e => setOnChange("penyulit_selama_hd", "Hipotensi")}
                                    label="Hipotensi"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="penyulit_selama_hd"
                                    id="penyulit_selama_hd12"
                                    checked={dataKeperawatan ? dataKeperawatan.penyulit_selama_hd?.includes("Menggigil\/Dingin") : ''}
                                    onChange={e => setOnChange("penyulit_selama_hd", "Menggigil\/Dingin")}
                                    label="Menggigil /  Dingin"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="penyulit_selama_hd"
                                    id="penyulit_selama_hd13"
                                    checked={dataKeperawatan ? dataKeperawatan.penyulit_selama_hd?.includes("Mual & Muntah") : ''}
                                    onChange={e => setOnChange("penyulit_selama_hd", "Mual & Muntah")}
                                    label="Mual & Muntah"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="penyulit_selama_hd"
                                    id="penyulit_selama_hd14"
                                    checked={dataKeperawatan ? dataKeperawatan.penyulit_selama_hd?.includes("Hipertensi") : ''}
                                    onChange={e => setOnChange("penyulit_selama_hd", "Hipertensi")}
                                    label="Hipertensi"/>
                            </td>
                            <td>
                                <Form.Check
                                    type="checkbox"
                                    name="penyulit_selama_hd"
                                    id="penyulit_selama_hd15"
                                    checked={dataKeperawatan ? dataKeperawatan.penyulit_selama_hd?.includes("Lain-lain") : ''}
                                    onChange={e => setOnChange("penyulit_selama_hd", "Lain-lain")}
                                    label="Lain-lain"/>
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="9">
                                <strong>Keterangan</strong>
                                <Form.Control controlId="keterangan"
                                              size="sm"
                                              style={{marginTop: "5px"}}
                                              autoComplete="off"
                                              name="keterangan"
                                              type="text"
                                              value={dataKeperawatan?.keterangan}
                                              onChange={e => setOnChange(e.target.name, e.target.value)}
                                              placeholder="Keterangan"/>
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="9">
                                <strong>Evaluasi Keperawatan</strong>
                                <Form.Control controlId="evaluasi_keperawatan"
                                              size="sm"
                                              style={{marginTop: "5px"}}
                                              autoComplete="off"
                                              name="evaluasi_keperawatan"
                                              type="text"
                                              value={dataKeperawatan?.evaluasi_keperawatan}
                                              onChange={e => setOnChange(e.target.name, e.target.value)}
                                              placeholder="Evaluasi Keperawatan"/>
                            </td>
                        </tr>

                        </tbody>
                    </table>

                    <table className="table table-condensed table-form-askep">

                        <tbody>
                        <tr>
                            <td style={{
                                "backgroundColor": "none",
                                "fontWeight": "bold",
                                "fontSize": "16px"
                            }} colSpan="6" align="center">
                                <button
                                    type="button"
                                    id="tambahTransfusi"
                                    onClick={addTransfusi}
                                    className="btn btn-info btn-lg">
                                    <i className="fa fa-plus mr-2"></i> Tambah Transfusi
                                </button>
                            </td>
                        </tr>

                        <tr>
                            <td colSpan="6">
                                <CardColumns>
                                    {renderDataTransfusi()}
                                </CardColumns>
                            </td>
                        </tr>

                        <tr>
                            <td width="8%">
                                <strong>Tusuk Cimino 1x ?</strong>
                            </td>
                            <td width="1%">:</td>
                            <td width="18%">
                                <Form.Control
                                    size="sm"
                                    style={{width: '30%'}}
                                    autoComplete="off"
                                    name="tusuk_cimino"
                                    id="tusuk_cimino"
                                    as="select"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    value={dataKeperawatan ? dataKeperawatan.tusuk_cimino : ''}
                                >
                                    <option value="">- Pilih -</option>
                                    <option value="Ya">Ya</option>
                                    <option value="Tidak">Tidak</option>

                                </Form.Control>
                            </td>
                            <td width="9%">
                                <strong>Infeksi exit site CDL?</strong><br/>Terjadi nanah/ pus di exit site
                            </td>
                            <td width="1%">:</td>
                            <td width="19%">
                                <Form.Control
                                    style={{width: '40%'}}
                                    size="sm"
                                    autoComplete="off"
                                    name="infeksi_exit_site"
                                    id="infeksi_exit_site"
                                    as="select"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    value={dataKeperawatan ? dataKeperawatan.infeksi_exit_site : ''}
                                >
                                    <option value="">- Pilih -</option>
                                    <option value="Ya">Ya</option>
                                    <option value="Tidak">Tidak</option>

                                </Form.Control>
                            </td>


                        </tr>
                        <tr>
                            <td colSpan="6">
                                <strong>TERAPI ESA</strong>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Nama Obat</strong></td>
                            <td>:</td>
                            <td>
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="nama_obat"
                                    type="text"
                                    value={dataKeperawatan?.nama_obat}
                                    placeholder="Nama Obat"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}/>
                            </td>
                            <td align={"right"}><strong>Dosis(IU)</strong></td>
                            <td>:</td>
                            <td>
                                <NumberFormat
                                    style={{width: "40%"}}
                                    name="dosis"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    className="form-control form-control-sm"
                                    value={dataKeperawatan ? dataKeperawatan.dosis : ''}
                                    thousandSeparator={false}
                                    inputMode="numeric"
                                    autoComplete="off"
                                    placeholder="Dosis(IU)"/>
                            </td>

                        </tr>

                        <tr>
                            <td><strong>Jumlah(x)</strong></td>
                            <td>:</td>
                            <td>
                                <NumberFormat
                                    style={{width: "40%"}}
                                    name="jumlah"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    className="form-control form-control-sm"
                                    value={dataKeperawatan ? dataKeperawatan.jumlah : ''}
                                    thousandSeparator={false}
                                    inputMode="numeric"
                                    autoComplete="off"
                                    placeholder="Jumlah(x)"/>
                            </td>
                            <td align={"right"}><strong>Waktu</strong></td>
                            <td>:</td>
                            <td>
                                <Form.Control
                                    size="sm"
                                    style={{width: "40%"}}
                                    autoComplete="off"
                                    name="waktu"
                                    as="select"
                                    value={dataKeperawatan?.waktu}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}>
                                    <option value="">- Pilih -</option>
                                    <option value="Minggu">Minggu</option>
                                    <option value="Bulan">Bulan</option>

                                </Form.Control>
                            </td>

                        </tr>

                        <tr>
                            <td><strong>Terapi Besi</strong></td>
                            <td>:</td>
                            <td>
                                <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="terapi_besi"
                                    type="text"
                                    value={dataKeperawatan?.terapi_besi}
                                    placeholder="Terapi Besi"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}/>
                            </td>
                            <td align={"right"}><strong>Terapi Besi Ke</strong></td>
                            <td>:</td>
                            <td>
                                <NumberFormat
                                    style={{width: "40%"}}
                                    name="terapi_ke"
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    className="form-control form-control-sm"
                                    value={dataKeperawatan ? dataKeperawatan.terapi_ke : ''}
                                    thousandSeparator={false}
                                    inputMode="numeric"
                                    autoComplete="off"
                                    placeholder="Terapi Besi Ke"/>
                            </td>

                        </tr>
                        </tbody>
                    </table>

                    <button
                        type="button"
                        id="submitFormAskepTindakanKeperawatan"
                        onClick={(e) => handleSubmit()}
                        className="btn btn-success btn-sm">Submit Form Tindakan Keperawatan
                    </button>
                </Form>


            )}
            <AppModal
                show={addForm}
                form={frmUser}
                size="xs"
                backdrop={false}
                keyboard={false}
                title={tipeHD === 'pre-hd' ? 'Add Pre HD' : 'Add/Edit Intra HD'}
                titleButton="Save change"
                themeButton="success"
                handleClose={closeFormHD}
                /*isLoading={this.props.isAddLoading ? this.props.isAddLoading : this.state.loadingForm}*/
                formSubmit={handleValidHD}
            ></AppModal>

            <AppModal
                show={addFormPostHD}
                form={frmPostHD}
                size="xs"
                backdrop={false}
                keyboard={false}
                title="Post HD"
                titleButton="Save change"
                themeButton="success"
                handleClose={closeFormHD}
                /*isLoading={this.props.isAddLoading ? this.props.isAddLoading : this.state.loadingForm}*/
                formSubmit={handleValidPostHD}
            ></AppModal>

            <AppModal
                show={addFormTransfusi}
                form={frmTransfusi}
                size="xs"
                backdrop={false}
                keyboard={false}
                title='Tambah Transfusi'
                titleButton="Save change"
                themeButton="success"
                handleClose={closeFormHD}
                /*isLoading={this.props.isAddLoading ? this.props.isAddLoading : this.state.loadingForm}*/
                formSubmit={handleSubmitTransfusi}
            ></AppModal>

            <AppModal
                show={confirmDel}
                form={contentDelete}
                size="xs"
                backdrop={false}
                keyboard={false}
                title='Delete Transfusi'
                titleButton="Delete"
                themeButton="warning"
                handleClose={closeFormHD}
                /*isLoading={this.props.isAddLoading ? this.props.isAddLoading : this.state.loadingForm}*/
                formSubmit={handleDeleteTransfusi}
            ></AppModal>
        </div>
    )
}
