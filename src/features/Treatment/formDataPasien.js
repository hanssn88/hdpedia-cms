import axios from "axios";
import moment from 'moment';
import React, {useEffect, useState} from "react";
import {Form, Image} from 'react-bootstrap';
import Datetime from "react-datetime";
import Swal from 'sweetalert2';
import noImg from '../../assets/noPhoto.jpg';

const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;


export const FormDataPasien = (data) => {
    const [datas, setDatas] = useState(data?.data);
    const [user, setUser] = useState(sessionStorage.getItem('level_id'));
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        async function setData() {
            await setUser(sessionStorage.getItem('level_id'));
            await setDatas(data?.data);
            await new Promise(res => setTimeout(res, 1200));
            await setIsLoading(false);
        }

        setData();
    }, [data])


    const setOnChange = async (key, val) => {
        if (key === 'bb_pre_hd' || key === 'bb_post_hd_yang_lalu' || key === 'bb_kering' || key === 'bb_post_hd') {
            if (parseInt(val) >= 1000) {
                await setDatas({...datas});
            } else {
                await setDatas({...datas, [key]: val});
            }
        } else {
            await setDatas({...datas, [key]: val});
        }
    };

    const handleSubmit = async (e) => {
        const patient_id = sessionStorage.getItem('idUserHDPedia');

        const {id, created_at, updated_at, ...params} = datas;


        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            await axios.post(API_URL + '/dokter/add_or_update_data_awal_patient', {...params, patient_id: patient_id}, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            return Swal.fire({
                title: 'Informasi',
                text: "Success",
                icon: 'success',
                confirmButtonText: 'Ok',
                allowOutsideClick: true
            })
        } catch (error) {
            return Swal.fire({
                title: 'Informasi',
                text: error,
                icon: 'error',
                confirmButtonText: 'Ok',
                allowOutsideClick: false
            })

        }
    };

    return (
        <div>
            <Form>
                <h3>Form Data Pasien</h3>
                <table className="table table-condensed ">

                    <tbody>
                    <tr>
                        <td style={{
                            "backgroundColor": "rgba(0,0,0,.1)",
                            "fontWeight": "bold",
                            "fontSize": "16px"
                        }} colSpan="9" align="center">
                            Data Pasien
                        </td>
                    </tr>
                    <tr>
                        <td width="8%">
                            <strong>Nama</strong>
                        </td>
                        <td width="1%">:</td>
                        <td width="28%">{datas?.name ? datas?.name : '-'}</td>
                        <td width="8%"><strong>No.Rekam Medis</strong>
                        </td>
                        <td width="1%">:</td>
                        <td width="28%">{datas?.rm_number ? datas?.rm_number : '-'}</td>

                        <td width="8%"><strong>Tanggal Lahir</strong>

                        </td>
                        <td width="1%">:</td>

                        <td>{datas?.ttl ? datas?.ttl : '-'}</td>
                    </tr>

                    <tr>
                        <td width="8%">
                            <strong>Umur(Tahun)</strong>
                        </td>
                        <td width="1%">:</td>
                        <td width="28%">{datas?.age ? datas?.age : '-'}</td>
                        <td width="8%"><strong>Riwayat Alergi</strong>
                        </td>
                        <td width="1%">:</td>
                        <td width="28%">

                            {
                                user === '6' || user === '1' ?
                                    <Form.Control
                                        size="sm"
                                        autoComplete="off"
                                        name="riwayat_alergi"
                                        type="text"
                                        value={datas ? datas.riwayat_alergi : ''}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                        placeholder="Riwayat Alergi"/>
                                    :

                                    datas?.riwayat_alergi ? datas?.riwayat_alergi : '-'
                            }


                        </td>

                        <td width="8%"><strong>Tgl. Pertama kali HD.Tempat</strong>

                        </td>
                        <td width="1%">:</td>

                        <td>

                            {
                                user === '6' || user === '1' ?

                                    <Datetime
                                        onChange={e => setOnChange("tgl_first_hd", e)}
                                        timeFormat={false}
                                        closeOnSelect={true}
                                        inputProps={{
                                            readOnly: true,

                                            autoComplete: "off",
                                            placeholder: 'Tgl. Pertama kali HD.Tempat',
                                            name: 'tgl_first_hd',
                                            className: 'form-control form-control-sm'
                                        }}
                                    />
                                    :

                                    datas?.tgl_first_hd ? datas?.tgl_first_hd : '-'
                            }
                        </td>
                    </tr>

                    <tr>
                        <td width="8%">
                            <strong>Tgl. Pertama kali di MCH</strong>
                        </td>
                        <td width="1%">:</td>
                        <td>{datas?.tgl_first_mch ? datas?.tgl_first_mch : '-'}</td>


                        <td width="8%"><strong>Tanggal Transfusi Terakhir</strong>
                        </td>
                        <td width="1%">:</td>
                        <td>

                            {
                                user === '6' || user === '1' ?

                                    <Datetime

                                        onChange={e => setOnChange("tgl_last_transfusi", e)}
                                        timeFormat={false}
                                        closeOnSelect={true}
                                        inputProps={{
                                            readOnly: true,

                                            autoComplete: "off",
                                            placeholder: 'Tanggal Transfusi Terakhir',
                                            name: 'tgl_last_transfusi',
                                            className: 'form-control form-control-sm'
                                        }}
                                    />
                                    :

                                    datas?.tgl_last_transfusi ? datas?.tgl_last_transfusi : '-'
                            }


                        </td>

                        <td width="8%"><strong>Golongan Darah</strong>

                        </td>
                        <td width="1%">:</td>

                        <td>{datas?.blood_type ? datas?.blood_type : '-'}</td>
                    </tr>

                    <tr>
                        <td width="8%">
                            <strong>Rhesus +/-</strong>
                        </td>
                        <td width="1%">:</td>
                        <td>
                            {datas?.rhesus ? datas?.rhesus : '-'}

                        </td>


                        <td width="8%"><strong>Anamnesis</strong>
                        </td>
                        <td width="1%">:</td>
                        <td>
                            {
                                user === '6' || user === '1' ?
                                    (<Form.Control
                                        size="sm"
                                        autoComplete="off"
                                        name="anamnesis"
                                        type="text"
                                        value={datas ? datas.anamnesis : ''}
                                        onChange={e => setOnChange(e.target.name, e.target.value)}
                                        placeholder="Anamnesis"/>) :

                                    datas?.anamnesis ? datas?.anamnesis : '-'
                            }


                        </td>

                        <td width="8%"><strong></strong>
                        </td>
                        <td width="1%"></td>
                        <td>
                        </td>
                    </tr>
                    </tbody>
                </table>

                {
                    user === '6' || user === '1' ?
                        (
                            <table className="table table-condensed">
                                <tbody>
                                <tr>
                                    <td style={{
                                        "backgroundColor": "rgba(0,0,0,.1)",
                                        "fontWeight": "bold",
                                        "fontSize": "16px"
                                    }} colSpan="9" align="center">
                                        Pemeriksaan Fisik
                                    </td>
                                </tr>
                                <tr>
                                    <td width="8%">
                                        <strong>KU</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">

                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="ku"
                                            style={{width: "90%"}}
                                            type="text"
                                            value={datas ? datas.ku : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="KU"/>
                                    </td>


                                    <td width="8%"><strong>Kesadaran</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">

                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="kesadaran"
                                            style={{width: "90%"}}
                                            type="text"
                                            value={datas ? datas.kesadaran : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="Kesadaran"/>
                                    </td>

                                    <td width="8%"><strong>BB(kg)</strong>

                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">
                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="bb"
                                            style={{width: "90%"}}
                                            type="text"
                                            value={datas ? datas.bb : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="BB(kg)"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="8%">
                                        <strong>Tinggi(cm)</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">

                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="tb"
                                            style={{width: "90%"}}
                                            type="text"
                                            value={datas ? datas.tb : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="Tinggi(cm)"/>
                                    </td>


                                    <td width="8%"><strong>Tek.Darah:/mmHg</strong></td>
                                    <td width="1%">:</td>
                                    <td width="21%">

                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="tekanan_darah"
                                            style={{width: "90%"}}
                                            type="text"
                                            value={datas ? datas.tekanan_darah : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="Tek.Darah:/mmHg"/>
                                    </td>

                                    <td width="8%"><strong>Nadi:x/mnt</strong></td>
                                    <td width="1%">:</td>

                                    <td width="21%">

                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="nadi"
                                            style={{width: "90%"}}
                                            type="text"
                                            value={datas ? datas.nadi : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="Nadi:x/mnt"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="8%">
                                        <strong>RR:x/mnt</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">
                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="rr"
                                            style={{width: "90%"}}
                                            type="text"
                                            value={datas ? datas.rr : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="RR:x/mnt"/>
                                    </td>

                                    <td width="8%"><strong>Suhu(&#8451;)</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">
                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="suhu"
                                            style={{width: "90%"}}
                                            type="text"
                                            value={datas ? datas.suhu : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="Suhu"/>
                                    </td>

                                    <td width="8%"></td>
                                    <td width="1%"></td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td width="8%">
                                        <strong>Catatan</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">
                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="catatan"
                                            style={{width: "90%"}}
                                            type="text"
                                            value={datas ? datas.catatan : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="Catatan"/>
                                    </td>

                                </tr>
                                <tr>
                                    <td width="8%">
                                        <strong>WD(termasuk etiologi GGK)</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">
                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="wd"
                                            style={{width: "90%"}}
                                            type="text"
                                            value={datas ? datas.wd : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="WD(termasuk etiologi GGK)"/>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        )

                        :
                        (
                            <table className="table table-condensed">
                                <tbody>
                                <tr>
                                    <td style={{
                                        "backgroundColor": "rgba(0,0,0,.1)",
                                        "fontWeight": "bold",
                                        "fontSize": "16px"
                                    }} colSpan="9" align="center">
                                        Pemeriksaan Fisik
                                    </td>
                                </tr>
                                <tr>
                                    <td width="8%">
                                        <strong>KU</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="28%">{datas?.ku ? datas?.ku : '-'}</td>
                                    <td width="8%"><strong>Kesadaran</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="28%">{datas?.kesadaran ? datas?.kesadaran : '-'}</td>

                                    <td width="8%"><strong>BB(kg)</strong>

                                    </td>
                                    <td width="1%">:</td>

                                    <td>{datas?.bb ? datas?.bb : '-'}</td>
                                </tr>

                                <tr>
                                    <td width="8%">
                                        <strong>Tinggi(cm)</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="28%">{datas?.tb ? datas?.tb : '-'}</td>
                                    <td width="8%"><strong>Tek.Darah:/mmHg</strong></td>
                                    <td width="1%">:</td>
                                    <td width="28%">{datas?.tekanan_darah ? datas?.tekanan_darah : '-'}</td>

                                    <td width="8%"><strong>Nadi:x/mnt</strong></td>
                                    <td width="1%">:</td>

                                    <td>{datas?.nadi ? datas?.nadi : '-'}</td>
                                </tr>

                                <tr>
                                    <td width="8%">
                                        <strong>RR:x/mnt</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="28%">{datas?.rr ? datas?.rr : '-'}</td>
                                    <td width="8%"><strong>Suhu(&#8451;)</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="28%">{datas?.suhu ? `${datas?.suhu} &#8451;` : '-'}</td>

                                    <td width="8%"></td>
                                    <td width="1%"></td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td width="8%">
                                        <strong>Catatan</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="28%" colSpan="7">{datas?.catatan ? datas?.catatan : '-'}</td>

                                </tr>
                                <tr>
                                    <td width="8%">
                                        <strong>WD(termasuk etiologi GGK)</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="28%" colSpan="7">{datas?.wd ? datas?.wd : '-'}</td>
                                </tr>
                                </tbody>
                            </table>

                        )
                }

                {
                    user === '6' || user === '1' ?
                        (
                            <table className="table table-condensed">
                                <tbody>
                                <tr>
                                    <td style={{
                                        "backgroundColor": "rgba(0,0,0,.1)",
                                        "fontWeight": "bold",
                                        "fontSize": "16px"
                                    }} colSpan="9" align="center">
                                        Data Penunjang
                                    </td>
                                </tr>

                                <tr>
                                    <td width="7%">
                                        <strong>HBs Ag</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">
                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="hbsag_result"
                                            style={{width: "90%", marginBottom: "5%"}}
                                            type="text"
                                            value={datas ? datas.hbsag_result : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}

                                            placeholder="HBs Ag"/>
                                        <Datetime
                                            setViewDate={datas && datas.hbsag_date ? new Date(datas.hbsag_date) : new Date()}
                                            value={datas && datas.hbsag_date ? new Date(datas.hbsag_date) : new Date()}
                                            onChange={e => setOnChange("hbsag_date", e)}
                                            timeFormat={false}
                                            closeOnSelect={true}
                                            inputProps={{
                                                value: datas && datas.hbsag_date ? moment(new Date(datas.hbsag_date)).format('DD-MM-YYYY') : '',
                                                readOnly: true,
                                                autoComplete: "off",
                                                placeholder: 'Tanggal HBs Ag',
                                                name: 'tanggal HBs Ag',
                                                className: 'form-control form-control-sm date-askep'
                                            }}
                                        />

                                    </td>

                                    <td width="8%">
                                        <strong>Anti HBs</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">
                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="anti_hbs_result"
                                            style={{width: "90%", marginBottom: "5%"}}
                                            type="text"
                                            value={datas ? datas.anti_hbs_result : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="Anti HBs"/>


                                        <Datetime
                                            setViewDate={datas && datas.anti_hbs_date ? new Date(datas.anti_hbs_date) : new Date()}
                                            value={datas && datas.anti_hbs_date ? new Date(datas.anti_hbs_date) : new Date()}
                                            onChange={e => setOnChange("anti_hbs_date", e)}
                                            timeFormat={false}
                                            closeOnSelect={true}
                                            inputProps={{
                                                value: datas && datas.anti_hbs_date ? moment(new Date(datas.anti_hbs_date)).format('DD-MM-YYYY') : '',
                                                readOnly: true,
                                                autoComplete: "off",
                                                placeholder: 'Tanggal HBs',
                                                name: 'tanggal HBs',
                                                className: 'form-control form-control-sm date-askep'
                                            }}
                                        />
                                    </td>

                                    <td width="8%">
                                        <strong>Anti HCV</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">
                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="anti_hcv_result"
                                            style={{width: "90%", marginBottom: "5%"}}
                                            type="text"
                                            value={datas ? datas.anti_hcv_result : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="Anti HCV"/>

                                        <Datetime
                                            setViewDate={datas && datas.anti_hcv_date ? new Date(data.anti_hcv_date) : new Date()}
                                            value={datas && datas.anti_hcv_date ? new Date(datas.anti_hcv_date) : new Date()}
                                            onChange={e => setOnChange("anti_hcv_date", e)}
                                            timeFormat={false}
                                            closeOnSelect={true}
                                            inputProps={{
                                                value: datas && datas.anti_hcv_date ? moment(new Date(datas.anti_hcv_date)).format('DD-MM-YYYY') : '',
                                                readOnly: true,
                                                autoComplete: "off",
                                                placeholder: 'Tanggal Anti HCV',
                                                name: 'tanggal Anti HCV',
                                                className: 'form-control form-control-sm date-askep'
                                            }}
                                        />


                                    </td>

                                </tr>

                                <tr>
                                    <td width="7%">
                                        <strong>Anti HIV</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">
                                        {datas?.anti_hiv_date ? `${datas?.anti_hiv_date} - ${datas?.anti_hiv_result}` : '-'}
                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="anti_hiv_result"
                                            style={{width: "90%", marginBottom: "5%"}}
                                            type="text"
                                            value={datas ? datas.anti_hiv_result : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="Anti HIV"/>

                                        <Datetime
                                            setViewDate={datas && datas.anti_hiv_date ? new Date(data.anti_hiv_date) : new Date()}
                                            value={datas && datas.anti_hiv_date ? new Date(datas.anti_hiv_date) : new Date()}
                                            onChange={e => setOnChange("anti_hiv_date", e)}
                                            timeFormat={false}
                                            closeOnSelect={true}
                                            inputProps={{
                                                value: datas && datas.anti_hiv_date ? moment(new Date(datas.anti_hiv_date)).format('DD-MM-YYYY') : '',
                                                readOnly: true,
                                                autoComplete: "off",
                                                placeholder: 'Tanggal Anti HIV',
                                                name: 'tanggal Anti HIV',
                                                className: 'form-control form-control-sm date-askep'
                                            }}
                                        />


                                    </td>

                                    <td width="8%">
                                        <strong>Hb</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">

                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="hemoglobin_result"
                                            style={{width: "90%", marginBottom: "5%"}}
                                            type="text"
                                            value={datas ? datas.hemoglobin_result : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="Hb"/>


                                        <Datetime
                                            setViewDate={datas && datas.hemoglobin_date ? new Date(data.hemoglobin_date) : new Date()}
                                            value={datas && datas.hemoglobin_date ? new Date(datas.hemoglobin_date) : new Date()}
                                            onChange={e => setOnChange("hemoglobin_date", e)}
                                            timeFormat={false}
                                            closeOnSelect={true}
                                            inputProps={{
                                                value: datas && datas.hemoglobin_date ? moment(new Date(datas.hemoglobin_date)).format('DD-MM-YYYY') : '',
                                                readOnly: true,
                                                autoComplete: "off",
                                                placeholder: 'Tanggal Hb',
                                                name: 'tanggal Hb',
                                                className: 'form-control form-control-sm date-askep'
                                            }}
                                        />


                                    </td>

                                    <td width="8%">
                                        <strong>Ureum</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">

                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="ureum_result"
                                            style={{width: "90%", marginBottom: "5%"}}
                                            type="text"
                                            value={datas ? datas.ureum_result : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="Ureum"/>

                                        <Datetime
                                            setViewDate={datas && datas.ureum_date ? new Date(data.ureum_date) : new Date()}
                                            value={datas && datas.ureum_date ? new Date(datas.ureum_date) : new Date()}
                                            onChange={e => setOnChange("ureum_date", e)}
                                            timeFormat={false}
                                            closeOnSelect={true}
                                            inputProps={{
                                                value: datas && datas.ureum_date ? moment(new Date(datas.ureum_date)).format('DD-MM-YYYY') : '',
                                                readOnly: true,
                                                autoComplete: "off",
                                                placeholder: 'Tanggal Ureum',
                                                name: 'tanggal Ureumv',
                                                className: 'form-control form-control-sm date-askep'
                                            }}
                                        />
                                    </td>

                                </tr>

                                <tr>
                                    <td width="7%">
                                        <strong>Creatinin</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">
                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="creatinin_result"
                                            style={{width: "90%", marginBottom: "5%"}}
                                            type="text"
                                            value={datas ? datas.creatinin_result : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="Creatinin"/>


                                        < Datetime
                                            setViewDate={datas && datas.creatinin_date ? new Date(data.creatinin_date) : new Date()}
                                            value={datas && datas.creatinin_date ? new Date(datas.creatinin_date) : new Date()}
                                            onChange={e => setOnChange("creatinin_date", e)}
                                            timeFormat={false}
                                            closeOnSelect={true}
                                            inputProps={{
                                                value: datas && datas.creatinin_date ? moment(new Date(datas.creatinin_date)).format('DD-MM-YYYY') : '',
                                                readOnly: true,
                                                autoComplete: "off",
                                                placeholder: 'Tanggal Creatinin',
                                                name: 'tanggal Creatinin',
                                                className: 'form-control form-control-sm date-askep'
                                            }}
                                        />

                                    </td>

                                    <td width="8%">
                                        <strong>Kalium</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">
                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="kalium_result"
                                            style={{width: "90%", marginBottom: "5%"}}
                                            type="text"
                                            value={datas ? datas.kalium_result : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="Kalium"/>

                                        <Datetime
                                            setViewDate={datas && datas.kalium_date ? new Date(data.kalium_date) : new Date()}
                                            value={datas && datas.kalium_date ? new Date(datas.kalium_date) : new Date()}
                                            onChange={e => setOnChange("kalium_date", e)}
                                            timeFormat={false}
                                            closeOnSelect={true}
                                            inputProps={{
                                                value: datas && datas.kalium_date ? moment(new Date(datas.kalium_date)).format('DD-MM-YYYY') : '',
                                                readOnly: true,
                                                autoComplete: "off",
                                                placeholder: 'Tanggal Kalium',
                                                name: 'tanggal Kalium',
                                                className: 'form-control form-control-sm date-askep'
                                            }}
                                        />

                                    </td>

                                    <td width="8%">
                                        <strong>Terapi</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="28%" colSpan="7">
                                        <Form.Control
                                            size="sm"
                                            autoComplete="off"
                                            name="terapi"
                                            style={{width: "90%"}}
                                            type="text"
                                            value={datas ? datas.terapi : ''}
                                            onChange={e => setOnChange(e.target.name, e.target.value)}
                                            placeholder="Terapi"/>


                                    </td>


                                </tr>
                                {/* <tr>
                        <td width="8%">
                            <strong>Terapi</strong>
                        </td>
                        <td width="1%">:</td>
                        <td width="28%" colSpan="7">
                            <Form.Control
                                    size="sm"
                                    autoComplete="off"
                                    name="terapi"
                                    style={{width: "90%"}}
                                    type="text"
                                    value={datas ? datas.terapi : ''}
                                    onChange={e => setOnChange(e.target.name, e.target.value)}
                                    placeholder="Terapi"/>
                        </td>

                    </tr> */}

                                <tr>
                                    <td width="8%">
                                        <strong>Photo USG</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="28%" colSpan="7">
                                        <Image
                                            src={datas?.image_usg && datas?.image_usg !== null ? datas?.image_usg : noImg}
                                            thumbnail
                                            style={{maxWidth: "300px", maxHeight: "300px"}}/>
                                    </td>

                                </tr>


                                </tbody>
                            </table>
                        )
                        :
                        (
                            <table className="table table-condensed">
                                <tbody>
                                <tr>
                                    <td style={{
                                        "backgroundColor": "rgba(0,0,0,.1)",
                                        "fontWeight": "bold",
                                        "fontSize": "16px"
                                    }} colSpan="9" align="center">
                                        Data Penunjang
                                    </td>
                                </tr>


                                <tr>
                                    <td width="7%">
                                        <strong>HBs Ag</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">
                                        {datas?.hbsag_date ? `${datas?.hbsag_date} - ${datas?.hbsag_result}` : '-'}
                                    </td>

                                    <td width="8%">
                                        <strong>Anti HBs</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">
                                        {datas?.anti_hbs_date ? `${datas?.anti_hbs_date} - ${datas?.anti_hbs_result}` : '-'}
                                    </td>

                                    <td width="8%">
                                        <strong>Anti HCV</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">
                                        {datas?.anti_hcv_date ? `${datas?.anti_hcv_date} - ${datas?.anti_hcv_result}` : '-'}
                                    </td>

                                </tr>

                                <tr>
                                    <td width="7%">
                                        <strong>Anti HIV</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">
                                        {datas?.anti_hiv_date ? `${datas?.anti_hiv_date} - ${datas?.anti_hiv_result}` : '-'}
                                    </td>

                                    <td width="8%">
                                        <strong>Hb</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">
                                        {datas?.hemoglobin_date ? `${datas?.hemoglobin_date} - ${datas?.hemoglobin_result}` : '-'}
                                    </td>

                                    <td width="8%">
                                        <strong>Ureum</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">
                                        {datas?.ureum_date ? `${datas?.ureum_date} - ${datas?.ureum_result}` : '-'}
                                    </td>

                                </tr>

                                <tr>
                                    <td width="7%">
                                        <strong>Creatinin</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">
                                        {datas?.creatinin_date ? `${datas?.creatinin_date} - ${datas?.creatinin_result}` : '-'}
                                    </td>

                                    <td width="8%">
                                        <strong>Kalium</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="21%">
                                        {datas?.kalium_date ? `${datas?.kalium_date} - ${datas?.kalium_result}` : '-'}
                                    </td>

                                    <td width="8%">

                                    </td>
                                    <td width="1%"></td>
                                    <td width="21%">

                                    </td>

                                </tr>
                                <tr>
                                    <td width="8%">
                                        <strong>Terapi</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="28%" colSpan="7">
                                        {datas?.terapi ? datas?.terapi : '-'}
                                    </td>

                                </tr>

                                <tr>
                                    <td width="8%">
                                        <strong>Photo USG</strong>
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="28%" colSpan="7">
                                        <Image
                                            src={datas?.image_usg && datas?.image_usg !== null ? datas?.image_usg : noImg}
                                            thumbnail
                                            style={{maxWidth: "300px", maxHeight: "300px"}}/>
                                    </td>

                                </tr>

                                </tbody>
                            </table>
                        )
                }

                <button type="button" onClick={() => handleSubmit()}

                        className="btn btn-success btn-sm">Submit Form Data Pasien
                </button>
            </Form>
        </div>
    )
}
