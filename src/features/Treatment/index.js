import ReactDatatable from '@ashvin27/react-datatable';
import moment from 'moment';
import "moment/locale/id";
import React, {Component, Fragment} from 'react';
import {Button, Form} from "react-bootstrap";
import Datetime from 'react-datetime';
import "react-datetime/css/react-datetime.css";
import {connect} from 'react-redux';
import AppButton from '../../components/button/Button';
import AppModal from '../../components/modal/MyModal';
import {SelectPasiens} from "../../components/modal/MySelect";
import {AppSwalSuccess} from '../../components/modal/SwalSuccess';

import {fetchData as fetchDataKlinik} from "../Klinik/klinikSlice";
import {
    addData,
    addForm,
    changeTreatmentStatus,
    clearError,
    closeForm,
    confirmDel,
    confirmHadir,
    deleteData,
    fetchData,
    fetchDataShift,
} from './treatmentSlice';


var yesterday = moment();
var valid_startDate = function (current) {
    return current.isBefore(yesterday);
};

class Treatment extends Component {

    constructor(props) {
        super(props);
        this.initSelected = {
            klinik_id: '',
            user_id: '',
            date: '',
            shift: '',
            end_date: '',
            start_date: '',
        }
        this.state = {
            validSd: valid_startDate,
            validEd: valid_startDate,
            sort_order: "ASC",
            sort_column: "name",
            keyword: '',
            page_number: 1,
            per_page: 10,
            selected: this.initSelected,
            errMsg: this.initSelected,
            loadingForm: false,
            klinik_id: sessionStorage.getItem('klinikHD'),
            end_date: '',
            start_date: '',
        }
        this.handleChangeDate = this.handleChangeDate.bind(this);
        this.handleChangeStartDate = this.handleChangeStartDate.bind(this);
        this.handleChangeEndDate = this.handleChangeEndDate.bind(this);
    }

    async componentDidMount() {
        sessionStorage.removeItem('idPasienHDPedia');
        sessionStorage.removeItem('idUserHDPedia');
        await this.fetchProfileAdmin();
        await this.props.onLoad(this.state);
    }

    fetchProfileAdmin = async () => {
        const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;
        const token = localStorage.getItem(tokenLogin);
        console.log(token);
        if (!token) {
            alert('session invalid');
            window.location.reload();
        }
    }
    handleClose = () => {
        this.props.closeModal();
    };

    handleCloseSwal = () => {
        this.props.closeSwal(this.state);
    }

    tableChangeHandler = (data) => {
        let queryString = this.state;
        Object.keys(data).map((key) => {
            if (key === "sort_order" && data[key]) {
                queryString.sort_order = data[key].order;
                queryString.sort_column = data[key].column;
            }
            if (key === "page_number") {
                queryString.page_number = data[key];
            }
            if (key === "page_size") {
                queryString.per_page = data[key];
            }
            if (key === "filter_value") {
                queryString.keyword = data[key];
            }
            return true;
        });
        this.props.onLoad(this.state);
    }

    handleChange(event) {
        const {name, value} = event.target
        var val = value;
        this.props.resetError();
        this.setState({
            selected: {
                ...this.state.selected,
                [name]: val
            },
            errMsg: {
                ...this.state.errMsg,
                [name]: val ? '' : this.state.errMsg[name]
            },
            loadingForm: false
        });

    }

    handleChangeFilter(event) {
        const {name, value} = event.target
        var val = value;
        this.props.resetError();
        this.setState({
            ...this.state,
            [name]: val,
            errMsg: {
                ...this.state.errMsg,
                [name]: val ? '' : this.state.errMsg[name]
            },
            loadingForm: false
        });

    }

    discardChanges = () => {
        this.setState({errMsg: {}, selected: this.initSelected, loadingForm: false});
        this.props.showForm();
    }

    editRecord = async (record) => {

        this.setState({
            loadingForm: false,
            errMsg: this.initSelected,
            selected: {...record}
        });
        this.props.showForm(true);
    }

    deleteRecord = (record) => {
        this.setState({
            selected: {...record, operator_by: this.props.user.id_operator}
        });
        this.props.showConfirmDel(true);
    }

    handleSubmit() {
        var errors = this.state.errMsg;
        this.setState({
            ...this.state,
            loadingForm: true,
        });

        errors.user_id = !this.state.selected.user_id ? "Required" : '';
        errors.shift = !this.state.selected.shift ? "Required" : '';
        errors.date = !this.state.selected.date ? "Required" : '';


        this.setState({errors});

        if (this.validateForm(this.state.errMsg)) {
            const param = {
                ...this.state.selected,
                klinik_id: sessionStorage.getItem('klinikHD')
            }
            this.props.onAdd(param);
        } else {
            console.error('Invalid Form')
        }

        this.setState({
            ...this.state,
            loadingForm: false,
        });
    }

    validateForm(errors) {
        let valid = true;
        Object.values(errors).forEach(
            (val) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    handleDelete() {
        this.props.onDelete(this.state.selected)
    }

    async onchangeSelect(evt) {

        await this.setState({
            ...this.setState,
            selected: {
                ...this.state.selected,
                user_id: evt.value, name: evt.label
            }
        });
    };

    async handleChangeDate(date) {
        this.setState({
            errMsg: this.initSelected
        });

        if (date) {
            const selectedDate = new Date(date);
            const _date = moment(selectedDate).format('YYYY-MM-DD');
            await this.setState({...this.state, selected: {...this.state.selected, date: _date}})
        } else {
            await this.setState({...this.state, selected: {...this.state.selected, date: ''}})
        }

    }

    handleSearch() {
        this.props.onSearch(this.state);
    }

    handleReset() {
        this.handleChangeEndDate('');
        this.handleChangeStartDate('');
        this.setState({keyword: "", shift: ""});
        const param = {
            ...this.initSelected,
            sort_order: this.state.sort_order,
            sort_column: this.state.sort_column,
            page_number: this.state.page_number,
            per_page: this.state.per_page,
            klinik_id: sessionStorage.getItem('klinikHD')
        }
        this.props.onSearch(param);
    }

    handleChangeEndDate(date) {
        this.setState({page_number: 1})
        if (date) {
            const selectedDate = new Date(date);
            const _date = moment(selectedDate).format('YYYY-MM-DD');
            this.setState({end_date: _date})
        } else {
            this.setState({end_date: ''})
        }
        if (!this.state.id_operator) this.setState({id_operator: this.props.user.id_operator});
    }

    handleChangeStartDate(date) {
        this.setState({page_number: 1})
        if (date) {
            const selectedDate = new Date(date);
            const _date = moment(selectedDate).format('YYYY-MM-DD');
            this.setState({start_date: _date})
        } else {
            this.setState({start_date: ''})
        }
        if (!this.state.id_operator) this.setState({id_operator: this.props.user.id_operator});
    }

    isiForm = async (record) => {
        if (record) {
            await sessionStorage.setItem('idPasienHDPedia', record.id);
            await sessionStorage.setItem('idUserHDPedia', record.patient_id);
            await sessionStorage.setItem('level_id', this.props?.user?.id_level);
        }
        this.props.history.push("treatment-form");
    }

    kehadiran = async (record) => {
        this.setState({
            selected: {...record, operator_by: this.props.user.id_operator}
        });
        this.props.showConfirmHadir(true);
    }

    handleKehadiran() {
        const param = {status: 1, treatment_id: this.state.selected.id}
        this.props.onHadir(param)
    }

    renderView(mode, renderDefault, name) {
        // Only for years, months and days view
        if (mode === "time") return renderDefault();

        return (
            <div className="wrapper">
                {renderDefault()}
                <div className="controls">
                    <Button variant="warning" type="button" onClick={() => this.clear(name)}>Clear</Button>
                </div>
            </div>
        );
    }

    handleFilterKlinik(event) {
        const {name, value} = event.target
        var val = value;

        this.setState({
            [name]: val,
            loadingForm: false
        });
        const params = {
            ...this.state,
            klinik_id: val,
        }
        this.props.onFilterKlinik(params);
    }

    clear(name) {
        if (name === "date") {
            this.handleChangeDate();
        }
    }

    render() {
        const {data, dataKlinik, dataShift, user} = this.props;
        const {selected, errMsg, klinik_id} = this.state;
        const permission = user?.permission;

        const columns = [
            {
                key: "no",
                text: "No.",
                width: 20,
                align: "center",
                sortable: false,
                cell: (row, index) => <div
                    style={{textAlign: "center"}}>{((this.state.page_number - 1) * this.state.per_page) + index + 1 + '.'}</div>,
                row: 0
            },
            {
                key: "rm_number",
                text: "No RM",
                align: "center",
                sortable: true,
            },
            {
                key: "date",
                text: "Date",
                align: "center",
                sortable: true,
                cell: record => {
                    return (moment(new Date(record.date)).format('DD-MM-YYYY'))
                }
            },
            {
                key: "name",
                text: "Name",
                align: "center",
                sortable: true,
            },
            {
                key: "shift_name",
                text: "shift_name",
                align: "center",
                sortable: true,
            },
            {
                key: "status_infeksius",
                text: "Status Infeksius",
                align: "center",
                sortable: false,
                cell: record => {

                    return (<div style={{textAlign: "center"}}>
                        <Fragment>
                            {
                                record.status_infeksius != null ?
                                    record.status_infeksius.map(item => (
                                        <>
                                            {
                                                item.label === "Non Hep" ?
                                                    <span
                                                        className="text-error badge badge-success mb-1">{item.label}</span>
                                                    : item.label === "Hep B" && item.label === "Hep C" ?
                                                        <div>
                                                            <span style={{marginRight: '5px'}}
                                                                  className="text-error badge badge-danger mb-1">{item.label}</span>

                                                            <span
                                                                className="text-error badge badge-warning mb-1">{item.label}</span>
                                                        </div>
                                                        : item.label === "Hep B" ?
                                                            <span
                                                                className="text-error badge badge-danger mb-1">{item.label}</span>
                                                            :
                                                            <span
                                                                className="text-error badge badge-warning mb-1">{item.label}</span>

                                            }
                                        </>
                                    ))

                                    : null
                            }
                        </Fragment>

                    </div>)
                },
            },
            {
                key: "status_name",
                text: "Change Status Treatment",
                align: "center",
                sortable: true,
            },
            {
                key: "action",
                text: "Action",
                width: 255,
                align: "center",
                sortable: false,
                cell: record => {

                    return (
                        <div style={{textAlign: "center"}}>
                            <Fragment>
                                <button
                                    className="btn btn-warning btn-xs"
                                    onClick={() => this.kehadiran(record)}
                                    title="Hadir"
                                    style={{marginRight: '5px'}}>
                                    <i className="fa fa-check"></i>
                                </button>
                                <button
                                    className="btn btn-info btn-xs"
                                    onClick={e => this.isiForm(record)}
                                    title="Isi Form"
                                    style={{marginRight: '5px'}}>
                                    <i className="fa fa-file-medical"></i>
                                </button>
                                <button
                                    disabled={user && parseInt(user.id_klinik) > 0 && parseInt(permission?.treatment_edit) > 0 ? false : true}
                                    className="btn btn-xs btn-success"
                                    onClick={e => this.editRecord(record)}
                                    title="Edit"
                                    style={{marginRight: '5px'}}>
                                    <i className="fa fa-edit"></i>
                                </button>
                                <button
                                    disabled={parseInt(permission?.treatment_delete) > 0 ? false : true}
                                    className="btn btn-danger btn-xs"
                                    title="Delete"
                                    onClick={() => this.deleteRecord(record)}>
                                    <i className="fa fa-trash"></i>
                                </button>
                            </Fragment>
                        </div>
                    );
                }
            }
        ];
        const config = {
            key_column: 'id',
            page_size: 10,
            length_menu: [10, 20, 50],
            show_filter: true,
            show_pagination: true,
            pagination: 'advance',
            button: {
                excel: false,
                print: false
            },
            language: {
                loading_text: "Please be patient while data loads..."
            }
        }
        const frmUser = <Form id="myForm">

            <Form.Group controlId="date" style={{marginTop: "10px"}}>
                <Form.Label>Tanggal</Form.Label>
                {errMsg.date ?
                    (<span className="float-right text-error badge badge-danger mb-2">{errMsg.date}
                    </span>) : ''}
                {/* {this.props.errorPriority ?
                    (<span className="float-right text-error badge badge-danger mb-2">{this.props.errorPriority}
                    </span>) : ''} */}
                <Datetime
                    closeOnSelect={true}
                    timeFormat={false}
                    setViewDate={selected.date ? (new Date(selected.date)) : new Date()}
                    value={selected.date ? (new Date(selected.date)) : ''}
                    onChange={this.handleChangeDate}
                    inputProps={{
                        value: selected.date ? (moment(new Date(selected.date)).format('DD-MM-YYYY')) : '',
                        readOnly: true,
                        autoComplete: "off",
                        placeholder: 'Tanggal',
                        name: 'date',
                        className: 'form-control form-control-sm'
                    }}

                    locale="id"
                />
            </Form.Group>

            <Form.Group controlId="user_id" style={{marginTop: "35px"}}>
                <Form.Label>Pasien</Form.Label>
                {errMsg.user_id ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.user_id}
                    </span>) : ''}
                <SelectPasiens
                    myVal={selected.user_id ? ({value: selected.user_id, label: selected.name}) : ''}
                    onChange={this.onchangeSelect.bind((this))}
                />
            </Form.Group>


            <Form.Group controlId="shift" style={{marginTop: "30px"}}>
                <Form.Label>Shift</Form.Label>
                {errMsg.shift ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.shift}
                        </span>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="shift"
                    as="select"
                    value={selected.shift}
                    onChange={this.handleChange.bind(this)}
                >
                    <option value="">- Pilih -</option>
                    {dataShift ? (
                        dataShift.map(function (shift) {
                            return <option
                                selected={shift.id === selected.shift ? true : false}
                                value={shift.id}
                                key={shift.id}>{shift.name}
                            </option>
                        })

                    ) : ''}

                </Form.Control>
            </Form.Group>
            <br/>
            <br/>
            <br/>

        </Form>;

        const contentDelete = <div
            dangerouslySetInnerHTML={{__html: '<div id="caption" style="padding-bottom:20px;">Apakah anda yakin <br/>akan menghapus data ini ?</div>'}}/>;
        const contentHadir = <div
            dangerouslySetInnerHTML={{__html: '<div id="caption" style="padding-bottom:20px;">Apakah anda yakin <br/>pasien ini hadir ?</div>'}}/>;
        return (

            <div className="content-wrapper">
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0">Jadwal Treatment</h1>
                            </div>
                            {/* /.col */}

                        </div>
                        {/* /.row */}
                    </div>
                    {/* /.container-fluid */}
                </div>
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                {/* card start */}
                                <div className="card card-success shadow-lg" style={{"minHeight": "800px"}}>

                                    <div className="card-header card-header-content">
                                        <AppButton
                                            disabled={user && parseInt(user.id_klinik) > 0 && parseInt(permission?.treatment_add) > 0 ? false : true}
                                            isLoading={this.props.isLoading}
                                            theme="info"
                                            onClick={this.discardChanges}
                                            icon='add'
                                        >
                                            Add
                                        </AppButton>

                                    </div>


                                    <div className="card-body">
                                        <Form>
                                            {[1, 7].includes(parseInt(user.id_level)) ?

                                                <Form.Group controlId="id_kliniks">
                                                    <Form.Label>Filter Klinik</Form.Label>
                                                    <Form.Control
                                                        size="sm"
                                                        name="klinik_id"
                                                        as="select"
                                                        value={klinik_id ? klinik_id : ""}
                                                        onChange={this.handleFilterKlinik.bind(this)}>
                                                        <option value="">- Pilih Klinik -</option>
                                                        {dataKlinik ? (
                                                            dataKlinik.map(function (klinik) {
                                                                return <option
                                                                    selected={klinik.id === klinik_id ? true : false}
                                                                    value={klinik.id}
                                                                    key={klinik.id}>{klinik.name}
                                                                </option>
                                                            })

                                                        ) : ''}

                                                    </Form.Control>

                                                </Form.Group>

                                                : ''}
                                            <Form.Group controlId="keyword">
                                                <Form.Label>Filter Name</Form.Label>
                                                <Form.Control
                                                    size="sm"
                                                    autoComplete="off"
                                                    id="keyword"
                                                    name="keyword"
                                                    type="text"
                                                    value={this.state.keyword ? this.state.keyword : ''}
                                                    onChange={this.handleChangeFilter.bind(this)}
                                                    placeholder="Filter Name"/>
                                            </Form.Group>
                                            <Form.Group controlId="start_date">
                                                <Form.Label>Filter Date</Form.Label>
                                                <Datetime
                                                    setViewDate={this.state.start_date ? (new Date(this.state.start_date)) : ''}
                                                    onChange={this.handleChangeStartDate}
                                                    timeFormat={false}
                                                    closeOnSelect={true}
                                                    inputProps={{
                                                        value: this.state.start_date ? (moment(new Date(this.state.start_date)).format('DD-MM-YYYY')) : '',
                                                        readOnly: true,
                                                        autoComplete: "off",
                                                        placeholder: 'Filter Date',
                                                        name: 'start_date',
                                                        className: 'form-control form-control-sm'
                                                    }}

                                                    locale="id"
                                                />
                                            </Form.Group>
                                            <Form.Group controlId="shift">
                                                <Form.Label>Filter Shift</Form.Label>
                                                <Form.Control
                                                    size="sm"
                                                    autoComplete="off"
                                                    id="shift"
                                                    name="shift"
                                                    type="text"
                                                    value={this.state.shift ? this.state.shift : ''}
                                                    onChange={this.handleChangeFilter.bind(this)}
                                                    placeholder="Filter Shift"/>
                                            </Form.Group>

                                            {/*<Form.Row>
                                                <Form.Group as={Col} xs={2} controlId="start_date">
                                                    <Form.Label>Start Date</Form.Label>
                                                    <Datetime
                                                        setViewDate={this.state.start_date ? (new Date(this.state.start_date)) : ''}
                                                        onChange={this.handleChangeStartDate}
                                                        timeFormat={false}
                                                        closeOnSelect={true}
                                                        inputProps={{
                                                            value: this.state.start_date ? (moment(new Date(this.state.start_date)).format('DD-MM-YYYY')) : '',
                                                            readOnly: true,
                                                            autoComplete: "off",
                                                            placeholder: 'Start Date',
                                                            name: 'start_date',
                                                            className: 'form-control form-control-sm'
                                                        }}

                                                        locale="id"
                                                    />
                                                </Form.Group>
                                                <Form.Group as={Col} xs={2} controlId="start_date">
                                                    <Form.Label>End Date</Form.Label>
                                                    <Datetime
                                                        closeOnSelect={true}
                                                        setViewDate={this.state.end_date ? (new Date(this.state.end_date)) : new Date()}
                                                        onChange={this.handleChangeEndDate}
                                                        timeFormat={false}
                                                        inputProps={{
                                                            value: this.state.end_date ? (moment(new Date(this.state.end_date)).format('DD-MM-YYYY')) : '',
                                                            readOnly: true,
                                                            placeholder: 'End Date',
                                                            autoComplete: "off",
                                                            name: 'end_date',
                                                            className: 'form-control form-control-sm'
                                                        }}

                                                        locale="id"
                                                    />
                                                </Form.Group>
                                            </Form.Row>*/}
                                        </Form>
                                        <AppButton
                                            onClick={this.handleSearch.bind(this)}
                                            isLoading={this.props.isLoading}
                                            type="button"
                                            style={{marginRight: 5}}
                                            theme="success">
                                            Search
                                        </AppButton>
                                        <AppButton
                                            onClick={this.handleReset.bind(this)}
                                            isLoading={this.props.isLoading}
                                            type="button"
                                            style={{marginRight: 5}}
                                            theme="warning">
                                            Reset
                                        </AppButton>
                                        <hr/>
                                        <br/>
                                        {data ? (
                                            <ReactDatatable
                                                config={config}
                                                records={data}
                                                columns={columns}
                                                dynamic={true}
                                                onChange={this.tableChangeHandler}
                                                loading={this.props.isLoading}
                                                total_record={this.props.totalData}
                                            />
                                        ) : (<p>No Data ...</p>)}

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <AppModal
                    show={this.props.showFormAdd}
                    form={frmUser}
                    size="xs"
                    backdrop={false}
                    keyboard={false}
                    title="Add/Edit Jadwal Treatment"
                    titleButton="Save change"
                    themeButton="success"
                    handleClose={this.handleClose}
                    isLoading={this.props.isAddLoading ? this.props.isAddLoading : this.state.loadingForm}
                    formSubmit={this.handleSubmit.bind(this)}
                ></AppModal>
                <AppModal
                    show={this.props.showFormDelete}
                    size="xs"
                    form={contentDelete}
                    handleClose={this.handleClose}
                    backdrop="static"
                    keyboard={false}
                    title="Delete"
                    titleButton="Delete"
                    themeButton="danger"
                    isLoading={this.props.isAddLoading}
                    formSubmit={this.handleDelete.bind(this)}
                ></AppModal>
                <AppModal
                    show={this.props.showFormHadir}
                    size="xs"
                    form={contentHadir}
                    handleClose={this.handleClose}
                    backdrop="static"
                    keyboard={false}
                    title="Kehadiran"
                    titleButton="Hadir"
                    themeButton="danger"
                    isLoading={this.props.isAddLoading}
                    formSubmit={this.handleKehadiran.bind(this)}
                ></AppModal>
                {this.props.showFormSuccess ? (<AppSwalSuccess
                    show={this.props.showFormSuccess}
                    title={<div dangerouslySetInnerHTML={{__html: this.props.contentMsg}}/>}
                    type={this.props.tipeSWAL}
                    handleClose={this.handleCloseSwal}
                >
                </AppSwalSuccess>) : ''}
            </div>


        )
    }
}

const mapStateToProps = (state) => ({
    data: state.treatment.data || [],
    dataKlinik: state.klinik.data || [],
    dataShift: state.treatment.dataShift || [],
    totalData: state.treatment.totalData,
    isError: state.treatment.isError,
    isLoading: state.treatment.isLoading,
    showFormAdd: state.treatment.showFormAdd,
    isAddLoading: state.treatment.isAddLoading,
    errorPriority: state.treatment.errorPriority || null,
    contentMsg: state.treatment.contentMsg || null,
    showFormSuccess: state.treatment.showFormSuccess,
    showFormDelete: state.treatment.showFormDelete,
    showFormHadir: state.treatment.showFormHadir,
    tipeSWAL: state.treatment.tipeSWAL,
    user: state.main.currentUser,
});
const mapDispatchToPros = (dispatch) => {
    return {
        onLoad: (param) => {
            dispatch(fetchData(param));
            dispatch(fetchDataShift(param));
            dispatch(fetchDataKlinik());
        },
        onFilterKlinik: (param) => {
            dispatch(fetchData(param));
            dispatch(fetchDataShift(param));
        },
        onSearch: (param) => {
            dispatch(fetchData(param));
        },
        onAdd: (param) => {
            dispatch(addData(param));
        },
        showConfirmDel: (data) => {
            dispatch(confirmDel(data));
        },
        showForm: () => {
            dispatch(addForm());
        },
        onDelete: (param) => {
            dispatch(deleteData(param));
        },
        showConfirmHadir: (data) => {
            dispatch(confirmHadir(data));
        },
        onHadir: (data) => {
            dispatch(changeTreatmentStatus(data));
        },
        closeModal: () => {
            dispatch(closeForm());
        },
        closeSwal: (param) => {
            dispatch(closeForm());
            dispatch(fetchData(param));
        },
        resetError: () => {
            dispatch(clearError());
        },
        closeSwalError: () => {
            dispatch(closeForm());
        }
    }
}
export default connect(mapStateToProps, mapDispatchToPros)(Treatment);