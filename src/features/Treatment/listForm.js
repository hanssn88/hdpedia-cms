import React, {Component} from 'react';
import {connect} from 'react-redux';
import {DaftarTindakanObat} from "./daftarTindakanObat";
import {FormAskepCatatanTerintegrasi} from "./formAskepCatatanTerintegrasi";
import {FormAskepDiagnosaKeperawatan} from "./formAskepDiagnosaKeperawatan";
import {FormAskepHeparinisasi} from "./formAskepHeparinisasi";
import {FormAskepInstruksiMedis} from "./formAskepInstruksiMedis";
import {FormAskepIntervensiKeperawatan} from "./formAskepIntervensiKeperawatan";
import {FormAskepIntervensiKolaborasi} from "./formAskepIntervensiKolaborasi";
import {FormAskepPemeriksaanFisik} from "./formAskepPemeriksaanFisik";
import {FormAskepPemeriksaanPenunjang} from "./formAskepPemeriksaanPenunjang";
import {FormAskepPengkajianKeperawatan} from "./formAskepPengkajianKeperawatan";
import {FormAskepResikoJatuh} from "./formAskepResikoJatuh";
import {FormAskepRiwayatPsikososial} from "./formAskepRiwayatPsikososial";
import {FormAskepTindakanKeperawatan} from "./formAskepTindakanKeperawatan";
import {FormDataPasien} from "./formDataPasien";
import {FormHasilLab} from "./formHasilLab";
import {FormInformConcent} from "./formInformConcent";
import {
    clearError,
    closeForm,
    fetchDataAskepDiagnosaKeperawatan,
    fetchDataAskepHeparinisasi,
    fetchDataAskepInstruksiMedis,
    fetchDataAskepIntervensiKeperawatan,
    fetchDataAskepIntervensiKolaborasi,
    fetchDataAskepPemeriksaanFisik,
    fetchDataAskepPemeriksaanPenunjang,
    fetchDataAskepPengkajianKeperawatan,
    fetchDataAskepResikoJatuh,
    fetchDataAskepRiwayatPsikososial,
    fetchDataAwalPasien,
    fetchDataCatatanTerintegrasi,
    fetchDataTindakanKeperawatan,
    fetchDataTindakanKeperawatanHD,
    fetchDataTindakanObat
} from "./treatmentSlice";


class ListForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            key: 1,
            treatment_id: '',
            patient_id: '',
        }
        this.handleSelectTab = this.handleSelectTab.bind(this);


    }

    componentDidMount() {
        const treatment_id = sessionStorage.getItem('idPasienHDPedia');
        const patient_id = sessionStorage.getItem('idUserHDPedia');
        const reload = sessionStorage.getItem('reload211');
        this.setState({treatment_id});
        this.setState({patient_id});
        if (reload === "true") this.handleSelectTab(2.11);
        if (this.state.key === 1) this.props.onLoad({patient_id});
    }

    handleSelectTab = async (key) => {
        const treatment_id = this.state.treatment_id || sessionStorage.getItem('idPasienHDPedia');
        const patient_id = this.state.patient_id || sessionStorage.getItem('idUserHDPedia');

        if (key === 1) await this.props.onLoad({patient_id});
        if (key === 2) await this.props.onLoadAskepPengkajianKeperawatan({treatment_id});
        if (key === 2.1) await this.props.onLoadAskepPemeriksaanFisik({treatment_id});
        if (key === 2.2) await this.props.onLoadAskepResikoJatuh({treatment_id});
        if (key === 2.3) await this.props.onLoadAskepPemeriksaanPenunjang({treatment_id});
        if (key === 2.4) await this.props.onLoadAskepRiwayatPsikososial({treatment_id});
        if (key === 2.5) await this.props.onLoadAskepDiagnosaKeperawatan({treatment_id});
        if (key === 2.6) await this.props.onLoadAskepIntervensiKeperawatan({treatment_id});
        if (key === 2.7) await this.props.onLoadAskepIntervensiKolaborasi({treatment_id});
        if (key === 2.8) await this.props.onLoadAskepInstruksiMedis({treatment_id});
        if (key === 2.9) await this.props.onLoadAskepHeparinisasi({treatment_id});
        if (key === 2.11) await this.props.onLoadAskepTindakanKeperawatan({treatment_id});
        if (key === 2.12) await this.props.onLoadAskepCatatanTerintegrasi({treatment_id});
        if (key === 2.13) await this.props.onLoadAskepTidakanObat({treatment_id});
        await this.setState({key});
    }


    render() {
        const {
            dataPengkajianKeperawatan,
            dataAskepPemeriksaanFisik,
            dataAskepResikoJatuh,
            dataAskepPemeriksaanPenunjang,
            dataAskepRiwayatPsikososial,
            dataAskepDiagnosaKeperawatan,
            dataAskepIntervensiKeperawatan,
            dataAskepIntervensiKolaborasi,
            dataAskepInstruksiMedis,
            dataAskepHeparinisasi,
            dataTindakanKeperawatanHD,
            dataTindakanKeperawatan,
            dataAwalPasien,
            dataTindakanObat,
            dataCatatanTerintegrasi
        } = this.props;
        const dataTindakanKeperawatans = {dataTindakanKeperawatanHD, dataTindakanKeperawatan};
        return (
            <div className="content-wrapper">
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0">Form Rekam Medis</h1>
                            </div>
                            {/* /.col */}

                        </div>
                        {/* /.row */}
                    </div>
                    {/* /.container-fluid */}
                </div>
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                {/* card start */}
                                <div className="card shadow-lg" style={{"minHeight": "470px"}}>

                                    <div className="card-body">
                                        <div className="row parent p-5">
                                            <button
                                                className={this.state.key === 1 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(1)}>
                                                Form Data Pasien
                                            </button>
                                            {/*  <button
                                                className={this.state.key === 3 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(3)}>
                                                Form Inform Concent
                                            </button>
                                            <button
                                                className={this.state.key === 4 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(4)}>
                                                Form Hasil Lab
                                            </button>
                                            <button
                                                className={this.state.key === 5 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(5)}>
                                                Form Traveling
                                            </button>
                                            <button
                                                className={this.state.key === 6 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(6)}>
                                                Form Data Pindah Pasien
                                            </button>
                                            <button
                                                className={this.state.key === 7 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(7)}>
                                                Form Data Pasien Meninggal
                                            </button>
                                            <button
                                                className={this.state.key === 8 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(8)}>
                                                Form Edukasi Pasien
                                            </button>*/}
                                            <button
                                                className={this.state.key === 2 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(2)}>
                                                Form Askep - Pengkajian Keperawatan
                                            </button>
                                            <button
                                                className={this.state.key === 2.1 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(2.1)}>
                                                Form Askep - Pemeriksaan Fisik
                                            </button>
                                            <button
                                                className={this.state.key === 2.2 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(2.2)}>
                                                Form Askep - Resiko Jatuh
                                            </button>
                                            <button
                                                className={this.state.key === 2.3 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(2.3)}>
                                                Form Askep - Pemeriksaan Penunjang
                                            </button>
                                            <button
                                                className={this.state.key === 2.4 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(2.4)}>
                                                Form Askep - Riwayat Psikososial
                                            </button>
                                            <button
                                                className={this.state.key === 2.5 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(2.5)}>
                                                Form Askep - Diagnosa Keperawatan
                                            </button>
                                            <button
                                                className={this.state.key === 2.6 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(2.6)}>
                                                Form Askep - Intervensi Keperawatan
                                            </button>
                                            <button
                                                className={this.state.key === 2.7 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(2.7)}>
                                                Form Askep - Intervensi Kolaborasi
                                            </button>
                                            <button
                                                className={this.state.key === 2.8 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(2.8)}>
                                                Form Askep - Instruksi Medis
                                            </button>
                                            <button
                                                className={this.state.key === 2.9 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(2.9)}>
                                                Form Askep - Heparinisasi, Akses Vaskuler, Balance Cairan
                                            </button>
                                            <button
                                                className={this.state.key === 2.11 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(2.11)}>
                                                Form Askep - Tindakan Keperawatan
                                            </button>
                                            <button
                                                className={this.state.key === 2.12 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(2.12)}>
                                                Catatan Terintegrasi
                                            </button>
                                            <button
                                                className={this.state.key === 2.13 ? "button-66 col-sm-2 button-66-active" : "button-66 col-sm-2"}
                                                onClick={(e) => this.handleSelectTab(2.13)}>
                                                Daftar Tindakan Dan Obat Pasien
                                            </button>
                                        </div>

                                        <hr/>
                                        <div className="tab-content">

                                            {this.state.key === 1 && (<FormDataPasien data={dataAwalPasien}/>)}
                                            {this.state.key === 2 && (
                                                <FormAskepPengkajianKeperawatan data={dataPengkajianKeperawatan}/>)}
                                            {this.state.key === 2.1 && (
                                                <FormAskepPemeriksaanFisik data={dataAskepPemeriksaanFisik}/>)}
                                            {this.state.key === 2.2 && (
                                                <FormAskepResikoJatuh data={dataAskepResikoJatuh}/>)}
                                            {this.state.key === 2.3 && (
                                                <FormAskepPemeriksaanPenunjang data={dataAskepPemeriksaanPenunjang}/>)}
                                            {this.state.key === 2.4 && (
                                                <FormAskepRiwayatPsikososial data={dataAskepRiwayatPsikososial}/>)}
                                            {this.state.key === 2.5 && (
                                                <FormAskepDiagnosaKeperawatan data={dataAskepDiagnosaKeperawatan}/>)}
                                            {this.state.key === 2.6 && (<FormAskepIntervensiKeperawatan
                                                data={dataAskepIntervensiKeperawatan}/>)}
                                            {this.state.key === 2.7 && (
                                                <FormAskepIntervensiKolaborasi data={dataAskepIntervensiKolaborasi}/>)}
                                            {this.state.key === 2.8 && (
                                                <FormAskepInstruksiMedis data={dataAskepInstruksiMedis}/>)}
                                            {this.state.key === 2.9 && (
                                                <FormAskepHeparinisasi data={dataAskepHeparinisasi}/>)}
                                            {this.state.key === 2.11 && (
                                                <FormAskepTindakanKeperawatan data={dataTindakanKeperawatans}/>)}
                                            {this.state.key === 2.12 && (
                                                <FormAskepCatatanTerintegrasi data={dataCatatanTerintegrasi}/>)}
                                            {this.state.key === 2.13 && (
                                                <DaftarTindakanObat data={dataTindakanObat}/>)}
                                            {this.state.key === 3 && (<FormInformConcent/>)}
                                            {this.state.key === 4 && (<FormHasilLab/>)}
                                            {this.state.key === 5 && (<h3>Form Traveling(View only)</h3>)}
                                            {this.state.key === 6 && (<h3>Form Data Pasien Pindah</h3>)}
                                            {this.state.key === 7 && (<h3>Form Data Pasien Meninggal</h3>)}
                                            {this.state.key === 8 && (<h3>Form Edukasi Pasien</h3>)}

                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>


        )
    }
}

const mapStateToProps = (state) => ({
    dataPengkajianKeperawatan: state.treatment.dataPengkajianKeperawatan || {},
    dataAskepPemeriksaanFisik: state.treatment.dataAskepPemeriksaanFisik || {},
    dataAskepResikoJatuh: state.treatment.dataAskepResikoJatuh || {},
    dataAskepPemeriksaanPenunjang: state.treatment.dataAskepPemeriksaanPenunjang || {},
    dataAskepRiwayatPsikososial: state.treatment.dataAskepRiwayatPsikososial || {},
    dataAskepDiagnosaKeperawatan: state.treatment.dataAskepDiagnosaKeperawatan || {},
    dataAskepIntervensiKeperawatan: state.treatment.dataAskepIntervensiKeperawatan || {},
    dataAskepIntervensiKolaborasi: state.treatment.dataAskepIntervensiKolaborasi || {},
    dataAskepInstruksiMedis: state.treatment.dataAskepInstruksiMedis || {},
    dataAskepHeparinisasi: state.treatment.dataAskepHeparinisasi || {},
    dataTindakanKeperawatanHD: state.treatment.dataTindakanKeperawatanHD || {},
    dataTindakanKeperawatan: state.treatment.dataTindakanKeperawatan || {},
    dataAwalPasien: state.treatment.dataAwalPasien || {},
    dataCatatanTerintegrasi: state.treatment.dataCatatanTerintegrasi || {},
    dataTindakanObat: state.treatment.dataTindakanObat || {},

    isError: state.treatment.isError,
    isLoading: state.treatment.isLoading,
    showFormAdd: state.treatment.showFormAdd,
    isAddLoading: state.treatment.isAddLoading,
    errorPriority: state.treatment.errorPriority || null,
    contentMsg: state.treatment.contentMsg || null,
    showFormSuccess: state.treatment.showFormSuccess,
    showFormDelete: state.treatment.showFormDelete,
    tipeSWAL: state.treatment.tipeSWAL,
    user: state.main.currentUser,
});
const mapDispatchToPros = (dispatch) => {
    return {
        onLoad: (param) => {
            dispatch(fetchDataAwalPasien(param));
        },
        onLoadAskepPengkajianKeperawatan: (param) => {
            dispatch(fetchDataAskepPengkajianKeperawatan(param));
        },
        onLoadAskepPemeriksaanFisik: (param) => {
            dispatch(fetchDataAskepPemeriksaanFisik(param));
        },
        onLoadAskepResikoJatuh: (param) => {
            dispatch(fetchDataAskepResikoJatuh(param));
        },
        onLoadAskepPemeriksaanPenunjang: (param) => {
            dispatch(fetchDataAskepPemeriksaanPenunjang(param));
        },
        onLoadAskepRiwayatPsikososial: (param) => {
            dispatch(fetchDataAskepRiwayatPsikososial(param));
        },
        onLoadAskepDiagnosaKeperawatan: (param) => {
            dispatch(fetchDataAskepDiagnosaKeperawatan(param));
        },
        onLoadAskepIntervensiKeperawatan: (param) => {
            dispatch(fetchDataAskepIntervensiKeperawatan(param));
        },
        onLoadAskepIntervensiKolaborasi: (param) => {
            dispatch(fetchDataAskepIntervensiKolaborasi(param));
        },
        onLoadAskepInstruksiMedis: (param) => {
            dispatch(fetchDataAskepInstruksiMedis(param));
        },
        onLoadAskepHeparinisasi: (param) => {
            dispatch(fetchDataAskepHeparinisasi(param));
        },
        onLoadAskepTindakanKeperawatan: (param) => {
            dispatch(fetchDataTindakanKeperawatanHD(param));
            dispatch(fetchDataTindakanKeperawatan(param));
        },
        onLoadAskepCatatanTerintegrasi: (param) => {
            dispatch(fetchDataCatatanTerintegrasi(param));
        },
        onLoadAskepTidakanObat: (param) => {
            dispatch(fetchDataTindakanObat(param));
        },
        closeModal: () => {
            dispatch(closeForm());
        },
        closeSwal: (param) => {
            dispatch(closeForm());
        },
        resetError: () => {
            dispatch(clearError());
        },

        closeSwalError: () => {
            dispatch(closeForm());
        }
    }
}
export default connect(mapStateToProps, mapDispatchToPros)(ListForm);