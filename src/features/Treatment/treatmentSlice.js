import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import axios from 'axios';


const API_URL = process.env.REACT_APP_URL_API;
const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;

export const fetchData = createAsyncThunk(
    'treatment/get',
    async (param, thunkAPI) => {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/treatment_cms', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    res = {
                        data: data,
                        totalData: dataa.total_data
                    }
                    return res;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return thunkAPI.rejectWithValue(res);
                }
            } else {

                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const addData = createAsyncThunk(
    'treatment/storeData',
    async (param, thunkAPI) => {
        let res = {};
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/add_treatment', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            //let data = '';
            let _data = response;
            if (response.status === 200) {
                let dataa = _data.data;
                //data = dataa.data;
                if (dataa.err_code === '00') {
                    return dataa;
                } else {
                    res = {
                        isAddLoading: false,
                        showFormSuccess: false,
                        showFormAdd: true,
                        err_msg: dataa.err_msg,
                        contentMsg: null,
                    }

                    return thunkAPI.rejectWithValue(res);
                }
            } else {
                res = {
                    isAddLoading: false,
                    showFormSuccess: true,
                    showFormAdd: false,
                    contentMsg: "<div style='font-size:20px; text-align:center;'><strong>Failed</strong>, Something wrong</div>",
                    err_msg: null
                }
                console.log('Error', _data);
                return thunkAPI.rejectWithValue(res);
            }
        } catch (e) {
            res = {
                isAddLoading: false,
                showFormSuccess: true,
                showFormAdd: false,
                contentMsg: "<div style='font-size:20px; text-align:center;'><strong>Failed</strong>, Something wrong</div>",
                err_msg: null
            }
            console.log('Error catch', e.response.data);
            return thunkAPI.rejectWithValue(res);
        }
    }
);

export const deleteData = createAsyncThunk(
    'treatment/delete',
    async (param, thunkAPI) => {
        let res = {};
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/delete_treatment', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    return dataa;
                } else {
                    res = {
                        isAddLoading: false,
                        showFormSuccess: true,
                        showFormDelete: false,
                        contentMsg: "<div style='font-size:20px; text-align:center;'><strong>Failed</strong>, Something wrong</div>",
                        err_msg: dataa.err_msg
                    }
                    console.log('Error err_code', data);

                    return thunkAPI.rejectWithValue(res);
                }
            } else {
                res = {
                    isAddLoading: false,
                    showFormSuccess: true,
                    showFormDelete: false,
                    contentMsg: "<div style='font-size:20px; text-align:center;'><strong>Failed</strong>, Something wrong</div>",
                    err_msg: null
                }
                console.log('Error', _data);
                return thunkAPI.rejectWithValue(res);
            }
        } catch (e) {
            res = {
                isAddLoading: false,
                showFormSuccess: true,
                showFormDelete: false,
                contentMsg: "<div style='font-size:20px; text-align:center;'><strong>Failed</strong>, Something wrong</div>",
                err_msg: null
            }
            console.log('Error catch', e.response.data);
            return thunkAPI.rejectWithValue(res);
        }
    }
);

export const changeTreatmentStatus = createAsyncThunk(
    'treatment/changeTreatmentStatus',
    async (param, thunkAPI) => {
        let res = {};
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/dokter/change_treatment_status', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    return dataa;
                } else {
                    res = {
                        isAddLoading: false,
                        showFormSuccess: true,
                        showFormDelete: false,
                        contentMsg: "<div style='font-size:20px; text-align:center;'><strong>Failed</strong>, Something wrong</div>",
                        err_msg: dataa.err_msg
                    }
                    console.log('Error err_code', data);

                    return thunkAPI.rejectWithValue(res);
                }
            } else {
                res = {
                    isAddLoading: false,
                    showFormSuccess: true,
                    showFormDelete: false,
                    contentMsg: "<div style='font-size:20px; text-align:center;'><strong>Failed</strong>, Something wrong</div>",
                    err_msg: null
                }
                console.log('Error', _data);
                return thunkAPI.rejectWithValue(res);
            }
        } catch (e) {
            res = {
                isAddLoading: false,
                showFormSuccess: true,
                showFormDelete: false,
                contentMsg: "<div style='font-size:20px; text-align:center;'><strong>Failed</strong>, Something wrong</div>",
                err_msg: null
            }
            console.log('Error catch', e.response.data);
            return thunkAPI.rejectWithValue(res);
        }
    }
);

export const fetchDataShift = createAsyncThunk(
    'treatment/getShift',
    async (param, thunkAPI) => {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/get_shift', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    res = {
                        data: data,
                    }
                    return res;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return thunkAPI.rejectWithValue(res);
                }
            } else {

                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const fetchDataAskepPengkajianKeperawatan = createAsyncThunk(
    'treatment/getDataAskepPengkajianKeperawatan',
    async (param, thunkAPI) => {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/dokter/get_pengkajian_keperawatan', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    res = {
                        data: data,
                        totalData: dataa.total_data
                    }
                    return res;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return thunkAPI.rejectWithValue(res);
                }
            } else {

                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const fetchDataAskepPemeriksaanFisik = createAsyncThunk(
    'treatment/getDataAskepPemeriksaanFisik',
    async (param, thunkAPI) => {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/dokter/get_pemeriksaan_fisik', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    res = {
                        data: data,
                        totalData: dataa.total_data
                    }
                    return res;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return thunkAPI.rejectWithValue(res);
                }
            } else {

                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const fetchDataAskepResikoJatuh = createAsyncThunk(
    'treatment/getDataAskepResikoJatuh',
    async (param, thunkAPI) => {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/dokter/get_resiko_jatuh', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    res = {
                        data: data,
                        totalData: dataa.total_data
                    }
                    return res;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return thunkAPI.rejectWithValue(res);
                }
            } else {

                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const fetchDataAskepPemeriksaanPenunjang = createAsyncThunk(
    'treatment/getDataAskepDataAskepPemeriksaanPenunjang',
    async (param, thunkAPI) => {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/dokter/get_pemeriksaan_penunjang', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    res = {
                        data: data,
                        totalData: dataa.total_data
                    }
                    return res;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return thunkAPI.rejectWithValue(res);
                }
            } else {

                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const fetchDataAskepRiwayatPsikososial = createAsyncThunk(
    'treatment/getDataAskepRiwayatPsikososial',
    async (param, thunkAPI) => {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/dokter/get_riwayat_psikososial', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    res = {
                        data: data,
                        totalData: dataa.total_data
                    }
                    return res;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return thunkAPI.rejectWithValue(res);
                }
            } else {

                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const fetchDataAskepDiagnosaKeperawatan = createAsyncThunk(
    'treatment/getDataAskepDiagnosaKeperawatan',
    async (param, thunkAPI) => {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/dokter/get_diagnosa_keperawatan', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    res = {
                        data: data,
                        totalData: dataa.total_data
                    }
                    return res;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return thunkAPI.rejectWithValue(res);
                }
            } else {

                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const fetchDataAskepIntervensiKeperawatan = createAsyncThunk(
    'treatment/getDataAskepIntervensiKeperawatan',
    async (param, thunkAPI) => {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/dokter/get_intervensi_keperawatan', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    res = {
                        data: data,
                        totalData: dataa.total_data
                    }
                    return res;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return thunkAPI.rejectWithValue(res);
                }
            } else {

                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const fetchDataAskepIntervensiKolaborasi = createAsyncThunk(
    'treatment/getDataAskepIntervensiKolaborasi',
    async (param, thunkAPI) => {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/dokter/get_intervensi_kolaborasi', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    res = {
                        data: data,
                        totalData: dataa.total_data
                    }
                    return res;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return thunkAPI.rejectWithValue(res);
                }
            } else {

                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const fetchDataAskepInstruksiMedis = createAsyncThunk(
    'treatment/getDataAskepInstruksiMedis',
    async (param, thunkAPI) => {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/dokter/get_instruksi_medis', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    res = {
                        data: data,
                        totalData: dataa.total_data
                    }
                    return res;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return thunkAPI.rejectWithValue(res);
                }
            } else {

                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const fetchDataAskepHeparinisasi = createAsyncThunk(
    'treatment/getDataAskepHeparinisasi',
    async (param, thunkAPI) => {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/dokter/get_heparinisasi', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    res = {
                        data: data,
                        totalData: dataa.total_data
                    }
                    return res;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return thunkAPI.rejectWithValue(res);
                }
            } else {

                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const fetchDataTindakanKeperawatan = createAsyncThunk(
    'treatment/getDataTindakanKeperawatan',
    async (param, thunkAPI) => {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/dokter/get_tindakan_keperawatan', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    res = {
                        data: data,
                        totalData: dataa.total_data
                    }
                    return res;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return thunkAPI.rejectWithValue(res);
                }
            } else {

                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const fetchDataTindakanKeperawatanHD = createAsyncThunk(
    'treatment/getDataTindakanKeperawatanHD',
    async (param, thunkAPI) => {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/dokter/get_hd_table', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    res = {
                        data: data,
                        totalData: dataa.total_data
                    }
                    return res;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return thunkAPI.rejectWithValue(res);
                }
            } else {

                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const fetchDataAwalPasien = createAsyncThunk(
    'treatment/getDataAwalPatient',
    async (param, thunkAPI) => {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/dokter/data_awal_patient', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    res = {
                        data: data,
                        totalData: dataa.total_data
                    }
                    return res;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return thunkAPI.rejectWithValue(res);
                }
            } else {

                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const fetchDataCatatanTerintegrasi = createAsyncThunk(
    'treatment/getDataCatatanTerintegrasi',
    async (param, thunkAPI) => {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/dokter/get_catatan_terintegrasi', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    res = {
                        data: data,
                        totalData: dataa.total_data
                    }
                    return res;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return thunkAPI.rejectWithValue(res);
                }
            } else {

                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);


export const fetchDataTindakanObat = createAsyncThunk(
    'dokter/getDataTindakanObat',
    async (param, thunkAPI) => {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/dokter/get_tindakan', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
            let data = '';
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa.data;
                if (dataa.err_code === '00') {
                    res = {
                        data: data,
                        totalData: dataa.total_data
                    }
                    return res;
                } else {
                    res = {
                        err_msg: dataa.err_msg
                    }

                    return thunkAPI.rejectWithValue(res);
                }
            } else {

                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {
            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);


const initialState = {
    data: [],
    dataShift: [],
    dataPengkajianKeperawatan: {},
    dataAskepPemeriksaanFisik: {},
    dataAskepResikoJatuh: {},
    dataAskepPemeriksaanPenunjang: {},
    dataAskepRiwayatPsikososial: {},
    dataAskepDiagnosaKeperawatan: {},
    dataAskepIntervensiKeperawatan: {},
    dataAskepIntervensiKolaborasi: {},
    dataAskepInstruksiMedis: {},
    dataAskepHeparinisasi: {},
    dataTindakanKeperawatanHD: {},
    dataTindakanKeperawatan: {},
    dataTindakanObat: {},
    dataAwalPasien: {},
    dataCatatanTerintegrasi: {},
    totalData: 0,
    isError: false,
    isLoading: false,
    isAddLoading: false,
    errorPriority: null,
    contentMsg: null,
    showFormAdd: false,
    showFormSuccess: false,
    showFormDelete: false,
    showFormHadir: false,
    tipeSWAL: "success"
};


export const treatmentSlice = createSlice({
    name: 'treatment',
    initialState,
    reducers: {
        addForm: (state) => {
            state.isAddLoading = false;
            state.showFormAdd = true;
            state.errorPriority = null;
            return state;
        },
        clearError: (state) => {
            state.isAddLoading = false;
            state.errorPriority = null;
            return state;
        },
        closeForm: (state) => {
            state.showFormAdd = false;
            state.showFormDelete = false;
            state.showFormHadir = false;
            state.errorPriority = null;
            state.showFormSuccess = false;
        },
        confirmDel: (state) => {
            state.isAddLoading = false;
            state.showFormDelete = true;
            state.errorPriority = false;
            return state;
        },
        confirmHadir: (state) => {
            state.isAddLoading = false;
            state.showFormHadir = true;
            state.errorPriority = false;
            return state;
        }
    },
    extraReducers: {
        [fetchData.fulfilled]: (state, {payload}) => {
            state.totalData = payload.totalData;
            state.data = payload.data;
            state.isLoading = false;
            state.isError = false;
        },
        [fetchData.rejected]: (state, {payload}) => {
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [fetchData.pending]: (state) => {
            state.totalData = 0;
            state.data = [];
            state.isLoading = true;

        },
        [addData.fulfilled]: (state) => {
            state.isError = false;
            state.tipeSWAL = "success";
            state.showFormSuccess = true;
            state.showFormAdd = false;
            state.isAddLoading = false;
            state.contentMsg = "<div style='font-size:20px; text-align:center;'><strong>Success</strong>, Data berhasil disimpan</div>";
        },
        [addData.rejected]: (state, {payload}) => {
            console.log(payload);
            state.isAddLoading = payload !== undefined ? payload.isAddLoading : false;
            state.showFormSuccess = payload !== undefined ? payload.showFormSuccess : true;
            state.showFormAdd = payload !== undefined ? payload.showFormAdd : false;
            state.tipeSWAL = "error";
            state.contentMsg = payload !== undefined ? payload.contentMsg : "<div style='font-size:20px; text-align:center;'><strong>Failed</strong>, Something wrong</div>";
            state.isError = true;
            state.errorPriority = payload !== undefined ? payload.err_msg : null;
        },
        [addData.pending]: (state) => {
            state.isAddLoading = true;
        },
        [deleteData.fulfilled]: (state) => {
            state.showFormDelete = false;
            state.isError = false;
            state.tipeSWAL = "success";
            state.showFormSuccess = true;
            state.isAddLoading = false;
            state.contentMsg = "<div style='font-size:20px; text-align:center;'><strong>Success</strong>, Data berhasil dihapus</div>";
        },
        [deleteData.rejected]: (state, {payload}) => {
            state.isAddLoading = payload !== undefined ? payload.isAddLoading : false;
            state.showFormSuccess = payload !== undefined ? payload.showFormSuccess : true;
            state.showFormDelete = payload !== undefined ? payload.showFormDelete : false;
            state.tipeSWAL = "error";
            state.contentMsg = payload !== undefined ? payload.contentMsg : "<div style='font-size:20px; text-align:center;'><strong>Failed</strong>, Something wrong</div>";
            state.isError = true;
            state.errorPriority = payload !== undefined ? payload.err_msg : null;
        },
        [deleteData.pending]: (state) => {
            state.isAddLoading = true;
        },
        [fetchDataShift.fulfilled]: (state, {payload}) => {
            state.dataShift = payload.data;
            state.isLoading = false;
            state.isError = false;
        },
        [fetchDataShift.rejected]: (state, {payload}) => {
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [fetchDataShift.pending]: (state) => {
            state.dataShift = [];
            state.isLoading = true;
        },
        [fetchDataAskepPengkajianKeperawatan.fulfilled]: (state, {payload}) => {
            state.dataPengkajianKeperawatan = payload.data;
            state.isLoading = false;
            state.isError = false;
        },
        [fetchDataAskepPengkajianKeperawatan.rejected]: (state, {payload}) => {
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [fetchDataAskepPengkajianKeperawatan.pending]: (state) => {
            state.dataPengkajianKeperawatan = {};
            state.isLoading = true;
        },
        [fetchDataAskepPemeriksaanFisik.fulfilled]: (state, {payload}) => {
            state.dataAskepPemeriksaanFisik = payload.data;
            state.isLoading = false;
            state.isError = false;
        },
        [fetchDataAskepPemeriksaanFisik.rejected]: (state, {payload}) => {
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [fetchDataAskepPemeriksaanFisik.pending]: (state) => {
            state.dataAskepPemeriksaanFisik = {};
            state.isLoading = true;
        },
        [fetchDataAskepResikoJatuh.fulfilled]: (state, {payload}) => {
            state.dataAskepResikoJatuh = payload.data;
            state.isLoading = false;
            state.isError = false;
        },
        [fetchDataAskepResikoJatuh.rejected]: (state, {payload}) => {
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [fetchDataAskepResikoJatuh.pending]: (state) => {
            state.dataAskepResikoJatuh = {};
            state.isLoading = true;
        },
        [fetchDataAskepPemeriksaanPenunjang.fulfilled]: (state, {payload}) => {
            state.dataAskepPemeriksaanPenunjang = payload.data;
            state.isLoading = false;
            state.isError = false;
        },
        [fetchDataAskepPemeriksaanPenunjang.rejected]: (state, {payload}) => {
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [fetchDataAskepPemeriksaanPenunjang.pending]: (state) => {
            state.dataAskepPemeriksaanPenunjang = {};
            state.isLoading = true;
        },
        [fetchDataAskepRiwayatPsikososial.fulfilled]: (state, {payload}) => {
            state.dataAskepRiwayatPsikososial = payload.data;
            state.isLoading = false;
            state.isError = false;
        },
        [fetchDataAskepRiwayatPsikososial.rejected]: (state, {payload}) => {
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [fetchDataAskepRiwayatPsikososial.pending]: (state) => {
            state.dataAskepRiwayatPsikososial = {};
            state.isLoading = true;
        },
        [fetchDataAskepDiagnosaKeperawatan.fulfilled]: (state, {payload}) => {
            state.dataAskepDiagnosaKeperawatan = payload.data;
            state.isLoading = false;
            state.isError = false;
        },
        [fetchDataAskepDiagnosaKeperawatan.rejected]: (state, {payload}) => {
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [fetchDataAskepDiagnosaKeperawatan.pending]: (state) => {
            state.dataAskepDiagnosaKeperawatan = {};
            state.isLoading = true;
        },
        [fetchDataAskepIntervensiKeperawatan.fulfilled]: (state, {payload}) => {
            state.dataAskepIntervensiKeperawatan = payload.data;
            state.isLoading = false;
            state.isError = false;
        },
        [fetchDataAskepIntervensiKeperawatan.rejected]: (state, {payload}) => {
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [fetchDataAskepIntervensiKeperawatan.pending]: (state) => {
            state.dataAskepIntervensiKeperawatan = {};
            state.isLoading = true;
        },
        [fetchDataAskepIntervensiKolaborasi.fulfilled]: (state, {payload}) => {
            state.dataAskepIntervensiKolaborasi = payload.data;
            state.isLoading = false;
            state.isError = false;
        },
        [fetchDataAskepIntervensiKolaborasi.rejected]: (state, {payload}) => {
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [fetchDataAskepIntervensiKolaborasi.pending]: (state) => {
            state.dataAskepIntervensiKolaborasi = {};
            state.isLoading = true;
        },
        [fetchDataAskepInstruksiMedis.fulfilled]: (state, {payload}) => {
            state.dataAskepInstruksiMedis = payload.data;
            state.isLoading = false;
            state.isError = false;
        },
        [fetchDataAskepInstruksiMedis.rejected]: (state, {payload}) => {
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [fetchDataAskepInstruksiMedis.pending]: (state) => {
            state.dataAskepInstruksiMedis = {};
            state.isLoading = true;
        },
        [fetchDataAskepHeparinisasi.fulfilled]: (state, {payload}) => {
            state.dataAskepHeparinisasi = payload.data;
            state.isLoading = false;
            state.isError = false;
        },
        [fetchDataAskepHeparinisasi.rejected]: (state, {payload}) => {
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [fetchDataAskepHeparinisasi.pending]: (state) => {
            state.dataAskepHeparinisasi = {};
            state.isLoading = true;
        },
        [fetchDataTindakanKeperawatanHD.fulfilled]: (state, {payload}) => {
            state.dataTindakanKeperawatanHD = payload.data;
            state.isLoading = false;
            state.isError = false;
        },
        [fetchDataTindakanKeperawatanHD.rejected]: (state, {payload}) => {
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [fetchDataTindakanKeperawatanHD.pending]: (state) => {
            state.dataTindakanKeperawatanHD = {};
            state.isLoading = true;
        },
        [fetchDataTindakanKeperawatan.fulfilled]: (state, {payload}) => {
            state.dataTindakanKeperawatan = payload.data;
            state.isLoading = false;
            state.isError = false;
        },
        [fetchDataTindakanKeperawatan.rejected]: (state, {payload}) => {
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [fetchDataTindakanKeperawatan.pending]: (state) => {
            state.dataTindakanKeperawatan = {};
            state.isLoading = true;
        },
        [fetchDataAwalPasien.fulfilled]: (state, {payload}) => {
            state.dataAwalPasien = payload.data;
            state.isLoading = false;
            state.isError = false;
        },
        [fetchDataAwalPasien.rejected]: (state, {payload}) => {
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [fetchDataAwalPasien.pending]: (state) => {
            state.dataAwalPasien = {};
            state.isLoading = true;
        },
        [fetchDataCatatanTerintegrasi.fulfilled]: (state, {payload}) => {
            state.dataCatatanTerintegrasi = payload.data;
            state.isLoading = false;
            state.isError = false;
        },
        [fetchDataCatatanTerintegrasi.rejected]: (state, {payload}) => {
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [fetchDataCatatanTerintegrasi.pending]: (state) => {
            state.dataTindakanObat = {};
            state.isLoading = true;
        },
        [fetchDataTindakanObat.fulfilled]: (state, {payload}) => {
            state.dataTindakanObat = payload.data;
            state.isLoading = false;
            state.isError = false;
        },
        [fetchDataTindakanObat.rejected]: (state, {payload}) => {
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = payload !== undefined ? payload.err_msg : null;
        },
        [fetchDataTindakanObat.pending]: (state) => {
            state.dataTindakanObat = {};
            state.isLoading = true;
        },
        [changeTreatmentStatus.fulfilled]: (state) => {
            state.showFormHadir = false;
            state.isError = false;
            state.tipeSWAL = "success";
            state.showFormSuccess = true;
            state.isAddLoading = false;
            state.contentMsg = "<div style='font-size:20px; text-align:center;'><strong>Success</strong>, Data berhasil diupdate</div>";
        },
        [changeTreatmentStatus.rejected]: (state, {payload}) => {
            state.isAddLoading = payload !== undefined ? payload.isAddLoading : false;
            state.showFormSuccess = payload !== undefined ? payload.showFormSuccess : true;
            state.showFormHadir = payload !== undefined ? payload.showFormDelete : false;
            state.tipeSWAL = "error";
            state.contentMsg = payload !== undefined ? payload.contentMsg : "<div style='font-size:20px; text-align:center;'><strong>Failed</strong>, Something wrong</div>";
            state.isError = true;
            state.errorPriority = payload !== undefined ? payload.err_msg : null;
        },
        [changeTreatmentStatus.pending]: (state) => {
            state.isAddLoading = true;
        },
    }
})

export const {addForm, clearError, confirmDel, closeForm, confirmHadir} = treatmentSlice.actions;
export const userSelector = (state) => state.treatment;
//export default mainSlice.reducer;