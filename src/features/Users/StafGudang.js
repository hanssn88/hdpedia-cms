import React, {Component, Fragment} from 'react'
import {Col, Form} from 'react-bootstrap';
import {connect} from 'react-redux';
import {addData, addForm, clearError, closeForm, confirmDel, deleteData, fetchData} from './usersSlice'
import ReactDatatable from '@ashvin27/react-datatable';
import AppModal from '../../components/modal/MyModal';
import AppButton from '../../components/button/Button';
import {AppSwalSuccess} from '../../components/modal/SwalSuccess';
import {fetchData as fetchDataKlinik} from '../Klinik/klinikSlice'

class StafGudang extends Component {

    constructor(props) {
        super(props);
        this.initSelected = {
            operator_by: '',
            name: '',
            pass: '',
            id_levels: 5,
            id_klinik: '',
            email: '',
            id_operator: '',
        }
        this.state = {
            sort_order: "ASC",
            sort_column: "name",
            keyword: '',
            page_number: 1,
            id_levels: 5,
            per_page: 10,
            selected: this.initSelected,
            errMsg: this.initSelected,
            loadingForm: false,
        }
    }

    componentDidMount() {
        this.fetchProfileAdmin();
        this.props.onLoad(this.state);
    }

    fetchProfileAdmin = () => {
        const token = localStorage.getItem(process.env.REACT_APP_TOKEN_LOGIN);
        if (!token) {
            alert('session invalid');
            window.location.reload();
        }
    }

    handleClose = () => {
        this.props.closeModal();
        this.setState({
            errMsg: {},
            selected: this.initSelected,
            loadingForm: false
        });
    };

    tableChangeHandler = (data) => {
        let queryString = this.state;
        Object.keys(data).map((key) => {
            if (key === "sort_order" && data[key]) {
                queryString.sort_order = data[key].order;
                queryString.sort_column = data[key].column;
            }
            if (key === "page_number") {
                queryString.page_number = data[key];
            }
            if (key === "page_size") {
                queryString.per_page = data[key];
            }
            if (key === "filter_value") {
                queryString.keyword = data[key];
            }
            return true;
        });
        this.props.onLoad(this.state);
    }

    handleChange(event) {
        const {name, value} = event.target
        this.setState({
            selected: {
                ...this.state.selected,
                [name]: value
            }
        });
        this.setState({errMsg: this.initSelected});
        this.props.resetError();
        if (!this.state.selected.operator_by) this.setState({
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
    }

    discardChanges = () => {
        this.setState({errMsg: {}, selected: this.initSelected, loadingForm: false});
        if (!this.state.selected.operator_by) this.setState({
            selected: {
                ...this.state.selected,
                status: 1,
                operator_by: this.props.user.id_operator
            }
        });
        this.props.showForm();
    }

    editRecord = (record) => {
        this.setState({
            loadingForm: false,
            errMsg: this.initSelected,
            selected: {...record, operator_by: this.props.user.id_operator}
        });
        this.props.showForm(true);
    }

    deleteRecord = (record) => {
        this.setState({
            selected: {...record, operator_by: this.props.user.id_operator}
        });
        this.props.showConfirmDel(true);
    }

    handleSubmit() {
        var errors = this.state.errMsg;
        this.setState({
            ...this.state,
            loadingForm: true,
        });
        const patternEmail = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

        const uppercaseRegExp = new RegExp(/(?=.*?[A-Z])/);
        const lowercaseRegExp = new RegExp(/(?=.*?[a-z])/);
        const digitsRegExp = new RegExp(/(?=.*?[0-9])/);
        //const specialCharRegExp = /(?=.*?[#?!@$%^&*-])/;
        const minLengthRegExp = new RegExp(/.{8,}/);
        errors.name = !this.state.selected.name ? "Name required" : '';
        // errors.id_levels = !this.state.selected.id_levels ? "Level required" : '';
        errors.email = !this.state.selected.email ? "Email required" : '';
        // errors.pass = !this.state.selected.pass ? "Password required" : '';
        errors.id_klinik = this.state.selected.id_levels > 1 && !this.state.selected.id_klinik ? "Klinik required" : '';
        if (this.state.selected.email) {
            errors.email = !patternEmail.test(this.state.selected.email) ? "Email invalid" : errors.email;
        }
        if (this.state.selected.pass) {
            const minLengthPassword = minLengthRegExp.test(this.state.selected.pass);
            const digitsPassword = digitsRegExp.test(this.state.selected.pass);
            const uppercasePassword = uppercaseRegExp.test(this.state.selected.pass);
            const lowercasePassword = lowercaseRegExp.test(this.state.selected.pass);
            errors.pass = !uppercasePassword ? "At least one Uppercase" : '';
            errors.pass = !lowercasePassword ? "At least one Lowercase" : errors.pass;
            errors.pass = !digitsPassword ? "At least one digit" : errors.pass;
            errors.pass = !minLengthPassword ? "At least minimum 8 characters" : errors.pass;
        }
        if (!this.state.selected.operator_by) this.setState({
            selected: {
                ...this.state.selected,
                operator_by: this.props.user.id_operator
            }
        });
        this.setState({errors});

        if (this.validateForm(this.state.errMsg)) {
            this.props.onAdd(this.state.selected);
        } else {
            console.error('Invalid Form')
        }
        this.setState({
            ...this.state,
            loadingForm: false,
        });
    }

    validateForm(errors) {
        let valid = true;
        Object.values(errors).forEach(
            (val) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    handleDelete() {
        this.props.onDelete(this.state.selected)
    }

    render() {
        const {data, dataKlinik, user} = this.props;
        const {selected, errMsg} = this.state;
        const permission = user?.permission;

        const columns = [
            {
                key: "no",
                text: "No.",
                width: 20,
                align: "center",
                sortable: false,
                cell: (row, index) => <div
                    style={{textAlign: "center"}}>{((this.state.page_number - 1) * this.state.per_page) + index + 1 + '.'}</div>,
                row: 0
            },
            {
                key: "name",
                text: "Name",
                align: "center",
                sortable: true
            },
            {
                key: "email",
                text: "Email",
                align: "center",
                sortable: true
            },
            {
                key: "klinik_name",
                text: "Klinik",
                align: "center",
                sortable: true
            },
            {
                key: "action",
                text: "Action",
                width: 140,
                align: "center",
                sortable: false,
                cell: record => {
                    return (
                        <div style={{textAlign: "center"}}>
                            <Fragment>
                                <button
                                    disabled={parseInt(permission?.staff_gudang_edit) > 0 ? false : true}
                                    className="btn btn-xs btn-success"
                                    onClick={e => this.editRecord(record)}
                                    style={{marginRight: '5px'}}>
                                    <i className="fa fa-edit"></i> Edit
                                </button>
                                <button
                                    disabled={parseInt(permission?.staff_gudang_delete) > 0 ? false : true}
                                    className="btn btn-danger btn-xs"
                                    onClick={() => this.deleteRecord(record)}>
                                    <i className="fa fa-trash"></i> Delete
                                </button>
                            </Fragment>
                        </div>
                    );
                }
            }
        ];
        const config = {
            key_column: 'id_operator',
            page_size: 10,
            length_menu: [10, 20, 50],
            show_filter: true,
            show_pagination: true,
            pagination: 'advance',
            button: {
                excel: false,
                print: false
            },
            language: {
                loading_text: "Please be patient while data loads..."
            }
        }
        const frmUser = <Form id="myForm">
            <Form.Group controlId="name">
                <Form.Label>Name</Form.Label>

                {errMsg.name ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.name}
                    </span>) : ''}
                <Form.Control
                    size="sm"
                    autoComplete="off"
                    name="name"
                    type="text"
                    value={selected.name}
                    onChange={this.handleChange.bind(this)}
                    placeholder="Name"/>
            </Form.Group>


            <Form.Group controlId="id_klinik">
                <Form.Label>Klinik</Form.Label>
                {errMsg.id_klinik ?
                    (<span className="float-right text-error badge badge-danger">{errMsg.id_klinik}
                    </span>) : ''}
                <Form.Control
                    size="sm"
                    name="id_klinik"
                    as="select"
                    value={selected.id_klinik ? selected.id_klinik : ""}
                    onChange={this.handleChange.bind(this)}>
                    <option value="">- Pilih Klinik -</option>
                    {dataKlinik ? (
                        dataKlinik.map(function (klinik) {
                            return <option
                                selected={klinik.id === selected.id_klinik ? true : false}
                                value={klinik.id}
                                key={klinik.id}>{klinik.name}
                            </option>
                        })

                    ) : ''}

                </Form.Control>
            </Form.Group>

            <Form.Row>
                <Form.Group as={Col} controlId="username">
                    <Form.Label>Email</Form.Label>
                    {this.props.errorPriority ? (
                        <span className="float-right text-error badge badge-danger">{this.props.errorPriority}
                </span>) : ''}
                    {errMsg.email ?
                        (<span className="float-right text-error badge badge-danger">{errMsg.email}
                        </span>) : ''}
                    <Form.Control
                        size="sm"
                        autoComplete="off"
                        name="email"
                        type="text"
                        value={selected.email}
                        onChange={this.handleChange.bind(this)}
                        placeholder="Email"/>
                </Form.Group>
                <Form.Group as={Col} controlId="pass">
                    <Form.Label>Password</Form.Label>
                    {errMsg.pass ?
                        (<span className="float-right text-error badge badge-danger">{errMsg.pass}
                        </span>) : ''}
                    <Form.Control
                        size="sm"
                        autoComplete="off"
                        name="pass"
                        type="password"
                        value={selected.pass}
                        onChange={this.handleChange.bind(this)}
                        placeholder="Password"/>
                </Form.Group>
            </Form.Row>


        </Form>;

        const contentDelete = <div
            dangerouslySetInnerHTML={{__html: '<div id="caption" style="padding-bottom:20px;">Apakah anda yakin <br/>akan menghapus data ini ?</div>'}}/>;
        return (

            <div className="content-wrapper">
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0">Staf Gudang</h1>
                            </div>
                            {/* /.col */}

                        </div>
                        {/* /.row */}
                    </div>
                    {/* /.container-fluid */}
                </div>
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                {/* card start */}
                                <div className="card card-success shadow-lg" style={{"minHeight": "800px"}}>
                                    <div className="card-header card-header-content">
                                        <AppButton
                                            disabled={parseInt(permission?.staff_gudang_add) > 0 ? false : true}
                                            isLoading={this.props.isLoading}
                                            theme="info"
                                            onClick={this.discardChanges}
                                            icon='add'
                                        >
                                            Add
                                        </AppButton>

                                    </div>
                                    <div className="card-body">
                                        {data ? (
                                            <ReactDatatable
                                                config={config}
                                                records={data}
                                                columns={columns}
                                                dynamic={true}
                                                onChange={this.tableChangeHandler}
                                                loading={this.props.isLoading}
                                                total_record={this.props.totalData}
                                            />
                                        ) : (<p>No Data ...</p>)}

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <AppModal
                    show={this.props.showFormAdd}
                    form={frmUser}
                    size="xs"
                    backdrop={false}
                    keyboard={false}
                    title="Add/Edit Staf Gudang"
                    titleButton="Save change"
                    themeButton="success"
                    handleClose={this.handleClose}
                    isLoading={this.props.isAddLoading ? this.props.isAddLoading : this.state.loadingForm}
                    formSubmit={this.handleSubmit.bind(this)}
                ></AppModal>
                <AppModal
                    show={this.props.showFormDelete}
                    size="xs"
                    form={contentDelete}
                    handleClose={this.handleClose}
                    backdrop="static"
                    keyboard={false}
                    title="Delete"
                    titleButton="Delete"
                    themeButton="danger"
                    isLoading={this.props.isAddLoading}
                    formSubmit={this.handleDelete.bind(this)}
                ></AppModal>
                {this.props.showFormSuccess ? (<AppSwalSuccess
                    show={this.props.showFormSuccess}
                    title={<div dangerouslySetInnerHTML={{__html: this.props.contentMsg}}/>}
                    type={this.props.tipeSWAL}
                    handleClose={this.props.isError ? this.props.closeSwalError : this.props.closeSwal}
                >
                </AppSwalSuccess>) : ''}
            </div>


        )
    }
}

const mapStateToProps = (state) => ({
    data: state.usersAdm.data || [],
    dataKlinik: state.klinik.data || [],
    totalData: state.usersAdm.totalData,
    isError: state.usersAdm.isError,
    isLoading: state.usersAdm.isLoading,
    isAddLoading: state.usersAdm.isAddLoading,
    showFormAdd: state.usersAdm.showFormAdd,
    errorPriority: state.usersAdm.errorPriority || null,
    contentMsg: state.usersAdm.contentMsg || null,
    showFormSuccess: state.usersAdm.showFormSuccess,
    showFormDelete: state.usersAdm.showFormDelete,
    tipeSWAL: state.usersAdm.tipeSWAL,
    user: state.main.currentUser,
});
const mapDispatchToPros = (dispatch) => {
    return {
        onLoad: (param) => {
            dispatch(fetchData(param));
            dispatch(fetchDataKlinik());

        },
        showForm: () => {
            dispatch(addForm());
        },
        showConfirmDel: (data) => {
            dispatch(confirmDel(data));
        },
        onAdd: (param) => {
            dispatch(addData(param));
        },
        onDelete: (param) => {
            dispatch(deleteData(param));
        },
        closeModal: () => {
            dispatch(closeForm());
        },
        resetError: () => {
            dispatch(clearError());
        },
        closeSwal: () => {
            dispatch(closeForm());
            const queryString = {
                sort_order: "ASC",
                sort_column: "name",
                per_page: 10,
                id_levels: 5,
            }
            dispatch(fetchData(queryString));
        },
        closeSwalError: () => {
            dispatch(closeForm());
        }
    }
}
export default connect(mapStateToProps, mapDispatchToPros)(StafGudang);