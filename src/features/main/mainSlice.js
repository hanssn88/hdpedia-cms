import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import axios from 'axios';
import Swal from 'sweetalert2';

const tokenLogin = process.env.REACT_APP_TOKEN_LOGIN;
const API_URL = process.env.REACT_APP_URL_API;

export const loginUser = createAsyncThunk(
    'users/login',
    async ({email, pass}, thunkAPI) => {

        try {
            const response = await fetch(
                API_URL + '/login_admin',
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        "email": email,
                        "pass": pass,
                    }),
                }
            );

            let data = '';
            let _data = await response.json();
            if (response.status === 200) {
                data = _data.data;
                if (_data.err_code === '00') {
                    localStorage.setItem(tokenLogin, data.token_login);
                    sessionStorage.setItem('klinikHD', data.id_klinik);
                    return data;
                } else {
                    return thunkAPI.rejectWithValue(_data);
                }
            } else {
                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {

            console.log('Error', e.response.data);
            thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const fetchUserBytoken = createAsyncThunk(
    'users/fetchUserByToken',
    async (prm, thunkAPI) => {
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await fetch(
                API_URL + '/admin_detail',
                {
                    method: 'GET',
                    headers: {
                        'Authorization': 'Bearer ' + token
                    },

                }
            );

            let data = '';
            let _data = await response.json();
            if (response.status === 200) {
                data = _data.data;
                if (_data.err_code === '00') {
                    sessionStorage.setItem('klinikHD', data.id_klinik);
                    return {...data};
                } else {
                    // localStorage.removeItem(tokenLogin);
                    return thunkAPI.rejectWithValue(_data);
                }
            } else {
                return thunkAPI.rejectWithValue(_data);
            }
        } catch (e) {

            console.log('Error', e.response.data);
            return thunkAPI.rejectWithValue(e.response.data);
        }
    }
);

export const addChange = createAsyncThunk(
    'user/changePassword',
    async (param, thunkAPI) => {
        let res = {};
        const token = localStorage.getItem(tokenLogin) ? localStorage.getItem(tokenLogin) : "";
        try {
            const response = await axios.post(API_URL + '/dokter/change_pass', param, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });

            let data = "";
            let _data = response;
            let res = {};
            if (response.status === 200) {
                let dataa = _data.data;
                data = dataa;
                if (data.err_code === '00') {

                    return Swal.fire({
                        title: 'Informasi',
                        text: "Success add Hasil Rekapitulasi Laboratorium",
                        icon: 'success',
                        confirmButtonText: 'Ok',
                        allowOutsideClick: true
                    })
                } else {
                    res = {
                        err_msg: data.err_msg
                    }

                    return Swal.fire({
                        title: 'Error',
                        text: res.err_msg,
                        icon: 'error',
                        confirmButtonText: 'Ok',
                        allowOutsideClick: true
                    })
                }
            }
        } catch (e) {
            res = {
                isAddLoading: false,
                showFormSuccess: true,
                showFormAdd: false,
                contentMsg: "<div style='font-size:20px; text-align:center;'><strong>Failed</strong>, Something wrong</div>",
                err_msg: null
            }
            console.log('Error catch', e.response.data);
            return thunkAPI.rejectWithValue(res);
        }
    }
);

const initialState = {
    expandMenu: true,
    isLoggedIn: !!localStorage.getItem(tokenLogin),
    isFetching: false,
    isSuccess: false,
    isLoading: false,
    isAddLoading: false,
    showFormAdd: false,
    showFormSuccess: false,
    showFormDelete: false,
    isError: false,
    errorPriority: null,
    contentMsg: null,
    tipeSWAL: "success",
    errorMessage: '',
    defaultOpenKeys: '/',
    currentUser: {
        id_operator: null,
        name: '',
        email: '',
        status: '',
        id_level: '',
        password: null
    }
};

export const mainSlice = createSlice({
    name: 'main',
    initialState,
    reducers: {
        addForm: (state) => {
            state.isAddLoading = false;
            state.isLoading = false;
            state.showFormAdd = true;
            state.errorPriority = null;
            return state;
        },
        closeForm: (state) => {
            state.showFormAdd = false;
            state.showFormDelete = false;
            state.errorPriority = null;
            state.showFormSuccess = false;
        },
        confirmDel: (state) => {
            state.isAddLoading = false;
            state.showFormDelete = true;
            state.errorPriority = false;
            return state;
        },
        clickExpand: (state) => {
            state.expandMenu = !state.expandMenu ? true : false;
            return state;
        },
        clearState: (state) => {
            state.isAddLoading = false;
            state.isLoading = false;
            state.isError = false;
            state.isSuccess = false;
            state.isFetching = false;
            state.errorMessage = null;
            return state;
        },
        onLogout: (state) => {
            localStorage.removeItem(tokenLogin);
            state.isAddLoading = false;
            state.isLoading = false;
            state.isLoggedIn = false;
            state.token = null;
            state.currentUser = initialState.currentUser;
            return state;
        },
        setDefaultOpenKeys: (state, dt) => {
            state.defaultOpenKeys = dt.payload;
        }
    },
    extraReducers: {
        [loginUser.fulfilled]: (state, {payload}) => {
            state.isFetching = false;
            state.isAddLoading = false;
            state.showFormSuccess = false;
            state.showFormAdd = false;
            state.isLoading = false;
            state.isSuccess = true;
            state.isLoggedIn = !!localStorage.getItem(tokenLogin);
            state.currentUser = payload;
            state.errorMessage = payload.err_msg;
            return state;
        },
        [loginUser.rejected]: (state, {payload}) => {
            //console.log('payload', payload);
            state.isFetching = false;
            state.isAddLoading = false;
            state.isLoading = false;
            state.showFormAdd = payload !== undefined ? payload.showFormAdd : false;
            state.isError = true;
            state.errorMessage = payload.err_msg;
        },
        [loginUser.pending]: (state) => {
            state.isFetching = true;
        },
        [fetchUserBytoken.pending]: (state, {payload}) => {
            state.currentUser = payload;
            state.isFetching = true;
        },
        [fetchUserBytoken.fulfilled]: (state, {payload}) => {
            state.isFetching = false;
            state.showFormAdd = false;
            state.showFormSuccess = false;
            state.isSuccess = true;
            state.currentUser = payload;
        },
        [fetchUserBytoken.rejected]: (state) => {
            state.isFetching = false;
            state.showFormAdd = state !== undefined ? state.showFormAdd : false;
            state.isError = true;
        },
        [addChange.fulfilled]: (state) => {
            state.isError = false;
            state.tipeSWAL = "success";
            state.showFormSuccess = true;
            state.showFormAdd = false;
            state.isAddLoading = false;
            state.contentMsg = "<div style='font-size:20px; text-align:center;'><strong>Success</strong>, Data berhasil disimpan</div>";
        },
        [addChange.rejected]: (state, {payload}) => {
            console.log(payload);
            state.isAddLoading = payload !== undefined ? payload.isAddLoading : false;
            state.showFormSuccess = payload !== undefined ? payload.showFormSuccess : true;
            state.showFormAdd = payload !== undefined ? payload.showFormAdd : false;
            state.tipeSWAL = "error";
            state.contentMsg = payload !== undefined ? payload.contentMsg : "<div style='font-size:20px; text-align:center;'><strong>Failed</strong>, Something wrong</div>";
            state.isError = true;
            state.errorPriority = payload !== undefined ? payload.err_msg : null;
        },
        [addChange.pending]: (state) => {
            state.isAddLoading = true;
        },

    }
})

export const {
    addForm,
    clickExpand,
    clearState,
    clearError,
    confirmDel,
    closeForm,
    onLogout,
    setDefaultOpenKeys
} = mainSlice.actions;
export const userSelector = (state) => state.main;
//export default mainSlice.reducer;